﻿'Imports Telerik.Reporting.Processing
Imports Telerik.Reporting.Processing


Partial Class report2
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        
       

    End Sub

    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click
        Dim rep As New peireport.pei

        '0001162055"
        '"0001180704"
        '0001180704
        'Dim peiname As String = Session("nomprenom")
        Dim peiname As String = "_"

        'rep.peiname = Trim(peiname) & "_0001226619"
        'rep.peiname = Trim(peiname) & Session("eleveid")
        rep.peiname = Trim(peiname) & "0001205693"
        rep.annee_scolaire = "2017-2018"

        rep.donnees_generales = Me.CheckBoxList1.Items.FindByValue("donnees_generales").Selected
        rep.donnees_evaluations = Me.CheckBoxList1.Items.FindByValue("donnees_evaluations").Selected
        rep.ress_humaines = Me.CheckBoxList1.Items.FindByValue("ress_humaines").Selected
        rep.forces_besoins = Me.CheckBoxList1.Items.FindByValue("forces_besoins").Selected
        rep.matieres = Me.CheckBoxList1.Items.FindByValue("matieres").Selected
        rep.adaptations = Me.CheckBoxList1.Items.FindByValue("adaptations").Selected
        rep.tests_provinciaux = Me.CheckBoxList1.Items.FindByValue("Eval_Prov").Selected
        rep.buts = Me.CheckBoxList1.Items.FindByValue("buts").Selected
        rep.plan_transition = Me.CheckBoxList1.Items.FindByValue("plan_transition").Selected
        rep.plan_transition2 = Me.CheckBoxList1.Items.FindByValue("plan_transition2").Selected
        rep.details_consultations = Me.CheckBoxList1.Items.FindByValue("details_consultations").Selected

        rep.page_signature = Me.CheckBoxList1.Items.FindByValue("page_signature").Selected

        'rep.SetDataSource("0001226619")

        rep.SetDataSource("0001205693")

        'rep.SetDataSource(Session("eleveid"))


        'rep.SetDataSource("0001221854")


        ExportToPDF(rep, rep.peiname)
        'Me.ReportViewer1.Report = rep
    End Sub
    'Sub ExportToPDF(ByVal reportToExport As Telerik.Reporting.Report)

    '    Dim reportProcessor As New ReportProcessor()
    '    Dim instanceReportSource As New Telerik.Reporting.InstanceReportSource()
    '    instanceReportSource.ReportDocument = reportToExport
    '    Dim result As RenderingResult = reportProcessor.RenderReport("PDF", instanceReportSource, Nothing)

    '    Dim fileName As String = result.DocumentName + "." + result.Extension
    '    Response.Clear()
    '    Response.ContentType = result.MimeType
    '    Response.Cache.SetCacheability(HttpCacheability.Private)
    '    Response.Expires = -1
    '    Response.Buffer = True
    '    Response.AddHeader("Content-Disposition", String.Format("{0};FileName=""{1}""", "attachment", fileName))
    '    Response.BinaryWrite(result.DocumentBytes)
    '    Response.End()
    'End Sub
    Sub ExportToPDF(ByVal reportToExport As Telerik.Reporting.Report, docname As String)
        Dim reportProcessor As New Telerik.Reporting.Processing.ReportProcessor()
        Dim result As RenderingResult = reportProcessor.RenderReport("PDF", reportToExport, Nothing)

        'Dim fileName As String = result.DocumentName + ".pdf"
        Dim fileName As String = docname + ".pdf"

        Response.Clear()
        Response.ContentType = result.MimeType
        Response.Cache.SetCacheability(HttpCacheability.Private)
        Response.Expires = -1
        Response.Buffer = True
        Response.AddHeader("Content-Disposition", String.Format("{0};FileName=""{1}""", "attachment", fileName))
        Response.BinaryWrite(result.DocumentBytes)
        Response.End()
    End Sub


    Sub SetAllCheckBox(boolVal As Boolean)
        Dim _checked As Boolean
        _checked = Me.CheckBoxList1.Items(0).Selected

        For Each lstItem As ListItem In Me.CheckBoxList1.Items
            lstItem.Selected = boolVal
        Next
    End Sub

    Protected Sub CheckBoxListPrintAll_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CheckBoxListPrintAll.SelectedIndexChanged
        Dim _checked As Boolean
        _checked = Me.CheckBoxListPrintAll.Items(0).Selected
        SetAllCheckBox(_checked)
    End Sub
End Class
