Imports System
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports Oracle.DataAccess.Client
Imports System.DirectoryServices
Imports System.DirectoryServices.Protocols


Namespace PeiWebAccess

Public Module fonctions
    Public ErreurMsg As String = ""
        Private _ConnString As String = ConfigurationSettings.AppSettings("ConnectionString")

        Sub AddDatagridCol(ByVal dGrid As DataGrid, ByVal Header As String, ByVal ColName As String, ByVal Visible As Boolean)
        Dim datagridcol As New BoundColumn
        Dim DataGrid As DataGrid

        DataGrid = dGrid

        datagridcol.HeaderText = Header
        datagridcol.DataField = ColName
        datagridcol.Visible = Visible
        DataGrid.Columns.Add(datagridcol)

        End Sub
        Function GetDateFormatted(Val As String) As Date
            Dim format = "dd/MM/yyyy"
            Return Date.ParseExact(Val, format, System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.None)
        End Function
    Sub SetSelectedValueDropDownList(ByVal DDL As DropDownList, ByVal Value As String)
        Dim myCheck As Boolean
        Dim DropDownList As DropDownList
        Dim myVar As String = Value
        Dim Quot As String = ""

        myVar = Replace(myVar, " ", "")
        DropDownList = DDL

        If Not myVar <> Quot Then
            DropDownList.SelectedValue = DropDownList.Items.FindByValue("").ToString
        Else
            'DropDownList.SelectedValue = DropDownList.Items(0).ToString
            DropDownList.SelectedValue = myVar
        End If

    End Sub
    Sub BindRadioButtonListNiveauEtude(ByVal Conn As String, ByVal RBL As RadioButtonList)
        Try

            Dim oProg As PEIDAL.programme = New PEIDAL.programme
            Dim RadioButtonList As RadioButtonList

            RadioButtonList = RBL

            RadioButtonList.DataSource = oProg.GetListNiveauEtude
            RadioButtonList.DataValueField = "niveau_id"
            RadioButtonList.DataTextField = "description_niveau"
            RadioButtonList.DataBind()

        Catch ex As Exception
            ErreurMsg = ex.Message & ", fonction " & "BindRadioButtonListNiveauEtude"
        End Try

    End Sub
    Sub BindDropDownListNiveauEtude(ByVal Conn As String, ByVal DDL As DropDownList)
        Try

            Dim oProg As PEIDAL.programme = New PEIDAL.programme
            'Dim RadioButtonList As RadioButtonList
            Dim DropDownList As DropDownList

            'RadioButtonList = RBL
            DropDownList = DDL

            DropDownList.DataSource = oProg.GetListNiveauEtude
            DropDownList.DataValueField = "niveau_id"
            DropDownList.DataTextField = "description_niveau"
            DropDownList.DataBind()
            DropDownList.Items.Add("")

        Catch ex As Exception
            ErreurMsg = ex.Message & ", fonction " & "BindDropDownListNiveauEtude"
        End Try

    End Sub
    Sub BindDropDownListContacts(ByVal EleveID As String, ByVal DDL As DropDownList)
        Dim oGeneral As PEIDAL.general = New PEIDAL.general
        Dim DropDownList As DropDownList

        Try
            DropDownList = DDL

            DropDownList.Items.Clear()
            DropDownList.DataSource = oGeneral.GetInfosContactByeEleveID(ConfigurationSettings.AppSettings("ConnectionString"), EleveID)
            DropDownList.DataTextField = "TYPE_CONTACT"
            DropDownList.DataValueField = "ROW_ID"
            DropDownList.DataBind()

        Catch ex As Exception
            ErreurMsg = ex.Message
        End Try

    End Sub
    Sub BindDropDownListButs(ByVal strEleveId As String, ByVal DDL As DropDownList, ByVal SelectedValue As Integer)
        Dim oAttente As PEIDAL.attente = New PEIDAL.attente
        Dim oBut As PEIDAL.but = New PEIDAL.but
        Dim ddlButs As DropDownList

        Try
            ddlButs = DDL

                'If obut.GetCountButByEleveID(strEleveId) > 0 Then
                ddlButs.DataSource = oAttente.GetListButsByEleveID(strEleveId)
                ddlButs.DataTextField = "but_titre"
                ddlButs.DataValueField = "pk_but_id"
                ddlButs.DataBind()

                If SelectedValue <> 0 Then
                    ddlButs.SelectedValue = SelectedValue
                End If

                'ddlButs.ClearSelection()

                'End If

            Catch ex As Exception
                Throw New Exception("Erreur lors du remplissage de la liste des buts. Details de l'erreur:" & ex.Message)

            End Try

    End Sub
    Sub BindDropDownListAttentes(ByVal strEleveId As String, ByVal DDL As DropDownList, ByVal SelectedValue As Integer)
        Dim oAttente As PEIDAL.attente = New PEIDAL.attente
        Dim oBut As PEIDAL.but = New PEIDAL.but
        Dim ddlButs As DropDownList

        Try
            ddlButs = DDL

            If oBut.GetCountButByEleveID(strEleveId) > 0 Then
                ddlButs.DataSource = oAttente.GetListButsByEleveID(strEleveId)
                ddlButs.DataTextField = "but_titre"
                ddlButs.DataValueField = "pk_but_id"
                ddlButs.DataBind()

                If SelectedValue <> 0 Then
                    ddlButs.SelectedValue = SelectedValue
                End If
            Else
                'ddlButs.ClearSelection()
            End If

        Catch ex As Exception
            Throw New Exception("Erreur lors du remplissage de la liste des buts. Details de l'erreur:" & ex.Message)

        End Try

    End Sub
    Sub BindCheckBoxListButs(ByVal strEleveId As String, ByVal ChBxList As CheckBoxList)
        Dim oAttente As PEIDAL.attente = New PEIDAL.attente
        Dim oBut As PEIDAL.but = New PEIDAL.but
        'Dim ddlButs As DropDownList
        Dim CBL As CheckBoxList

        CBL = ChBxList

        If oBut.GetCountButByEleveID(strEleveId) > 0 Then
            CBL.DataSource = oAttente.GetListButsByEleveID(strEleveId)
            CBL.DataTextField = "but_titre"
            CBL.DataValueField = "pk_but_id"
            CBL.DataBind()

        End If

    End Sub
    Sub BindDropDownListEcole(ByVal ConseilId As String, ByVal DDL As DropDownList)
        'Dim oAttente As PEIDAL.attente = New PEIDAL.attente
        Dim oEcole As PEIDAL.ecole = New PEIDAL.ecole
        Dim DropDownList As DropDownList

        DropDownList = DDL

        DropDownList.Items.Clear()
        DropDownList.DataSource = oEcole.GetListEcoleByConseilId(ConfigurationSettings.AppSettings("ConnectionString"), ConseilId)
        DropDownList.DataTextField = "nom_ecole"
        DropDownList.DataValueField = "pk_ecole_id"
        DropDownList.DataBind()
    End Sub
    Public Function SetCheckBoxList(ByVal ctlCheckBoxList As CheckBoxList, ByVal TheReader As SqlDataReader)

        ' Create Instance of Connection and Command Object
        Dim myReader As SqlDataReader
        Dim chBoxList As CheckBoxList
        'Dim Id As Integer
        Dim strId As String

        chBoxList = ctlCheckBoxList
        myReader = TheReader

        Try
            If TheReader.HasRows Then
                Do While myReader.Read

                    'ici id contient la cl� etrangere a la table source. le requet doit seulement contenir un champ
                    'Id = myReader.GetValue(0)
                    strId = CStr(myReader.GetValue(0))

                    'chBoxList.Items.FindByValue(Id).Selected = True
                    chBoxList.Items.FindByValue(strId).Selected = True

                Loop
            End If

        Catch ex As Exception
            Throw New System.Exception("Erreur survenu lors du remplissage des zones � chochez")
        End Try

    End Function
    Public Function SetRadioButtonList(ByVal ctlRadioButtonList As RadioButtonList, ByVal TheReader As SqlDataReader)

        ' Create Instance of Connection and Command Object
        Dim myReader As SqlDataReader
        Dim RadioButtonList As RadioButtonList
        Dim Id As Integer

        RadioButtonList = ctlRadioButtonList

        myReader = TheReader

        Try
            If TheReader.HasRows Then
                Do While myReader.Read

                    'ici id contient la cl� etrangere a la table source. le requet doit seulement contenir un champ
                    Id = myReader.GetValue(0)

                    RadioButtonList.Items.FindByValue(Id).Selected = True

                Loop
            End If

        Catch ex As Exception
            Throw New System.Exception("Erreur survenu lors du remplissage des boutons radio")
        End Try


    End Function
    Public Function SetListIntervenants(ByVal ctlCheckBoxList As CheckBoxList, ByVal strInter As String, ByVal intButId As Integer)

        ' Create Instance of Connection and Command Object
        Dim myConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim myCommand As SqlCommand = New SqlCommand("peisp_Inter_Buts_Select", myConnection)
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim chBoxList As CheckBoxList
        Dim strIntervenants As String
        Dim myArrayInter() As String

        Try
            chBoxList = ctlCheckBoxList
            strIntervenants = strInter

            ' Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure

            ' Add Parameters to SPROC
            'Dim parameterCategoryID As SqlParameter = New SqlParameter("@CategoryID", SqlDbType.Int, 4)
            Dim parameterButID As SqlParameter = New SqlParameter("@fk_but_id", SqlDbType.Int, 8)
            'parameterCategoryID.Value = categoryID
            parameterButID.Value = intButId
            'myCommand.Parameters.Add(parameterCategoryID)
            myCommand.Parameters.Add(parameterButID)

            Dim parameterAttenteNo As SqlParameter = New SqlParameter("@fk_attente_no", SqlDbType.Int, 4)
            'parameterCategoryID.Value = categoryID
            parameterAttenteNo.Value = 1
            'myCommand.Parameters.Add(parameterCategoryID)
            myCommand.Parameters.Add(parameterAttenteNo)

            ' Execute the command

            ' fcote()
            'myConnection.Open()

            'Dim result As SqlDataReader = 
            'myCommand.ExecuteReader(CommandBehavior.CloseConnection)

            da.SelectCommand = myCommand
            da.Fill(ds)

            Dim r As DataRow
            Dim id As String

            'les evaluations inter sont ins�r� dans un seul champ s�par� d'un espace
            Dim i As String
            myArrayInter = Split(strInter)
            For Each i In myArrayInter
                If i <> "" Then
                    i = Replace(i, Chr(13), "")
                    i = Replace(i, "/", "")
                    'i = Replace(i, " ", "")
                    chBoxList.Items.FindByText(i).Selected = True
                End If
            Next
        Catch ex As Exception
            ErreurMsg = ex.Message & ", fonction " & SetListIntervenants
        End Try

    End Function
    Function LoadCheckBoxList(ByVal sqlReader As SqlDataReader, ByVal ctlCheckBoxList As CheckBoxList)
        'Dim myConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim chBoxList As CheckBoxList
        Dim strIntervenants As String
        Dim myArrayInter() As String
        Dim myArraySource() As String
        Dim myHashTable As New Hashtable
        Dim myReader As SqlDataReader
        'Dim oIden As New PEI.identification
        Dim x As Integer
        Dim y As Integer
        Dim firstItem As String
        Dim strItem As String
        Dim temp As String
        Dim tempID As Integer
        x = 0
        y = 0

        Try
            chBoxList = ctlCheckBoxList
            myReader = sqlReader

            'sqlReader = oFB.GetBesoinsByEleveId(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            'myReader = oIden.GetListSourceInformations
            If sqlReader.HasRows Then
                Do While sqlReader.Read
                    ' txtBesoins.Text = sqlReader.GetString(0)
                    'myArraySource(0, y) = sqlReader.Item(0)

                    strItem = sqlReader.GetString(1)
                    myArraySource = Split(strItem, ";", -1, CompareMethod.Text)
                    strItem = myArraySource.GetValue(0)

                    myHashTable.Add(sqlReader.Item(0), strItem)

                    'strItem = CStr(sqlReader.GetString(1)) 'ID converted to string

                    'myArraySource(0, y) = CStr(sqlReader.GetString(0)) 'ID converted to string
                    '
                    y = y + 1
                Loop
            End If

            chBoxList.DataSource = myHashTable
            chBoxList.DataValueField = "key"
            chBoxList.DataTextField = "value"
            chBoxList.DataBind()

            ''les evaluations inter sont ins�r� dans un seul champ s�par� d'un espace
            'Dim i As String
            'myArrayInter = Split(strInter)
            'For Each i In myArrayInter
            '    If i <> "" Then
            '        i = Replace(i, Chr(13), "")
            '        i = Replace(i, "/", "")
            '        'i = Replace(i, " ", "")
            '        chBoxList.Items.FindByText(i).Selected = True
            '    End If
            'Next
        Catch ex As Exception
            ErreurMsg = ex.Message & ", fonction LoadCheckBoxList"
        End Try
    End Function
    Function LoadRadioButtonList(ByVal sqlReader As SqlDataReader, ByVal Ctl As RadioButtonList)
        'Dim myConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim ctlRadioButtonList As RadioButtonList
        Dim strIntervenants As String
        Dim myArrayInter() As String
        Dim myArraySource() As String
        Dim myHashTable As New Hashtable
        Dim myReader As SqlDataReader
        'Dim oIden As New PEI.identification
        Dim x As Integer
        Dim y As Integer
        Dim firstItem As String
        Dim strItem As String
        Dim temp As String
        Dim tempID As Integer
        x = 0
        y = 0

        Try
            ctlRadioButtonList = Ctl
            myReader = sqlReader

            'sqlReader = oFB.GetBesoinsByEleveId(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            'myReader = oIden.GetListSourceInformations
            If sqlReader.HasRows Then
                Do While sqlReader.Read

                    strItem = sqlReader.GetString(1)
                    myArraySource = Split(strItem, ";", -1, CompareMethod.Text)
                    strItem = myArraySource.GetValue(0)

                    myHashTable.Add(sqlReader.Item(0), strItem)

                    y = y + 1
                Loop
            End If

            ctlRadioButtonList.DataSource = myHashTable
            ctlRadioButtonList.DataValueField = "key"
            ctlRadioButtonList.DataTextField = "value"
            ctlRadioButtonList.DataBind()

        Catch ex As Exception
            ErreurMsg = ex.Message & ", fonction LoadCheckBoxList"
        End Try
    End Function

    Function GetItemFromCheckBoxList(ByVal ctlCheckBoxList As CheckBoxList) As String
        Dim ctl As CheckBoxList
        Dim myString As String
        Dim li As ListItem
        'Dim checkBox As CheckBox
        ctl = ctlCheckBoxList

        For Each li In ctl.Items
            If li.Selected Then
                myString = myString & li.Text & " "
            End If
        Next

        Return myString

    End Function
    Function IsSessionExpired(ByVal EleveID As String) As Boolean

        If EleveID <> "" Then
            IsSessionExpired = False
        Else
            IsSessionExpired = True
        End If

    End Function
    Sub BindDropDownListCategorie(ByVal DDL As DropDownList, ByVal TypeContenu As String)
        Dim oGen As New PEIDAL.general
        Dim DropDownList As DropDownList

        DropDownList = DDL

        DropDownList.DataSource = oGen.GetListCategorie(TypeContenu)
        DropDownList.DataValueField = "categorie"
        DropDownList.DataTextField = "categorie"
        DropDownList.DataBind()

    End Sub
    Sub BindDropDownListContenus(ByVal DDL As DropDownList, ByVal TypeContenu As String, ByVal Categorie As String)
        Dim oGen As New PEIDAL.general
        Dim DropDownList As DropDownList

        DropDownList = DDL

        DropDownList.Items.Clear()

        DropDownList.DataSource = oGen.GetListContenuByTypeAndCategorie(TypeContenu, Categorie)
        DropDownList.DataValueField = "contenu"
        DropDownList.DataTextField = "contenu"
        DropDownList.DataBind()

    End Sub
    Sub Opennewwindow(ByVal Opener As System.Web.UI.WebControls.WebControl, ByVal PagePath As String)

        Dim Clientscript As String

        Clientscript = "window.open('" & PagePath & "')"

        Opener.Attributes.Add("Onclick", Clientscript)

    End Sub
    Sub BindAdaptationsEvalProv(ByVal ctlTextBox As TextBox, ByVal strEleveId As String)
        Dim oEvalProv As New PEIDAL.evalprov
        Dim dsAdaptEvalProv1 As New PEIDAL.dsAdaptEvalProv
        Dim oTextBox As TextBox

        oTextBox = ctlTextBox

        dsAdaptEvalProv1 = oEvalProv.GetdsAdaptEvalProv(ConfigurationSettings.AppSettings("ConnectionString"), strEleveId)
        Dim r As PEIDAL.dsAdaptEvalProv.ADAPT_EVAL_PROVRow

        Try

            If dsAdaptEvalProv1.ADAPT_EVAL_PROV.Rows.Count < 1 Then

                r = dsAdaptEvalProv1.ADAPT_EVAL_PROV.NewADAPT_EVAL_PROVRow

                r.FK_ELEVE_ID = strEleveId
                r.ADAPTATION = ""


            Else
                r = dsAdaptEvalProv1.ADAPT_EVAL_PROV.FindByFK_ELEVE_ID(strEleveId)

                'txtAdaptTestsProv.Text = r.ADAPTATION
                oTextBox.Text = r.ADAPTATION

            End If

            oEvalProv.UpdatedsAdaptEvalProv(ConfigurationSettings.AppSettings("ConnectionString"), dsAdaptEvalProv1)

        Catch ex As Exception
            'lblMsgErr.Visible = True
            'lblMsgErr.Text = ex.Message
            Throw New Exception(ex.Message)
        End Try

    End Sub
    Sub BindEvalTestsProv(ByVal ctlTextBox As TextBox, ByVal EleveId As String)
        Dim oEleve As PEIDAL.eleve = New PEIDAL.eleve
        Dim dsEleve As New DataSet
        Dim oTextBoxt As TextBox

        oTextBoxt = ctlTextBox

        Dim oEvalProv As New PEIDAL.evalprov
        Dim dsAdaptEvalProv1 As New PEIDAL.dsAdaptEvalProv

        Try

            dsAdaptEvalProv1 = oEvalProv.GetdsAdaptEvalProv(ConfigurationSettings.AppSettings("ConnectionString"), EleveId)

            If dsAdaptEvalProv1.ADAPT_EVAL_PROV.Rows.Count < 1 Then
                Dim dr As PEIDAL.dsAdaptEvalProv.ADAPT_EVAL_PROVRow

                dr = dsAdaptEvalProv1.ADAPT_EVAL_PROV.NewADAPT_EVAL_PROVRow

                'Select Case Champ

                dr.FK_ELEVE_ID = EleveId
                'dr.Item(Champ) = strValue
                dr.ADAPTATION = ""


                'End Select

                dsAdaptEvalProv1.ADAPT_EVAL_PROV.Rows.InsertAt(dr, 0)

            Else

                '     oTextBoxt.Text = dsAdaptEvalProv1.ada
            End If


            'dsEleve = oEleve.GetEleveByID(ConfigurationSettings.AppSettings("ConnectionString"), EleveId)


        Catch ex As Exception

            'lblMsgErr.Visible = True
            'lblMsgErr.Text = "Erreur survenue lors de la lecture des �valuations aux tests provinciaux"
            Throw New Exception("Erreur lors de la lecture des �valuations. -->" & ex.Message)

        End Try

    End Sub
    Function GetInfosEleve(ByVal EleveId As String, ByVal sTableName As String) As String
        Dim oEleve As PEIDAL.eleve = New PEIDAL.eleve
        Dim dsEleve As New DataSet

        Try
            dsEleve = oEleve.GetEleveByID(ConfigurationSettings.AppSettings("ConnectionString"), EleveId)

            Return dsEleve.Tables(0).Rows(0).Item(sTableName)
        Catch ex As Exception

            'lblMsgErr.Visible = True
            'lblMsgErr.Text = "Erreur survenue lors de la lecture des �valuations aux tests provinciaux"
            Throw New Exception("Erreur lors de la lecture des informations de l'�l�ve. -->" & ex.Message)

        End Try

    End Function
    Sub BindExemptionsEvalProv(ByVal ctlTextBox As TextBox, ByVal EleveId As String)
        Dim oEvalProv As New PEIDAL.evalprov
        Dim dsExempEvalProv1 As New PEIDAL.dsExempEvalProv
        Dim oTextBox As TextBox

        oTextBox = ctlTextBox

        dsExempEvalProv1 = oEvalProv.GetdsExempEvalProv(ConfigurationSettings.AppSettings("ConnectionString"), EleveId)
        Dim r As PEIDAL.dsExempEvalProv.EXEMPTIONS_EVAL_PROVRow

        Try

            If dsExempEvalProv1.EXEMPTIONS_EVAL_PROV.Rows.Count < 1 Then

                r = dsExempEvalProv1.EXEMPTIONS_EVAL_PROV.NewEXEMPTIONS_EVAL_PROVRow

                r.FK_ELEVE_ID = EleveId
                r.MOTIF_EDUCATIONNEL = ""

                dsExempEvalProv1.EXEMPTIONS_EVAL_PROV.Rows.Add(r)

                oEvalProv.UpdatedsExempEvalProv(ConfigurationSettings.AppSettings("ConnectionString"), dsExempEvalProv1)

            Else
                r = dsExempEvalProv1.EXEMPTIONS_EVAL_PROV.FindByFK_ELEVE_ID(EleveId)
                oTextBox.Text = r.MOTIF_EDUCATIONNEL

            End If

        Catch ex As Exception
            'lblMsgErr.Visible = True
            'lblMsgErr.Text = ex.Message
            Throw New Exception("Erreur lors de la lecture des exemptions. --> " & ex.Message)
        End Try
    End Sub
    Public Function SetListSourceInformationsByEleveId(ByVal ctlCheckBoxList As CheckBoxList, ByVal EleveID As String)
        'OLD
        ' Create Instance of Connection and Command Object
        Dim myConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim myCommand As SqlCommand = New SqlCommand("peisp_SourcesEleves_Select", myConnection)
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim chBoxList As CheckBoxList

        chBoxList = ctlCheckBoxList

        ' Mark the Command as a SPROC
        myCommand.CommandType = CommandType.StoredProcedure

        ' Add Parameters to SPROC
        'Dim parameterCategoryID As SqlParameter = New SqlParameter("@CategoryID", SqlDbType.Int, 4)
        Dim parameterEleveID As SqlParameter = New SqlParameter("@fk_eleve_id", SqlDbType.Char, 20)
        'parameterCategoryID.Value = categoryID
        parameterEleveID.Value = EleveID
        'myCommand.Parameters.Add(parameterCategoryID)
        myCommand.Parameters.Add(parameterEleveID)

        ' Execute the command
        myConnection.Open()
        'Dim result As SqlDataReader = 
        'myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        da.SelectCommand = myCommand
        da.Fill(ds)

        Dim r As DataRow
        Dim id As String

        For Each r In ds.Tables(0).Rows
            id = r.Item("fk_source_id")
            chBoxList.Items.FindByValue(id).Selected = True
        Next

        myConnection.Close()

        ' Return the datareader result
        'Return result

    End Function
    Public Function SetListFormatsCommunicationsByEleveId(ByVal ctlCheckBoxList As CheckBoxList, ByVal EleveID As String)
        'OLD
        ' Create Instance of Connection and Command Object
        Dim myConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim myCommand As SqlCommand = New SqlCommand("peisp_FormatsCommunications_Eleves_Select", myConnection)
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim chBoxList As CheckBoxList

        chBoxList = ctlCheckBoxList

        ' Mark the Command as a SPROC
        myCommand.CommandType = CommandType.StoredProcedure

        ' Add Parameters to SPROC
        'Dim parameterCategoryID As SqlParameter = New SqlParameter("@CategoryID", SqlDbType.Int, 4)
        Dim parameterEleveID As SqlParameter = New SqlParameter("@fk_eleve_id", SqlDbType.Char, 20)
        'parameterCategoryID.Value = categoryID
        parameterEleveID.Value = EleveID
        'myCommand.Parameters.Add(parameterCategoryID)
        myCommand.Parameters.Add(parameterEleveID)

        ' Execute the command
        myConnection.Open()
        'Dim result As SqlDataReader = 
        'myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        da.SelectCommand = myCommand
        da.Fill(ds)

        Dim r As DataRow
        Dim id As String

        For Each r In ds.Tables(0).Rows
            id = r.Item("fk_format_communication_id")
            chBoxList.Items.FindByValue(id).Selected = True
        Next

        myConnection.Close()

    End Function
    Public Function SetListPlacementsByEleveId(ByVal ctlRadioButtonList As RadioButtonList, ByVal EleveID As String)
        'OLD
        ' Create Instance of Connection and Command Object
        Dim myConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim myCommand As SqlCommand = New SqlCommand("peisp_PlacementsEleves_Select", myConnection)
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        'Dim chBoxList As CheckBoxList
        Dim RadioButtonList As RadioButtonList

        'chBoxList = ctlCheckBoxList
        RadioButtonList = ctlRadioButtonList

        ' Mark the Command as a SPROC
        myCommand.CommandType = CommandType.StoredProcedure

        ' Add Parameters to SPROC
        'Dim parameterCategoryID As SqlParameter = New SqlParameter("@CategoryID", SqlDbType.Int, 4)
        Dim parameterEleveID As SqlParameter = New SqlParameter("@fk_eleve_id", SqlDbType.Char, 20)
        'parameterCategoryID.Value = categoryID
        parameterEleveID.Value = EleveID
        'myCommand.Parameters.Add(parameterCategoryID)
        myCommand.Parameters.Add(parameterEleveID)

        ' Execute the command
        myConnection.Open()
        'Dim result As SqlDataReader = 
        'myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        da.SelectCommand = myCommand
        da.Fill(ds)

        Dim r As DataRow
        Dim id As String

        For Each r In ds.Tables(0).Rows
            id = r.Item("fk_placement_id")
            RadioButtonList.Items.FindByValue(id).Selected = True
        Next

        myConnection.Close()

    End Function
    Public Function SetListRaisonsByEleveId(ByVal ctlRadioButtonList As RadioButtonList, ByVal EleveID As String)
        'OLD
        ' Create Instance of Connection and Command Object
        Dim myConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim myCommand As SqlCommand = New SqlCommand("peisp_RaisonsEleves_Select", myConnection)
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        'Dim chBoxList As CheckBoxList
        Dim RadioButtonList As RadioButtonList

        'chBoxList = ctlCheckBoxList
        RadioButtonList = ctlRadioButtonList

        ' Mark the Command as a SPROC
        myCommand.CommandType = CommandType.StoredProcedure

        ' Add Parameters to SPROC
        'Dim parameterCategoryID As SqlParameter = New SqlParameter("@CategoryID", SqlDbType.Int, 4)
        Dim parameterEleveID As SqlParameter = New SqlParameter("@fk_eleve_id", SqlDbType.Char, 20)
        'parameterCategoryID.Value = categoryID
        parameterEleveID.Value = EleveID
        'myCommand.Parameters.Add(parameterCategoryID)
        myCommand.Parameters.Add(parameterEleveID)

        ' Execute the command
        myConnection.Open()
        'Dim result As SqlDataReader = 
        'myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        da.SelectCommand = myCommand
        da.Fill(ds)

        Dim r As DataRow
        Dim id As String

        For Each r In ds.Tables(0).Rows
            id = r.Item("fk_raison_id")
            RadioButtonList.Items.FindByValue(id).Selected = True
        Next

        myConnection.Close()

    End Function
    Public Function SetListAnomaliesByEleveId(ByVal ctlRadioButtonList As RadioButtonList, ByVal EleveID As String)
        'OLD
        ' Create Instance of Connection and Command Object
        Dim myConnection As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
        Dim myCommand As SqlCommand = New SqlCommand("peisp_AnomaliesEleves_Select", myConnection)
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        'Dim chBoxList As CheckBoxList
        Dim RadioButtonList As RadioButtonList

        'chBoxList = ctlCheckBoxList
        RadioButtonList = ctlRadioButtonList

        ' Mark the Command as a SPROC
        myCommand.CommandType = CommandType.StoredProcedure

        ' Add Parameters to SPROC
        'Dim parameterCategoryID As SqlParameter = New SqlParameter("@CategoryID", SqlDbType.Int, 4)
        Dim parameterEleveID As SqlParameter = New SqlParameter("@fk_eleve_id", SqlDbType.Char, 20)
        'parameterCategoryID.Value = categoryID
        parameterEleveID.Value = EleveID
        'myCommand.Parameters.Add(parameterCategoryID)
        myCommand.Parameters.Add(parameterEleveID)

        ' Execute the command
        myConnection.Open()
        'Dim result As SqlDataReader = 
        'myCommand.ExecuteReader(CommandBehavior.CloseConnection)

        da.SelectCommand = myCommand
        da.Fill(ds)

        Dim r As DataRow
        Dim id As String

        For Each r In ds.Tables(0).Rows
            id = r.Item("fk_anomalie_id")
            RadioButtonList.Items.FindByValue(id).Selected = True
        Next

        myConnection.Close()

    End Function
    Function GetConseilID(ByVal Username As String) As String

        Dim oEcole As New PEIDAL.ecole

        Try
            GetConseilID = Trim(oEcole.GetConseilIdByUtilisateurId(ConfigurationSettings.AppSettings("ConnectionString"), Username))
        Catch ex As Exception
            'errMsg.Visible = True
            'errMsg.Text = "Erreurs lors de la lecture de ConseilID"
            Throw New Exception("Erreurs lors de la lecture de ConseilID." & ex.Message)
        End Try

        End Function
        Public Function GetDataTableEcoles(ByVal conseilid As String) As DataTable
            Dim oEcole As New PEIDAL.ecole

            Dim _ds As New DataSet
            Dim tbEcoles As New Data.DataTable
            Dim dcEcoleId = New DataColumn("pk_ecole_id", GetType(String))
            Dim dcFullName = New DataColumn("nom_ecole", GetType(String))
            tbEcoles.Columns.Add(dcEcoleId)
            tbEcoles.Columns.Add(dcFullName)

            _ds = oEcole.GetDsListEcole(ConfigurationSettings.AppSettings("ConnectionString"))

            Dim _fullname As String = ""
            For Each item In _ds.Tables(0).Rows
                '    'Dim row As Data.DataRow
                If Not item("fk_conseil_id") <> conseilid Then
                    _fullname = item("nom_ecole") & " (" & item("ville").ToString & ")"
                    tbEcoles.Rows.Add(item("pk_ecole_id").ToString, _fullname)
                End If
            Next

            Return tbEcoles
            'Try

            'Catch ex As Exception
            '    lblMsgErr.Text = ex.Message
            'End Try

        End Function
        Sub BindDropDownListEcoleByUserIdAdmin(ByVal username As String, ByVal DropDownList As DropDownList, Optional ByVal _ValueField As String = "SCHOOL_CODE")
            Dim ds As New DataSet
            Dim oGen As New PEIDAL.general
            Dim sql As String
            sql = "select a.fk_ecole_id as ecole_id,b.school_code, b.nom_ecole + ' (' + rtrim(b.ville) + ')' as ecole" & _
            " from UTILISATEURS_ECOLES a, ECOLES b" & _
            " where " & _
            " a.fk_ecole_id = b.pk_ecole_id " & _
            " and a.fk_utilisateur_id='" & username & "'" & _
            " order by b.nom_ecole"

            'sql = "select "

            ds = oGen.GetDataset(sql, ConfigurationSettings.AppSettings("ConnectionString"))

            DropDownList.DataSource = ds
            If _ValueField <> "pk_ecole_id" Then
                DropDownList.DataValueField = _ValueField
            Else
                DropDownList.DataValueField = "ecole_id"
            End If

            DropDownList.DataTextField = "ecole"
            DropDownList.DataBind()


        End Sub
        Function IsUserAdmin(ByVal username As String) As Boolean
            Dim sql As String
            Dim ds As New DataSet
            Dim oGen As New PEIDAL.general

            sql = "select fk_utilisateur_id from utilisateurs_ecoles where fk_utilisateur_id = '" & username & "'"
            ds = oGen.GetDataset(sql, ConfigurationSettings.AppSettings("ConnectionString"))

            If ds.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        End Function
       

        Sub BindDropDownListEcoleByUserID(ByVal username As String, ByVal ctlDropDownList As DropDownList, Optional ByVal _ValueField As String = "pk_ecole_id")

            If IsUserAdmin(username) Then
                BindDropDownListEcoleByUserIdAdmin(username, ctlDropDownList, _ValueField)
            Else
                '_BindDropDownListEcoleByUserID(username, ctlDropDownList, _ValueField)
                _BindDropDownListEcoleByUserID2(username, ctlDropDownList, _ValueField)
            End If
        End Sub

        Sub _BindDropDownListEcoleByUserID(ByVal username As String, ByVal DropDownList As DropDownList, Optional ByVal _ValueField As String = "pk_ecole_id")
            Dim _school_year As String = ConfigurationSettings.AppSettings("school_year").ToString

            Dim sql As String
            sql = "select a.fk_ecole_id as ecole_id, b.nom_ecole + ' (' + rtrim(b.ville) + ')' as ecole" & _
            " from UTILISATEURS_ECOLES a, ECOLES b" & _
            " where " & _
            " a.fk_ecole_id = b.pk_ecole_id " & _
            " and a.fk_utilisateur_id='" & username & "'" & _
            " order by b.nom_ecole"

            Dim oraSQL As String
            oraSQL = "select distinct g.SCHOOL_NAME, a.SCHOOL_CODE, a.person_id, k.user_profile as USER_PROFILE, " & _
" h.LEGAL_NAME_UPPER,h.LEGAL_FIRST_NAME,h.LEGAL_SURNAME, " & _
" f.STAFF_TYPE_NAME as staff_instr_STAFF_TYPE_NAME," & _
" to_char(a.START_DATE,'yyyy-mm-dd') as start_date," & _
" to_char(a.END_DATE,'yyyy-mm-dd') as end_date, " & _
" to_char(f.START_DATE,'yyyy-mm-dd') as staff_instr_START_DATE, " & _
" to_char(f.END_DATE,'yyyy-mm-dd') as staff_instr_time_END_DATE, " & _
" a.STAFF_TYPE_NAME as school_staff_STAFF_TYPE_NAME, " & _
" DECODE(a.STAFF_TYPE_NAME,'Principal','Directeur','Vice Principal','Directeur','Vice Principal A','Direction','Principal A','Directeur','Teacher','Enseignant','Teacher Spec','Directeur','Teacher LTO','Enseignant','Secretary','Secretaire','','N/A') as TYPE_UTILISATEUR " & _
" from school_classes i, class_courses b, course_codes c, subjects d, staff_instructional_time f, schools g, persons h,user_profiles k, school_staff a, trlm_user_map_ldap ldap where a.PERSON_ID = i.REPORTING_TEACHER(+) and i.CLASS_CODE = b.CLASS_CODE (+) and i.SCHOOL_CODE = b.school_code(+) and i.school_year = b.school_year(+) and b.COURSE_CODE = c.COURSE_CODE(+) and b.SCHOOL_CODE = c.SCHOOL_CODE(+) and b.SCHOOL_YEAR = c.SCHOOL_YEAR(+) and c.SUBJECT_CODE = d.SUBJECT_CODE(+) and a.person_id = k.person_id(+) and a.SCHOOL_CODE = f.SCHOOL_CODE(+) and a.SCHOOL_YEAR = " & _
" f.SCHOOL_YEAR(+) and a.person_id = f.PERSON_ID(+) and a.SCHOOL_CODE = g.SCHOOL_CODE " & _
" and a.person_id = h.PERSON_ID and a.person_id = i.REPORTING_TEACHER(+) and a.SCHOOL_CODE = i.SCHOOL_CODE(+) " & _
" and a.SCHOOL_YEAR = i.SCHOOL_YEAR(+) AND to_char(a.START_DATE,'yyyy-mm-dd') =  " & _
" (SELECT MAX(to_char(START_DATE,'yyyy-mm-dd')) " & _
" FROM SCHOOL_STAFF Z  WHERE Z.PERSON_ID = A.person_id AND Z.SCHOOL_CODE = A.SCHOOL_CODE AND A.SCHOOL_YEAR = Z.SCHOOL_YEAR) " & _
" and k.USER_PROFILE = ldap.TRLM_USER " & _
" and a.SCHOOL_YEAR in ('" & _school_year & "') " & _
" and ldap.LDAP_USER  = '" & username & "'" & _
" and ((f.INSTR_TIME_CODE in ('RSW','SED','ADM')) OR (a.STAFF_TYPE_NAME = 'Teacher Spec'))" & _
" and a.START_DATE -14 <= sysdate " & _
" and sysdate - 21 < a.end_date"

            oraSQL = "select * from VW_PEI_AFFECTATION v where v.LDAP_USER = '" & username & "'"

            Dim ds As New DataSet
            Dim dt1 As New DataTable
            Dim oGen As New PEIDAL.general

            'dt1 = GetDatatable(oraSQL, ConfigurationSettings.AppSettings("ConnectionStringTrillium"))


            ds = oGen.GetDataset(oraSQL, ConfigurationSettings.AppSettings("ConnectionString"))
            dt1 = ds.Tables(0)

            Dim dsEcoles As New DataSet(ConfigurationSettings.AppSettings("ConnectionString"))


            dsEcoles = oGen.GetDataset("select pk_ecole_id,school_code from ecoles", ConfigurationSettings.AppSettings("ConnectionString"))

            dt1.Columns.Add("pk_ecole_id", GetType(String))

            For Each r As DataRow In dt1.Rows
                For Each r2 As DataRow In dsEcoles.Tables(0).Rows
                    If Not r.Item("school_code").ToString <> r2.Item("school_code").ToString Then
                        r.Item("pk_ecole_id") = r2.Item("pk_ecole_id").ToString
                    End If
                Next
            Next

            DropDownList.DataSource = dt1
            If _ValueField <> "pk_ecole_id" Then
                DropDownList.DataValueField = _ValueField
            Else
                DropDownList.DataValueField = "pk_ecole_id"
            End If

            DropDownList.DataTextField = "SCHOOL_NAME"
            DropDownList.DataBind()

        End Sub
        Function hasAccess(username As String) As String
            Dim dsUser As New DataSet
            Dim dsFonctions As New DataSet
            Dim _msg As String = ""

            If IsUserAdmin(username) Then
                _msg = "succes"
                Return _msg
            End If

            Dim sql As String = "select * from VW_AFFECTATIONS_ECOLES where username ='" & username & "'"
            Dim sqlfonctions As String
            Dim fonctions As String()
            Dim oGen As New PEIDAL.general

            Dim pos As Integer
            dsUser = oGen.GetDatasetOLEDB(sql, ConfigurationSettings.AppSettings("connectionStringSAP"))

            Dim fonctionsIN As String = ConfigurationSettings.AppSettings("num_fonctions_access")

            sqlfonctions = "select distinct num_fonction, desc_fonction from VW_AFFECTATIONS_ECOLES where NUM_FONCTION in (" & fonctionsIN & ")"

            dsFonctions = oGen.GetDatasetOLEDB(sqlfonctions, ConfigurationSettings.AppSettings("connectionStringSAP"))

            Dim _affected As Boolean = False
            Dim _numfonction As String = ""

            Dim fonctionDesc As String

            Dim _affectations As String = ""
            For Each f As DataRow In dsFonctions.Tables(0).Rows
                fonctionDesc = f.Item("desc_fonction").ToString & "  (numero de fonction:" & f.Item("num_fonction").ToString & ")"
                _affectations = _affectations & "<li>" & fonctionDesc
            Next

            If dsUser.Tables(0).Rows.Count = 0 Then
                _msg = "Vous n'avez pas acc�s aux PEI WEB.<br> Selon nos informations vous n'avez aucune affectation en date d'aujourd'hui. Pour acc�der aux pei vous devez avoir de l'affectation pour l'un des poses suivants:<br><br>"
            End If

            If dsUser.Tables(0).Rows.Count > 0 Then
                _msg = "Vous n'avez pas acc�s aux PEI WEB.<br> Selon nos informations vous'avez pas d'affectation active pour l'un des postes suivants:<br><br>"
            End If

            _msg = _msg & _affectations

            For Each r As DataRow In dsUser.Tables(0).Rows
                _numfonction = r.Item("num_fonction").ToString
                For Each f As DataRow In dsFonctions.Tables(0).Rows
                    If _numfonction = f.Item("num_fonction").ToString Then
                        _msg = "succes"
                        _affected = True
                    End If
                Next
            Next

            Return _msg
        End Function
        Sub _BindDropDownListEcoleByUserID2(ByVal username As String, ByVal DropDownList As DropDownList, Optional ByVal _ValueField As String = "pk_ecole_id")
            Dim _school_year As String = ConfigurationSettings.AppSettings("school_year").ToString

            Dim oraSQL As String
            Dim fonctionsIN As String = ConfigurationSettings.AppSettings("num_fonctions_access")
            oraSQL = "select school_code from SCHOOL_CODE_MAPPING sm where sm.MAIN_SCHOOL_CODE " & _
                " in (select sm2.MAIN_SCHOOL_CODE from SCHOOL_CODE_MAPPING sm2 where sm2.SCHOOL_CODE in " & _
                " (select v.school_code from VW_AFFECTATIONS_ECOLES v where username='" & username & "' and num_fonction in (" & fonctionsIN & " )))"

            Dim ds As New DataSet
            Dim dt1 As New DataTable
            Dim oGen As New PEIDAL.general

            ds = oGen.GetDatasetOLEDB(oraSQL, ConfigurationSettings.AppSettings("connectionStringSAP"))
            dt1 = ds.Tables(0)

            Dim dsEcoles As New DataSet(ConfigurationSettings.AppSettings("ConnectionString"))

            dsEcoles = oGen.GetDataset("select pk_ecole_id,school_code,nom_ecole from ecoles", ConfigurationSettings.AppSettings("ConnectionString"))

            dt1.Columns.Add("pk_ecole_id", GetType(String))
            dt1.Columns.Add("school_name", GetType(String))

            For Each r As DataRow In dt1.Rows
                For Each r2 As DataRow In dsEcoles.Tables(0).Rows
                    If Not r.Item("school_code").ToString <> r2.Item("school_code").ToString Then
                        r.Item("pk_ecole_id") = r2.Item("pk_ecole_id").ToString
                        r.Item("school_name") = r2.Item("nom_ecole").ToString
                        End If
                Next
            Next

            DropDownList.DataSource = dt1
            If _ValueField <> "pk_ecole_id" Then
                DropDownList.DataValueField = _ValueField
            Else
                DropDownList.DataValueField = "pk_ecole_id"
            End If

            DropDownList.DataTextField = "SCHOOL_NAME"
            DropDownList.DataBind()

        End Sub
        'Sub BindDropDownListEcoleByUserID(ByVal Username As String, ByVal DropDownList As DropDownList)
        '    'liste �l�ve

        '    'Dim oEleve As New PEIDAL.eleve
        '    'DropDownList.DataSource = oEleve.GetListElevesByUtilisateurId(ConfigurationSettings.AppSettings("ConnectionString"), Session("username"))
        '    'DropDownList.DataValueField = "pk_eleve_id"
        '    'DropDownList.DataTextField = "nom_eleve"
        '    'DropDownList.DataBind()


        '    'liste �cole by utilisateur_id
        '    Dim oEcole As New PEIDAL.ecole


        '    'DropDownList.DataSource = oEcole.GetListEcoleByUtilisateurId(ConfigurationSettings.AppSettings("ConnectionString"), Username)
        '    'DropDownList.DataValueField = "pk_ecole_id"
        '    'DropDownList.DataTextField = "nom_ecole"
        '    '    DropDownList.DataBind()
        '    '==============================

        '    Dim _ds As New DataSet
        '    Dim _oreader As SqlDataReader
        '    Dim tbEcoles As New Data.DataTable
        '    Dim dcEcoleId = New DataColumn("ecoleid", GetType(String))
        '    Dim dcFullName = New DataColumn("ecole", GetType(String))
        '    tbEcoles.Columns.Add(dcEcoleId)
        '    tbEcoles.Columns.Add(dcFullName)

        '    _ds = oEcole.GetDsListEcole(ConfigurationSettings.AppSettings("ConnectionString"))
        '    _oreader = oEcole.GetListEcoleByUtilisateurId(ConfigurationSettings.AppSettings("ConnectionString"), Username)

        '    Dim _fullname As String = ""
        '    Dim _ville As String = ""
        '    Dim dsfilter As New DataSet

        '    Try
        '        If _oreader.HasRows Then
        '            Do While _oreader.Read
        '                '_fullname = item("nom_ecole") & " (" & item("ville").ToString & ")"

        '                '_ds.Tables(0).DefaultView.RowFilter = "pk_ecole_id=683922"
        '                _ville = _ds.Tables(0).Rows(0).Item("ville").ToString
        '                _fullname = _oreader.Item("nom_ecole") & " " & _ville

        '                tbEcoles.Rows.Add(_oreader.Item("pk_ecole_id").ToString, _fullname)

        '            Loop
        '        End If


        '        'For Each item In _ds.Tables(0).Rows
        '        '    '    'Dim row As Data.DataRow
        '        '    _fullname = item("nom_ecole") & " (" & item("ville").ToString & ")"
        '        '    tbEcoles.Rows.Add(item("pk_ecole_id").ToString, _fullname)
        '        'Next


        '        DropDownList.DataSource = tbEcoles
        '        DropDownList.DataValueField = "ecoleid"
        '        DropDownList.DataTextField = "ecole"
        '        DropDownList.DataBind()



        '    Catch ex As Exception
        '        'lblMsgErr.Visible = True
        '        'lblMsgErr.Text = "Erreur survenue lors du chargement des listes d�roulantes:  " & ex.Message
        '        Throw New Exception(ex.Message)

        '    End Try

        'End Sub
        Sub CreateDates(ByVal Conn As String, ByVal EleveId As String)
            Dim oIden As New PEIDAL.identification
            Dim dsDatesEleve1 As New PEIDAL.dsDatesEleve

            Try
                dsDatesEleve1 = oIden.GetdsDatesEleve(Conn, EleveId)

                If dsDatesEleve1.DATES.Rows.Count < 1 Then
                    Dim dr As PEIDAL.dsDatesEleve.DATESRow
                    dr = dsDatesEleve1.DATES.NewDATESRow

                    dr.SetDATE_CIPR_RECENTNull()
                    dr.SetDATE_DEBUT_PLACEMENTNull()
                    dr.SetDATE_ETAPE_1Null()
                    dr.SetDATE_ETAPE_2Null()
                    dr.SetDATE_ETAPE_3Null()
                    dr.SetDATE_ETAPE_4Null()
                    dr.SetDATE_MODIFICATIONNull()
                    dr.SetDATE_PEI_TERMINENull()
                    dr.SetDATE_RENONCIATIONNull()
                    dr.SetDATE_IDENTIFICATIONNull()

                    dr.FK_ELEVE_ID = EleveId

                    dsDatesEleve1.DATES.Rows.InsertAt(dr, 0)

                    oIden.UpdateDsDatesEleve(Conn, dsDatesEleve1)

                End If

            Catch ex As Exception
                Throw New System.Exception("Erreur survenu lors de l'enregistrements des dates")
                'isFailed = True

            End Try
        End Sub
    Function SetFrenchCanDate(ByVal sDate As String) As String
        Dim Year, Month, Day As String

        Year = Mid(sDate, 7, 4)
        Month = Mid(sDate, 4, 2)
        Day = Mid(sDate, 1, 2)

        Return Year & "-" & Month & "-" & Day

        End Function
        Function GetDatatable(ByVal sqlQuery As String, ByVal sConnectionString As String) As DataTable
            Dim _dt As New DataTable
            Dim _dr As OleDb.OleDbDataReader
            Dim _oraconn As New OleDb.OleDbConnection(sConnectionString)
            Dim _cmd As New OleDb.OleDbCommand

            _oraconn.Open()
            _cmd.Connection = _oraconn
            _cmd.CommandText = sqlQuery
            _dr = _cmd.ExecuteReader
            _dt.Load(_dr)

            Return _dt

        End Function
        Function GetSqlDataReader(ByVal sqlQuery As String, ByVal ConnectionString As String) As SqlDataReader
            Dim _dr As SqlDataReader
            Dim _sql As String
            Dim objConn As New SqlConnection(ConnectionString)
            Dim oCmd As New SqlCommand

            objConn.Open()
            oCmd.Connection = objConn
            oCmd.CommandText = sqlQuery
            _dr = oCmd.ExecuteReader

            Return _dr

        End Function
        Function GetDataset(_sql As String) As DataSet
            Dim _oGen As New PEIDAL.general
            Return _oGen.GetDataset(_sql, _ConnString)
        End Function
        Function GetDsOracle(sql As String, connstring As String) As DataSet
            'Dim sql As String = "select * from departments where department_id < 60"
            Dim oradb As String = connstring
            Dim conn As New OracleConnection(oradb)
            Dim cmd As New OracleCommand(sql, conn)
            cmd.CommandType = CommandType.Text
            Dim ds As New DataSet
            Dim da As New OracleDataAdapter(cmd)
            Dim cb As New OracleCommandBuilder(da)

            ds = New DataSet()
            da.Fill(ds)
            conn.Dispose()

            Return ds
        End Function
        Public Function GetSAPaffectations(connstring As String, username As String) As DataSet
            Dim _sql As String = "select * from VW_AFFECTATIONS_ECOLES where username='" & username & "'"
            Return GetDsOracle(_sql, connstring)
        End Function
        Function GetListElevesTrillOracle(ByVal ConnectionString As String, ByVal School_Code As String, ByVal First_Name As String, ByVal Legal_Name As String, ByVal Year As String, ByVal BirthDay As Date, ByVal StudentNo As String, ByVal PersonID As String) As DataSet

            Dim objGen As New PEIDAL.general

            Dim strSQL As String
            Dim ANDflag As Boolean = False
            Dim WHEREflag As Boolean = False

            strSQL = "select PERSONS.PERSON_ID," & _
            "PERSONS.STUDENT_NO," & _
            "PERSONS.LEGAL_FIRST_NAME," & _
            "PERSONS.LEGAL_SURNAME," & _
            "TO_CHAR(PERSONS.BIRTH_DATE,'YYYY-MM-DD'), " & _
            "PERSONS.GENDER," & _
            "STUDENT_REGISTRATIONS.GRADE," & _
            "STUDENT_REGISTRATIONS.SCHOOL_YEAR," & _
            "SCHOOLS.SCHOOL_NAME," & _
            "SCHOOLS.SCHOOL_CODE " & _
            "from PERSONS inner join STUDENT_REGISTRATIONS on PERSONS.PERSON_ID = STUDENT_REGISTRATIONS.PERSON_ID " & _
            "INNER JOIN SCHOOLS ON STUDENT_REGISTRATIONS.SCHOOL_CODE=SCHOOLS.SCHOOL_CODE"

            If School_Code <> "" Then
                If WHEREflag = False Then
                    strSQL = strSQL & " WHERE "
                    WHEREflag = True
                    ANDflag = True
                End If

                'If ANDflag = True Then
                '    strSQL = strSQL & " AND "
                '    ANDflag = True
                'End If

                strSQL = strSQL & " SCHOOLS.SCHOOL_CODE='" & School_Code & "'"

            End If

            If First_Name <> "" Then
                If WHEREflag = False Then
                    strSQL = strSQL & " WHERE "
                    WHEREflag = True
                End If
                If ANDflag = True Then
                    strSQL = strSQL & " AND "
                    ANDflag = True
                End If

                strSQL = strSQL & " UPPER(PERSONS.LEGAL_FIRST_NAME) LIKE '%" & First_Name.ToUpper & "%'"

                ANDflag = True
            End If

            If Legal_Name <> "" Then
                If WHEREflag = False Then
                    strSQL = strSQL & " WHERE "
                    WHEREflag = True
                End If
                If ANDflag = True Then
                    strSQL = strSQL & " AND "
                    ANDflag = True
                End If

                strSQL = strSQL & " UPPER(PERSONS.LEGAL_SURNAME) LIKE '%" & Legal_Name.ToUpper & "%'"

                ANDflag = True
            End If

            If BirthDay <> Date.MinValue Then
                If WHEREflag = False Then
                    strSQL = strSQL & " WHERE "
                    WHEREflag = True
                End If
                If ANDflag = True Then
                    strSQL = strSQL & " AND "
                    ANDflag = True
                End If

                strSQL = strSQL & " TO_CHAR(PERSONS.BIRTH_DATE,'YYYY-MM-DD') ='" & BirthDay & "'"

                ANDflag = True
            End If

            If Year <> "" Then

                If WHEREflag = False Then
                    strSQL = strSQL & " WHERE "
                    WHEREflag = True
                End If
                If ANDflag = True Then
                    strSQL = strSQL & " AND "
                    ANDflag = True
                End If

                strSQL = strSQL & " STUDENT_REGISTRATIONS.SCHOOL_YEAR='" & Year & "'"

                ANDflag = True
            End If

            If StudentNo <> "" Then

                If WHEREflag = False Then
                    strSQL = strSQL & " WHERE "
                    WHEREflag = True
                End If
                If ANDflag = True Then
                    strSQL = strSQL & " AND "
                    ANDflag = True
                End If

                strSQL = strSQL & " PERSONS.STUDENT_NO LIKE '%" & StudentNo & "%'"

                ANDflag = True
            End If

            If PersonID <> "" Then

                If WHEREflag = False Then
                    strSQL = strSQL & " WHERE "
                    WHEREflag = True
                End If
                If ANDflag = True Then
                    strSQL = strSQL & " AND "
                    ANDflag = True
                End If

                strSQL = strSQL & " PERSONS.PERSON_ID LIKE '%" & PersonID & "%'"

                ANDflag = True
            End If

            Try
                'objGen.ExecuteCommand(ConnectionString, strSQL)
                'objGen.ExecuteOLEDBCommand(ConnectionString, strSQL)
                Dim ds As DataSet
                'ds = objGen.GetDatasetOLEDB(strSQL, ConnectionString)
                ds = GetDsOracle(strSQL, ConnectionString)
                Return ds

            Catch ex As Exception
                'WriteLog("Erreur suvenu dans la fonction eleves.GetListElevesTrillOLEDB " & "DETAILS: " & ex.Message)
                Throw New Exception("Une erreur suvenu:" & ex.Message)
                'Connect.Close()

            End Try


        End Function
        Sub insertLog(ByVal ConnectionString As String, ByVal Action As String, ByVal ModulePei As String, ByVal User_id As String, ByVal Eleve_id As String, ByVal Person_id As String, ByVal Date_action As Date, ByVal Browser As String, ByVal IP As String, sDetails As String)
            'Dim _dr As SqlCommand
            Dim _sql As String
            Dim objConn As New SqlConnection(ConnectionString)
            Dim oCmd As New SqlCommand

            _sql = "INSERT INTO LOGS (ACTION,MODULE,USER_ID,ELEVE_ID,PERSON_ID,DATE_ACTION,CLIENT,IP,DETAILS) VALUES ('" & _
            Action & "','" & ModulePei & "','" & User_id & "','" & Eleve_id & "','" & Person_id & "','" & Date_action & "','" & Browser & "','" & IP & "','" & Replace(sDetails, "'", "''") & "')"

            objConn.Open()
            oCmd.Connection = objConn
            oCmd.CommandText = _sql
            oCmd.ExecuteNonQuery()

        End Sub


        Public Function ADSIlogin(ByVal username As String, ByVal password As String, ByVal domaine As String, ByVal soAuth As Boolean) As Boolean


            'Dim entry As SearchResultEntry = Nothing
            Dim entry As DirectoryEntry = Nothing
            Dim serveur As String
            Dim aps As AppSettingsReader = New AppSettingsReader()

            If domaine = "CONSEIL" Then
                serveur = aps.GetValue("LDAPServeurCONSEIL", System.Type.GetType("System.String")).ToString()
            Else
                serveur = aps.GetValue("LDAPServeurACADEMIQUE", System.Type.GetType("System.String")).ToString()
            End If

            Try

                entry = New DirectoryEntry(serveur, domaine & "\" & username, password)


                Dim NativeObject As Object = entry.NativeObject
                Return True

                'Catch ex As LDAPSearchException
                '    entry.Close()
                '    entry.Dispose()
                '    Return _getFailedResult(ex.reason)
                'End Try
            Catch ex As Exception
                entry.Close()
                entry.Dispose()
                Throw New Exception("erreur LDAP - " & ex.Message)
            End Try
        End Function

    End Module

End Namespace
