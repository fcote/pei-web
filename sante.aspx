<%@ Register TagPrefix="uc1" TagName="menu_top" Src="menu_top.ascx" %>
<%@ Register TagPrefix="uc2" TagName="menu_top2" Src="WebUserMenuTop.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="sstchur.web.SmartNav" Assembly="sstchur.web.SmartNav" %>
<%@ Page Language="vb" AutoEventWireup="false" ValidateRequest="false" SmartNavigation = "true" Inherits="PeiWebAccess.sante" CodeFile="sante.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>sante</title>
		<LINK href="styles.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="tableMain " cellSpacing="0" cellPadding="0"  border="0" width="100%">
				<tr>
					<td style="width: 100%"><uc1:menu_top id="Menu_top1" runat="server"></uc1:menu_top></td>
				</tr>
				<!--<tr>
					<td><uc2:menu_top2 id="Menu_top2" runat="server"></uc2:menu_top2></td>>
				</tr>-->
				<tr>
					<td class="titleOnglet">Sant�, adaptations, �quipement sp�cialis�, ressources 
						humaines</td>
				</tr>
				<tr>
					<td class="titleOnglet">
						<asp:label id="lblMsgErr" runat="server" Visible="False" BorderWidth="1px" BorderColor="Firebrick"
							BackColor="Khaki" Width="100%" CssClass="fldlabel"></asp:label></td>
				</tr>
				<tr>
					<td class="titleOnglet" >
						<asp:Button id="Button1" runat="server" CssClass="fldTextBox" Width="300px" Text="Enregistrer"></asp:Button></td>
				</tr>
				<tr>
					<td class="fldLabel">Services auxilliaires de sant� et personnel de soutien requis:
						<asp:radiobuttonlist id="RadioButtonListProbSanteConn" runat="server" Width="40px" CssClass="fldLabel"
							RepeatDirection="Horizontal" AutoPostBack="True">
							<asp:ListItem Value="oui">oui</asp:ListItem>
							<asp:ListItem Value="non">non</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td style="HEIGHT: 21px"><asp:panel id="PanelProbSanteConn" runat="server">
							<TABLE id="Table1" cellSpacing="1" cellPadding="1"  border="0">
								<tr>
									<td>
										<asp:dropdownlist id="DropDownListProbSanteConnexes" runat="server" 
                                            CssClass="fldLabel" Width="333px"></asp:dropdownlist>
										<asp:ImageButton id="ImageButtonAjouterProbSanteConn" runat="server" ImageUrl="images/create.gif"></asp:ImageButton></td>
								</tr>
								<tr>
									<td>
										<asp:textbox id="txtProbSanteConnexes" runat="server" CssClass="fldTextBox"  Height="70px"
											TextMode="MultiLine" Width="350px"></asp:textbox></td>
								</tr>
								<tr>
									<td></td>
								</tr>
							</TABLE>
						</asp:panel></td>
				</tr>
				<tr>
					<td>
						<table  id="TableMainAdaptations" borderColor="#ff66ff" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
							    <td class="cbToolbar" Width="350px">Adaptations p�dagogiques</td>	
							    <td class="cbToolbar" width="350px">Adaptations environnementales</td>
							    <td class="cbToolbar" Width="350px">Adaptations en mati�re d'�valuation</td>
								<td class="cbToolbar" Width="200px"></td>
							</tr>
							
							<tr>
							
							<td align="left">
									<TABLE id="TableAdaptPedag" cellSpacing="0" cellPadding="0" border="0">
										<tr>
											<td style="HEIGHT: 19px"><asp:dropdownlist id="DropDownListAdaptPedagogique" runat="server" Width="330"   CssClass="fldLabel"></asp:dropdownlist>
											<asp:imagebutton id="ImageButtonAjouterAdapPedag" runat="server" ImageUrl="images/create.gif"></asp:imagebutton></td>
										</tr>
										<tr>
											<td><asp:textbox id="txtAdaptPedagogiques" runat="server" Width="350px"  CssClass="fldTextBox" Height="200px"
													TextMode="MultiLine"></asp:textbox></td>
										</tr>
									</TABLE>
								</td>
								
						    
								
							<td align="left">
									<TABLE id="TableAdaptEnv" cellSpacing="0" cellPadding="0" border="0">
										<tr>
											<td style="HEIGHT: 19px"><asp:dropdownlist id="DropDownListAdaptationsEnv" runat="server" Width="330"  CssClass="fldLabel"></asp:dropdownlist>
											<asp:imagebutton id="ImageButtonAjouterAdaptations" runat="server" ImageUrl="images/create.gif"></asp:imagebutton></td>
										</tr>
										<tr>
											<td><asp:textbox id="txtAdaptEnv" runat="server" Width="350px" CssClass="fldTextBox" Height="200px"
													TextMode="MultiLine"></asp:textbox></td>
										</tr>
									</TABLE>
								</td>
								
							<td align="left">
									<TABLE id="TableAdapEval" cellSpacing="0" cellPadding="0" border="0">
										<tr>
											<td style="HEIGHT: 19px"><asp:dropdownlist id="DropDownListAdaptMatiereEval" runat="server" Width="330"   CssClass="fldLabel"></asp:dropdownlist>
											<asp:imagebutton id="ImageButtonAjouterAdaptEval" runat="server" ImageUrl="images/create.gif"></asp:imagebutton></td>
										</tr>
										<tr>
											<td><asp:textbox id="TextBoxAdaptMatiereEval" runat="server" Width="350px"  CssClass="fldTextBox" Height="200px"
													TextMode="MultiLine"></asp:textbox></td>
										</tr>
									</TABLE>
								</td>
															
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<TABLE id="Table3" cellSpacing="1" cellPadding="1" width="100%" border="0" borderColor="red">
							<tr>
								<td class="cbToolbar">�quipement personnalis�</td>
							</tr>
							<tr>
								<td><asp:dropdownlist id="DropDownListEquipement" runat="server" Width="368px" CssClass="fldLabel"></asp:dropdownlist><asp:imagebutton id="ImageButton1" runat="server" ImageUrl="images/create.gif"></asp:imagebutton></td>
							</tr>
							<tr>
								<td><asp:textbox id="txtEquipPers" runat="server" Width="368px" CssClass="fldTextBox" Height="50px"
										TextMode="MultiLine"></asp:textbox></td>
							</tr>
						</TABLE>
					
						
					</td>
				</tr>
				<tr>
					<td class="cbToolbar" style="width: 600px">Ressources humaines (personnel enseignant et personnel non 
						enseignant)
						<asp:imagebutton id="ImageButtonAjouterRHeleves" runat="server" ImageUrl="images/create.gif"></asp:imagebutton></td>
				<tr>
					<td class="fldLabel" style="width: 816px">Indiquez le type de services, la date du d�but des services, 
						la fr�quence ou l 'intensit� des services et les endroits o� sont dispens�s les 
						services.</td>
				</tr>
				<tr>
					<td><asp:datagrid id="DataGrid1" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC"
							BorderWidth="1px" CssClass="fldLabel" Height="0px" DataMember="RH_ELEVES" DataKeyField="ROW_ID"
							CellPadding="3" BorderStyle="None" AutoGenerateColumns="False">
							<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
							<ItemStyle ForeColor="#000066"></ItemStyle>
							<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699"></HeaderStyle>
							<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
							<Columns>
								<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;img border=0 src=&quot;Images/save.gif&quot;&gt;"
									CancelText="Annuler" EditText="&lt;img border=0 src=&quot;Images/P_Draw_Lg_N.gif&quot;&gt;"></asp:EditCommandColumn>
								<asp:ButtonColumn Text="&lt;img border=0 src=&quot;Images/delete.gif&quot;&gt;" CommandName="Delete"></asp:ButtonColumn>
								<asp:BoundColumn Visible="False" DataField="ROW_ID" SortExpression="ROW_ID" HeaderText="ROW_ID"></asp:BoundColumn>
								<asp:BoundColumn DataField="Date_Inst" SortExpression="DATE_INST" HeaderText="Date d'instauration"
									DataFormatString="{0:dd/MM/yyyy}">
									<HeaderStyle Wrap="False"></HeaderStyle>
									<ItemStyle Wrap="False"></ItemStyle>
								</asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Type de service">
									<HeaderStyle Wrap="False"></HeaderStyle>
									<ItemStyle Wrap="False"></ItemStyle>
									<ItemTemplate>
										<asp:Label ID="lblTypEval" Text='<%# DataBinder.Eval(Container.DataItem, "FK_TYPE_SERVICE") %>' Runat="server"/>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label ID="lblTypeEvalHold" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FK_TYPE_SERVICE") %>' />
										<asp:DropDownList id="ddlTypeEval" DataSource="<%# BindListIntervenant() %>" OnPreRender="SetDropDownListIntervenant" DataTextField="description" DataValueField="description" runat="server" />
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="FREQ" SortExpression="FREQ" HeaderText="Fr&#233;quence">
									<HeaderStyle Wrap="False"></HeaderStyle>
									<ItemStyle Wrap="False"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="LIEU_INST" SortExpression="LIEU_INST" HeaderText="Lieu/endroit">
									<HeaderStyle Wrap="False"></HeaderStyle>
									<ItemStyle Wrap="False"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="LIEU_INST" SortExpression="LIEU_INST" HeaderText="Lieu/endroit">
									<HeaderStyle Wrap="False"></HeaderStyle>
									<ItemStyle Wrap="False"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
						<cc1:SmartScroller id="SmartScroller1" runat="server"></cc1:SmartScroller></td>
				</tr>
			</TABLE>
		</form>
		
	</body>
</HTML>
