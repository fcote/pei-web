Imports System
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient


Namespace PeiWebAccess

Partial Class print_attentes
    Inherits System.Web.UI.Page

#Region " Code g�n�r� par le Concepteur Web Form "

    'Cet appel est requis par le Concepteur Web Form.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Public intRowID As Integer
    Public strNiveau As String
    Public strNiveauID As String

    Public strMatiere As String
    Public intMatiereID As Integer

    Public strCoteNote As String
    Public strTypeAttente As String 'use for the two datagrids
    Public strCompetence As String
    Public strRendement As String

    Public intButNo As Integer = 0

    Protected WithEvents kljlkjl As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents TDtitulaires As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents PanelGenerales As System.Web.UI.WebControls.Panel
    Protected WithEvents TDNomEleve_temp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents TDDateCiprRecent_temp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents TDdateNaissance_temp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents TDdateRenonciation_temp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents TDnoIdentification_temp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents TDdateDebutPlacement_temp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents TDsexe_temp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents TDdatePeiTermine_temp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents TDNiveau_temp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents TDtitulaire_temp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents TDplacement_temp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents TDanomalie_temp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents TDcaracteristiques As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents TDraison_temp As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents TDExemp_Prog_Scol_1 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents TDExemp_Prog_Scol_2 As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdEquipPersonnalise As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents Panel2 As System.Web.UI.WebControls.Panel
    Protected WithEvents PanelButs As System.Web.UI.WebControls.Panel
    Protected WithEvents DataList1 As System.Web.UI.WebControls.DataList
    Protected WithEvents txtProbSanteConn As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents tdAdaptations As System.Web.UI.HtmlControls.HtmlTableCell

    'REMARQUE�: la d�claration d'espace r�serv� suivante est requise par le Concepteur Web Form.
    'Ne pas supprimer ou d�placer.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN�: cet appel de m�thode est requis par le Concepteur Web Form
        'Ne le modifiez pas en utilisant l'�diteur de code.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Placez ici le code utilisateur pour initialiser la page

        If Not Session("eleveid") <> "" Then
            Response.Redirect("frameset1.htm")
        End If

        If Not Page.IsPostBack Then
            BindCheckBoxListButs(Session("eleveid"), CheckboxlistButs)
        End If

        Dim str_but_Titreue As String = "� complimenter ou r�compenser selon son comportement"
        Dim intASCII As Integer

        intASCII = Asc("�")


    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Panel1.Visible = False
        PanelInfosEcole.Visible = True

        DisplayPage1()
        DisplayPage1Dates()
        DisplayPage1Identification()
        DisplaySanteEleves()

        'generales
        If CheckBoxPage1.Checked Then
            'dispaly form panel donn�es g�n�rales
            panelGenerale.Visible = True

        Else
            panelGenerale.Visible = False
        End If

        If CheckBoxForcesBesoins.Checked Then
            PanelForcesBesoins.Visible = True
            displayForcesBesoins()
            DisplayRapportsEvaluation()
            DisplayDatagridRH()
        Else
            PanelForcesBesoins.Visible = False
        End If

        If CheckBoxRendement.Checked Then
            'PanelRendement.Visible = True
            PanelCompetencesHabil.Visible = True
            DisplayDatagridMatieresEleves()

            DisplayDatagridCompetencesHabil()

            'sources informations
            DisplaySourcesInformation()
            'adapt, equip..
            DisplaySante()
        Else
            PanelCompetencesHabil.Visible = False
        End If

        '-- buts----

        Dim ch As CheckBox
        Dim chbl As CheckBoxList
        Dim i As Integer
        Dim li As ListItem

        Dim butId As Integer
        Dim strSelectedButs As String

        For Each li In CheckboxlistButs.Items
            If li.Selected Then
                butId = li.Value
                If strSelectedButs <> "" Then
                    strSelectedButs = strSelectedButs & ";" & li.Value
                Else
                    strSelectedButs = li.Value
                End If

            End If
            'if checkboxlistbuts.Items.IndexOf(
        Next

        'si checkboxlist checked
        If strSelectedButs <> "" Then
            BindTitle(strSelectedButs, DropDownListEtape.SelectedValue)
        End If

        '-------------
        Dim itmFood As ListItem
        Dim strList As String

        'For Each itmFood In chklFavoriteFoods.Items
        '    If itmFood.Selected Then
        '        strList &= "<li>" & itmFood.Text
        '    End If
        'Next
        'lblSelectedFoods.Text = strList
        '--------------


        'For i = 0 To CheckboxlistButs.Items.Count
        '    If CheckboxlistButs.Items(i).Selected Then
        '        butId = CheckboxlistButs.ID
        '    End If
        'Next



        '-------------
        'If CheckBoxButs.Checked Then
        '    DisplayAttentes() 'affiche tous les buts et attentes
        '    PanelButs.Visible = True
        'Else
        '    PanelButs.Visible = False
        'End If

        If CheckBoxTestsProvinciaux.Checked Then
            panelTestsProvinciaux.Visible = True
            BindTestProvinciaux()
            'TDevalProv.InnerText = GetInfosEleve(Session("eleveid"), "evaluationstestsprov")
            'BindAdaptationsEvalProv(Session("eleveid"))
            'BindExemptionsEvalProv(Session("eleveid"))

            'tdAdaptations.InnerHtml = GetInfosEleve(Session("eleveid"), "evaluationstestsprov")
            'tdexemptions.InnerHtml=

        End If

        If CheckBoxPlanTransistion.Checked Then
            PanelPlanTransition.Visible = True
            DisplayPlanTransition()
            DisplayMesure()

        End If

    End Sub
    Sub DisplayAttentes()
        Dim oAt As New PEIDAL.attente
        Dim myReader As SqlDataReader
        Dim myDS As New DataSet

        'myDS = oAt.GetDsAttentesByButId(65)
        myDS = oAt.GetDsAttentesAndButsByEleveId(Session("eleveid"))
        'myReader = oAt.GetReaderAttentesByButID(ConfigurationSettings.AppSettings("ConnectionString"), 49)

        DataList1.DataSource = myDS.Tables(0).DefaultView
        DataList1.DataBind()
    End Sub
    Sub DisplayPlanTransition()
        Try
            Dim oPlan As New PEIDAL.plan

            DataGridButTransition.DataSource = oPlan.GetdsButsTransition(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            DataGridButTransition.DataBind()

        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try

    End Sub
    Sub DisplayMesure()
        Dim oPlan As New PEIDAL.plan

        Try
            DataGridMesures.DataSource = oPlan.GetdsPlanIntervention(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            DataGridMesures.DataBind()

        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try

    End Sub
    Sub DisplayAttentes_2(ByVal ButID As Integer)
        Dim oAtt As New PEIDAL.attente
        Dim sqlReader As SqlDataReader

        'sqlReader = oFB.GetForcesByEleveId(ConfigurationSettings.AppSettings("ConnectionString"), "AZ199511185_2")


        sqlReader = oAtt.GetReaderAttentesByButID(ConfigurationSettings.AppSettings("ConnectionString"), ButID)


        'sqlReader = oFB.GetForcesTEST_ListElevesByUtilisateurId(ConfigurationSettings.AppSettings("ConnectionString"), "jcote")
        Try
            If sqlReader.HasRows Then
                Response.Write("count: " & sqlReader.RecordsAffected)
                'Response.Write("count: " & sqlReader.Ite)
                Response.Write("<table width=600 border=1>")

                Do While sqlReader.Read
                    Response.Write("<tr><td>")
                    Response.Write("niveau :" & sqlReader.GetString(4))
                    Response.Write("/<td><td>")
                    Response.Write("aa :" & sqlReader.GetString(5))
                    Response.Write("/<td><td>")
                    Response.Write("bb :" & sqlReader.GetString(6))
                    Response.Write("/<td><td>")
                    Response.Write("cc :" & sqlReader.GetString(7))
                    Response.Write("</td>")

                    Response.Write("</tr><tr><td>")

                    'TextBox1.Text = sqlReader.GetString(2)
                    'Response.Write("<br>attente :" & sqlReader.GetString(3))
                    'Response.Write("<br>attente :" & sqlReader.GetString(1))
                    Response.Write("<br>attente :" & sqlReader.GetString(2))
                    Response.Write("<br>strategie :" & sqlReader.GetString(3))
                    Response.Write("<br>niveau :" & sqlReader.GetString(4))
                    'Response.Write("<br>attente :" & sqlReader.GetString(5))
                    'Response.Write("<br>attente :" & sqlReader.GetString(6))
                    Response.Write("</tr><tr><td>")
                    Response.Write("<hr></td></tr>")

                Loop
                Response.Write("</table>")

            End If

        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try


        sqlReader.Close()

    End Sub
    Public Sub SetTextBoxValue(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ed As System.Web.UI.WebControls.TextBox
        ed = sender
        'ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(intRowID))
        ed.Text = "alfjaflajkfaljdfl;s"
        'ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strVal))
    End Sub
    Public Function FormatData(ByVal strData As String) As String
        'Dim temp As String
        'Dim asctemp As Integer

        'strData = Replace(strData, "-", "<li>")
        strData = Chr(13) & strData
        'strData = Replace(strData, Chr(13), "<br>")
        strData = Replace(strData, Chr(149), "<li>")
        strData = Replace(strData, Chr(13), "<br>")

        ''strData = Replace(strData, Asc(149), "<li>")
        'strData = Replace(strData, "�", "<li>")

        'asctemp = Asc(Left(strData, 1))

        Return strData

    End Function
    Sub DisplayPage1Identification()
        Dim oIden As New PEIDAL.identification

        Try
            TDraison.InnerText = oIden.GetStringRaisonByEleveID(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            TDplacement.InnerText = oIden.GetStringPlacementByEleveID(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            TDanomalie.InnerText = oIden.GetStringAnomalieByEleveID(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

            DataGridElaborations.DataSource = oIden.GetdsElaborePar(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            DataGridElaborations.DataBind()
            DataGridElaborations.ShowHeader = False

        Catch ex As Exception
            lblMsg.Text = "erreure survenu dans la fonction 'DisplayPage1Identification'"

        End Try

        'TDanomalie.InnerText()
        'TDanomalie.InnerText = DsEleve.Tables(0).Rows(0).Item("anomalie_description")
        'TDcaracteristiques.InnerText = DsEleve.Tables(0).Rows(0).Item("identifie_caracteristiques")
        'TDraison.InnerHtml = DsEleve.Tables(0).Rows(0).Item("raison_description")
        'TDExemp_Prog_Scol_1.InnerHtml = DsEleve.Tables(0).Rows(0).Item("Exemp_Prog_Scol_1")
        'TDExemp_Prog_Scol_2.InnerHtml = DsEleve.Tables(0).Rows(0).Item("Exemp_Prog_Scol_2")


        ''infos �cole:
        'TDecole.InnerText = DsEleve.Tables(0).Rows(0).Item("nom_ecole")
        'TDadresseEcole.InnerText = DsEleve.Tables(0).Rows(0).Item("adresse_ecole")
        'TDdirection.InnerText = DsEleve.Tables(0).Rows(0).Item("directeur")
        'TDvilleEcole.InnerText = DsEleve.Tables(0).Rows(0).Item("ville")


        ''infos conseil
        'TDconseil.InnerText = DsEleve.Tables(0).Rows(0).Item("conseil_description")
        'TDadresseConseil.InnerText = DsEleve.Tables(0).Rows(0).Item("CONSEIL_adresse")
    End Sub
    Sub DisplayPage1Dates()
        Dim oIden As New PEIDAL.identification
        Dim dsDatesEleve1 As New PEIDAL.dsDatesEleve
        Dim r As PEIDAL.dsDatesEleve.DATESRow

        Try
            dsDatesEleve1 = oIden.GetdsDatesEleve(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

            If dsDatesEleve1.DATES.Rows.Count > 0 Then

                r = dsDatesEleve1.DATES.Rows(0)

                If Not r.IsDATE_CIPR_RECENTNull Then
                    TDDateCiprRecent.InnerText = r.DATE_CIPR_RECENT.ToShortDateString
                End If

                If Not r.IsDATE_RENONCIATIONNull Then
                    TDdateRenonciation.InnerText = r.DATE_RENONCIATION.ToShortDateString
                End If

                If Not r.IsDATE_DEBUT_PLACEMENTNull Then
                    TDdateDebutPlacement.InnerText = r.DATE_DEBUT_PLACEMENT.ToShortDateString
                End If

                If Not r.IsDATE_PEI_TERMINENull Then
                    TDdatePeiTermine.InnerText = r.DATE_PEI_TERMINE.ToShortDateString
                End If

            End If


        Catch ex As Exception
            'send email notification error !
            lblMsg.Text = "erreure survenu dans la fonction 'DisplayPage1Dates'"

        End Try

    End Sub
    Sub DisplayPage1()
        Dim oEleve As New PEIDAL.eleve
        Dim oGen As New PEIDAL.general
        Dim DsEleve As New DataSet
        Dim dsEleve1 As New PEIDAL.dsEleve


        Try
            '        DsEleve = oEleve.GetEleveByID(Session("eleveid"))
            DsEleve = oGen.GetDsInfosEleveEleveId(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

            TDNomEleve.InnerText = DsEleve.Tables(0).Rows(0).Item("prenom_eleve") & " " & DsEleve.Tables(0).Rows(0).Item("nom_eleve")
            TDdateNaissance.InnerText = DsEleve.Tables(0).Rows(0).Item("date_naissance")
            TDnoIdentification.InnerText = DsEleve.Tables(0).Rows(0).Item("pk_eleve_id")
            TDsexe.InnerText = DsEleve.Tables(0).Rows(0).Item("sexe")
            TDNiveau.InnerText = DsEleve.Tables(0).Rows(0).Item("description_niveau")
            TDtitulaire.InnerText = DsEleve.Tables(0).Rows(0).Item("titulaire")
            'TDcaracteristiques.InnerText = DsEleve.Tables(0).Rows(0).Item("identifie_caracteristiques")


            TD_exemp_prog_scol_1.InnerText = DsEleve.Tables(0).Rows(0).Item("exemp_prog_scol_1")

            If Not Trim(DsEleve.Tables(0).Rows(0).Item("palier")) <> "SEC" Then
                TDdiplome.InnerHtml = "L'�l�ve travaille pr�sentement pour l'obtention du " & DsEleve.Tables(0).Rows(0).Item("diplome_description")
            End If

            'identification
            If Not IsDBNull(DsEleve.Tables(0).Rows(0).Item("placement_description")) Then
                TDplacement.InnerText = DsEleve.Tables(0).Rows(0).Item("placement_description")
            End If

            'infos Ecole
            TDconseil.InnerText = DsEleve.Tables(0).Rows(0).Item("CONSEIL_description")
            TDadresseConseil.InnerText = DsEleve.Tables(0).Rows(0).Item("CONSEIL_ADRESSE")
            TDecole.InnerText = DsEleve.Tables(0).Rows(0).Item("nom_ecole")
            TDdirection.InnerText = DsEleve.Tables(0).Rows(0).Item("directeur")
            TDadresseEcole.InnerText = DsEleve.Tables(0).Rows(0).Item("adresse_ecole")
            TDvilleEcole.InnerText = DsEleve.Tables(0).Rows(0).Item("ville")


        Catch ex As Exception
            lblMsg.Text = "erreur lors de la lecture des infos de l'�l�ve.." & ex.Message

        End Try

    End Sub
    Sub DisplayRapportsEvaluation()
        Dim oFB As New PEIDAL.forces_besoins

        Try
            DataGridRapEval.DataSource = oFB.GetdsRapportsEvaluationList(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            DataGridRapEval.DataBind()

        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try

    End Sub
    Sub DisplayDatagridMatieresEleves()
        Dim oProg As New PEIDAL.programme

        Try
            DataGrid1.DataSource = oProg.GetReaderMatieres_ElevesOrderByDescRowID(Session("eleveid"), ConfigurationSettings.AppSettings("ConnectionString"))
            DataGrid1.DataBind()

        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try

    End Sub
    Sub DisplayDatagridCompetencesHabil()
        Dim oProg As New PEIDAL.programme

        Try
            DataGrid2.DataSource = oProg.GetdsCompetences_Eleves(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            DataGrid2.DataBind()

        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try

    End Sub
    Sub DisplayDatagridRH()
        Dim oFB As New PEIDAL.forces_besoins
        Dim oSante As New PEIDAL.sante

        Try
            DataGridRH.DataSource = oSante.GetdsRhEleves(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            DataGridRH.DataBind()

        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try

    End Sub
    Sub displayForcesBesoins()
        Dim oFB As New PEIDAL.forces_besoins
        Dim sqlReader As SqlDataReader
        Dim strData As String

        sqlReader = oFB.GetForcesByEleveId(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

        If sqlReader.HasRows Then
            Do While sqlReader.Read
                strData = strData & sqlReader.GetString(0)
                strData = Replace(strData, "-", "<li>")

                strData = FormatData(strData)
                'lblForces.Text = sqlReader.GetString(0) & Chr(32)
                'TDforces.NoWrap = False
                'TDforces.InnerText = sqlReader.GetString(0) & Chr(32)
            Loop
            TDforces.InnerHtml = strData
        End If

        strData = ""
        sqlReader = oFB.GetBesoinsByEleveId(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

        If sqlReader.HasRows Then
            Do While sqlReader.Read
                strData = strData & sqlReader.GetString(0)
                strData = Replace(strData, "-", "<li>")
                'strData = "<p>" & strData & "</p>"

                strData = FormatData(strData)

                'lblBesoins.Text = sqlReader.GetString(0) & Chr(32)
                'TDforces.NoWrap = False
                'TDbesoins.InnerText = sqlReader.GetString(0) & Chr(32)
            Loop
            'TDbesoins.InnerText = strData
            TDbesoins.InnerHtml = strData
        End If

    End Sub
    Sub DisplaySanteEleves()
        Dim oSante As New PEIDAL.sante
        Dim dsSanteEleves1 As New PEIDAL.dsSanteEleves
        Dim r As PEIDAL.dsSanteEleves.SANTE_ELEVESRow
        Dim strData As String

        Try
            dsSanteEleves1 = oSante.GetdsSanteEleves(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

            If dsSanteEleves1.SANTE_ELEVES.Rows.Count < 1 Then
                Dim dr As DataRow = dsSanteEleves1.SANTE_ELEVES.NewRow

                dr("prob_sante_conn") = ""
                dr("recomm_particuliere") = ""
                dr("adapt_pedagogiques") = ""
                dr("equip_personnalise") = ""
                dr("adapt_environnementales") = ""
                dr("FK_ELEVE_ID") = Session("eleveid")

                dsSanteEleves1.SANTE_ELEVES.Rows.InsertAt(dr, 0)

                oSante.UpdateDsSanteEleves(ConfigurationSettings.AppSettings("ConnectionString"), dsSanteEleves1)

            Else

                r = dsSanteEleves1.SANTE_ELEVES.FindByFK_ELEVE_ID(Session("eleveid"))

                strData = Replace(r.PROB_SANTE_CONN.ToString, Chr(13), "<br>")
                'strData = r.PROB_SANTE_CONN.ToString
                'TDprob_sante_conn.InnerText = "<br>ljl;kj<br>jjjj<br>0000<br>hhhh"
                TDProbSanteConn.InnerHtml = strData
                'txtRecommPart.Text = r.RECOMM_PARTICULIERE.ToString
                'txtAdaptPedagogiques.Text = r.ADAPT_PEDAGOGIQUES.ToString
                'txtEquipPers.Text = r.EQUIP_PERSONNALISE.ToString
                tdEquipPers.InnerHtml = FormatData(r.EQUIP_PERSONNALISE)
                'txtAdaptEnv.Text = r.ADAPT_ENVIRONNEMENTALES.ToString
                tdAdapt_pedagogiques.InnerHtml = FormatData(r.ADAPT_PEDAGOGIQUES)
                TDadapt_envrionnementales.InnerHtml = FormatData(r.ADAPT_ENVIRONNEMENTALES)
                TDadapt_matiereevaluation.InnerHtml = FormatData(r.ADAPT_MATIEREEVALUATION)

                If r.PROB_SANTE_CONN.ToString <> "" Then
                    CheckBoxListProbSanteConn.SelectedValue = "oui"
                Else
                    CheckBoxListProbSanteConn.SelectedValue = "non"
                End If

            End If

        Catch ex As Exception
            lblMsg.Visible = True
            lblMsg.Text = ex.Message

        End Try

    End Sub
    Public Sub SetTextBoxRowID(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ed As System.Web.UI.WebControls.TextBox
        ed = sender
        'ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(intRowID))
        ed.Text = intRowID
        'ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strVal))
    End Sub
    'Public Function BindNiveau()
    '    Try
    '        Dim oProg As New PEIDAL.programme

    '        'Return oFB.GetTypeEval(ConfigurationSettings.AppSettings("ConnectionString"))
    '        Return oProg.GetListNiveauEtude


    '    Catch ex As Exception
    '        lblMsg.Text = ex.Message
    '    End Try

    'End Function
    'Public Function Bindmatiere()
    '    Try
    '        Dim oProg As New PEIDAL.programme

    '        'Return oFB.GetTypeEval(ConfigurationSettings.AppSettings("ConnectionString"))
    '        Return oProg.GetListMatiere


    '    Catch ex As Exception
    '        lblMsg.Text = ex.Message
    '    End Try

    'End Function
    Public Sub SetDropDownIndexNiveau(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ed As System.Web.UI.WebControls.DropDownList
        ed = sender
        ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strNiveau))
        'ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strVal))
    End Sub
    'Public Sub SetDropDownIndexMatiere(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim ed As System.Web.UI.WebControls.DropDownList
    '    ed = sender
    '    ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strMatiere))
    '    'ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strVal))
    'End Sub
    'Public Sub SetDropDownIndexCoteNote(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim ed As System.Web.UI.WebControls.DropDownList
    '    ed = sender
    '    ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strRendement))
    '    'ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strVal))
    'End Sub
    'Public Function BindCoteNote()
    '    Try
    '        Dim oAtt As New PEIDAL.attente

    '        'Return oFB.GetTypeEval(ConfigurationSettings.AppSettings("ConnectionString"))

    '        Return oAtt.GetListTypeEvaluation()


    '    Catch ex As Exception
    '        lblMsg.Text = ex.Message
    '    End Try

    'End Function
    'Public Function BindTypeAttente()
    '    Try
    '        Dim oProg As New PEIDAL.programme

    '        'Return oFB.GetTypeEval(ConfigurationSettings.AppSettings("ConnectionString"))
    '        'Return oProg.GetListMatiere


    '    Catch ex As Exception
    '        lblMsg.Text = ex.Message
    '    End Try

    'End Function
    'Public Sub SetDropDownIndexTypeAttente(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim ed As System.Web.UI.WebControls.DropDownList
    '    ed = sender
    '    ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strTypeAttente))
    '    'ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strVal))
    'End Sub
    Sub DisplaySourcesInformation()
        Dim oIdentification As New PEIDAL.identification
        Dim oReader As SqlDataReader

        Try
            oReader = oIdentification.GetListSourceInformations
            LoadCheckBoxList(oReader, CheckBoxListeSources)

            oReader = oIdentification.GetListSourceInformationsByEleveId(Session("eleveid"))
            SetCheckBoxList(CheckBoxListeSources, oReader)

        Catch ex As Exception
            lblMsg.Text = ex.Message

        End Try

    End Sub
    Sub DisplaySante()
        'Dim objSante As New PEIDAL.sante
        'Dim dsSanteEleves1 As PEIDAL.dsSanteEleves
        'Dim strValue As String

        'Try
        '    dsSanteEleves1 = objSante.GetdsSanteEleves((ConfigurationSettings.AppSettings("ConnectionString")), Session("eleveid"))
        '    Dim r As PEIDAL.dsSanteEleves.SANTE_ELEVESRow

        '    r = dsSanteEleves1.SANTE_ELEVES.FindByFK_ELEVE_ID(Session("eleveid"))

        '    tdEquipPers.InnerHtml = FormatData(r.EQUIP_PERSONNALISE)
        '    tdAdaptEnv.InnerHtml = FormatData(r.ADAPT_ENVIRONNEMENTALES)
        '    tdAdapt_environnementales.InnerHtml = FormatData(r.ADAPT_ENVIRONNEMENTALES)

        '    TDProbSanteConn.InnerHtml = FormatData(r.PROB_SANTE_CONN)



        'Catch ex As Exception
        '    lblMsg.Visible = True
        '    lblMsg.Text = "erreur lors de l'affichage des donn�es de sant�. --- :" & ex.Message
        'End Try

    End Sub
    Sub BindTitle(ByVal SelectedButs As String, ByVal SelectedComment As String)
        Dim ds As New DataSet
        Dim ID As String
        Dim sWhereClause As String
        Dim Commentaire As String
        Dim sqlStmt As String

        'Commentaire = "commentaires." & SelectedCommentaire

        ID = Session("eleveid")

        'Dim sqlStmt As String = "select buts.pk_but_id,buts.but_titre, attente, strategie,niveau_attente,b1,b2,b3,b4,inter,commentaire1,commentaire2,commentaire3,commentaire4 from attentes inner join buts on buts.pk_but_id=attentes.fk_but_id inner join eleves on eleves.pk_eleve_id=buts.fk_eleve_id where eleves.pk_eleve_id ='" & ID & "'"
        'Dim sqlStmt As String = "select buts.pk_but_id,buts.but_titre, buts.programme,buts.niveau_rendement,buts.annee_etude, attente, strategie,methode, commentaires.commentaire1 as commentaire, niveau_attente,b1,b2,b3,b4,inter from attentes inner join buts on buts.pk_but_id=attentes.fk_but_id inner join eleves on eleves.pk_eleve_id=buts.fk_eleve_id inner join commentaires on commentaires.fk_but_id = buts.pk_but_id where eleves.pk_eleve_id ='" & ID & "'"
        sqlStmt = "select buts.pk_but_id,buts.but_titre, buts.programme,buts.niveau_rendement,buts.annee_etude, attente, strategie,methode, commentaires.commentaireSelected  as commentaire, niveau_attente,b1,b2,b3,b4,inter from attentes inner join buts on buts.pk_but_id=attentes.fk_but_id inner join eleves on eleves.pk_eleve_id=buts.fk_eleve_id inner join commentaires on commentaires.fk_but_id = buts.pk_but_id"

        sqlStmt = Replace(sqlStmt, "commentaires.commentaireSelected", SelectedComment)

        If SelectedButs <> "" Then
            sWhereClause = " WHERE pk_but_id=" & Replace(SelectedButs, ";", " OR pk_but_id=")
        End If
        sqlStmt = sqlStmt & sWhereClause

        Dim conString As String = ConfigurationSettings.AppSettings("ConnectionString")
        Dim myda As SqlDataAdapter = New SqlDataAdapter(sqlStmt, conString)

        Try
            myda.Fill(ds, "Table")
            DataList2.DataSource = ds
            DataList2.DataBind()

        Catch ex As Exception
            lblMsg.Visible = True
            lblMsg.Text = ex.Message
        End Try

    End Sub

    Private Sub DataList2_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs)
        ''If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then

        ''    Try
        ''        Dim strval As String = CType(e.Item.FindControl("lblbut_titre"), Label).Text
        ''        Dim strProgramme As String = CType(e.Item.FindControl("lblbut_programme"), Label).Text
        ''        Dim strValCommentaire As String = CType(e.Item.FindControl("lblCommentaire"), Label).Text
        ''        Dim strStrategie As String = CType(e.Item.FindControl("lblStrategie"), Label).Text
        ''        Dim strMethode As String = CType(e.Item.FindControl("lblMethode"), Label).Text

        ''        Dim but_titre As String = ViewState("but_titre")
        ''        Dim but_programme As String = viewstate("but_programme")
        ''        Dim but_niveau_rendement As String = viewstate("but_niveau_rendement")
        ''        Dim but_annee_etude As String = viewstate("but_annee_etude")

        ''        Dim commentaire As String = viewstate("commentaire")

        ''        If but_titre = strval Then

        ''            Response.Write("<table><tr><td>" & commentaire & "</td></tr></table>")

        ''            CType(e.Item.FindControl("lblbut_titre"), Label).Text = ""
        ''            CType(e.Item.FindControl("lblbut_programme"), Label).Text = ""
        ''            CType(e.Item.FindControl("lblbut_niveau_rendement"), Label).Text = ""
        ''            CType(e.Item.FindControl("lblbut_niveau_annee_etude"), Label).Text = ""
        ''            e.Item.BackColor = Color.White
        ''            e.Item.Visible = False
        ''            e.Item.Height = 33
        ''            e.Item.Height = System.Web.UI.WebControls.Unit.Pixel(1)

        ''            CType(e.Item.FindControl("lblHeaderAttente"), Label).Visible = False
        ''            CType(e.Item.FindControl("lblHeaderStrategie"), Label).Visible = False
        ''            CType(e.Item.FindControl("lblHeaderMethode"), Label).Visible = False

        ''            CType(e.Item.FindControl("lblSubHeaderAttente"), Label).Visible = False
        ''            CType(e.Item.FindControl("lblSubHeaderStrategie"), Label).Visible = False
        ''            CType(e.Item.FindControl("lblSubHeaderMethode"), Label).Visible = False

        ''            CType(e.Item.FindControl("lblHeaderCommentaire"), Label).Visible = False

        ''        Else

        ''            but_titre = strval
        ''            ViewState("but_titre") = but_titre
        ''            viewstate("but_programme") = but_programme
        ''            viewstate("but_niveau_rendement") = but_niveau_rendement
        ''            viewstate("but_annee_etude") = but_annee_etude
        ''            CType(e.Item.FindControl("lblbut_titre"), Label).Text = "But: " & but_titre
        ''            CType(e.Item.FindControl("lblbut_titre"), Label).Text = "Programme: " & but_programme
        ''            CType(e.Item.FindControl("lblbut_niveau_rendement"), Label).Text = "Niveau de rendement: " & but_niveau_rendement
        ''            CType(e.Item.FindControl("lblbut_annee_etude"), Label).Text = "Ann�e d'�tude au sein du curriculum: " & but_annee_etude
        ''            e.Item.Visible = True
        ''            e.Item.BackColor = Color.White

        ''            CType(e.Item.FindControl("lblHeaderAttente"), Label).Visible = True
        ''            CType(e.Item.FindControl("lblHeaderStrategie"), Label).Visible = True
        ''            CType(e.Item.FindControl("lblHeaderMethode"), Label).Visible = True

        ''            CType(e.Item.FindControl("lblSubHeaderAttente"), Label).Visible = True
        ''            CType(e.Item.FindControl("lblSubHeaderStrategie"), Label).Visible = True
        ''            CType(e.Item.FindControl("lblSubHeaderMethode"), Label).Visible = True

        ''            CType(e.Item.FindControl("lblHeaderCommentaire"), Label).Visible = True

        ''        End If

        ''        strStrategie = FormatData(strStrategie)
        ''        CType(e.Item.FindControl("lblStrategie"), Label).Text = strStrategie

        ''        If commentaire <> strValCommentaire Then
        ''            commentaire = strValCommentaire
        ''            ViewState("commentaire") = commentaire
        ''            CType(e.Item.FindControl("lblCommentaire"), Label).Text = viewstate("commentaire")
        ''            e.Item.Visible = True
        ''            CType(e.Item.FindControl("LabelSeparator"), Label).Visible = True
        ''        Else
        ''            CType(e.Item.FindControl("lblCommentaire"), Label).Text = ""
        ''            e.Item.Visible = False
        ''            CType(e.Item.FindControl("LabelSeparator"), Label).Visible = False
        ''        End If

        ''    Catch ex As Exception
        ''        lblMsg.Visible = True
        ''        lblMsg.Text = ex.Message
        ''    End Try

        ''End If

    End Sub

    Private Sub DataList2_ItemDataBound1(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DataList2.ItemDataBound

        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then

            Try
                Dim str_but_Titre As String = CType(e.Item.FindControl("lblbut_titre"), Label).Text
                Dim str_But_Programme As String = CType(e.Item.FindControl("lblbut_programme"), Label).Text
                Dim str_But_Niveau_Rendement As String = CType(e.Item.FindControl("lblbut_niveau_rendement"), Label).Text
                Dim str_But_Annee_Etude As String = CType(e.Item.FindControl("lblbut_annee_etude"), Label).Text
                Dim str_But_Commentaire As String = CType(e.Item.FindControl("lblCommentaire"), Label).Text

                Dim strStrategie As String = CType(e.Item.FindControl("lblStrategie"), Label).Text
                Dim strMethode As String = CType(e.Item.FindControl("lblMethode"), Label).Text

                Dim but_titre As String = ViewState("but_titre")
                Dim but_programme As String = viewstate("but_programme")
                Dim but_niveau_rendement As String = viewstate("but_niveau_rendement")
                Dim but_annee_etude As String = viewstate("but_annee_etude")
                Dim but_commentaire As String = viewstate("commentaire")


                If but_titre = str_but_Titre Then

                    'Response.Write("<table><tr><td>" & commentaire & "</td></tr></table>")

                    CType(e.Item.FindControl("lblbut_titre"), Label).Text = ""
                    CType(e.Item.FindControl("lblbut_programme"), Label).Text = ""
                    CType(e.Item.FindControl("lblbut_niveau_rendement"), Label).Text = ""
                    CType(e.Item.FindControl("lblbut_annee_etude"), Label).Text = ""
                    CType(e.Item.FindControl("lblCommentaire"), Label).Text = ""

                        'e.Item.BackColor = Color.White
                    e.Item.Visible = False
                    'e.Item.Height = 33
                    e.Item.Height = System.Web.UI.WebControls.Unit.Pixel(1)

                    CType(e.Item.FindControl("lblHeaderAttente"), Label).Visible = False
                    CType(e.Item.FindControl("lblHeaderStrategie"), Label).Visible = False
                    CType(e.Item.FindControl("lblHeaderMethode"), Label).Visible = False

                    CType(e.Item.FindControl("lblbut_titre"), Label).Visible = False
                    CType(e.Item.FindControl("lblbut_programme"), Label).Visible = False
                    CType(e.Item.FindControl("lblbut_niveau_rendement"), Label).Visible = False
                    CType(e.Item.FindControl("lblbut_annee_etude"), Label).Visible = False
                    CType(e.Item.FindControl("lblCommentaire"), Label).Visible = False
                    'CType(e.Item.FindControl("lblHeaderCommentaire"), Label).Visible = False

                Else

                    but_titre = str_but_Titre
                    intButNo = intButNo + 1

                    but_programme = str_But_Programme
                    but_niveau_rendement = str_But_Niveau_Rendement
                    but_annee_etude = str_But_Annee_Etude
                    but_commentaire = str_But_Commentaire

                    ViewState("but_titre") = but_titre
                    viewstate("but_programme") = but_programme
                    viewstate("but_niveau_rendement") = but_niveau_rendement
                    viewstate("but_annee_etude") = but_annee_etude
                    viewstate("commentaire") = but_commentaire

                    CType(e.Item.FindControl("lblbut_titre"), Label).Text = "But " & intButNo & ": " & but_titre
                    e.Item.Visible = True
                    CType(e.Item.FindControl("lblbut_programme"), Label).Text = "Programme: " & but_programme
                    e.Item.Visible = True
                    CType(e.Item.FindControl("lblbut_niveau_rendement"), Label).Text = "Niveau de rendement: " & but_niveau_rendement
                    e.Item.Visible = True
                    CType(e.Item.FindControl("lblbut_annee_etude"), Label).Text = "Ann�e d'�tude au sein du curriculum: " & but_annee_etude
                    e.Item.Visible = True
                    CType(e.Item.FindControl("lblCommentaire"), Label).Text = "COMMENTAIRES: " & but_commentaire
                    e.Item.Visible = True

                        'e.Item.BackColor = Color.White

                    CType(e.Item.FindControl("lblHeaderAttente"), Label).Visible = True
                    CType(e.Item.FindControl("lblHeaderStrategie"), Label).Visible = True
                    CType(e.Item.FindControl("lblHeaderMethode"), Label).Visible = True

                    CType(e.Item.FindControl("lblbut_titre"), Label).Visible = True
                    CType(e.Item.FindControl("lblbut_programme"), Label).Visible = True
                    CType(e.Item.FindControl("lblbut_niveau_rendement"), Label).Visible = True
                    CType(e.Item.FindControl("lblbut_annee_etude"), Label).Visible = True
                    CType(e.Item.FindControl("lblCommentaire"), Label).Visible = True

                End If

                'strStrategie = FormatData(strStrategie)
                'CType(e.Item.FindControl("lblStrategie"), Label).Text = strStrategie

                'If but_commentaire <> str_But_Commentaire Then
                '    but_commentaire = str_But_Commentaire
                '    ViewState("commentaire") = but_commentaire
                '    CType(e.Item.FindControl("lblCommentaire"), Label).Text = viewstate("commentaire")
                '    e.Item.Visible = True
                '    'CType(e.Item.FindControl("LabelSeparator"), Label).Visible = True
                'Else
                '    CType(e.Item.FindControl("lblCommentaire"), Label).Text = ""
                '    e.Item.Visible = False
                '    CType(e.Item.FindControl("lblbut_programme"), Label).Visible = False
                '    CType(e.Item.FindControl("lblbut_niveau_rendement"), Label).Visible = False
                '    CType(e.Item.FindControl("lblbut_annee_etude"), Label).Visible = False

                '    'CType(e.Item.FindControl("LabelSeparator"), Label).Visible = False
                'End If

            Catch ex As Exception
                lblMsg.Visible = True
                lblMsg.Text = ex.Message
            End Try

        End If


    End Sub
    Sub BindAdaptationsEvalProv(ByVal strEleveId As String)
        Dim oEvalProv As New PEIDAL.evalprov
        Dim dsAdaptEvalProv1 As New PEIDAL.dsAdaptEvalProv

        dsAdaptEvalProv1 = oEvalProv.GetdsAdaptEvalProv(ConfigurationSettings.AppSettings("ConnectionString"), strEleveId)
        Dim r As PEIDAL.dsAdaptEvalProv.ADAPT_EVAL_PROVRow

        Try

            If dsAdaptEvalProv1.ADAPT_EVAL_PROV.Rows.Count > 0 Then
                r = dsAdaptEvalProv1.ADAPT_EVAL_PROV.FindByFK_ELEVE_ID(strEleveId)

                If r.ADAPTATION <> "" Then
                    tdAdaptations.InnerHtml = r.ADAPTATION
                Else
                    tdAdaptations.InnerHtml = "aucun"
                End If

            Else
                tdAdaptations.InnerHtml = "aucun"
            End If

        Catch ex As Exception
            lblMsg.Visible = True
            lblMsg.Text = "BindAdaptationsEvalProv: " & ex.Message
            'Throw New Exception(ex.Message)
        End Try

    End Sub
    Sub BindExemptionsEvalProv(ByVal EleveId As String)
        Dim oEvalProv As New PEIDAL.evalprov
        Dim dsExempEvalProv1 As New PEIDAL.dsExempEvalProv

        dsExempEvalProv1 = oEvalProv.GetdsExempEvalProv(ConfigurationSettings.AppSettings("ConnectionString"), EleveId)
        Dim r As PEIDAL.dsExempEvalProv.EXEMPTIONS_EVAL_PROVRow

        Try

            If dsExempEvalProv1.EXEMPTIONS_EVAL_PROV.Rows.Count > 0 Then
                r = dsExempEvalProv1.EXEMPTIONS_EVAL_PROV.FindByFK_ELEVE_ID(EleveId)
                tdExemptions.InnerHtml = r.MOTIF_EDUCATIONNEL
            Else
                tdExemptions.InnerHtml = "aucun"
            End If

        Catch ex As Exception
            lblMsg.Visible = True
            lblMsg.Text = ex.Message
            'Throw New Exception("Erreur lors de la lecture des exemptions. --> " & ex.Message)
        End Try
    End Sub

    Private Sub CheckboxAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckboxAll.CheckedChanged
        If CheckboxAll.Checked = True Then
            CheckBoxPage1.Checked = True
            CheckBoxForcesBesoins.Checked = True
            CheckBoxRendement.Checked = True
            CheckBoxPlanTransistion.Checked = True
            CheckBoxTestsProvinciaux.Checked = True

            SetCheckboxlistButs(True)

        Else
            CheckBoxPage1.Checked = False
            CheckBoxForcesBesoins.Checked = False
            CheckBoxRendement.Checked = False
            CheckBoxPlanTransistion.Checked = False
            CheckBoxTestsProvinciaux.Checked = False

            SetCheckboxlistButs(False)
        End If

    End Sub
    Sub BindTestProvinciaux()
        Dim oEvalProv As New PEIDAL.evalprov
        Dim dsTestProv1 As PEIDAL.DsTestsProvinciaux
        Dim strTest As String

        dsTestProv1 = oEvalProv.GetdsTestsProvinciaux(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

        If dsTestProv1.TESTS_PROVINCIAUX.Rows.Count > 0 Then
            Dim r As PEIDAL.DsTestsProvinciaux.TESTS_PROVINCIAUXRow
            r = dsTestProv1.TESTS_PROVINCIAUX.FindByFK_ELEVE_ID(Session("eleveid"))

            TDevalProv.InnerText = r.EVALUATIONS
            tdAdaptationsTests.InnerText = r.ADAPTATIONS
            tdExemptions.InnerText = r.EXEMPTIONS

        End If

    End Sub
    Sub SetCheckboxlistButs(ByVal oVal As Boolean)
        Dim ch As CheckBox
        Dim chbl As CheckBoxList
        Dim i As Integer
        Dim li As ListItem

        Dim butId As Integer
        Dim strSelectedButs As String

        For Each li In CheckboxlistButs.Items
            li.Selected = oVal
        Next
    End Sub
End Class

End Namespace

