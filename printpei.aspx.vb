Imports System
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient


Namespace PeiWebAccess

Partial Class printpei
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            Me.lblErrMsg.Text = ""

            If Not Session("eleveid") <> "" Then
                Response.Redirect("listeeleves.aspx")
            End If

            'If Not Session("username") <> "" Then
            '    Response.Redirect("login.aspx")
            'End If

            'Me.user.InnerText = "Utilisateur en cours : " & Session("username")
            Me.user2.InnerText = "*" & Page.User.Identity.Name & " | " & Date.Now
            'Response.Write("useridentityname = " & Page.User.Identity.Name)

            If Not Page.IsPostBack Then

                Dim ds As New DataSet
                Dim oEle As New PEIDAL.eleve

                'ds = oEle.GetEleveByID(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

                _BindDropDownListEcoleByUserID(Page.User.Identity.Name)

                'Me.DropDownListEcoles.SelectedValue = Trim(ds.Tables(0).Rows(0).Item("fk_ecole_id").ToString)

                BindListeEleves(Me.DropDownListEcoles.SelectedValue)
                'Me.DropDownListEleves.SelectedValue = ds.Tables(0).Rows(0).Item("pk_eleve_id").ToString

                SetCheckbox(Me.DropDownListEleves.SelectedValue)

                If (Session("etape")) <> "" Then
                    Me.DropDownListEtape.SelectedValue = Session("etape")
                End If

                SetSessionVariables()
            End If


        End Sub
        Sub SetDropDropListEcoleBySessionEleve()

            Dim obj As New PEIDAL.eleve
            Dim ds As New DataSet
            ds = obj.GetEleveByID(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            Dim ecole = ds.Tables(0).Rows(0).Item("fk_ecole_id").ToString

            For Each i As ListItem In DropDownListEcoles.Items
                If Not i.Value <> ecole Then
                    DropDownListEcoles.SelectedValue = i.Value
                End If
            Next
        End Sub
        Sub _BindDropDownListEcoleByUserID(ByVal username As String, Optional ByVal _ValueField As String = "pk_ecole_id")

            Dim _sql As String = String.Empty
            Dim _ds As New DataSet
            Dim oGen As New PEIDAL.general

            If _IsUserAdmin(username) Then
                'BindDropDownListEcoleByUserIdAdmin(username, ctlDropDownList, _ValueField)
                _sql = "SELECT E.pk_ecole_id,'* ' + E.nom_ecole + ' ' + e.ville as ecole " & _
                    " FROM ECOLES E, UTILISATEURS_ECOLES AFF" & _
                    " WHERE(E.pk_ecole_id = AFF.FK_ECOLE_ID) " & _
                    " AND AFF.FK_UTILISATEUR_ID = '" & username & "' order by e.nom_ecole"
            Else
                '_BindDropDownListEcoleByUserID(username, ctlDropDownList, _ValueField)
                _sql = "SELECT E.pk_ecole_id,'* ' + E.nom_ecole + ' ' + e.ville as ecole " & _
                    " FROM ECOLES E, UTILISATEURS_ECOLES_EED AFF" & _
                    " WHERE(E.pk_ecole_id = AFF.FK_ECOLE_ID) " & _
                    " AND AFF.FK_UTILISATEUR_ID = '" & username & "' order by e.nom_ecole"

                _sql = "select distinct e.pk_ecole_id as pk_ecole_id,e.nom_ecole as ecole " & _
                    " from ECOLES e, VW_PEI_AFFECATION aff" & _
                    " where aff.SCHOOL_CODE COLLATE DATABASE_DEFAULT = e.school_code COLLATE DATABASE_DEFAULT and aff.ldap_user = '" & username & "'"
            End If

            insertLog(ConfigurationSettings.AppSettings("ConnectionString"), "DEBUG", "PRINT", username, Me.DropDownListEleves.SelectedValue, "", Date.Now, "", "", "dropdownlist ecole SQL: " & _sql)

            _ds = oGen.GetDataset(_sql, ConfigurationSettings.AppSettings("ConnectionString"))
            DropDownListEcoles.DataSource = _ds
            DropDownListEcoles.DataValueField = "pk_ecole_id"
            DropDownListEcoles.DataTextField = "ecole"
            DropDownListEcoles.DataBind()

            SetDropDropListEcoleBySessionEleve()

        End Sub
        Function _IsUserAdmin(ByVal username As String) As Boolean
            Dim sql As String
            Dim ds As New DataSet
            Dim oGen As New PEIDAL.general

            sql = "select fk_utilisateur_id from utilisateurs_ecoles where fk_utilisateur_id = '" & username & "'"
            ds = oGen.GetDataset(sql, ConfigurationSettings.AppSettings("ConnectionString"))

            If ds.Tables(0).Rows.Count > 0 Then
                insertLog(ConfigurationSettings.AppSettings("ConnectionString"), "DEBUG", "PRINT", Session("user") & "/" & Page.User.Identity.Name, Me.DropDownListEleves.SelectedValue, "", Date.Now, "", "", "user " & Page.User.Identity.Name & " is ADMIN")
                Return True
            Else
                insertLog(ConfigurationSettings.AppSettings("ConnectionString"), "DEBUG", "PRINT", Session("user") & "/" & Page.User.Identity.Name, Me.DropDownListEleves.SelectedValue, "", Date.Now, "", "", "user " & Page.User.Identity.Name & " is not admin")
                Return False
            End If
        End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click


            If (DropDownListEleves.SelectedValue <> "") Then
                PrintPei()
            Else
                Me.lblErrMsg.Text = "Veuillez s�lectionnez l'�l�ve dans la liste d�roulante"
                Me.lblErrMsg.Visible = "true"
            End If
            'Dim strEleveId As String = Session("eleveid")
            
        End Sub

        Sub PrintPei()

            Dim _http As String
            Dim strEleveId As String = Me.DropDownListEleves.SelectedValue
            Dim strPersonID As String
            Dim Etape As String
            Dim DDN As String
            Dim strNom As String
            Dim oEleve As New PEIDAL.eleve
            Dim ds As New DataSet
            Dim Url As String

            Dim year As String = System.Configuration.ConfigurationManager.AppSettings("annee_scolaire")

            Try
                ds = oEleve.GetEleveByID(ConfigurationSettings.AppSettings("ConnectionString"), strEleveId)
                Session("infoseleve") = ds.Tables(0).Rows(0).Item("prenom_eleve") & " " & ds.Tables(0).Rows(0).Item("nom_eleve") & ",         " & ds.Tables(0).Rows(0).Item("date_naissance")
                strNom = ds.Tables(0).Rows(0).Item("prenom_eleve") & " " & ds.Tables(0).Rows(0).Item("nom_eleve")
                DDN = ds.Tables(0).Rows(0).Item("date_naissance")
                strPersonID = ds.Tables(0).Rows(0).Item("fk_person_id")

                Etape = DropDownListEtape.SelectedValue


            Catch ex As Exception
                lblErrMsg.Visible = True
                lblErrMsg.Text = ex.Message

            End Try
            'window.open('report2.aspx','mywindow','menubar=yes,scrollbars=yes')

            '<script language=javascript>window.open('report2.aspx?fk_eleve_id=" & strEleveId & "&DDN=" & DDN & "&nom=" & strNom & "&rh=" & ReverseBooleanValue(rh.Checked) & "&forces=" & ReverseBooleanValue(forces.Checked) & "&attentes=" & ReverseBooleanValue(buts.Checked) & "&matieres=" & ReverseBooleanValue(matieres.Checked) & "&adaptations=" & ReverseBooleanValue(adaptations.Checked) & "&evaluations=" & ReverseBooleanValue(evaluations.Checked) & "&signature=" & ReverseBooleanValue(signature.Checked) & "&generales=" & ReverseBooleanValue(generales.Checked)','new_Win');</script>
            Url = "<script language=javascript>window.open('"
            Url = Url & "report2.aspx?fk_eleve_id=" & strEleveId & "&person_id=" & strPersonID & "&DDN=" & DDN & "&nom=" & strNom & "&rh=" & ReverseBooleanValue(rh.Checked) & "&forces=" & ReverseBooleanValue(forces.Checked) & "&attentes=" & ReverseBooleanValue(buts.Checked) & "&matieres=" & ReverseBooleanValue(matieres.Checked) & "&adaptations=" & ReverseBooleanValue(adaptations.Checked) & "&evaluations=" & ReverseBooleanValue(evaluations.Checked) & "&signature=" & ReverseBooleanValue(signature.Checked) & "&generales=" & ReverseBooleanValue(generales.Checked) & "&buts_transition=" & ReverseBooleanValue(buts_transition.Checked) & "&evaluation_prov=" & ReverseBooleanValue(evaluation_prov.Checked)
            Url = Url & "','new_Win');</script>"

            'Response.Write(Url)

            'liens original
            'Response.Redirect("report2.aspx?fk_eleve_id=" & strEleveId & "&person_id=" & strPersonID & "&Etape=" & Etape & "&DDN=" & DDN & "&nom=" & strNom & "&rh=" & ReverseBooleanValue(rh.Checked) & "&forces=" & ReverseBooleanValue(forces.Checked) & "&attentes=" & ReverseBooleanValue(buts.Checked) & "&matieres=" & ReverseBooleanValue(matieres.Checked) & "&adaptations=" & ReverseBooleanValue(adaptations.Checked) & "&evaluations=" & ReverseBooleanValue(evaluations.Checked) & "&signature=" & ReverseBooleanValue(signature.Checked) & "&generales=" & ReverseBooleanValue(generales.Checked) & "&buts_transition=" & ReverseBooleanValue(buts_transition.Checked) & "&evaluation_prov=" & ReverseBooleanValue(evaluation_prov.Checked))

            'request brower info
            Dim s As String = ""
            Dim _ip As String = ""

            With Request.Browser
                s &= "Browser Capabilities" & vbCrLf
                s &= "Type = " & .Type & vbCrLf
                s &= "Name = " & .Browser & vbCrLf
                s &= "Version = " & .Version & vbCrLf
                s &= "Major Version = " & .MajorVersion & vbCrLf
                s &= "Minor Version = " & .MinorVersion & vbCrLf
                s &= "Platform = " & .Platform & vbCrLf
                s &= "Is Beta = " & .Beta & vbCrLf
                s &= "Is Crawler = " & .Crawler & vbCrLf
                s &= "Is AOL = " & .AOL & vbCrLf
                s &= "Is Win16 = " & .Win16 & vbCrLf
                s &= "Is Win32 = " & .Win32 & vbCrLf
                s &= "Supports Frames = " & .Frames & vbCrLf
                s &= "Supports Tables = " & .Tables & vbCrLf
                s &= "Supports Cookies = " & .Cookies & vbCrLf
                s &= "Supports VBScript = " & .VBScript & vbCrLf
                s &= "Supports JavaScript = " & _
                    .EcmaScriptVersion.ToString() & vbCrLf
                s &= "Supports Java Applets = " & .JavaApplets & vbCrLf
                s &= "Supports ActiveX Controls = " & .ActiveXControls & _
                    vbCrLf
                s &= "Supports JavaScript Version = " & _
                    Request.Browser("JavaScriptVersion") & vbCrLf

            End With

            '_ip = Request.ServerVariables("REMOTE_ADDR")
            _ip = Request.UserHostAddress

            Dim _date As Date
            _date = System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")


            ''liens modifi�, ajouter parmat�tre datenow afin de g�n�rer un liens diff�rents a chaque fois et evitez la mise en cache...
            '    Response.Redirect("report3.aspx?fk_eleve_id=" & strEleveId & _
            '    "&person_id=" & strPersonID & _
            '    "&Etape=" & Etape & _
            '    "&DDN=" & DDN & _
            '    "&nom=" & strNom & _
            '    "&rh=" & ReverseBooleanValue(rh.Checked) & _
            '    "&forces=" & ReverseBooleanValue(forces.Checked) & _
            '    "&attentes=" & ReverseBooleanValue(buts.Checked) & _
            '    "&matieres=" & ReverseBooleanValue(matieres.Checked) & _
            '    "&competences=" & ReverseBooleanValue(competences.Checked) & _
            '    "&adaptations=" & ReverseBooleanValue(adaptations.Checked) & _
            '    "&evaluations=" & ReverseBooleanValue(evaluations.Checked) & _
            '    "&signature=" & ReverseBooleanValue(signature.Checked) & _
            '    "&generales=" & ReverseBooleanValue(generales.Checked) & _
            '    "&buts_transition=" & ReverseBooleanValue(buts_transition.Checked) & _
            '    "&evaluation_prov=" & ReverseBooleanValue(evaluation_prov.Checked) & _
            '    "&year=" & year & _
            '    "&currenttime=" & _date)

            ''response.Write("<script language=javascript>window.open('report2.aspx?fk_eleve_id=" & strEleveId & "&DDN=" & DDN & "&nom=" & strNom & "&rh=" & ReverseBooleanValue(rh.Checked) & "&forces=" & ReverseBooleanValue(forces.Checked) & "&attentes=" & ReverseBooleanValue(buts.Checked) & "&matieres=" & ReverseBooleanValue(matieres.Checked) & "&adaptations=" & ReverseBooleanValue(adaptations.Checked) & "&evaluations=" & ReverseBooleanValue(evaluations.Checked) & "&signature=" & ReverseBooleanValue(signature.Checked) & "&generales=" & ReverseBooleanValue(generales.Checked)','new_Win');</script>")

            '    'Response.Redirect("report2.aspx?fk_eleve_id=" & strEleveId & "&DDN=" & DDN & "&nom=" & strNom & "&rh=" & ReverseBooleanValue(rh.Checked) & "&forces=" & ReverseBooleanValue(forces.Checked) & "&attentes=" & ReverseBooleanValue(buts.Checked) & "&matieres=" & ReverseBooleanValue(matieres.Checked) & "&adaptations=" & ReverseBooleanValue(adaptations.Checked) & "&evaluations=" & ReverseBooleanValue(evaluations.Checked) & "&signature=" & ReverseBooleanValue(signature.Checked) & "&generales=" & ReverseBooleanValue(generales.Checked))


            _http = "http://ss-sql-01/ReportServer/?/CSDCCS/EED/PEI_CSDCCS_09C&rs:Command=Render&rc:Parameters=false&rs:Command=Render" & _
            "&matieres=" & ReverseBooleanValue(matieres.Checked) & _
            "&adaptations=" & ReverseBooleanValue(adaptations.Checked) & _
            "&evaluation_prov=" & ReverseBooleanValue(evaluation_prov.Checked) & _
            "&forces=" & ReverseBooleanValue(forces.Checked) & _
            "&signature=" & ReverseBooleanValue(signature.Checked) & _
            "&evaluations=" & ReverseBooleanValue(evaluations.Checked) & _
            "&rc:toolbard=false" & _
            "&attentes=" & ReverseBooleanValue(buts.Checked) & _
            "&nom=" & strNom & _
            "&generales=" & ReverseBooleanValue(generales.Checked) & _
            "&rs:Format=PDF" & _
            "&year=" & year & _
            "&person_id=" & strPersonID & _
            "&competences=" & ReverseBooleanValue(competences.Checked) & _
            "&fk_eleve_id=" & strEleveId & _
            "&rh=" & ReverseBooleanValue(rh.Checked) & _
            "&DDN=" & DDN & _
            "&buts_transition=" & ReverseBooleanValue(buts_transition.Checked) & _
            "&curtime=" & _date & _
            "&Etape=" & Etape


            insertLog(ConfigurationSettings.AppSettings("ConnectionString"), "PRINT_PEI", "PRINT", Session("user"), Me.DropDownListEleves.SelectedValue, strPersonID, Date.Now, s, _ip, "report server link: " & _http)


            '            http://ss-sql-01/ReportServer/?/CSDCCS/EED/PEI_CSDCCS_09&rs:Command=Render&rc:Parameters=false&rs:Command=Render
            '&matieres=True&Etape=1&adaptations=True&evaluation_prov=True&forces=True&signature=True&evaluations=True&rc:toolbar=false&attentes=True&nom=Stephane
            '            Balogh()
            '&generales=True&rs:Format=PDF&rh=True&year=2015 - 2016&person_id=0001126156&competences=True&fk_eleve_id=0001126156&DDN=12/4/2003&buts_transition=True&curtime=2/3/2016 10:25:51 AM 
            'width="688px" height="408px" style="border: 1 solid #C0C0C0" border="0" frameborder="0"

            '            _http = _http & " width=688px height=408px style=border: 1 solid ""#C0C0C0"" border=""0"" frameborder=0"


            Response.Redirect(_http, False)

            'Context.Response.Write("<script> language='javascript'>window.open('" & _http & "','_newtab');</script>")

            ''Response.Write(_http)
            'HyperLink1.NavigateUrl = _http


            'HyperLink1.NavigateUrl = _http
            'HyperLink1.Width = "688"
            'HyperLink1.Height = "408"
            Dim i As Integer
            i = 1


            'HyperLink1.Style(BorderStyle.Solid
        End Sub
    Function ReverseBooleanValue(ByVal bValue As Boolean) As Boolean
        If bValue Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub CheckboxAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckboxAll.CheckedChanged
        Dim Ctl As Control

        'If CheckboxAll.Checked Then
        '    For Each chBx In Page.Controls
        '        chBx.Checked = True
        '    Next
        'Else
        '    For Each Ctl In Page.Controls
        '        If Ctl.Then Then
        '            chBx.Checked = False
        '    Next
        'End If

        If CheckboxAll.Checked Then
            generales.Checked = True

            If forces.Enabled Then
                forces.Checked = True
            End If

            If matieres.Enabled Then
                matieres.Checked = True
            End If

            If competences.Enabled Then
                competences.Checked = True
            End If

            If adaptations.Enabled Then
                adaptations.Checked = True
            End If

            If evaluations.Enabled Then
                evaluations.Checked = True
            End If

            If rh.Enabled Then
                rh.Checked = True
            End If

            If buts_transition.Enabled Then
                buts_transition.Checked = True
            End If

            If evaluation_prov.Enabled Then
                evaluation_prov.Checked = True
            End If

            If buts.Enabled Then
                buts.Checked = True
            End If

            If signature.Enabled Then
                signature.Checked = True
            End If

        Else

            generales.Checked = False
            forces.Checked = False
            matieres.Checked = False
            competences.Checked = False
            adaptations.Checked = False
            evaluations.Checked = False
            rh.Checked = False
            buts_transition.Checked = False
            evaluation_prov.Checked = False
            buts.Checked = False
            signature.Checked = False

        End If

        End Sub
        Sub SetAllCheckBoxToFalse()
            generales.Checked = False
            forces.Checked = False
            matieres.Checked = False
            competences.Checked = False
            adaptations.Checked = False
            evaluations.Checked = False
            rh.Checked = False
            buts_transition.Checked = False
            evaluation_prov.Checked = False
            buts.Checked = False
            signature.Checked = False
        End Sub
        Sub SetTestTestProvinciaux(id As String)
            Dim ogen As New PEIDAL.general
            Dim _ds As New DataSet
            Dim _sql As String

            ' _sql = 
        End Sub
        Sub SetForceBesoinsNEW(ByVal id As String)
            Dim oGen As New PEIDAL.general
            Dim _ds As New Data.DataSet
            Dim _sql As String

            _sql = "SELECT * from forcesbesoins where fk_eleve_id = '" & id & "'"
            _ds = oGen.GetDataset(_sql, ConfigurationSettings.AppSettings("ConnectionString"))

            forces.Enabled = False

            For Each r As DataRow In _ds.Tables(0).Rows

                If r.Item("HAB_COGNITIVE").ToString <> "" Then
                    forces.Enabled = True
                End If
            Next

        End Sub

        Sub SetForceCheckBox(ByVal id As String)
            Dim oFB As New PEIDAL.forces_besoins
            Dim sqlReader As SqlDataReader
            Dim oFlag As Boolean

            Try
                sqlReader = oFB.GetForcesByEleveId(ConfigurationSettings.AppSettings("ConnectionString"), id)

                If sqlReader.HasRows Then
                    Do While sqlReader.Read
                        If sqlReader.GetString(0).ToString <> "" Then
                            forces.Enabled = True
                        Else
                            forces.Enabled = False
                        End If
                    Loop
                Else
                    forces.Enabled = False
                End If

                sqlReader.Close()

                sqlReader = oFB.GetBesoinsByEleveId(ConfigurationSettings.AppSettings("ConnectionString"), id)

                If sqlReader.HasRows Then
                    Do While sqlReader.Read
                        If sqlReader.GetString(0).ToString <> "" Then
                            forces.Enabled = True
                        Else
                            forces.Enabled = False
                        End If
                    Loop
                Else
                    forces.Enabled = False
                End If

                sqlReader.Close()

            Catch ex As Exception
                lblErrMsg.Visible = True
                lblErrMsg.Text = "Une erreur est survenue (case a coch� points forts et besoins) " & ex.Message
            End Try

        End Sub
        Sub setMatieresCheckbox(ByVal id As String)
            Dim oProg As New PEIDAL.programme
            Dim sqlReader As SqlDataReader

            Try
                sqlReader = oProg.GetReaderMatieres_ElevesOrderByDescRowID(id, ConfigurationSettings.AppSettings("ConnectionString"))

                If sqlReader.HasRows Then

                    matieres.Enabled = True

                    'Do While sqlReader.Read
                    '    If sqlReader.GetString(0).ToString <> "" Then
                    '        matieres.Enabled = False
                    '    Else
                    '        matieres.Enabled = True
                    '    End If
                    'Loop
                Else
                    matieres.Enabled = False
                End If

            Catch ex As Exception
                lblErrMsg.Text = "Erreur survenu sur l'objet 'Mati�res'. " & ex.Message

            End Try

        End Sub
        Sub SetAdaptationsCheckBox(ByVal id As String)
            Dim oSante As New PEIDAL.sante
            Dim dsSanteEleve1 As PEIDAL.DsSanteEleves
            Dim r As PEIDAL.DsSanteEleves.SANTE_ELEVESRow

            Try
                dsSanteEleve1 = oSante.GetdsSanteEleves(ConfigurationSettings.AppSettings("ConnectionString"), id)

                If dsSanteEleve1.SANTE_ELEVES.Rows.Count < 1 Then
                    adaptations.Enabled = False
                Else
                    r = dsSanteEleve1.SANTE_ELEVES.FindByFK_ELEVE_ID(id)
                    If r.ADAPT_ENVIRONNEMENTALES <> "" Or r.ADAPT_MATIEREEVALUATION <> "" Or r.ADAPT_PEDAGOGIQUES <> "" Then
                        adaptations.Enabled = True
                    Else
                        adaptations.Enabled = False
                    End If
                End If

            Catch ex As Exception
                lblErrMsg.Text = "Erreur survenu sur l'objet 'Adaptations'. " & ex.Message
            End Try

        End Sub
        Sub SetDonneesEvaluation(ByVal id As String)
            Dim oFB As New PEIDAL.forces_besoins
            Dim dsDonneesEval1 As PEIDAL.dsRapportsEvaluationList
            'Dim r As PEIDAL.dsRapportsEvaluationList.RAPPORTS_EVALUATIONRow

            Try
                dsDonneesEval1 = oFB.GetdsRapportsEvaluationList(ConfigurationSettings.AppSettings("ConnectionString"), id)

                If dsDonneesEval1.RAPPORTS_EVALUATION.Rows.Count < 1 Then
                    evaluations.Enabled = False
                Else
                    evaluations.Enabled = True
                End If
            Catch ex As Exception
                lblErrMsg.Text = "Erreur survenu sur l'objet '�valuations'. " & ex.Message
            End Try

        End Sub
        Sub SetRHcheckBox(ByVal id As String)
            Dim oSante As New PEIDAL.sante
            Dim dsRHEleves1 As PEIDAL.DsRhEleves
            'Dim r As PEIDAL.DsRhEleves.RH_ELEVESRow

            Try
                dsRHEleves1 = oSante.GetdsRhEleves(ConfigurationSettings.AppSettings("ConnectionString"), id)

                If dsRHEleves1.RH_ELEVES.Count < 1 Then
                    rh.Enabled = False
                Else
                    rh.Enabled = True
                End If

            Catch ex As Exception

                lblErrMsg.Text = "Erreur survenu sur l'objet 'Ressources humaines'. " & ex.Message
            End Try

        End Sub
        Sub setButs_Transition(ByVal id As String)
            Dim oPlan As New PEIDAL.plan
            Dim DsButsTransition As PEIDAL.dsButsTransition

            Try
                DsButsTransition = oPlan.GetdsButsTransition(ConfigurationSettings.AppSettings("ConnectionString"), id)

                If DsButsTransition.BUTS_TRANSITION.Count < 1 Then
                    buts_transition.Enabled = False
                Else
                    buts_transition.Enabled = True
                End If


            Catch ex As Exception
                lblErrMsg.Text = "Erreur survenu sur l'objet 'Buts_Transition'. " & ex.Message

            End Try

        End Sub

    Private Sub DropDownListEtape_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DropDownListEtape.SelectedIndexChanged

        Session("etape") = Me.DropDownListEtape.SelectedValue

        End Sub
        Sub BindListeEleves(ByVal EcoleID As String)

            Dim oEleve As New PEIDAL.eleve
            Dim dr As SqlDataReader
            Dim ds As New DataSet
            Dim dt As New DataTable("ELEVES")
            Dim r As DataRow

            dt.Columns.Add("pk_eleve_id")
            dt.Columns.Add("fullname")

            Try
                dr = oEleve.GetListElevesByEcoleId(ConfigurationSettings.AppSettings("ConnectionString"), EcoleID)

                While dr.Read
                    r = dt.NewRow
                    r("pk_eleve_id") = dr.Item("pk_eleve_id")
                    r("fullname") = dr.Item("nom_eleve") & ", " & dr.Item("prenom_eleve") & " (" & dr.Item("description_niveau") & ") "
                    dt.Rows.Add(r)
                End While

                dt.AcceptChanges()

                Me.DropDownListEleves.DataSource = dt
                Me.DropDownListEleves.DataTextField = "fullname"
                Me.DropDownListEleves.DataValueField = "pk_eleve_id"
                Me.DropDownListEleves.DataBind()
                Me.DropDownListEleves.Items.Insert(0, New ListItem("Selectionnez l'�l�ve", String.Empty))

                'Session("infoseleve") = Me.DropDownListEleves.SelectedValue
                'SetCheckbox()

                For Each i As ListItem In DropDownListEleves.Items
                    If Not i.Value <> Session("eleveid") Then
                        DropDownListEleves.SelectedValue = Session("eleveid")
                        SetCheckbox(Me.DropDownListEleves.SelectedValue)
                    End If
                Next



                'DisplayEleveToPrint()

            Catch ex As Exception
                Me.lblErrMsg.Text = "Erreure est survenue: " & ex.Message
            End Try

        End Sub

        'Protected Sub DropDownListEeleves_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownListEcoles.SelectedIndexChanged
        '    BindListeEleves(Me.DropDownListEcoles.SelectedValue)
        '    Session("infoseleve") = Me.DropDownListEleves.SelectedValue
        '    SetCheckbox()

        'End Sub
        'Protected Sub DropDownListEcoles_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownListEcoles.SelectedIndexChanged
        '    BindListeEleves(Me.DropDownListEcoles.SelectedValue)
        '    Session("infoseleve") = Me.DropDownListEleves.SelectedValue
        '    SetCheckbox()

        'End Sub

        Sub SetCheckbox(ByVal EleveID As String)

            'SetForceCheckBox(EleveID)
            SetForceBesoinsNEW(EleveID)
            setMatieresCheckbox(EleveID)
            SetAdaptationsCheckBox(EleveID)
            SetDonneesEvaluation(EleveID)
            SetRHcheckBox(EleveID)
            setButs_Transition(EleveID)

        End Sub

        Protected Sub DropDownListEleves_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownListEleves.SelectedIndexChanged
            'Me.EleveToPrint.InnerHtml = Me.DropDownListEleves.SelectedItem.ToString
            SetCheckbox(Me.DropDownListEleves.SelectedValue)
            'DisplayEleveToPrint()

            SetSessionVariables()
            

        End Sub

        Protected Sub DropDownListEcoles_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownListEcoles.SelectedIndexChanged
            BindListeEleves(Me.DropDownListEcoles.SelectedValue)
            'DisplayEleveToPrint()

            SetSessionVariables()
        End Sub

        Sub DisplayEleveToPrint()
            Me.EleveToPrint.InnerHtml = "�l�ve: " & Me.DropDownListEleves.SelectedItem.ToString
        End Sub
        Sub SetSessionVariables()
            Session("eleveid") = Me.DropDownListEleves.SelectedValue
            Session("infoseleve") = Me.DropDownListEleves.SelectedItem.ToString
            Response.Write("personid: " & Session("eleveid"))
            Me.Menu_top1.fullname = Me.DropDownListEleves.SelectedItem.ToString
        End Sub

        Protected Sub Page_LoadComplete(sender As Object, e As System.EventArgs) Handles Me.LoadComplete

        End Sub

        Protected Sub Page_SaveStateComplete(sender As Object, e As System.EventArgs) Handles Me.SaveStateComplete

        End Sub

        
        Protected Sub Page_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload

        End Sub
    End Class

End Namespace
