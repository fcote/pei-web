﻿Imports System.Data
Imports System.Data.SqlClient
Imports CECCE.StudioPedagogique.Data

Namespace PeiWebAccess
    Partial Class desactive_eleve
        Inherits System.Web.UI.Page

        Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
            If Not Page.IsPostBack Then
                Dim oEleve As New PEIDAL.eleve
                'Me.Label1.Text = "Je désire retirer l'éléve " & Session("infoseleve") & " de la liste"
                'BindDropDownListEcoleByUserID(Page.User.Identity.Name, Me.DropDownList1)
                'SetDropDropListEcoleBySessionEleve()
                Me.DropDownList1.DataSource = oEleve.GetListElevesByUtilisateurId(ConfigurationSettings.AppSettings("ConnectionString"), Session("user"))
            End If
        End Sub

        Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click
            If Me.CheckBox1.Checked Then
                DesactiverEleve(Session("eleve_id"))
            Else
                Me.LabelMsg.Text = "Veuillez cocher la case pour confirmer le changement"
            End If
        End Sub
        Sub DesactiverEleve(eleveid As String)
            'Dim oGen As New PEIDAL.general
            'Dim _sql As String = "update eleves set fk_ecole_id = '9999' where pk_eleve_id = '" & eleveid & "')"
            Dim oEle As New PEIDAL.eleve

            Try
                'oGen.ExecuteCommand(ConfigurationSettings.AppSettings("ConnectionString"), _sql)
                oEle.UpdateEleveByOneField(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"), "FK_ECOLE_ID", "9999")
                Me.LabelMsg.Text = "L'élève a été retiré de la liste"
            Catch ex As Exception
                Me.LabelMsg.Text = "Une erreur est survenue :" & ex.Message
            End Try

        End Sub
    End Class
End Namespace
