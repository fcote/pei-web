Imports System.Data
Imports System.Data.SqlClient
Imports CECCE.StudioPedagogique.Data

Namespace PeiWebAccess

    Partial Class ListeEleves
        Inherits System.Web.UI.Page

#Region " Code g�n�r� par le Concepteur Web Form "

        'Cet appel est requis par le Concepteur Web Form.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'REMARQUE�: la d�claration d'espace r�serv� suivante est requise par le Concepteur Web Form.
        'Ne pas supprimer ou d�placer.

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN�: cet appel de m�thode est requis par le Concepteur Web Form
            'Ne le modifiez pas en utilisant l'�diteur de code.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Placez ici le code utilisateur pour initialiser la page

            Me.lblErrMsg.Text = ""

            'With Request.Browser
            '    'display browser....
            '    'Response.Write("Nav " & .Browser.ToString)
            '    If UCase(.Browser.ToString) <> "CHROME" Then
            '        Response.Redirect("login.aspx")
            '    End If

            'End With


            'Else
            If Not Page.IsPostBack Then
                'BindDropDownListEcoleByUserID(Session("username"), DropDownListEcoles)
                'BindDropDownListEcoleByUtilisateurId(Page.User.Identity.Name, DropDownListEcoles)

                BindDropDownListEcoleByUserID(Page.User.Identity.Name, DropDownListEcoles)
                SetDropDropListEcoleBySessionEleve()


                'If IsUserAdmin(Page.User.Identity.Name) Then
                '    BindDropDownListEcoleByUserIdAdmin(Page.User.Identity.Name, DropDownListEcoles)
                'Else
                '    BindDropDownListEcoleByUserID(Page.User.Identity.Name, DropDownListEcoles)
                'End If

                '================
                'Dim _mydt As New DataTable
                '_mydt = GetDatatable("select school_code,school_name from schools", ConfigurationSettings.AppSettings("connectionStringTrillium"))

                'For Each r As DataRow In _mydt.Rows
                '    Response.Write("<br>row : " & r.Item("school_code").ToString & " - " & r.Item("school_name").ToString)
                'Next
                '============================

                'temporairment d�sativ�, genere erreur si le user n a pas acc�s a l �cole de l�l�ve.. (si l �l�ve a �t� trans�f�r� juste avant..)
                If Session("eleveid") <> "" Then
                    'Me.DropDownListEcoles.SelectedValue = GetEcoleIdByCurrentEleveID(Session("eleveid"))
                End If

                BindDatagridElevesByEcoleID(ConfigurationSettings.AppSettings("ConnectionString"), DropDownListEcoles.SelectedValue)

            End If

            'BindDatagridEleves(GetCookiesValues("user", "username"))
            'BindDatagridEleves(Session("username"))
            'End If

            If Not Session("eleveid") <> "" Then
                Me.lblErrMsg.Text = "Veuillez s�lectionner un �l�ve dans la liste"
                Me.lblErrMsg.Visible = True
            End If

        End Sub

  

        Function GetEcoleIdByCurrentEleveID(ByVal EleveID As String) As String
            Dim oEleve As New PEIDAL.eleve
            Dim DsEleve As New DataSet
            Dim r As DataRow
            Dim sEleveID As String

            sEleveID = EleveID

            Try
                DsEleve = oEleve.GetEleveByID(ConfigurationSettings.AppSettings("ConnectionString"), sEleveID)
                GetEcoleIdByCurrentEleveID = DsEleve.Tables(0).Rows(0).Item("fk_ecole_id")

            Catch ex As Exception
                Throw New Exception("Erreur survenue dans la fonction GetEcoleIdByCurrentEleveID")
            End Try

        End Function
        Function GetCookiesValues(ByVal CookieName As String, ByVal Key As String) As String

            Dim Cookie As HttpCookie
            Cookie = Request.Cookies(CookieName)

            If Not Cookie Is Nothing Then

                Select Case (Key)

                    Case "username"
                        Return Cookie.Values("username")
                End Select
            End If

            Return ""

        End Function
        Sub BindDatagridElevesByEcoleID(ByVal Conn As String, ByVal EcoleID As String)
            Dim oEleve As New PEIDAL.eleve

            Try
                DataGridListeEleves.DataSource = oEleve.GetListElevesByEcoleId(Conn, EcoleID)
                DataGridListeEleves.DataBind()
                DataGridListeEleves.Columns(1).HeaderText = "ID"

                Dim oIden As New PEIDAL.identification
                Dim _ds As New DataSet
                _ds = oIden.GetDSListeElevesAndAnomalies(EcoleID, Conn)
                

                Dim _sql As String = "select e.fk_ecole_id, d.FK_ELEVE_ID," & _
                " d.DATE_IDENTIFICATION," & _
                " d.DATE_CIPR_RECENT," & _
                " d.DATE_DEBUT_PLACEMENT, " & _
                " d.DATE_RENONCIATION," & _
                " d.DATE_MODIFICATION, " & _
                " d.DATE_PEI_TERMINE " & _
                "from eleves e, DATES d where e.pk_eleve_id = d.FK_ELEVE_ID " & _
                "and e.fk_ecole_id = '" & Me.DropDownListEcoles.SelectedValue & "'"

                Dim _ds2 As New DataSet
                Dim oGen As New PEIDAL.general
                _ds2 = oGen.GetDataset(_sql, Conn)
                Dim _dt As New DataTable
                Dim _dt2 As New DataTable
                _dt2 = _ds2.Tables(0)
                _dt = _ds.Tables(0)
                Dim _colCIPRInit As DataColumn = New DataColumn("DATE_IDENTIFICATION")
                _colCIPRInit.DataType = System.Type.GetType("System.DateTime")
                Dim _colCIPRRecent As DataColumn = New DataColumn("DATE_CIPR_RECENT")
                _colCIPRRecent.DataType = System.Type.GetType("System.DateTime")
                Dim _colCIPRRenonciation As DataColumn = New DataColumn("DATE_RENONCIATION")
                _colCIPRRenonciation.DataType = System.Type.GetType("System.DateTime")

                _dt.Columns.Add(_colCIPRInit)
                _dt.Columns.Add(_colCIPRRecent)
                _dt.Columns.Add(_colCIPRRenonciation)

                For Each r As DataRow In _dt.Rows
                    For Each r2 As DataRow In _dt2.Rows

                        If (r.Item("pk_eleve_id") = r2.Item("fk_eleve_id")) Then
                            r.Item("DATE_IDENTIFICATION") = r2.Item("DATE_IDENTIFICATION")
                            r.Item("DATE_CIPR_RECENT") = r2.Item("DATE_CIPR_RECENT")
                            r.Item("DATE_RENONCIATION") = r2.Item("DATE_RENONCIATION")
                        End If
                    Next
                Next

                Me.DataGrid1.DataSource = _dt
                Me.DataGrid1.DataBind()
                Me.DataGrid1.Columns(1).HeaderText = "ID"

            Catch ex As Exception
                lblErrMsg.Visible = True
                lblErrMsg.Text = ex.Message
            End Try

        End Sub
        Sub SetDropDropListEcoleBySessionEleve()

            Dim obj As New PEIDAL.eleve
            Dim ds As New DataSet
            ds = obj.GetEleveByID(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

            If ds.Tables(0).Rows.Count > 0 Then
                Dim ecole = ds.Tables(0).Rows(0).Item("fk_ecole_id").ToString
                For Each i As ListItem In DropDownListEcoles.Items
                    If Not i.Value <> ecole Then
                        DropDownListEcoles.SelectedValue = i.Value
                    End If
                Next
            End If

        End Sub

        Sub BindDatagridEleves(ByVal UtilisateurId As String)
            Dim oEleve As New PEIDAL.eleve
            Dim str As String

            Try
                DataGridListeEleves.DataSource = oEleve.GetListElevesByUtilisateurId(ConfigurationSettings.AppSettings("ConnectionString"), UtilisateurId)
                DataGridListeEleves.DataBind()

                DataGridListeEleves.Columns(1).HeaderText = "ID"
                'DataGridListeEleves.Columns(1).HeaderText()
                'DataGridListeEleves.Columns(2).HeaderText = "nom"
                'DataGridListeEleves.Columns(3).HeaderText = "prenom"
                'DataGridListeEleves.Columns(4).HeaderText = "ecole"
                'DataGridListeEleves.Columns(5).HeaderText = "niveau"
                'DataGridListeEleves.Columns(6).HeaderText = "date de naissance"


                'DataGridListeEleves.Columns.Item(1).HeaderText = "no identification"


            Catch ex As Exception
                lblErrMsg.Visible = True
                lblErrMsg.Text = ex.Message
            End Try

        End Sub

        Private Sub DataGridListeEleves_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridListeEleves.SelectedIndexChanged
            Dim strEleveId As String = DataGridListeEleves.DataKeys(DataGridListeEleves.SelectedIndex)
            Session("eleveid") = strEleveId

            Dim oEleve As New PEIDAL.eleve
            Dim ds As New DataSet

            Try
                ds = oEleve.GetEleveByID(ConfigurationSettings.AppSettings("ConnectionString"), strEleveId)
                Session("infoseleve") = ds.Tables(0).Rows(0).Item("prenom_eleve") & " " & ds.Tables(0).Rows(0).Item("nom_eleve") & ",         " & ds.Tables(0).Rows(0).Item("date_naissance")
                Session("nomprenom") = ds.Tables(0).Rows(0).Item("prenom_eleve") & "_" & ds.Tables(0).Rows(0).Item("nom_eleve")

                Response.Redirect("general.aspx")

            Catch ex As Exception
                lblErrMsg.Visible = True
                lblErrMsg.Text = ex.Message

            End Try


            '' Dim au_fnameCell As TableCell = e.Item.Cells(0)
            'Dim itemIndex As Integer
            'Dim index As String
            'Dim index2 As String

            'Try

            '    Dim oAtt As PEIDAL.attente = New PEIDAL.attente

            '    'index2 = e.Item.Cells(0).Text

            '    'index = e.Item.Cells(1).Text
            '    oAtt.DeleteAdaptationById(ConfigurationSettings.AppSettings("ConnectionString"), index)

            'Catch ex As Exception
            '    'ErrorLabel.Text = "erreur lors de la suppression d'une adaptation..."
            'End Try
        End Sub

        Private Sub DataGridListeEleves_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DataGridListeEleves.ItemDataBound
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='AliceBlue'")
                e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='white'")
            End If
        End Sub
        Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
            Session("infoseleve") = ""
            Session("eleveid") = ""

            'Response.Redirect("dupplicate.aspx")
            Response.Redirect("createEleve.aspx")

        End Sub

        Private Sub DropDownListEcoles_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DropDownListEcoles.SelectedIndexChanged

            Session("eleveid") = ""
            BindDatagridElevesByEcoleID(ConfigurationSettings.AppSettings("ConnectionString"), DropDownListEcoles.SelectedValue)


        End Sub

        Protected Sub DataGrid1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGrid1.SelectedIndexChanged
            Dim strEleveId As String = DataGrid1.DataKeys(DataGrid1.SelectedIndex)
            Session("eleveid") = strEleveId


            Dim oEleve As New PEIDAL.eleve
            Dim ds As New DataSet

            Try
                ds = oEleve.GetEleveByID(ConfigurationSettings.AppSettings("ConnectionString"), strEleveId)
                Session("infoseleve") = ds.Tables(0).Rows(0).Item("prenom_eleve") & " " & ds.Tables(0).Rows(0).Item("nom_eleve") & ",         " & ds.Tables(0).Rows(0).Item("date_naissance")
                Session("nomprenom") = ds.Tables(0).Rows(0).Item("prenom_eleve") & "_" & ds.Tables(0).Rows(0).Item("nom_eleve")
                Response.Redirect("general.aspx")

            Catch ex As Exception
                lblErrMsg.Visible = True
                lblErrMsg.Text = ex.Message

            End Try
        End Sub
    End Class

End Namespace

