Imports System
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient


Namespace PeiWebAccess

Partial Class forcesbesoins
    Inherits System.Web.UI.Page

#Region " Code g�n�r� par le Concepteur Web Form "

    'Cet appel est requis par le Concepteur Web Form.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents DsRapportsEvaluationList1 As PEIDAL.dsRapportsEvaluationList

    Public strTypeEval As String

    Protected WithEvents DsRapportsEvaluationList1 As PEIDAL.dsRapportsEvaluationList
    Protected WithEvents ImageButtonSave As System.Web.UI.WebControls.ImageButton

    'REMARQUE�: la d�claration d'espace r�serv� suivante est requise par le Concepteur Web Form.
    'Ne pas supprimer ou d�placer.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN�: cet appel de m�thode est requis par le Concepteur Web Form
        'Ne le modifiez pas en utilisant l'�diteur de code.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Placez ici le code utilisateur pour initialiser la page
        Dim EleveId As String = Session("eleveid")

        If Not Session("eleveid") <> "" Then
            Response.Redirect("listeeleves.aspx")
        Else
            Session("eleveid") = Session("eleveid")
        End If

        lblMsgErr.Visible = False

        If Not Page.IsPostBack Then
                Dim oFB As New PEIDAL.forces_besoins

                'BindDatagridRapportsEval("AZ199511185_2")
                Try
                    BindDG()

                    'FORCES (F)
                    'HAB
                    BindDropDownListCategorie(DropDownListCategorieForces, "points forts")
                    BindDropDownListContenus(DropDownListContenusForces, "points forts", DropDownListCategorieForces.SelectedValue)
                    'Dev Personnel
                    'BindDropDownListCategorie(Me.DropDownList_F_DEV_PERS_Categories, "points forts")
                    'BindDropDownListContenus(Me.DropDownList_F_DEV_PERS_Contenus, "points forts", Me.DropDownList_F_DEV_PERS_Categories.SelectedValue)


                    'BESOINS (B)
                    'HAB
                    BindDropDownListCategorie(Me.DropDownList_B_HAB_Categories, "Besoins")
                    BindDropDownListContenus(Me.DropDownList_B_HAB_Contenus, "Besoins", Me.DropDownList_B_HAB_Categories.SelectedValue)
                    'Dev Person
                    'BindDropDownListCategorie(Me.DropDownList_B_DEV_PERS_Categories, "points forts")
                    'BindDropDownListContenus(Me.DropDownList_B_DEV_PERS_Contenus, "points forts", Me.DropDownList_B_DEV_PERS_Categories.SelectedValue)

                    'BESOINS PRIORITAIRES (BP)
                    'HAB
                    BindDropDownListCategorie(Me.DropDownListBP_HAB_Categories, "Besoins Prioritaires")
                    BindDropDownListContenus(Me.DropDownListBP_HAB_Contenus, "Besoins Prioritaires", Me.DropDownListBP_HAB_Categories.SelectedValue)
                    'Dev Personnel
                    'BindDropDownListCategorie(Me.DropDownListBP_DEV_PERS_Categories, "points forts")
                    'BindDropDownListContenus(Me.DropDownListBP_DEV_PERS_Contenus, "points forts", DropDownListBP_DEV_PERS_Categories.SelectedValue)
                    '===============================================

                    'Dim sqlReader As SqlDataReader
                    'sqlReader = oFB.GetForcesByEleveId(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
                    ''sqlReader = oFB.GetForcesTEST_ListElevesByUtilisateurId(ConfigurationSettings.AppSettings("ConnectionString"), "jcote")

                    'If sqlReader.HasRows Then
                    '    Do While sqlReader.Read
                    '        txtForces.Text = Replace(sqlReader.GetString(0), "*", Chr(13))
                    '    Loop
                    'Else
                    '    oFB.InsertForces(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"), "")
                    'End If

                    'sqlReader.Close()

                    'sqlReader = oFB.GetBesoinsByEleveId(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
                    'If sqlReader.HasRows Then
                    '    Do While sqlReader.Read
                    '        txtBesoins.Text = Replace(sqlReader.GetString(0), "*", Chr(13))
                    '    Loop
                    'Else
                    '    oFB.InsertBesoins(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"), "")
                    'End If

                    'sqlReader.Close()

                    ''=======================================================

                    DisplayNewForcesBesoins(Session("eleveid"))

                Catch ex As Exception
                    lblMsgErr.Visible = True
                    lblMsgErr.Text = ex.Message

                End Try

            End If

        End Sub

        'Sub BindDropDownListCategorie(ByVal DDL As DropDownList, ByVal TypeContenu As String)
        '    Dim oGen As New PEIDAL.general
        '    Dim DropDownList As DropDownList

        '    DropDownList = DDL

        '    DropDownList.DataSource = oGen.GetListCategorie(TypeContenu)
        '    DropDownList.DataValueField = "categorie"
        '    DropDownList.DataTextField = "categorie"
        '    DropDownList.DataBind()

        'End Sub
        'Sub BindDropDownListContenus(ByVal DDL As DropDownList, ByVal TypeContenu As String, ByVal Categorie As String)
        '    Dim oGen As New PEIDAL.general
        '    Dim DropDownList As DropDownList

        '    DropDownList = DDL

        '    DropDownList.Items.Clear()

        '    DropDownList.DataSource = oGen.GetListContenuByTypeAndCategorie(TypeContenu, Categorie)
        '    DropDownList.DataValueField = "contenu"
        '    DropDownList.DataTextField = "contenu"
        '    DropDownList.DataBind()

        'End Sub

        Private Sub btnAjoutContenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        End Sub

        Private Sub DropDownListCategorieForces_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DropDownListCategorieForces.SelectedIndexChanged
            BindDropDownListContenus(DropDownListContenusForces, "points forts", DropDownListCategorieForces.SelectedValue)
        End Sub

        Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
            'SaveForces()
            'SaveBesoins()
            SaveNewForcesBesoins(Session("eleveid"))
        End Sub

        Sub SaveNewForcesBesoins(ByVal EleveID As String)
            Dim oFB As New PEIDAL.forces_besoins
            'Dim Ad As New PEIDAL.dsForcesBesoinsTableAdapters.FORCESBESOINSTableAdapter

            Try
                oFB.UpdateForcesBesoins(ConfigurationSettings.AppSettings("ConnectionString"), EleveID, "F", Me.txtForces.Text, "")
                oFB.UpdateForcesBesoins(ConfigurationSettings.AppSettings("ConnectionString"), EleveID, "B", Me.txtBesoins.Text, "")
                oFB.UpdateForcesBesoins(ConfigurationSettings.AppSettings("ConnectionString"), EleveID, "BP", Me.TextBoxBesoinsPrioHabCogn.Text, "")

                'Ad.UpdateForcesBesoins(EleveID, "F", Me.txtForces.Text, Me.TextBoxForcesDevPersonnel.Text)
                'Ad.UpdateForcesBesoins(EleveID, "B", Me.txtBesoins.Text, Me.TextBoxBesoinsDevPersonnel.Text)
                'Ad.UpdateForcesBesoins(EleveID, "BP", Me.TextBoxBesoinsPrioHabCogn.Text, Me.TextBoxBesoinsPrioDevPersonnel.Text)

            Catch ex As Exception
                Me.lblMsgErr.Text = "Erreur survenu lors de la mise � jour des forces, besoins (F,B,BP): " & ex.Message
            End Try

        End Sub
        Public Function BindTypeEval_2()
            'Dim objConn As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("workstation id=XP-FCOTE-LT;packet size=4096;integrated security=SSPI;data source=xp-fcote-lt;persist security info=False;initial catalog=Northwind"))
            Dim objConn As SqlConnection = New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))
            Dim cmdCustomers As SqlCommand = New SqlCommand("select distinct(type_eval) from RAPPORTS_EVALUATION", objConn)

            cmdCustomers.CommandType = CommandType.Text
            objConn.Open()
            Return cmdCustomers.ExecuteReader(CommandBehavior.CloseConnection)
        End Function

        Public Sub SetDropDownIndex_2(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim ed As System.Web.UI.WebControls.DropDownList
            ed = sender
            ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strTypeEval))
        End Sub
        Sub BindDG()
            ' get dataset
            Dim oFB As New PEIDAL.forces_besoins
            DataGrid1.DataSource = oFB.GetdsRapportsEvaluationList(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            'SqlDataAdapter1.Fill(DsRapportsEvaluationList1)
            'DataGrid1.DataKeyField = "row_id"
            DataGrid1.DataBind()
        End Sub

        Private Sub DataGrid1_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.EditCommand
            DataGrid1.EditItemIndex = e.Item.ItemIndex
            strTypeEval = CType(e.Item.FindControl("lblTypEval"), Label).Text

            BindDG()
        End Sub

        Private Sub DataGrid1_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.CancelCommand
            DataGrid1.EditItemIndex = -1
            BindDG()
        End Sub

        Private Sub DataGrid1_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.DeleteCommand
            Dim Index As Integer
            Dim Key As Integer
            Dim Conn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

            Key = DataGrid1.DataKeys(e.Item.ItemIndex).ToString


            'Dim oForcesBesoins As New PEIDAL.forces_besoins

            Try
                'Index = e.Item.ItemIndex
                Dim objCmd As New SqlCommand("delete from rapports_evaluation where row_id=" & Key, Conn)
                objCmd.CommandType = CommandType.Text
                Conn.Open()
                objCmd.ExecuteNonQuery()
                Conn.Close()

                'oForcesBesoins.DeleteRaportEvalById(ConfigurationSettings.AppSettings("ConnectionString"), Index)
            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message

            End Try

            'DataGrid1.DataBind()
            BindDG()
        End Sub

        Private Sub DataGrid1_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.UpdateCommand
            Dim TypeEval, Sommaire As String
            Dim DateEval As Date
            Dim Test As String

            ' Gets the value of the key field of the row being updated
            Test = DataGrid1.DataKeyField
            Test = DataGrid1.DataKeyField.ToString

            Dim key As String = DataGrid1.DataKeys(e.Item.ItemIndex).ToString()

            ' Gets get the value of the controls (textboxes) that the user
            ' updated. The DataGrid columns are exposed as the Cells collection.
            ' Each cell has a collection of controls. In this case, there is only one 
            ' control in each cell -- a TextBox control. To get its value,
            ' you copy the TextBox to a local instance (which requires casting)
            ' and extract its Text property.
            '
            ' The first column -- Cells(0) -- contains the Update and Cancel buttons.
            Dim tb As TextBox
            Dim temp As String

            Try

                ' Gets the value the TextBox control in the third column 
                '(DATE_EVAL)
                tb = CType(e.Item.Cells(3).Controls(0), TextBox)
                If Not tb.Text <> "" Then
                    DateEval = Date.Now.ToShortDateString
                Else
                    'DateEval = CDate(tb.Text).ToShortDateString
                    'Dim format() = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy"}
                    'DateEval = Date.ParseExact(tb.Text, format, System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.None)
                    DateEval = GetDateFormatted(tb.Text)
                End If

                ' Gets the value the TextBox control in the fourth column
                'tb = CType(e.Item.Cells(4).Controls(0), TextBox)
                'TypeEval = tb.Text

                'TypeEval = CType(e.Item.FindControl("ddlTypeEval"), DropDownList).SelectedItem.Value
                tb = CType(e.Item.Cells(5).Controls(0), TextBox)
                TypeEval = tb.Text

                ' Gets the value the TextBox control in the fourth column
                'tb = CType(e.Item.Cells(6).Controls(0), TextBox)
                'Sommaire = tb.Text

                Sommaire = CType(e.Item.FindControl("Txtsommaire"), TextBox).Text

                ' Finds the row in the dataset table that matches the 
                ' one the user updated in the grid. This example uses a 
                ' special Find method defined for the typed dataset, which
                ' returns a reference to the row.
                'SqlDataAdapter1.Fill(DsRapportsEvaluationList1)
                Dim objFB As New PEIDAL.forces_besoins

                DsRapportsEvaluationList1 = objFB.GetdsRapportsEvaluationList(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

                Dim r As PEIDAL.dsRapportsEvaluationList.RAPPORTS_EVALUATIONRow
                r = DsRapportsEvaluationList1.RAPPORTS_EVALUATION.FindByROW_ID(key)

                'temp = r.CategoryName
                'temp = r.Description()

                ' Updates the dataset table.
                r.TYPE_EVAL = TypeEval
                r.DATE_EVAL = DateEval
                r.SOMMAIRE = Sommaire
                r.FK_ELEVE_ID = Session("eleveid")

                ' Calls a SQL statement to update the database from the dataset
                'SqlDataAdapter1.Update(DsRapportsEvaluationList1)
                'Dim oFB As New PEIDAL.forces_besoins
                objFB.UpdateDsRapportsEvaluation(DsRapportsEvaluationList1)

                ' Takes the DataGrid row out of editing mode
                DataGrid1.EditItemIndex = -1

                ' Refreshes the grid
                'DataGrid1.DataBind()
                BindDG()

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message

            End Try
        End Sub
        Public Sub SetDropDownIndex(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim ed As System.Web.UI.WebControls.DropDownList
            ed = sender
            ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strTypeEval))
        End Sub
        Public Function BindTypeEval()
            Try
                Dim oFB As New PEIDAL.forces_besoins
                Return oFB.GetTypeEval(ConfigurationSettings.AppSettings("ConnectionString"))

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message
            End Try

        End Function

        Private Sub btnAddEval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


        End Sub


        Private Sub DropDownListCategorieBesoins_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DropDownList_B_HAB_Categories.SelectedIndexChanged
            BindDropDownListContenus(DropDownList_B_HAB_Contenus, "Besoins", DropDownList_B_HAB_Categories.SelectedValue)
        End Sub

        Private Sub ImageButtonSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSave.Click

        End Sub

        Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        End Sub

        Private Sub txtForces_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtForces.TextChanged

        End Sub

        Private Sub ImageButtonAjouterContenu_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAjouterContenu.Click
            Dim strText As String
            Dim myPos As Integer

            Try
                myPos = InStr(DropDownListContenusForces.SelectedValue, "-")
                strText = Mid(DropDownListContenusForces.SelectedValue, myPos)

                txtForces.Text = txtForces.Text & Chr(13) & strText

            Catch ex As Exception

                lblMsgErr.Visible = True
                lblMsgErr.Text = "Erreur lors de l'ajout d'un points fort..." & vbCrLf & _
                ex.Message

            End Try

        End Sub
        Sub SetTextBox(ByVal DDL As DropDownList, ByVal Tbox As TextBox)
            Dim TheDDL As DropDownList
            Dim TheBox As TextBox

            Dim strText As String
            Dim myPos As Integer

            TheDDL = DDL
            TheBox = Tbox

            Try
                'myPos = InStr(DropDownListContenusForces.SelectedValue, "-")
                myPos = InStr(TheDDL.SelectedValue, "-")

                'strText = Mid(DropDownListContenusForces.SelectedValue, myPos)
                strText = Mid(TheDDL.SelectedValue, myPos)

                'txtForces.Text = txtForces.Text & Chr(13) & strText
                TheBox.Text = TheBox.Text & Chr(13) & strText

            Catch ex As Exception

                lblMsgErr.Visible = True
                lblMsgErr.Text = "Erreur lors de l'ajout des donn�es dans la zonne de texte (" & Tbox.ID.ToString & ")" & vbCrLf & _
                ex.Message

            End Try


        End Sub

        'Private Sub ImageButtonAjouterContenuBesoins_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAjouterContenuBesoins.Click
        '    SetTextBox(Me.DropDownList_F_DEV_PERS_Contenus, Me.TextBoxForcesDevPersonnel)

        'End Sub

    Private Sub ImageButtonAddDonneeEval_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAddDonneeEval.Click
        Dim objFB As New PEIDAL.forces_besoins
        'Dim ds As New PEIDAL.dsRapportsEvaluationList

        'DsRapportsEvaluationList1 = objFB.GetdsRapportsEvaluationList("az199511185_2")
        'BindDG()
        DsRapportsEvaluationList1 = New PEIDAL.dsRapportsEvaluationList
        Dim dr As DataRow = DsRapportsEvaluationList1.RAPPORTS_EVALUATION.NewRow
        'Dim dr As DataRow = ds.RAPPORTS_EVALUATION.NewRow

        Try
            dr("type_eval") = ""
            dr("date_eval") = Date.Now
            dr("sommaire") = ""
            dr("fk_eleve_id") = Session("eleveid")

            DsRapportsEvaluationList1.RAPPORTS_EVALUATION.Rows.InsertAt(dr, 0)
            'ds.RAPPORTS_EVALUATION.Rows.InsertAt(dr, 0)

            'Session("ds") = DsRapEval1
            'SqlDataAdapter1.Update(DsRapportsEvaluationList1)
            Dim oFB As New PEIDAL.forces_besoins
            oFB.UpdateDsRapportsEvaluation(DsRapportsEvaluationList1)
            'oFB.UpdateDsRapportsEvaluation(ds)

            DataGrid1.EditItemIndex = 0

            DataGrid1.DataSource = objFB.GetdsRapportsEvaluationListOrderByDescRowID(Session("eleveid"), ConfigurationSettings.AppSettings("ConnectionString"))
            DataGrid1.DataBind()
            'BindDG()

            'DataGrid1.DataBind()


        Catch ex As Exception
            lblMsgErr.Visible = True
            lblMsgErr.Text = ex.Message
        End Try
    End Sub

    Private Sub btnEnregistrerPointsForts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        End Sub
        Sub SaveAllForcesBesoins(ByVal EleveID As String)
            'Dim oFB As New PEIDAL.dsForcesBesoinsTableAdapters.FORCESBESOINSTableAdapter


            'oFB.getds()

        End Sub
        Sub DisplayNewForcesBesoins(ByVal EleveId As String)
            Dim oFB As New PEIDAL.forces_besoins
            Dim TB As New PEIDAL.dsForcesBesoins.FORCESBESOINSDataTable
            Dim ds As New DataSet

            '=========================================
            'display FORCES (hab et dev personnel)
            ds = oFB.GetDSForcesBesoinsByEleveIDAndType(ConfigurationSettings.AppSettings("ConnectionString"), EleveId, "F")

            If ds.Tables(0).Rows.Count < 1 Then
                oFB.InsertForcesBesoins(ConfigurationSettings.AppSettings("ConnectionString"), EleveId, "F", "", "")
            Else
                Me.txtForces.Text = ds.Tables(0).Rows(0)("HAB_COGNITIVE").ToString
                'Me.TextBoxForcesDevPersonnel.Text = ds.Tables(0).Rows(0)("DEV_PERSONNEL").ToString
            End If
            '==========================================


            '==========================================
            'display BESOIN (dev personnel)
            ds = oFB.GetDSForcesBesoinsByEleveIDAndType(ConfigurationSettings.AppSettings("ConnectionString"), EleveId, "B")
            If ds.Tables(0).Rows.Count < 1 Then
                oFB.InsertForcesBesoins(ConfigurationSettings.AppSettings("ConnectionString"), EleveId, "B", "", "")
            Else
                Me.txtBesoins.Text = ds.Tables(0).Rows(0)("HAB_COGNITIVE").ToString
                'Me.TextBoxBesoinsDevPersonnel.Text = ds.Tables(0).Rows(0)("DEV_PERSONNEL").ToString
            End If
            '===========================================


            '===========================================
            'display BESOINS PRIORITAIRE (HABILETES ET DEV PERSONNEL)
            ds = oFB.GetDSForcesBesoinsByEleveIDAndType(ConfigurationSettings.AppSettings("ConnectionString"), EleveId, "BP")
            If ds.Tables(0).Rows.Count < 1 Then
                oFB.InsertForcesBesoins(ConfigurationSettings.AppSettings("ConnectionString"), EleveId, "BP", "", "")
            Else
                Me.TextBoxBesoinsPrioHabCogn.Text = ds.Tables(0).Rows(0)("HAB_COGNITIVE").ToString
                'Me.TextBoxBesoinsPrioDevPersonnel.Text = ds.Tables(0).Rows(0)("DEV_PERSONNEL").ToString
            End If
            '===========================================


        End Sub
    Sub SaveForces()
        Try
            Dim objFB As New PEIDAL.forces_besoins

            objFB.UpdateForces(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"), txtForces.Text)
            'objFB.UpdateBesoins(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"), txtBesoins.Text)

        Catch ex As Exception
            lblMsgErr.Visible = True
            lblMsgErr.Text = "erreur survenue lors de la mise � jour des points forts...." & ex.Message

        End Try
    End Sub

    Private Sub btnEnregistrerBesoins_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Sub SaveBesoins()
        Try
            Dim objFB As New PEIDAL.forces_besoins

            'objFB.UpdateForces(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"), txtForces.Text)
            objFB.UpdateBesoins(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"), txtBesoins.Text)

        Catch ex As Exception
            lblMsgErr.Visible = True
            lblMsgErr.Text = "erreur survenue lors de la mise � jour des besoins...." & ex.Message

        End Try
    End Sub

  

        'Protected Sub DropDownList_F_DEV_PERS_Categories_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList_F_DEV_PERS_Categories.SelectedIndexChanged
        '    BindDropDownListContenus(Me.DropDownList_F_DEV_PERS_Contenus, "points forts", Me.DropDownList_F_DEV_PERS_Categories.SelectedValue)
        'End Sub

        'Protected Sub ImageButton6_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton6.Click
        '    SetTextBox(Me.DropDownList_B_DEV_PERS_Contenus, Me.TextBoxBesoinsDevPersonnel)
        'End Sub

       
        Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
            SetTextBox(Me.DropDownList_B_HAB_Contenus, Me.txtBesoins)
        End Sub

        Protected Sub ImageButton1_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
            SetTextBox(Me.DropDownListBP_HAB_Contenus, Me.TextBoxBesoinsPrioHabCogn)
        End Sub

        'Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        '    SetTextBox(Me.DropDownListBP_DEV_PERS_Contenus, Me.TextBoxBesoinsPrioDevPersonnel)
        'End Sub

        'Protected Sub DropDownListBP_DEV_PERS_Categories_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownListBP_DEV_PERS_Categories.SelectedIndexChanged
        '    BindDropDownListContenus(Me.DropDownListBP_DEV_PERS_Contenus, "points forts", DropDownListBP_DEV_PERS_Categories.SelectedValue)
        'End Sub

        Protected Sub DropDownListBP_HAB_Categories_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownListBP_HAB_Categories.SelectedIndexChanged
            BindDropDownListContenus(Me.DropDownListBP_HAB_Contenus, "Besoins", Me.DropDownListBP_HAB_Categories.SelectedValue)
        End Sub

        'Protected Sub DropDownList_B_DEV_PERS_Categories_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList_B_DEV_PERS_Categories.SelectedIndexChanged
        '    BindDropDownListContenus(Me.DropDownList_B_DEV_PERS_Contenus, "points forts", Me.DropDownList_B_DEV_PERS_Categories.SelectedValue)
        'End Sub

        Protected Sub DropDownList_B_HAB_Contenus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList_B_HAB_Contenus.SelectedIndexChanged
            '
        End Sub
    End Class

End Namespace
