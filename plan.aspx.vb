Imports System.Data
Namespace PeiWebAccess

    Partial Class plan
        Inherits System.Web.UI.Page

#Region " Code g�n�r� par le Concepteur Web Form "

        'Cet appel est requis par le Concepteur Web Form.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Public strButDescription As String
        Public strTypeContact As String

        Protected WithEvents dsButsTransition1 As PEIDAL.dsButsTransition
        Protected WithEvents dsPlanIntervention1 As PEIDAL.dsPlanIntervention

        'REMARQUE�: la d�claration d'espace r�serv� suivante est requise par le Concepteur Web Form.
        'Ne pas supprimer ou d�placer.

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN�: cet appel de m�thode est requis par le Concepteur Web Form
            'Ne le modifiez pas en utilisant l'�diteur de code.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Placez ici le code utilisateur pour initialiser la page

            Dim EleveId As String = Session("eleveid")

            If Not Session("eleveid") <> "" Then
                Response.Redirect("listeeleves.aspx")
            Else
                Session("eleveid") = Session("eleveid")
            End If

            lblMsgErr.Text = ""

            If Not Page.IsPostBack Then
                'bind drop down list categorie
                BindDropDownListCategorie(DropDownListCategorie, "PLAN")
                'bind datagrid Buts de transition
                BindDG()
                'bind datagrid plan intervention
                BindDG2()

                BindDG3()
            End If
        End Sub
        Sub BindDG()
            ' get dataset
            'Dim oPlan As New PEIDAL.plan

            'Try
            '    DataGrid1.DataSource = oPlan.GetdsButsTransition(ConfigurationSettings.AppSettings("ConnectionString"), (Session("eleveid")))
            '    DataGrid1.DataBind()

            'Catch ex As Exception
            '    lblMsgErr.Visible = True
            '    lblMsgErr.Text = ex.Message

            'End Try


        End Sub
        Sub BindDG3()
            ' get dataset
            Dim oPlan As New PEIDAL.plan

            Try
                Datagrid3.DataSource = oPlan.GetdsButsTransition(ConfigurationSettings.AppSettings("ConnectionString"), (Session("eleveid")))
                Datagrid3.DataBind()

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message

            End Try


        End Sub
        'Private Sub DataGrid1_EditCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        '    DataGrid1.EditItemIndex = e.Item.ItemIndex
        '    'strButTransition = CType(e.Item.FindControl("lblTypEval"), Label).Text

        '    BindDG()
        'End Sub

        Private Sub DataGrid1_Init(ByVal sender As System.Object, ByVal e As System.EventArgs)

        End Sub

        Private Sub DataGrid1_UpdateCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
            'Dim TypeEval, Sommaire As String
            'Dim ButTransition As String
            'Dim DateEval As Date
            'Dim Test As String

            '' Gets the value of the key field of the row being updated
            'Test = DataGrid1.DataKeyField
            'Test = DataGrid1.DataKeyField.ToString

            'Dim key As String = DataGrid1.DataKeys(e.Item.ItemIndex).ToString()

            '' Gets get the value of the controls (textboxes) that the user
            '' updated. The DataGrid columns are exposed as the Cells collection.
            '' Each cell has a collection of controls. In this case, there is only one 
            '' control in each cell -- a TextBox control. To get its value,
            '' you copy the TextBox to a local instance (which requires casting)
            '' and extract its Text property.
            ''
            '' The first column -- Cells(0) -- contains the Update and Cancel buttons.
            'Dim tb As TextBox
            'Dim temp As String

            'Try

            '    ' Gets the value the TextBox control in the third column

            '    strButDescription = CType(e.Item.FindControl("textboxDescription"), TextBox).Text

            '    Dim objFB As New PEIDAL.forces_besoins
            '    Dim oPlan As New PEIDAL.plan

            '    dsButsTransition1 = oPlan.GetdsButsTransition(ConfigurationSettings.AppSettings("ConnectionString"), (Session("eleveid")))
            '    'DsRapportsEvaluationList1 = objFB.GetdsRapportsEvaluationList(Session("eleveid"))

            '    'Dim r As PEIDAL.dsRapportsEvaluationList.RAPPORTS_EVALUATIONRow
            '    Dim r As PEIDAL.dsButsTransition.BUTS_TRANSITIONRow
            '    'r = DsRapportsEvaluationList1.RAPPORTS_EVALUATION.FindByROW_ID(key)
            '    r = dsButsTransition1.BUTS_TRANSITION.FindByBUTS_TRANSITION(key)

            '    ''temp = r.CategoryName
            '    ''temp = r.Description()

            '    '' Updates the dataset table.
            '    r.DESCRIPTION = strButDescription

            '    '' Calls a SQL statement to update the database from the dataset
            '    ''SqlDataAdapter1.Update(DsRapportsEvaluationList1)
            '    ''Dim oFB As New PEIDAL.forces_besoins

            '    'objFB.UpdateDsRapportsEvaluation(DsRapportsEvaluationList1)
            '    oPlan.UpdateDsButsTransition(ConfigurationSettings.AppSettings("ConnectionString"), dsButsTransition1)

            '    '' Takes the DataGrid row out of editing mode
            '    DataGrid1.EditItemIndex = -1

            '    '' Refreshes the grid
            '    ''DataGrid1.DataBind()
            '    BindDG()

            'Catch ex As Exception
            '    lblMsgErr.Visible = True
            '    lblMsgErr.Text = ex.Message

            'End Try
        End Sub

        Private Sub DataGrid1_DeleteCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
            'Dim Key As Integer
            'Dim oPlan As New PEIDAL.plan

            'Try
            '    Key = DataGrid1.DataKeys(e.Item.ItemIndex).ToString
            '    oPlan.DeleteButTranstion(ConfigurationSettings.AppSettings("ConnectionString"), Key)

            'Catch ex As Exception
            '    lblMsgErr.Visible = True
            '    lblMsgErr.Text = ex.Message

            'End Try

            ''DataGrid1.DataBind()
            'BindDG()
        End Sub

        Private Sub btnAjouterBut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        End Sub
        Sub BindDG2()
            ' get dataset
            Dim oPlan As New PEIDAL.plan
            Dim oGen As New PEIDAL.general

            Try
                DataGrid2.DataSource = oPlan.GetdsPlanIntervention(ConfigurationSettings.AppSettings("ConnectionString"), (Session("eleveid")))
                'datagrid2.datasource = ogen.GetDataset("select
                DataGrid2.DataBind()

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message

            End Try


        End Sub

        Private Sub DataGrid2_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid2.DeleteCommand
            Dim Key As Integer
            Dim oPlan As New PEIDAL.plan

            Try
                Key = DataGrid2.DataKeys(e.Item.ItemIndex).ToString
                oPlan.DeletePlanInterventin(ConfigurationSettings.AppSettings("ConnectionString"), Key)

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message

            End Try

            'DataGrid1.DataBind()
            BindDG2()
        End Sub

        Private Sub DataGrid2_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid2.UpdateCommand
            Dim TypeEval, Sommaire As String
            Dim ButTransition As String
            Dim DateEval As Date
            Dim Test As String

            Dim Mesures, Responsables, Echeancier As String

            ' Gets the value of the key field of the row being updated
            'Test = DataGrid1.DataKeyField
            'Test = DataGrid1.DataKeyField.ToString

            Dim key As String = DataGrid2.DataKeys(e.Item.ItemIndex).ToString()

            ' Gets get the value of the controls (textboxes) that the user
            ' updated. The DataGrid columns are exposed as the Cells collection.
            ' Each cell has a collection of controls. In this case, there is only one 
            ' control in each cell -- a TextBox control. To get its value,
            ' you copy the TextBox to a local instance (which requires casting)
            ' and extract its Text property.
            '
            ' The first column -- Cells(0) -- contains the Update and Cancel buttons.
            Dim tb As TextBox
            Dim temp As String

            Try

                ' Gets the value the TextBox control in the third column

                'strButDescription = CType(e.Item.FindControl("textboxDescription"), TextBox).Text
                Mesures = CType(e.Item.FindControl("txtMesures"), TextBox).Text
                Responsables = CType(e.Item.FindControl("txtResponsables"), TextBox).Text
                Echeancier = CType(e.Item.FindControl("txtEcheancier"), TextBox).Text

                'Dim objFB As New PEIDAL.forces_besoins
                Dim oPlan As New PEIDAL.plan

                'dsButsTransition1 = oPlan.GetdsButsTransition(ConfigurationSettings.AppSettings("ConnectionString"), (Session("eleveid")))
                dsPlanIntervention1 = oPlan.GetdsPlanIntervention(ConfigurationSettings.AppSettings("ConnectionString"), (Session("eleveid")))

                Dim r As PEIDAL.dsPlanIntervention.PLAN_INTERVENTIONRow
                'r = DsRapportsEvaluationList1.RAPPORTS_EVALUATION.FindByROW_ID(key)
                r = dsPlanIntervention1.PLAN_INTERVENTION.FindByROW_ID(key)

                ''temp = r.CategoryName
                ''temp = r.Description()

                '' Updates the dataset table.
                'r.DESCRIPTION = strButDescription
                r.MESURES = Mesures
                r.RESPONSABLES = Responsables
                r.ECHEANCIER = Echeancier

                'oPlan.UpdateDsButsTransition(ConfigurationSettings.AppSettings("ConnectionString"), dsButsTransition1)
                oPlan.UpdateDsPlanIntervention(ConfigurationSettings.AppSettings("ConnectionString"), dsPlanIntervention1)

                '' Takes the DataGrid row out of editing mode
                DataGrid2.EditItemIndex = -1

                '' Refreshes the grid
                ''DataGrid1.DataBind()
                BindDG2()

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message

            End Try
        End Sub

        Private Sub DataGrid2_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid2.EditCommand
            DataGrid2.EditItemIndex = e.Item.ItemIndex
            'strButTransition = CType(e.Item.FindControl("lblTypEval"), Label).Text

            BindDG2()
        End Sub

        Private Sub ImageButtonAjouterMesure_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAjouterMesure.Click

            dsPlanIntervention1 = New PEIDAL.dsPlanIntervention
            Dim dr As DataRow = dsPlanIntervention1.PLAN_INTERVENTION.NewRow

            Try
                dr("no_order") = 0
                dr("mesures") = ""
                dr("fk_eleve_id") = Session("eleveid")
                dr("responsables") = ""
                dr("echeancier") = ""

                dsPlanIntervention1.PLAN_INTERVENTION.Rows.InsertAt(dr, 0)

                Dim oPlan As New PEIDAL.plan

                oPlan.UpdateDsPlanIntervention(ConfigurationSettings.AppSettings("ConnectionString"), dsPlanIntervention1)

                DataGrid2.EditItemIndex = 0

                DataGrid2.DataSource = oPlan.GetDataReaderPlanInterventionOrderByDescRowId(Session("eleveid"), ConfigurationSettings.AppSettings("ConnectionString"))
                DataGrid2.DataBind()

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message
            End Try
        End Sub

        Private Sub ImageButtonAjouterBut_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAjouterBut.Click
            Dim objFB As New PEIDAL.forces_besoins
            'Dim ds As New PEIDAL.dsRapportsEvaluationList

            'DsRapportsEvaluationList1 = objFB.GetdsRapportsEvaluationList("az199511185_2")
            'BindDG()

            dsButsTransition1 = New PEIDAL.dsButsTransition
            'DsRapportsEvaluationList1 = New PEIDAL.dsRapportsEvaluationList
            'Dim dr As DataRow = DsRapportsEvaluationList1.RAPPORTS_EVALUATION.NewRow
            Dim dr As DataRow = dsButsTransition1.BUTS_TRANSITION.NewRow
            'Dim dr As DataRow = ds.RAPPORTS_EVALUATION.NewRow

            Try
                dr("description") = ""
                dr("fk_eleve_id") = Session("eleveid")

                dsButsTransition1.BUTS_TRANSITION.Rows.InsertAt(dr, 0)

                'Dim oFB As New PEIDAL.forces_besoins
                Dim oPlan As New PEIDAL.plan

                oPlan.UpdateDsButsTransition(ConfigurationSettings.AppSettings("ConnectionString"), dsButsTransition1)
                'oFB.UpdateDsRapportsEvaluation(ds)

                Datagrid3.EditItemIndex = 0

                'DataGrid1.DataSource = objFB.GetdsRapportsEvaluationListOrderByDescRowID(Session("eleveid"), ConfigurationSettings.AppSettings("ConnectionString"))
                Datagrid3.DataSource = oPlan.GetdsButsTransitionOrderByBut(Session("eleveid"), ConfigurationSettings.AppSettings("ConnectionString"))
                Datagrid3.DataBind()
                'BindDG3()

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message
            End Try
        End Sub

        Private Sub Datagrid3_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Datagrid3.EditCommand
            Datagrid3.EditItemIndex = e.Item.ItemIndex
            'strButTransition = CType(e.Item.FindControl("lblTypEval"), Label).Text

            BindDG3()
        End Sub
        Private Sub Datagrid3_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Datagrid3.UpdateCommand

            Dim key As String = Datagrid3.DataKeys(e.Item.ItemIndex).ToString()
            Dim strButDescription As String
            Dim objGen As New PEIDAL.general

            Try
                strButDescription = CType(e.Item.FindControl("textboxDescription2"), TextBox).Text
                objGen.ExecuteCommand(ConfigurationSettings.AppSettings("ConnectionString"), "update buts_transition set description = '" & Replace(strButDescription, "'", "''") & "' where buts_transition.buts_transition = " & key)
                Datagrid3.EditItemIndex = -1

                ' Refreshes the grid
                DataGrid1.DataBind()
                BindDG3()

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message

            End Try
        End Sub
        'Private Sub Datagrid3_UpdateCommandOLD(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Datagrid3.UpdateCommand
        '    Dim TypeEval, Sommaire As String
        '    Dim ButTransition As String
        '    Dim DateEval As Date
        '    Dim Test As String

        '    ' Gets the value of the key field of the row being updated
        '    Test = Datagrid3.DataKeyField
        '    Test = Datagrid3.DataKeyField.ToString

        '    Dim key As String = Datagrid3.DataKeys(e.Item.ItemIndex).ToString()

        '    ' Gets get the value of the controls (textboxes) that the user
        '    ' updated. The DataGrid columns are exposed as the Cells collection.
        '    ' Each cell has a collection of controls. In this case, there is only one 
        '    ' control in each cell -- a TextBox control. To get its value,
        '    ' you copy the TextBox to a local instance (which requires casting)
        '    ' and extract its Text property.
        '    '
        '    ' The first column -- Cells(0) -- contains the Update and Cancel buttons.
        '    Dim tb As TextBox
        '    Dim temp As String

        '    Try

        '        ' Gets the value the TextBox control in the third column

        '        strButDescription = CType(e.Item.FindControl("textboxDescription2"), TextBox).Text

        '        'strButDescription = "text " & Date.Now.Millisecond.ToString

        '        Dim objFB As New PEIDAL.forces_besoins
        '        Dim oPlan As New PEIDAL.plan

        '        dsButsTransition1 = oPlan.GetdsButsTransition(ConfigurationSettings.AppSettings("ConnectionString"), (Session("eleveid")))
        '        'DsRapportsEvaluationList1 = objFB.GetdsRapportsEvaluationList(Session("eleveid"))

        '        'Dim r As PEIDAL.dsRapportsEvaluationList.RAPPORTS_EVALUATIONRow
        '        Dim r As PEIDAL.dsButsTransition.BUTS_TRANSITIONRow
        '        'r = DsRapportsEvaluationList1.RAPPORTS_EVALUATION.FindByROW_ID(key)
        '        r = dsButsTransition1.BUTS_TRANSITION.FindByBUTS_TRANSITION(key)

        '        ''temp = r.CategoryName
        '        ''temp = r.Description()

        '        '' Updates the dataset table.
        '        r.DESCRIPTION = strButDescription

        '        '' Calls a SQL statement to update the database from the dataset
        '        ''SqlDataAdapter1.Update(DsRapportsEvaluationList1)
        '        ''Dim oFB As New PEIDAL.forces_besoins

        '        'objFB.UpdateDsRapportsEvaluation(DsRapportsEvaluationList1)
        '        oPlan.UpdateDsButsTransition(ConfigurationSettings.AppSettings("ConnectionString"), dsButsTransition1)

        '        '' Takes the DataGrid row out of editing mode
        '        Datagrid3.EditItemIndex = -1

        '        '' Refreshes the grid
        '        ''DataGrid1.DataBind()
        '        BindDG3()

        '    Catch ex As Exception
        '        lblMsgErr.Visible = True
        '        lblMsgErr.Text = ex.Message

        '    End Try
        'End Sub
        Public Sub SetDropDownIndex(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim ed As System.Web.UI.WebControls.DropDownList
            ed = sender
            ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strTypeContact))
        End Sub
        Public Function BindTypeContact()
            Try

                Dim oGen As New PEIDAL.general
                Dim dr As Data.SqlClient.SqlDataReader
                Dim _sql As String
                _sql = "select  rtrim(cast(contenu as varchar(100))) as contenu, contenu as contenuValue from contenus where type_contenu='PLAN' and categorie = '" & Replace(DropDownListCategorie.SelectedValue, "'", "''") & "'"

                'dr = oGen.GetListContenuByTypeAndCategorie("PLAN", DropDownListCategorie.SelectedValue)
                dr = GetSqlDataReader(_sql, ConfigurationSettings.AppSettings("ConnectionString"))
                Return dr

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message
            End Try

        End Function


        'Private Sub ddlTypeContact_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTypeContact.SelectedIndexChanged


        '    CType(e.Item.FindControl(" textboxDescription2"), TextBox).Text()
        'End Sub
        'Sub SetNewTextDescription(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Sub SetNewTextDescription(ByVal sender As Object, ByVal e As System.EventArgs)

            Dim ed As System.Web.UI.WebControls.DropDownList
            Dim ed2 As System.Web.UI.WebControls.TextBox
            Dim strTemp As String

            ed = sender
            'ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strTypeContact))
            ed2 = ed.FindControl("textboxDescription2")
            'ed2.Text = ed.Items.IndexOf(ed.Items.FindByText(strTypeContact))
            ed2.Text = ed.SelectedValue

            'ed2 = CType(e.Item.FindControl("textboxDescription2"), TextBox).Text()
            'ed2 = CType(e.FindControl("textboxDescription2"), TextBox)
            'ed2.Text = ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strTypeContact))

            'ed2.Text = "NEW TEXT HERE TESTING..!!!!"

        End Sub
        'Private Sub ddlTypeContact_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTypeContact.SelectedIndexChanged
        '    Dim ed As System.Web.UI.WebControls.DropDownList

        'End Sub

        'e As System.Web.UI.WebControls.DataGridCommandEventArgse As System.Web.UI.WebControls.DataGridCommandEventArgs

        Private Sub Datagrid3_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Datagrid3.CancelCommand
            Datagrid3.EditItemIndex = -1
            BindDG3()

        End Sub

        Private Sub Datagrid3_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Datagrid3.DeleteCommand
            Dim Key As Integer
            Dim oPlan As New PEIDAL.plan

            Try
                Key = Datagrid3.DataKeys(e.Item.ItemIndex).ToString
                oPlan.DeleteButTranstion(ConfigurationSettings.AppSettings("ConnectionString"), Key)

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message

            End Try

            'DataGrid1.DataBind()
            BindDG3()
        End Sub

        Private Sub DataGrid2_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid2.CancelCommand
            DataGrid2.EditItemIndex = -1
            BindDG2()
        End Sub
    End Class

End Namespace

