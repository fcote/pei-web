<%@ Register TagPrefix="uc1" TagName="menu_top" Src="menu_top.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="PeiWebAccess.liste" CodeFile="liste.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="sstchur.web.SmartNav" Assembly="sstchur.web.SmartNav" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>liste</title>
		<LINK href="styles.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <link href="styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableFrame" borderColor="red" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD><uc1:menu_top id="Menu_top1" runat="server"></uc1:menu_top></TD>
				</TR>
				<TR>
					<TD class="titleOnglet">Identification</TD>
				</TR>
				<TR>
					<TD class="titleOnglet"><asp:label id="lblMsgErr" runat="server" Width="100%" BorderWidth="1px" BorderColor="Firebrick"
							BackColor="Khaki" CssClass="fldlabel"></asp:label></TD>
				</TR>
				<TR>
					<TD class="titleOnglet">
						<asp:Button id="Button1" runat="server" CssClass="fldTextBox" Width="300px" Text="Enregistrer"></asp:Button></TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="Table1" borderColor="#4e6b8e" cellSpacing="0" cellPadding="0" width="100%" border="1">
							<TR>
								<TD vAlign="top">
									<TABLE id="Table10" borderColor="#ff0066" cellSpacing="0" cellPadding="0" width="440px" border="0">
										<TR>
											<TD class="cbToolbar">Placement&nbsp; (selon la d�cision du CIPR)
											</TD>
										</TR>
										<TR>
											<TD><asp:radiobutton id="RadioButtonNothing" runat="server" CssClass="fldLabel" Text="Aucun" AutoPostBack="True"></asp:radiobutton></TD>
										</TR>
										<TR>
											<TD><asp:radiobuttonlist id="RadioButtonListPlacement" runat="server" Width="350px" CssClass="fldLabel" AutoPostBack="True"
													 CellPadding="0" cellSpacing="0"></asp:radiobuttonlist></TD>
                                            
										</TR>
										<TR>
											<TD class="cbToolbar">PEI �labor� par:
												<asp:imagebutton id="ImageButtonAjouterElaboration" runat="server" ImageUrl="images/create.gif"></asp:imagebutton></TD>
										</TR>
										<TR>
											<TD><asp:datagrid id="DataGrid1" runat="server" Width="100%" BorderWidth="1px" BorderColor="#CCCCCC"
													BackColor="White" CssClass="fldLabel" CellPadding="3" DataMember="ELABORE_PAR" DataKeyField="ROW_ID"
													PageSize="20" BorderStyle="None" AutoGenerateColumns="False">
													<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
													<ItemStyle ForeColor="#000066"></ItemStyle>
													<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699"></HeaderStyle>
													<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
													<Columns>
														<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;img border=0 src=&quot;Images/save.gif&quot;&gt;"
															CancelText="Annuler" EditText="&lt;img border=0 src=&quot;Images/P_Draw_Lg_N.gif&quot;&gt;"></asp:EditCommandColumn>
														<asp:ButtonColumn Text="&lt;img border=0 src=&quot;Images/delete.gif&quot;&gt;" CommandName="Delete"></asp:ButtonColumn>
														<asp:BoundColumn Visible="False" DataField="row_id" SortExpression="row_id"></asp:BoundColumn>
														<asp:TemplateColumn HeaderText="&#201;labor&#233; par:">
															<HeaderStyle Width="100px"></HeaderStyle>
															<ItemTemplate>
																<asp:Label ID="lblPrenom" Text='<%# DataBinder.Eval(Container.DataItem, "prenom") %>' Runat="server"/>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox Width="150px" id="textboxPrenom" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.prenom") %>'>
																</asp:TextBox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Type d'intervenants">
															<HeaderStyle Wrap="False" Width="100px"></HeaderStyle>
															<ItemStyle Wrap="False"></ItemStyle>
															<ItemTemplate>
																<asp:Label ID="lblTitre" Visible=False Text='<%# DataBinder.Eval(Container.DataItem, "titre") %>' Runat="server"/>
																<asp:Label ID="lbltitreID" Text='<%# DataBinder.Eval(Container.DataItem, "titre") %>' Runat="server"/>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:Label ID="lblTitreHold" Visible=False Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "titre") %>' />
																<asp:DropDownList Width="150px" id="DropDownListIntervenant" DataSource="<%# BindListIntervenant() %>" OnPreRender="SetDropDownListIntervenant" DataTextField="description" DataValueField="description" runat="server"/>
															</EditItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
												</asp:datagrid></TD>
										</TR>
										<TR>
											<TD class="cbToolbar">Sources d'information utilis�es pour l'�laboration du PEI
											</TD>
										</TR>
										<TR>
											<TD><asp:checkboxlist id="CheckBoxListeSources" runat="server" Width="270px" CssClass="fldLabel" ForeColor="Black"></asp:checkboxlist></TD>
										</TR>
									</TABLE>
								</TD>
								<TD vAlign="top">
									<TABLE id="TableAnomalies" borderColor="#0066ff" cellSpacing="0" cellPadding="1" width="325px"
										border="0">
										<TR>
											<TD class="cbToolbar" id="tdAnomalies">Anomalies de l'�l�ve identifi� par un CIPR
											</TD>
										</TR>
										<TR>
											<TD><asp:radiobuttonlist id="RadioButtonListAnomalies" runat="server" Width="270px" BorderWidth="0px" BorderColor="White"
													CssClass="fldLabel" ForeColor="Black" RepeatColumns="1" Visible="False"></asp:radiobuttonlist></TD>
										</TR>
										<TR>
											<TD><asp:checkboxlist id="CheckBoxListAnomalies" runat="server" Width="273px" CssClass="fldLabel"></asp:checkboxlist></TD>
										</TR>
										<TR>
											<td><VwdCms:TitleCheckBoxList runat="server" ID="cblTitles"
     RepeatDirection="Vertical" RepeatColumns="1" 
     style="font-family:Arial;font-size:8pt;text-align:left;
     border:solid 0px #336699;" TitleWidth="120px" />
					                        </td>
										</TR>
										<TR>
											<TD class="cbToolbar">Raison&nbsp;justifiant l'�laboration du PEI</TD>
										</TR>
										<TR>
											<TD><asp:radiobuttonlist id="RadioButtonListRaisons" runat="server" Width="270px" 
                                                    CssClass="fldLabel" ForeColor="Black" AutoPostBack="True"></asp:radiobuttonlist></TD>
										</TR>
										<TR>
											<TD class="cbToolbar">Caract�ristiques de l'�l�ve non identifi�
											</TD>
										</TR>
										<TR>
											<TD><asp:textbox id="TextBoxCaracteristiquesEleve" runat="server" Width="288px" CssClass="fldTextBox"
													Height="75px" TextMode="MultiLine"></asp:textbox></TD>
										</TR>
										<TR>
											<TD><asp:radiobuttonlist id="Radiobuttonlist1" runat="server" Width="270px" CssClass="fldLabel" ForeColor="Black"></asp:radiobuttonlist></TD>
										</TR>
										<TR>
											<TD class="cbToolbar">Format de communication du rendement
											</TD>
										</TR>
										<TR>
											<TD><asp:checkboxlist id="CheckBoxListeFormat" runat="server" CssClass="fldLabel"></asp:checkboxlist></TD>
										</TR>
										<TR>
											<TD class="cbToolbar">
                                                Financement<br />
                                            </TD>
										</TR>
                                        <tr>
                                            <td style="height: 35px">
                                                <asp:CheckBox ID="CheckBoxFincancement" runat="server" CssClass="fldLabel" Text="Le dossier de l'�l�ve fait l'objet de financement suppl�mentaire aupr�s du minist�re de l'�ducation" /></td>
                                        </tr>
                                        <TR>
											<TD class="cbToolbar">
                                                Plan de s�curit�<br />
                                            </TD>
										</TR>
                                        <tr>
                                            <td style="height: 35px">
                                                <asp:CheckBox ID="CheckBoxPlanSecurite" runat="server" CssClass="fldLabel" Text="L'�l�ve a un plan de s�curit�" /></td>
                                        </tr>
									</TABLE>
								</TD>
								<TD vAlign="top" width="100%">
									<TABLE id="tableDate" borderColor="#006633" cellSpacing="0" cellPadding="1" width="100%"
										border="0">
										<TR>
											<TD class="cbToolbar" width="100%">Date d&#39;identification initiale:</TD>
										</TR>
										<TR>
											<TD  vAlign="bottom" align="left">
                                                <asp:TextBox ID="TxtDateIdentification" runat="server" CssClass="fldTextBox" ></asp:TextBox>
                                                <%--<a onclick="window.open('popup.aspx?textbox=TxtDateIdentification','cal','width=200,height=200,left=270,top=180')"
													href="javascript:;"><IMG src="images/SmallCalendar.gif" border="0">
                                                </a>--%>(jj/mm/aaaa) </TD>
										</TR>
										<TR>
											<TD vAlign="bottom" align="left" class="cbToolbar">Date du CIPR le plus r�cent 
                                                :</TD>
										</TR>
										<TR>
											<TD vAlign="bottom" align="left"><asp:textbox id="TxtDernierCipr" runat="server" CssClass="fldTextBox"></asp:textbox>
                                            <%--<a onclick="window.open('popup.aspx?textbox=TxtDernierCipr','cal','width=200,height=200,left=270,top=180')"
													href="javascript:;"><IMG src="images/SmallCalendar.gif" border="0"></a> --%>
                                                (jj/mm/aaaa)
											</TD>
										</TR>
										<TR>
											<TD class="cbToolbar">Date du d�but du placement&nbsp;(jj/mm/aaaa) :</TD>
										</TR>
										<TR>
											<TD><asp:textbox id="TxtDatePlacement" runat="server" CssClass="fldTextBox"></asp:textbox>
                                        <%--<a onclick="window.open('popup.aspx?textbox=TxtDatePlacement','cal','width=450,height=525,left=270,top=180')"
													href="javascript:;"><IMG src="images/SmallCalendar.gif" border="0"></a> --%>
                                                (jj/mm/aaaa)
											</TD>
										</TR>
										<TR>
											<TD class="cbToolbar" >�laboration du PEI termin�e le&nbsp; (jj/mm/aaaa) :</TD>
										</TR>
										<TR>
											<TD ><asp:textbox id="txtPeiDetermine" runat="server" CssClass="fldTextBox"></asp:textbox>
                                                <%--<a onclick="window.open('popup.aspx?textbox=txtPeiDetermine','cal','width=450,height=525,left=270,top=180')"
													href="javascript:;"><IMG src="images/SmallCalendar.gif" border="0"></a> --%>
                                                (jj/mm/aaaa)
											</TD>
										</TR>
										<TR>
											<TD></TD>
										</TR>
										<TR>
											<TD class="cbToolbar">Date de la renonciation&nbsp;(le cas �ch�ant) (jj/mm/aaaa) :</TD>
										</TR>
										<TR>
											<TD><asp:textbox id="txtDateRenonciation" runat="server" CssClass="fldTextBox"></asp:textbox>
                                            <%--<a onclick="window.open('popup.aspx?textbox=txtDateRenonciation','cal','width=450,height=525,left=270,top=180')"
													href="javascript:;"><IMG src="images/SmallCalendar.gif" border="0"></a> --%>
                                                (jj/mm/aaaa)
											</TD>
										</TR>
										<TR>
											<TD class="cbToolbar" id="TDdateRenonciation" runat="server">Dates 
												des&nbsp;bulletins&nbsp;
											    (jj/mm/aaaa) :</TD>
										</TR>
										<TR>
											<TD><asp:textbox id="txtDateBulletin1" runat="server" CssClass="fldTextBox"></asp:textbox>
                                    <%--<a onclick="window.open('popup.aspx?textbox=txtDateBulletin1','cal','width=250,height=325,left=270,top=180')"
													href="javascript:;"><IMG src="images/SmallCalendar.gif" border="0"></a>--%>(jj/mm/aaaa) </TD>
										</TR>
										<TR>
											<TD><asp:textbox id="txtDateBulletin2" runat="server" CssClass="fldTextBox"></asp:textbox>
                                <%--<a onclick="window.open('popup.aspx?textbox=txtDateBulletin2','cal','width=450,height=525,left=270,top=180')"
													href="javascript:;"><IMG src="images/SmallCalendar.gif" border="0"></a>--%>(jj/mm/aaaa) </TD>
										</TR>
										<TR>
											<TD><asp:textbox id="txtDateBulletin3" runat="server" CssClass="fldTextBox"></asp:textbox>
                                        <%--<a onclick="window.open('popup.aspx?textbox=txtDateBulletin3','cal','width=450,height=525,left=270,top=180')"
													href="javascript:;"><IMG src="images/SmallCalendar.gif" border="0"></a>--%>(jj/mm/aaaa) </TD>
										</TR>
										<TR>
											<TD><asp:textbox id="txtDateBulletin4" runat="server" CssClass="fldTextBox"></asp:textbox>
                                    <%--<a onclick="window.open('popup.aspx?textbox=txtDateBulletin4','cal','width=450,height=525,left=270,top=180')"
													href="javascript:;"><IMG src="images/SmallCalendar.gif" border="0"></a>--%>(jj/mm/aaaa) </TD>
										</TR>
										<TR>
											<TD class="cbToolbar" id="TD1" runat="server">D�sactivation
											</TD>
										</TR>
										<TR>
											<TD><asp:Button ID="ButtonDesactiver" runat="server" CssClass="fldTextBox" 
                                                    Text="Cliquez ici pour d�sactiver le pei" Height="57px" Width="203px" /><asp:Label
                                                        ID="LabelValidationRetirerEleve" runat="server" 
                                                    Text="Etes-vous certain de retirer l'�l�ve de la liste? " Visible="False"></asp:Label>
                                                <asp:CheckBox ID="CheckBoxValidationRetirerEleve"
                                                            runat="server" AutoPostBack="True" Text="Oui" Visible="False" /></TD>
										</TR>
										
									</TABLE>
								</TD>
							</TR>
						</TABLE>
						<asp:button id="btnEnregistrer" runat="server" CssClass="fldTextBox" Text="Enregistrer" Width="300px"></asp:button></TD>
				</TR>
				<tr>
					<td>
					    &nbsp;</td>
				</tr>
				<tr>
					<td><cc1:smartscroller id="SmartScroller1" runat="server"></cc1:smartscroller></td>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>
