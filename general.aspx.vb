Imports System
Imports System.Web
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient


Namespace PeiWebAccess

Partial Class general1
    Inherits System.Web.UI.Page
    'Protected WithEvents dsContactsList As PEIDAL.dsContactsList

#Region " Code g�n�r� par le Concepteur Web Form "

    'Cet appel est requis par le Concepteur Web Form.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.DsContactsList1 = New PEIDAL.dsContactsList
        CType(Me.DsContactsList1, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'DsContactsList1
        '
        Me.DsContactsList1.DataSetName = "dsContactsList"
        Me.DsContactsList1.Locale = New System.Globalization.CultureInfo("fr-CA")
        CType(Me.DsContactsList1, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Public strTypeContact As String
    Protected WithEvents DsContactsList1 As PEIDAL.dsContactsList
    Protected WithEvents dsResultatsConsultation1 As PEIDAL.dsResultatsConsultation


    'Protected WithEvents dsContactsList1 As PEIDAL.dsContactsList

    'REMARQUE�: la d�claration d'espace r�serv� suivante est requise par le Concepteur Web Form.
    'Ne pas supprimer ou d�placer.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN�: cet appel de m�thode est requis par le Concepteur Web Form
        'Ne le modifiez pas en utilisant l'�diteur de code.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Placez ici le code utilisateur pour initialiser la page

        errMsg.Visible = False

        Dim strEleveId As String = Session("eleveid")

        If Not Session("eleveid") <> "" Then
            Response.Redirect("listeeleves.aspx")
        End If

        If Not Page.IsPostBack Then
            Try
                    'BindDropDownListEcole(GetConseilID(Session("username")), DropDownListEcole)
                    BindDropDownListEcole("CSDCCS", DropDownListEcole)

                    'BindDropDownListEcole(GetConseilID(Session("username")), DropDownListEcoleFoyer)
                    'BindDropDownListEcole(GetConseilID(Page.User.Identity.Name), DropDownListEcoleFoyer)
                    BindDropDownListEcole("CSDCCS", DropDownListEcoleFoyer)

                'BindDropDownListContacts(strEleveId, DropDownListContacts)
                BindRadioButtonListNiveauEtude(ConfigurationSettings.AppSettings("ConnectionString"), RadioButtonListNiveaux)

                BindEleve(strEleveId)

                    If ConfigurationSettings.AppSettings("trillium") = "true" Then
                        BindEleveTrill(strEleveId)
                    Else
                        'txtPk_Eleve_id.Enabled = True
                        txtTitulaire.Enabled = True
                        'DropDownListEcole.Enabled = True
                        'DropDownListEcoleFoyer.Enabled = True
                        'RadioButtonListNiveaux.Enabled = True
                        'txtPrenom_Eleve.Enabled = True
                        'txtNom_Eleve.Enabled = True
                        'txtDate_Naissance.Enabled = True
                        'DropDownListSexe.Enabled = True
                    End If

                BindEcole(DropDownListEcole.SelectedValue)
                BindResultatsConsultation(strEleveId)
                'BindContacts(DropDownListContacts.SelectedValue)
                    BindDatagridContacts(strEleveId)
                    'BindContactsTrill(strEleveId)
                    BindContacts(strEleveId)


            Catch ex As Exception
                errMsg.Visible = True
                errMsg.Text = "Erreur survenu lors de l'affichage des informations de l'�leve. ---> " & ex.Message
            End Try
        End If

        'CreateJavaScriptConfirmationDeleteResultat()

        End Sub
        'Sub test(eleveid As String)
        '    Dim _context As New peireport.peiEntities

        '    Dim oEleve As New peireport.ELEVE

        '    oEleve = _context.ELEVES.Where(Function(e) e.pk_eleve_id = eleveId).FirstOrDefault

        '    Dim vbool As Boolean
        '    vbool = IIf(oEleve.RAPPORTS_EVALUATION.Count > 0, False, True)
        'End Sub

    Private Sub btnShowContact_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'PanelContacts.Visible = True
    End Sub

    Private Sub btnHideContacts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' PanelContacts.Visible = False
    End Sub

    'Sub DataGridContacts_EditCommand(ByVal s As Object, ByVal e As DataGridCommandEventArgs)
    '    DataGridContacts.EditItemIndex = e.Item.ItemIndex
    '    BindDatagridContacts(e.Item.ItemIndex)
    'End Sub
    Private Sub DataGridContacts_EditCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        DataGrid1.EditItemIndex = e.Item.ItemIndex
        'DataGridContacts.DataBind()
        BindDatagridContacts(Session("eleveid"))
    End Sub
    Sub BindResultatsConsultation(ByVal EleveID As String)
        Dim oGen As New PEIDAL.general

        Try
            DataGridResultatsConsultations.DataSource = oGen.GetListConsultationByEleveID(ConfigurationSettings.AppSettings("ConnectionString"), EleveID)
            DataGridResultatsConsultations.DataBind()

            'string that hold the javascript confirm message 
            Dim item As DataGridItem
            Dim confirm As String = "return confirm('Are you sure you want to delete?')"
            For Each item In DataGridResultatsConsultations.Items
                Dim DeleteBtn As New LinkButton
                'DeleteBtn=
                DeleteBtn.Attributes.Add("onclick", confirm)
            Next
            'foreach( DataGridItem item in dgItems.Items)
            '{
            ' LinkButton deleteBtn=(LinkButton)item.Cells[2].Controls[0];
            ' deleteBtn.Attributes.Add("onclick",confirm);
            '}

        Catch ex As Exception
            errMsg.Visible = True
            errMsg.Text = ex.Message
        End Try

    End Sub
    Sub BindDatagridContacts(ByVal EleveID As String)
        Dim oGen As PEIDAL.general = New PEIDAL.general
        'Dim ds As New DataSet
        'Dim dsContacts As New DataSet

        Try
            'Create ID column & add to DataGrid
            'AddDatagridCol(DataGridContacts, "ID", "ROW_ID", False)
            'AddDatagridCol(DataGridContacts, "Relation (p�re,m�re,grand-m�re...)", "TYPE_CONTACT", True)
            'AddDatagridCol(DataGridContacts, "Nom du contact", "NOM", True)
            'AddDatagridCol(DataGridContacts, "Adresse", "ADRESSE", True)
            'AddDatagridCol(DataGridContacts, "Tel. travail", "TEL_TRAVAIL", True)
            'AddDatagridCol(DataGridContacts, "Tel. maison", "TEL_MAISON", True)

            'dsContacts = oGen.GetInfosContactByRowID(ConfigurationSettings.AppSettings("ConnectionString"), RowID)
            DsContactsList1 = oGen.GetDsContactsListByElevedId(ConfigurationSettings.AppSettings("connectionString"), EleveID)
            'ds = oGen.GetDsContactsListByElevedId(EleveID)
            DataGrid1.DataSource = DsContactsList1
            'DataGrid1.DataKeys = "ROW_ID"

            'DataGrid1.DataSource = oGen.GetInfosContactByeEleveID(ConfigurationSettings.AppSettings("ConnectionString"), EleveID)
            DataGrid1.DataBind()

        Catch ex As Exception
            errMsg.Visible = True
            errMsg.Text = ex.Message
        End Try

        End Sub
        Sub BindContacts(eleveid As String)
            Dim ds As New DataSet
            Dim _sql As String
            Dim _contactid As String = ""
            Dim _adress As String = ""
            Dim _html As String = ""

            _sql = "SELECT * from VW_CONTACTS_INFO_TRILLIUM where person_id = '" & eleveid & "'"
            Try
                ds = GetDsOracle(_sql, ConfigurationSettings.AppSettings("connectionStringCSDCCS"))
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try


            For Each row As DataRow In ds.Tables(0).Rows
                If row("student_contact_id") <> _contactid Then
                    _html = _html & "<hr>"
                    _html = _html & row("contact_name").ToString & "(" & row("type_contact").ToString & ")"

                    If row("full_address").ToString <> "" Then
                        _html = _html & "<br><br>" & row("full_address").ToString & "<br>"
                    Else
                        _html = _html & "<br><br>* Il n'y a pas d'adresse active dans trillium pour ce contact" & "<br>"
                    End If

                End If

                If Not (IsDBNull(row("telecom_type"))) Then
                    If (row("telecom_type") = "Internet") Then
                        _html = _html & "<br>courriel: " & row("email_account").ToString
                    End If

                    If (row("telecom_type") = "Cell") Then
                        _html = _html & "<br>Tel. mobile: " & row("no_tel").ToString
                    End If
                    If (row("telecom_type") = "Home") Then
                        _html = _html & "<br>Tel. maison: " & row("no_tel").ToString
                    End If
                    If (row("telecom_type") = "Business") Then
                        _html = _html & "<br>Tel. travail: " & row("no_tel").ToString
                    End If
                End If

                
                _contactid = row("student_contact_id").ToString

            Next

            contacts.InnerHtml = _html

            'DataList1.DataSource = ds
            'DataList1.DataBind()


        End Sub
        'Sub BindContactsTrill(ByVal EleveId As String)
        '    Dim oGen As New PEIDAL.general
        '    Dim _ds As New DataSet
        '    Dim _ds2 As New DataSet


        '    _ds = oGen.GetTrilliumContactAdress(ConfigurationSettings.AppSettings("connectionString"), EleveId)

        '    For Each r As DataRow In _ds.Tables(0).Rows
        '        If r.Item("type_contact").ToString = "P�re" Then
        '            Me.nompere.InnerText = r.Item("nom").ToString
        '            'Me.adressepere.InnerText = Me.adressepere.InnerText & "&nbsp;" & r.Item("full_address").ToString
        '            Me.adressepere.InnerHtml = Me.adressepere.InnerHtml & r.Item("full_address").ToString & "<br>"
        '        ElseIf r.Item("type_contact").ToString = "M�re" Then
        '            Me.nommere.InnerText = r.Item("nom").ToString
        '            Me.adressemere.InnerHtml = Me.adressemere.InnerHtml & r.Item("full_address").ToString & "<br>"
        '        End If
        '    Next


        '    _ds2 = oGen.GetTrilliumContacPhone(ConfigurationSettings.AppSettings("connectionString"), EleveId)

        '    For Each r2 As DataRow In _ds2.Tables(0).Rows
        '        If r2.Item("type_contact").ToString = "P�re" Then
        '            Me.telpere.InnerHtml = Me.telpere.InnerHtml & r2.Item("telecom_type").ToString & ": " & r2.Item("no_tel").ToString & "<br>"
        '        ElseIf r2.Item("type_contact").ToString = "M�re" Then
        '            Me.telmere.InnerHtml = Me.telmere.InnerHtml & r2.Item("telecom_type").ToString & ": " & r2.Item("no_tel").ToString & "<br>"
        '        End If
        '    Next


        'End Sub

    Sub BindEleve(ByVal EleveId As String)
        'Dim oEleve As PEI.eleves = New PEI.eleves
        'Dim oAttente As PEIDAL.attente = New PEIDAL.attente
        Dim oEleve As PEIDAL.eleve = New PEIDAL.eleve
        Dim dsEleve As New DataSet
        'Dim dsAttente As New DataSet
        'Dim dsCommentaireGeneral As DataSet

        On Error Resume Next

        dsEleve = oEleve.GetEleveByID(ConfigurationSettings.AppSettings("ConnectionString"), EleveId)

        txtPk_Eleve_id.Text = dsEleve.Tables(0).Rows(0).Item("Pk_Eleve_id")
        txtTitulaire.Text = dsEleve.Tables(0).Rows(0).Item("titulaire")
        txtTitulaire.Text = dsEleve.Tables(0).Rows(0).Item("titulaire")
        DropDownListEcole.SelectedValue = dsEleve.Tables(0).Rows(0).Item("fk_ecole_id")
            SetSelectedValueDropDownList(DropDownListEcole, dsEleve.Tables(0).Rows(0).Item("fk_ecole_id"))
            'DropDownListEcoleFoyer.SelectedValue = dsEleve.Tables(0).Rows(0).Item("fk_ecole_foyer_id")
        'trim la valeur ds la bd
        RadioButtonListNiveaux.SelectedValue = Trim(dsEleve.Tables(0).Rows(0).Item("fk_niveau_id"))


            'If ConfigurationSettings.AppSettings("trillium") <> "true" Then

            txtPrenom_Eleve.Text = dsEleve.Tables(0).Rows(0).Item("prenom_eleve")
            txtNom_Eleve.Text = dsEleve.Tables(0).Rows(0).Item("nom_eleve")
            txtDate_Naissance.Text = dsEleve.Tables(0).Rows(0).Item("date_naissance")
            DropDownListSexe.SelectedValue = Trim(dsEleve.Tables(0).Rows(0).Item("sexe"))

            'End If

            'Catch ex As Exception
            'errMsg.Text = ex.Message
            'End Try

    End Sub
    Sub BindEleveTrill(ByVal EleveId As String)
        'Dim oEleve As PEI.eleves = New PEI.eleves
        'Dim oAttente As PEIDAL.attente = New PEIDAL.attente
        Dim oEleve As PEIDAL.eleve = New PEIDAL.eleve
        Dim dsEleve As New DataSet
        'Dim dsAttente As New DataSet
        'Dim dsCommentaireGeneral As DataSet

        Try
            'dsEleve = oEleve.GetEleveByID(ConfigurationSettings.AppSettings("ConnectionString"), EleveId)
            dsEleve = oEleve.GetListElevesTrill(ConfigurationSettings.AppSettings("connectionStringTrillium"), "", "", "", "", Date.MinValue, "", getPersonIDbyEleveID(EleveId))

            txtPrenom_Eleve.Text = dsEleve.Tables(0).Rows(0).Item("LEGAL_FIRST_NAME")
            txtNom_Eleve.Text = dsEleve.Tables(0).Rows(0).Item("LEGAL_SURNAME")

            ' ici c est le student_no
            TextBoxStudentNumber.Text = dsEleve.Tables(0).Rows(0).Item("STUDENT_NO")

            'PERSON_ID
            'txtPk_Eleve_id.Text = dsEleve.Tables(0).Rows(0).Item("PERSON_ID")

            txtDate_Naissance.Text = dsEleve.Tables(0).Rows(0).Item("TO_CHAR(PERSONS.BIRTH_DATE,'YYYY-MM-DD')")
            DropDownListSexe.SelectedValue = Trim(dsEleve.Tables(0).Rows(0).Item("GENDER"))

            'DropDownListEcole.SelectedValue = dsEleve.Tables(0).Rows(0).Item("fk_ecole_id")
            'SetSelectedValueDropDownList(DropDownListEcole, dsEleve.Tables(0).Rows(0).Item("fk_ecole_id"))
            'DropDownListEcoleFoyer.SelectedValue = dsEleve.Tables(0).Rows(0).Item("fk_ecole_foyer_id")

            'txtTitulaire.Text = dsEleve.Tables(0).Rows(0).Item("titulaire")

            'trim la valeur ds la bd
            'RadioButtonListNiveaux.SelectedValue = Trim(dsEleve.Tables(0).Rows(0).Item("fk_niveau_id"))

        Catch ex As Exception
            errMsg.Text = "Erreur lors de l'affichage des information de l'�l�ve (base de don�es trillium)" & ex.Message
            errMsg.Visible = True

        End Try

    End Sub

        Sub BindEcole(ByVal EcoleID As String)
            Dim oEcole As PEIDAL.ecole = New PEIDAL.ecole
            Dim dsEcole As New DataSet

            Try

                dsEcole = oEcole.GetInfosEcoleByEcoleID(ConfigurationSettings.AppSettings("ConnectionString"), EcoleID)
                txtNo_Identification.Text = dsEcole.Tables(0).Rows(0).Item("no_identification")
                txtDirecteur.Text = dsEcole.Tables(0).Rows(0).Item("directeur")
                txtAdresse.Text = dsEcole.Tables(0).Rows(0).Item("adresse")
                txtNo_Tel.Text = dsEcole.Tables(0).Rows(0).Item("no_tel")
                txtNo_Fax.Text = dsEcole.Tables(0).Rows(0).Item("no_fax")

            Catch ex As Exception
                errMsg.Visible = True
                errMsg.Text = ex.Message
            End Try

        End Sub

    Private Sub btnEnregistrer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            'UpdateEcole()
        UpdateEleve()

    End Sub
        'Sub UpdateEcole()

        '    'on ne devrais plus pouvoir faire la mise a jour des infos de l'�cole depuis l'interface utilisateur
        '    'donc ceci est d�sactiv�

        '    Dim oEcole As PEIDAL.ecole = New PEIDAL.ecole

        '    Try
        '        oEcole.UpdateEcole(ConfigurationSettings.AppSettings("ConnectionString"), DropDownListEcole.SelectedValue, "", txtNo_Identification.Text, txtDirecteur.Text, txtAdresse.Text, "", "", txtNo_Tel.Text, txtNo_Fax.Text)

        '    Catch ex As Exception
        '        errMsg.Visible = True
        '        errMsg.Text = ex.Message
        '    End Try

        'End Sub
    Sub UpdateEleve()
        Dim oEleve As PEIDAL.eleve = New PEIDAL.eleve

        Try
                'oEleve.UpdateDetailsEleve(ConfigurationSettings.AppSettings("ConnectionString"), UCase(txtPk_Eleve_id.Text), DropDownListEcole.SelectedValue, RadioButtonListNiveaux.SelectedValue, txtTitulaire.Text, txtPrenom_Eleve.Text, txtNom_Eleve.Text, DropDownListSexe.SelectedValue, "", "", "", "", "", "", txtDate_Naissance.Text, DropDownListEcoleFoyer.SelectedValue, "", "", "")
                oEleve.UpdateInfosGenerales(ConfigurationSettings.AppSettings("ConnectionString"), Replace(txtPk_Eleve_id.Text, "'", "''"), Replace(Me.txtNom_Eleve.Text, "'", "''"), Replace(Me.txtPrenom_Eleve.Text, "'", "''"), Me.DropDownListEcole.SelectedValue, Me.txtDate_Naissance.Text, Me.DropDownListSexe.SelectedValue, Me.DropDownListEcoleFoyer.SelectedValue, Me.RadioButtonListNiveaux.SelectedValue, Replace(Me.txtTitulaire.Text, "'", "''"))

        Catch ex As Exception
            errMsg.Visible = True
            errMsg.Text = ex.Message
        End Try

    End Sub

    Private Sub DropDownListEcole_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DropDownListEcole.SelectedIndexChanged
            BindEcole(DropDownListEcole.SelectedValue)
    End Sub

    Private Sub DropDownListContacts_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'BindContacts(DropDownListContacts.SelectedValue)
    End Sub

    Private Sub DataGrid1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Session("RowID") = DataGrid1.DataKeys(DataGrid1.SelectedIndex)
        Response.Redirect("ContactsDetails.aspx")
    End Sub
    Public Function BindTypeContact()
        Try
            'Dim oFB As New PEIDAL.forces_besoins
            Dim oEleve As New PEIDAL.eleve

            Return oEleve.GetTypeContacts(ConfigurationSettings.AppSettings("ConnectionString"))
            'Return oFB.GetTypeEval(ConfigurationSettings.AppSettings("ConnectionString"))

        Catch ex As Exception
            errMsg.Visible = True
            errMsg.Text = ex.Message
        End Try

    End Function
    Public Sub SetDropDownIndex(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ed As System.Web.UI.WebControls.DropDownList
        ed = sender
        ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strTypeContact))
    End Sub

    Private Sub DataGrid1_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.EditCommand
        DataGrid1.EditItemIndex = e.Item.ItemIndex
        strTypeContact = CType(e.Item.FindControl("lblTypeContact"), Label).Text

        BindDatagridContacts(Session("eleveid"))

    End Sub

    Private Sub DataGrid1_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.UpdateCommand
        Dim TypeContact, Nom, TelTravail, TelMaison, Adresse As String

        Dim Test As String

        ' Gets the value of the key field of the row being updated
        Test = DataGrid1.DataKeyField
        Test = DataGrid1.DataKeyField.ToString

        Dim key As String = DataGrid1.DataKeys(e.Item.ItemIndex).ToString()

        ' Gets get the value of the controls (textboxes) that the user
        ' updated. The DataGrid columns are exposed as the Cells collection.
        ' Each cell has a collection of controls. In this case, there is only one 
        ' control in each cell -- a TextBox control. To get its value,
        ' you copy the TextBox to a local instance (which requires casting)
        ' and extract its Text property.
        '
        ' The first column -- Cells(0) -- contains the Update and Cancel buttons.
        Dim tb As TextBox
        Dim temp As String

        Try

            TypeContact = CType(e.Item.FindControl("ddlTypeContact"), DropDownList).SelectedItem.Value

            ' Gets the value the TextBox control in the fourth column
            tb = CType(e.Item.Cells(4).Controls(0), TextBox)
            Nom = tb.Text

            ' Gets the value the TextBox control in the fourth column
            tb = CType(e.Item.Cells(5).Controls(0), TextBox)
            Adresse = tb.Text

            ' Gets the value the TextBox control in the fourth column
            tb = CType(e.Item.Cells(6).Controls(0), TextBox)
            TelMaison = tb.Text

            ' Gets the value the TextBox control in the fourth column
            tb = CType(e.Item.Cells(7).Controls(0), TextBox)
            TelTravail = tb.Text

            ' Finds the row in the dataset table that matches the 
            ' one the user updated in the grid. This example uses a 
            ' special Find method defined for the typed dataset, which
            ' returns a reference to the row.
            'SqlDataAdapter1.Fill(DsRapportsEvaluationList1)

            'Dim objFB As New PEIDAL.forces_besoins
            Dim oGen As New PEIDAL.general

            'DsRapportsEvaluationList1 = objFB.GetdsRapportsEvaluationList(Session("eleveid"))
            DsContactsList1 = oGen.GetDsContactsListByElevedId(ConfigurationSettings.AppSettings("connectionstring"), Session("eleveid"))

            'Dim r As PEIDAL.dsRapportsEvaluationList.RAPPORTS_EVALUATIONRow
            'Dim r As PEIDAL.dsContactsList.peisp_Contacts_SelectRow
            Dim r As PEIDAL.dsContactsList.CONTACTSRow
            'r = DsRapportsEvaluationList1.RAPPORTS_EVALUATION.FindByROW_ID(key)
            r = DsContactsList1.CONTACTS.FindByROW_ID(key)

            ' Updates the dataset table.
            r.TYPE_CONTACT = TypeContact
            r.NOM = Nom
            r.ADRESSE = Adresse
            r.TEL_MAISON = TelMaison
            r.TEL_TRAVAIL = TelTravail


            '    ' Calls a SQL statement to update the database from the dataset
            '    'SqlDataAdapter1.Update(DsRapportsEvaluationList1)
            '    'Dim oFB As New PEIDAL.forces_besoins

            oGen.UpdateDsContactsList(DsContactsList1)

            '    ' Takes the DataGrid row out of editing mode
            DataGrid1.EditItemIndex = -1

            '    ' Refreshes the grid
            BindDatagridContacts(Session("eleveid"))

        Catch ex As Exception
            '    lblMessage.Text = ex.Message

        End Try
    End Sub

    Private Sub btnAjouterContact_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


    End Sub

    Private Sub DataGrid1_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.DeleteCommand

        Dim Index As Integer
        Dim Key As Integer
        Dim Conn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        Key = DataGrid1.DataKeys(e.Item.ItemIndex).ToString


        'Dim oForcesBesoins As New PEIDAL.forces_besoins

        Try
            'Index = e.Item.ItemIndex
            Dim objCmd As New SqlCommand("delete from contacts where row_id=" & Key, Conn)
            objCmd.CommandType = CommandType.Text
            Conn.Open()
            objCmd.ExecuteNonQuery()
            Conn.Close()

            'oForcesBesoins.DeleteRaportEvalById(ConfigurationSettings.AppSettings("ConnectionString"), Index)
        Catch ex As Exception
            errMsg.Visible = True
            errMsg.Text = ex.Message

        End Try

        'DataGrid1.DataBind()
        BindDatagridContacts(Session("eleveid"))

    End Sub

    Private Sub DataGrid1_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.CancelCommand
        DataGrid1.EditItemIndex = -1
        BindDatagridContacts(Session("eleveid"))
    End Sub

    Private Sub ImageButtonNouveauContact_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonNouveauContact.Click
        'Dim objFB As New PEIDAL.forces_besoins
        Dim oGen As New PEIDAL.general

        'Dim ds As New PEIDAL.dsRapportsEvaluationList

        'DsRapportsEvaluationList1 = objFB.GetdsRapportsEvaluationList("az199511185_2")
        'BindDG()
        'DsRapportsEvaluationList1 = New PEIDAL.dsRapportsEvaluationList
        DsContactsList1 = New PEIDAL.dsContactsList

        'Dim dr As DataRow = DsRapportsEvaluationList1.RAPPORTS_EVALUATION.NewRow
        Dim dr As DataRow = DsContactsList1.CONTACTS.NewRow

        Try
            dr("type_contact") = ""
            dr("nom") = ""
            dr("adresse") = ""
            dr("tel_maison") = ""
            dr("tel_travail") = ""
            dr("fk_eleve_id") = Session("eleveid")

            'DsRapportsEvaluationList1.RAPPORTS_EVALUATION.Rows.InsertAt(dr, 0)
            DsContactsList1.CONTACTS.Rows.InsertAt(dr, 0)

            'ds.RAPPORTS_EVALUATION.Rows.InsertAt(dr, 0)

            'Session("ds") = DsRapEval1
            'SqlDataAdapter1.Update(DsRapportsEvaluationList1)
            'Dim oFB As New PEIDAL.forces_besoins
            'oFB.UpdateDsRapportsEvaluation(DsRapportsEvaluationList1)
            oGen.UpdateDsContactsList(DsContactsList1)
            'oFB.UpdateDsRapportsEvaluation(ds)

            DataGrid1.EditItemIndex = 0

            'DataGrid1.DataSource = objFB.GetdsRapportsEvaluationListOrderByDescRowID(Session("eleveid"), ConfigurationSettings.AppSettings("ConnectionString"))
            DataGrid1.DataSource = oGen.GetdsContactsListOrderByDescRowID(Session("eleveid"), ConfigurationSettings.AppSettings("ConnectionString"))
            DataGrid1.DataBind()

        Catch ex As Exception
            errMsg.Visible = True
            errMsg.Text = ex.Message
        End Try
    End Sub

    Private Sub ImageButtonSaveEleve_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Private Sub ImageButtonAjouterResultatConsultation_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAjouterResultatConsultation.Click
        dsResultatsConsultation1 = New PEIDAL.dsResultatsConsultation

        Dim dr As DataRow = dsResultatsConsultation1.RESULTATS_CONSULTATION.NewRow


        Try
            dr("date_consult") = Now
            dr("moyen_consult") = " "
            dr("resultat") = " "
            dr("fk_eleve_id") = Session("eleveid")

            dsResultatsConsultation1.RESULTATS_CONSULTATION.Rows.InsertAt(dr, 0)

            Dim oGen As New PEIDAL.general

            oGen.UpdateDsResultatsConsultation(ConfigurationSettings.AppSettings("ConnectionString"), dsResultatsConsultation1)

            DataGridResultatsConsultations.EditItemIndex = 0

            DataGridResultatsConsultations.DataSource = oGen.GetDataReaderResultatsConsultationOrderByDescRowId(Session("eleveid"), ConfigurationSettings.AppSettings("ConnectionString"))
            DataGridResultatsConsultations.DataBind()

        Catch ex As Exception
            errMsg.Visible = True
            errMsg.Text = ex.Message
        End Try
    End Sub

    Private Sub DataGridResultatsConsultations_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGridResultatsConsultations.UpdateCommand
        Dim Date_Consult As Date
        Dim Moyen_Consult, Resultat As String

        Dim key As String = DataGridResultatsConsultations.DataKeys(e.Item.ItemIndex).ToString()
        '
        ' The first column -- Cells(0) -- contains the Update and Cancel buttons.
        Dim tb As TextBox
        Dim temp As String

        Try

            ' Gets the value the TextBox control in the third column

            'strButDescription = CType(e.Item.FindControl("textboxDescription"), TextBox).Text
            'Date_Consult = CType(e.Item.FindControl("txtDate_Consult"), TextBox).Text
            tb = CType(e.Item.Cells(2).Controls(0), TextBox)
            If Not tb.Text <> "" Then
                Date_Consult = Date.Now.ToShortDateString
            Else
                    'Date_Consult = CDate(tb.Text).ToShortDateString
                    'Dim format() = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy"}
                    'Date_Consult = Date.ParseExact(tb.Text, format, System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.None)
                    Date_Consult = GetDateFormatted(tb.Text)
            End If

            Moyen_Consult = CType(e.Item.FindControl("txtMoyen_Consult"), TextBox).Text
            Resultat = CType(e.Item.FindControl("txtResultat"), TextBox).Text

            'Dim objFB As New PEIDAL.forces_besoins
            Dim oPlan As New PEIDAL.plan
            Dim oGen As New PEIDAL.general

            dsResultatsConsultation1 = oGen.GetdsResultatsConsultation(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

            Dim r As PEIDAL.dsResultatsConsultation.RESULTATS_CONSULTATIONRow

            r = dsResultatsConsultation1.RESULTATS_CONSULTATION.FindByROW_ID(key)

            r.NO_ORDER = 1 'must be set to NULL though
            r.DATE_CONSULT = Date_Consult
            r.MOYEN_CONSULT = Moyen_Consult
            r.RESULTAT = Resultat
            r.FK_ELEVE_ID = Session("eleveid")

            oGen.UpdateDsResultatsConsultation(ConfigurationSettings.AppSettings("ConnectionString"), dsResultatsConsultation1)

            '' Takes the DataGrid row out of editing mode
            DataGridResultatsConsultations.EditItemIndex = -1

            '' Refreshes the grid
            BindResultatsConsultation(Session("eleveid"))

        Catch ex As Exception
            errMsg.Visible = True
            errMsg.Text = ex.Message

        End Try
    End Sub

    Private Sub DataGrid1_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles DataGrid1.PageIndexChanged

    End Sub

    Private Sub DataGridResultatsConsultations_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGridResultatsConsultations.DeleteCommand
        Dim oGen As New PEIDAL.general
        Dim Index As Integer
        Dim Key As Integer
        Dim Conn As New SqlConnection(ConfigurationSettings.AppSettings("ConnectionString"))

        'DataGridResultatsConsultations.Attributes.Add("onClick", "return ConfirmDeletionResultat();")

        Key = DataGridResultatsConsultations.DataKeys(e.Item.ItemIndex).ToString

        Try
            oGen.DeleteResultatsConsultation(ConfigurationSettings.AppSettings("ConnectionString"), Key)

        Catch ex As Exception
            errMsg.Visible = True
            errMsg.Text = ex.Message

        End Try

        BindResultatsConsultation(Session("eleveid"))
    End Sub

    Private Sub DataGridResultatsConsultations_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGridResultatsConsultations.EditCommand
        DataGridResultatsConsultations.EditItemIndex = e.Item.ItemIndex
        'strTypeContact = CType(e.Item.FindControl("lblTypeContact"), Label).Text

        BindResultatsConsultation(Session("eleveid"))
    End Sub

        Private Sub btnEnregistrerInfosEleve_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnregistrerInfosEleve.Click
            'Response.Write("bouton 2")
            SaveInfosGenerales()
        End Sub

    Private Sub DataGridResultatsConsultations_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGridResultatsConsultations.CancelCommand
        DataGridResultatsConsultations.EditItemIndex = -1
        BindResultatsConsultation(Session("eleveid"))
    End Sub
    'Function GetConseilID(ByVal Username As String) As String

    '    Dim oEcole As New PEIDAL.ecole

    '    Try
    '        GetConseilID = Trim(oEcole.GetConseilIdByUtilisateurId(ConfigurationSettings.AppSettings("ConnectionString"), Username))
    '    Catch ex As Exception
    '        'errMsg.Visible = True
    '        'errMsg.Text = "Erreurs lors de la lecture de ConseilID"
    '        Throw New Exception("Erreurs lors de la lecture de ConseilID." & ex.Message)
    '    End Try

    'End Function
    Sub CreateJavaScriptConfirmationDeleteResultat()
        Dim empID As String = ""
        'empID = DropDownListAttentes.SelectedItem.Text
        '// Use a StringBuilder to append strings
        Dim sb As System.Text.StringBuilder = New System.Text.StringBuilder
        'System.Text.StringBuilder sb = new System.Text.StringBuilder();
        'sb.Append ("<script language=JavaScript> ");
        sb.Append("<script language=JavaScript> ")
        sb.Append("function ConfirmDeletionResultat() {")
        'sb.Append("return confirm('Etes-vous sur de vouloir supprimer le but suivant: " + empID + "');}")
        sb.Append("return confirm('Etes-vous sur de vouloir supprimer l attente s�lectionn�e ?');}")
        sb.Append("</script>")
        'string js = sb.ToString();
        Dim js As String = sb.ToString
        'If (!IsClientScriptBlockRegistered("ConfirmDeletion")) Then
        If (Not IsClientScriptBlockRegistered("clientScript")) Then
            RegisterClientScriptBlock("ConfirmDeletionResultat", js)
        End If

        'ImageButtonDeleteAttente.Attributes.Add("onClick", "return ConfirmDeletionResultat();")
        DataGridResultatsConsultations.Attributes.Add("onClick", "return ConfirmDeletionResultat();")
        'datagridresultatsconsultations
    End Sub
    Function getPersonIDbyEleveID(ByVal strEleveid As String) As String
        Dim oEleve As New PEIDAL.eleve
        Dim dsEleve1 As New PEIDAL.dsEleve
        Dim r As PEIDAL.dsEleve.ELEVESRow
        Dim PersonID As String

        Try
            dsEleve1 = oEleve.GetDsEleve(ConfigurationSettings.AppSettings("ConnectionString"), strEleveid)
            r = dsEleve1.ELEVES.FindBypk_eleve_id(strEleveid)

            Return r.fk_person_id.ToString

        Catch ex As Exception
            Throw New Exception("Erreur survenue dans 'getPersonIDbyEleveID'")
        End Try

        
    End Function

        Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
            'Response.Write("bouton 1")
            SaveInfosGenerales()
        End Sub
    Sub SaveInfosGenerales()
            'UpdateEcole()
        UpdateEleve()
    End Sub

    
End Class

End Namespace
