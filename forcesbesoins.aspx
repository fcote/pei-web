<%@ Register TagPrefix="cc1" Namespace="sstchur.web.SmartNav" Assembly="sstchur.web.SmartNav" %>
<%@ Page Language="vb" AutoEventWireup="false" ValidateRequest="false" SmartNavigation = "true" Inherits="PeiWebAccess.forcesbesoins" CodeFile="forcesbesoins.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="menu_top" Src="menu_top.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>forcesbesoins</title>
		<link href="styles.css" type="text/css" rel="stylesheet"/>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
        <link href="styles.css" rel="stylesheet" type="text/css" />
  
        
	</head>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE class="cbToolbar_2 tabBkgd_2" id="tableMain" borderColor="#ff3300" cellSpacing="0" cellPadding="0" width="100%" border="0">
					<TR>
						<TD colSpan="4"><uc1:menu_top id="Menu_top1" runat="server"></uc1:menu_top></TD>
					</TR>
					<TR>
						<TD borderColor="#0000ff"><FONT face="Arial" size="4">forces&nbsp;et besoins</FONT></TD>
					</TR>
					<TR>
						<TD class="fldLabel"  borderColor="#0000ff"><FONT size="2">Les 
								points forts et les besoins correspondent � la description dans l'�nonc� de 
								d�cision du CIPR (le cas �ch�ant) et sont fond�s sur les donn�es d'�valuation 
								pertinentes.</FONT></TD>
					</TR>
					<TR>
						<TD class="fldLabel"borderColor="#0000ff"><asp:label id="lblMsgErr" runat="server" BorderColor="Firebrick" BorderWidth="1px" BackColor="Khaki"
								CssClass="fldlabel" Visible="False" Width="100%"></asp:label></TD>
					</TR>
					<TR>
						<TD class="fldLabel" style="HEIGHT: 26px" borderColor="#0000ff"><asp:button id="Button1" runat="server" CssClass="fldTextBox" Text="Enregistrer"></asp:button></TD>
					</TR>
					<TR>
						<TD class="cbToolbar" style="height: 26px">
                            Forces</TD>
					</TR>
                <tr>
                    <td>
                        <table border="0" width="740">
                            <tr>
                                <td style="width: 280px" class="cbToolbar">
                                    Habilet�s cognitives</td>
                                <td style="width: 100px">
                                </td>
                                <td style="width: 280px" class="cbToolbar">
                                    D�veloppement personnel et social</td>
                                <td style="width: 5px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px; height: 21px;">
                                    <asp:DropDownList ID="DropDownListCategorieForces" runat="server" AutoPostBack="True" CssClass="fldLabel"
                                        Width="420px">
                                    </asp:DropDownList></td>
                                <td style="width: 100px; height: 21px;">
                                </td>
                                <td style="width: 100px; height: 21px;">
                                    <asp:DropDownList ID="DropDownList_F_DEV_PERS_Categories" runat="server" AutoPostBack="True" CssClass="fldLabel"
                                        Width="420px">
                                    </asp:DropDownList></td>
                                <td style="width: 5px; height: 21px;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px; height: 21px;">
                                    <asp:DropDownList ID="DropDownListContenusForces" runat="server" CssClass="fldLabel" Width="420px">
                                    </asp:DropDownList></td>
                                <td style="width: 100px; height: 21px;">
                                    <asp:ImageButton ID="ImageButtonAjouterContenu" runat="server" ImageUrl="images/create.gif" /></td>
                                <td style="width: 100px; height: 21px;">
                                    <asp:DropDownList ID="DropDownList_F_DEV_PERS_Contenus" runat="server" CssClass="fldLabel" Width="420px">
                                    </asp:DropDownList></td>
                                <td style="width: 5px; height: 21px;">
                                    <asp:ImageButton ID="ImageButtonAjouterContenuBesoins" runat="server" ImageUrl="images/create.gif" /></td>
                            </tr>
                            <tr>
                                <td style="width: 100px">
                                    <asp:TextBox ID="txtForces" runat="server" CssClass="fldTextBox" Height="180px" TextMode="MultiLine"
                                        Width="420px"></asp:TextBox></td>
                                <td style="width: 100px">
                                </td>
                                <td style="width: 100px">
                                    <asp:TextBox ID="TextBoxForcesDevPersonnel" runat="server" CssClass="fldTextBox" Height="180px" TextMode="MultiLine"
                                        Width="420px"></asp:TextBox></td>
                                <td style="width: 5px">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="normal">
                    </td>
                </tr>
					
					<tr>
						<td class="cbToolbar"  style="height: 26px">Besoins</td>
					</tr>
                <tr>
                    <td style="height: 26px">
                        <table border="0" width="740">
                            <tr>
                                <td style="width: 280px" class="cbToolbar">
                                    Habilet�s cognitives</td>
                                <td>
                                </td>
                                <td style="width: 280px" class="cbToolbar">
                                    D�veloppement personnel et social</td>
                                <td style="width: 5px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 280px">
                                    <asp:DropDownList ID="DropDownList_B_HAB_Categories" runat="server" CssClass="fldLabel" Width="420px" AutoPostBack="True">
                                    </asp:DropDownList></td>
                                <td>
                                </td>
                                <td class="cbToolbar" style="width: 280px">
                                    <asp:DropDownList ID="DropDownList_B_DEV_PERS_Categories" runat="server" Width="420px" AutoPostBack="True">
                                    </asp:DropDownList></td>
                                <td style="width: 5px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 280px">
                                    <asp:DropDownList ID="DropDownList_B_HAB_Contenus" runat="server" CssClass="fldLabel" Width="420px">
                                    </asp:DropDownList></td>
                                <td>
                                    <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="images/create.gif" /></td>
                                <td class="cbToolbar" style="width: 280px">
                                    <asp:DropDownList ID="DropDownList_B_DEV_PERS_Contenus" runat="server" CssClass="fldLabel" Width="420px">
                                    </asp:DropDownList></td>
                                <td style="width: 5px">
                                    <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="images/create.gif" /></td>
                            </tr>
                            <tr>
                                <td style="width: 100px">
                                    </td>
                                <td>
                                    </td>
                                <td style="width: 100px">
                                    </td>
                                <td style="width: 5px">
                                    </td>
                            </tr>
                            <tr>
                                <td style="width: 100px">
                                    <asp:TextBox ID="txtBesoins" runat="server" CssClass="fldTextBox" Height="180px" TextMode="MultiLine"
                                        Width="420px"></asp:TextBox></td>
                                <td>
                                </td>
                                <td style="width: 100px">
                                    <asp:TextBox ID="TextBoxBesoinsDevPersonnel" runat="server" CssClass="fldTextBox" Height="180px" TextMode="MultiLine"
                                        Width="420px"></asp:TextBox></td>
                                <td style="width: 5px">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="cbToolbar" colspan="2">
                        Besoins prioritaires</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table border="0" width="740">
                            <tr>
                                <td style="width: 280px" class="cbToolbar">
                                    Habilet�s cognitives</td>
                                <td>
                                </td>
                                <td style="width: 280px" class="cbToolbar">
                                    D�veloppement personnel et social</td>
                                <td style="width: 5px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px; height: 31px;">
                                    <asp:DropDownList ID="DropDownListBP_HAB_Categories" runat="server" AutoPostBack="True" CssClass="fldLabel"
                                        Width="420px">
                                    </asp:DropDownList></td>
                                <td style="width: 100px; height: 31px;">
                                </td>
                                <td style="width: 100px; height: 31px;">
                                    <asp:DropDownList ID="DropDownListBP_DEV_PERS_Categories" runat="server" CssClass="fldLabel" Width="420px" AutoPostBack="True">
                                    </asp:DropDownList></td>
                                <td style="width: 5px; height: 31px;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px">
                                    <asp:DropDownList ID="DropDownListBP_HAB_Contenus" runat="server" CssClass="fldLabel" Width="420px">
                                    </asp:DropDownList></td>
                                <td style="width: 100px">
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="images/create.gif" /></td>
                                <td style="width: 100px">
                                    <asp:DropDownList ID="DropDownListBP_DEV_PERS_Contenus" runat="server" CssClass="fldLabel" Width="420px">
                                    </asp:DropDownList></td>
                                <td style="width: 5px">
                                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="images/create.gif" /></td>
                            </tr>
                            <tr>
                                <td style="width: 100px">
                                    <asp:TextBox ID="TextBoxBesoinsPrioHabCogn" runat="server" CssClass="fldTextBox" Height="180px" TextMode="MultiLine"
                                        Width="420px"></asp:TextBox></td>
                                <td style="width: 100px">
                                </td>
                                <td style="width: 100px">
                                    <asp:TextBox ID="TextBoxBesoinsPrioDevPersonnel" runat="server" CssClass="fldTextBox" Height="180px" TextMode="MultiLine"
                                        Width="420px"></asp:TextBox></td>
                                <td style="width: 5px">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="fldLabel" colspan="2">
                    </td>
                </tr>
					<TR>
						<TD><asp:button id="Button2" runat="server" CssClass="fldTextBox" Text="Enregistrer"></asp:button></TD>
					</TR>
					<TR>
						<TD class="cbToolbar" style="height: 15px">Donn�es d'�valuation
							<asp:imagebutton id="ImageButtonAddDonneeEval" runat="server" ImageUrl="images/create.gif"></asp:imagebutton></TD>
					</TR>
					<TR>
						<TD class="fldLabel" style="HEIGHT: 12px"><STRONG>�num�rer les �valuations pertinentes, 
								y&nbsp;compris les �valuations&nbsp;�ducationnelles, les �valuations m�dicales 
								et de l'�tat de sant� (examen de la vue et de l'ou�e, examen&nbsp;physique, 
								neurologique), les �valuations psychologiques, de la parole&nbsp;et du langage, 
								les �valuations en &nbsp;physioth�rapie et en ergoth�rapie et du comportement</STRONG></TD>
					</TR>
					<TR>
						<TD><asp:datagrid id="DataGrid1" runat="server" BorderColor="#CCCCCC" BorderWidth="1px" BackColor="White"
								CssClass="fldLabel" Width="900px" Height="0px" DataMember="RAPPORTS_EVALUATION" DataKeyField="ROW_ID"
								BorderStyle="None" AutoGenerateColumns="False" CellPadding="3">
								<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
								<ItemStyle ForeColor="#000066"></ItemStyle>
								<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699"></HeaderStyle>
								<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
								<Columns>
									<asp:EditCommandColumn UpdateText="&lt;img border=0 src=&quot;Images/save.gif&quot;&gt;"
										CancelText="Annuler" EditText="&lt;img border=0 src=&quot;Images/P_Draw_Lg_N.gif&quot;&gt;"></asp:EditCommandColumn>
									<asp:ButtonColumn Text="&lt;img border=0 src=&quot;Images/delete.gif&quot;&gt;" CommandName="Delete"></asp:ButtonColumn>
									<asp:BoundColumn Visible="False" DataField="ROW_ID" SortExpression="ROW_ID" HeaderText="ROW_ID"></asp:BoundColumn>
									<asp:BoundColumn DataField="Date_Eval" SortExpression="DATE_EVAL" HeaderText="Date" DataFormatString="{0:yyyy/MM/dd}">
										<HeaderStyle Wrap="False"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:TemplateColumn HeaderText="Sources d'information" Visible="False">
										<HeaderStyle Wrap="False"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label ID="lblTypEval" Text='<%# DataBinder.Eval(Container.DataItem, "Type_Eval") %>' Runat="server"/>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label ID="lblTypeEvalHold" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Type_Eval") %>' />
											<asp:DropDownList id="ddlTypeEval" DataSource="<%# BindTypeEval() %>" OnPreRender="SetDropDownIndex" DataTextField="Type_Eval" DataValueField="Type_Eval" runat="server" />
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn DataField="Type_Eval" SortExpression="Type_Eval" HeaderText="Sources d'information">
										<HeaderStyle Wrap="False"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:TemplateColumn HeaderText="R&#233;sum&#233; des conclusions">
									<ItemTemplate>
										<asp:Label id="lblSommaire" runat="server" 
										Text='<%# DataBinder.Eval(Container, "DataItem.sommaire") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="Txtsommaire" TextMode=MultiLine Height=50px Width=300px runat="server" 
										Text='<%# DataBinder.Eval(Container, "DataItem.sommaire") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								</Columns>
								<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
							</asp:datagrid></TD>
					</TR>
					<TR>
						<TD><cc1:smartscroller id="SmartScroller1" runat="server"></cc1:smartscroller></TD>
					</TR>
				
			</TABLE>
	</FORM>
	</body>
</html>
