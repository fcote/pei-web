﻿Imports Telerik.Reporting.Processing
'Imports System
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient


Namespace PeiWebAccess

    Partial Class report2
        Inherits System.Web.UI.Page

        Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

            Me.lblErrMsg.Text = ""

            If Not Page.IsPostBack Then

                Dim ds As New DataSet
                Dim oEle As New PEIDAL.eleve

                'ds = oEle.GetEleveByID(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
                '_BindDropDownListEcoleByUserID(Page.User.Identity.Name)
                BindDropDownListEcoleByUserID(Page.User.Identity.Name, DropDownListEcoles)

                'Me.DropDownListEcoles.SelectedValue = Trim(ds.Tables(0).Rows(0).Item("fk_ecole_id").ToString)

                BindListeEleves(Me.DropDownListEcoles.SelectedValue)
                'Me.DropDownListEleves.SelectedValue = ds.Tables(0).Rows(0).Item("pk_eleve_id").ToString

                SetCheckbox(Me.DropDownListEleves.SelectedValue)
                'SetCheckBoxNew(Me.DropDownListEleves.SelectedValue)

                If (Session("etape")) <> "" Then
                    Me.DropDownListEtape.SelectedValue = Session("etape")
                End If

                'SetSessionVariables()
            End If

        End Sub
        Sub SetSessionVariables()
            Session("eleveid") = Me.DropDownListEleves.SelectedValue
            Session("infoseleve") = Me.DropDownListEleves.SelectedItem.ToString
            'Response.Write("personid: " & Session("eleveid"))
            Me.Menu_top1.fullname = Me.DropDownListEleves.SelectedItem.ToString
        End Sub

        Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click
            Dim rep As New peireport.pei


            If (Me.DropDownListEleves.SelectedValue) <> "" Then
                Dim peiname As String = ""
                'rep.peiname = RTrim(Me.DropDownListEcoles.SelectedValue) & "_" & RTrim(Me.DropDownListEleves.SelectedItem.Text) & "_" & RTrim(Me.DropDownListEleves.SelectedValue)
                rep.peiname = RTrim(Me.DropDownListEcoles.SelectedItem.Text) & "__" & Replace(Me.DropDownListEleves.SelectedItem.Text, " ", "") & "__" & RTrim(Me.DropDownListEleves.SelectedValue)
                rep.annee_scolaire = ConfigurationSettings.AppSettings("annee_scolaire")

                rep.etape = Me.DropDownListEtape.SelectedValue

                rep.donnees_generales = Me.CheckBoxList1.Items.FindByValue("donnees_generales").Selected
                rep.donnees_evaluations = Me.CheckBoxList1.Items.FindByValue("donnees_evaluations").Selected
                rep.ress_humaines = Me.CheckBoxList1.Items.FindByValue("ress_humaines").Selected
                rep.forces_besoins = Me.CheckBoxList1.Items.FindByValue("forces_besoins").Selected
                rep.matieres = Me.CheckBoxList1.Items.FindByValue("matieres").Selected
                rep.competences_habiletes = Me.CheckBoxList1.Items.FindByValue("competences_habiletes").Selected
                rep.adaptations = Me.CheckBoxList1.Items.FindByValue("adaptations").Selected
                rep.tests_provinciaux = Me.CheckBoxList1.Items.FindByValue("Eval_Prov").Selected
                rep.buts = Me.CheckBoxList1.Items.FindByValue("buts").Selected

                'rep.plan_transition = Me.CheckBoxList1.Items.FindByValue("plan_transition").Selected
                rep.plan_transition = False

                rep.plan_transition2 = Me.CheckBoxList1.Items.FindByValue("plan_transition2").Selected
                rep.details_consultations = Me.CheckBoxList1.Items.FindByValue("details_consultations").Selected
                rep.page_signature = Me.CheckBoxList1.Items.FindByValue("page_signature").Selected
                'rep.logo = "https://www.cscmonavenir.ca/publications/logo/cscmonavenir_logo-NOIR.png"

                rep.SetDataSource(Me.DropDownListEleves.SelectedValue)

                ExportToPDF(rep, rep.peiname)
            Else
                Me.lblErrMsg.Text = "Veuillez sélectionner l'élève"
            End If

        End Sub
        'Sub ExportToPDF(ByVal reportToExport As Telerik.Reporting.Report)

        '    Dim reportProcessor As New ReportProcessor()
        '    Dim instanceReportSource As New Telerik.Reporting.InstanceReportSource()
        '    instanceReportSource.ReportDocument = reportToExport
        '    Dim result As RenderingResult = reportProcessor.RenderReport("PDF", instanceReportSource, Nothing)

        '    Dim fileName As String = result.DocumentName + "." + result.Extension
        '    Response.Clear()
        '    Response.ContentType = result.MimeType
        '    Response.Cache.SetCacheability(HttpCacheability.Private)
        '    Response.Expires = -1
        '    Response.Buffer = True
        '    Response.AddHeader("Content-Disposition", String.Format("{0};FileName=""{1}""", "attachment", fileName))
        '    Response.BinaryWrite(result.DocumentBytes)
        '    Response.End()
        'End Sub
        Sub ExportToPDF(ByVal reportToExport As Telerik.Reporting.Report, docname As String)
            Dim reportProcessor As New Telerik.Reporting.Processing.ReportProcessor()
            Dim result As RenderingResult = reportProcessor.RenderReport("PDF", reportToExport, Nothing)

            'Dim fileName As String = result.DocumentName + ".pdf"
            Dim fileName As String = docname + ".pdf"

            Response.Clear()
            Response.ContentType = result.MimeType
            Response.Cache.SetCacheability(HttpCacheability.Private)
            Response.Expires = -1
            Response.Buffer = True
            Response.AddHeader("Content-Disposition", String.Format("{0};FileName=""{1}""", "attachment", fileName))
            Response.BinaryWrite(result.DocumentBytes)
            Response.End()
        End Sub


        Sub SetAllCheckBox(boolVal As Boolean)
            Dim _checked As Boolean
            _checked = Me.CheckBoxList1.Items(0).Selected

            For Each lstItem As ListItem In Me.CheckBoxList1.Items
                If lstItem.Enabled = True Then
                    lstItem.Selected = boolVal
                End If
            Next
        End Sub
        

   

        Sub SetDropDropListEcoleBySessionEleve()

            If Not (Session("eleveid")) <> "" Then
                Response.Redirect("listeeleves.aspx")
            End If

            Dim obj As New PEIDAL.eleve
            Dim ds As New System.Data.DataSet
            ds = obj.GetEleveByID(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            Dim ecole = ds.Tables(0).Rows(0).Item("fk_ecole_id").ToString

            For Each i As ListItem In DropDownListEcoles.Items
                If Not i.Value <> ecole Then
                    DropDownListEcoles.SelectedValue = i.Value
                End If
            Next
        End Sub

        Sub _BindDropDownListEcoleByUserID(ByVal username As String, Optional ByVal _ValueField As String = "pk_ecole_id")

            Dim _sql As String = String.Empty
            Dim _ds As New DataSet
            Dim oGen As New PEIDAL.general

            If _IsUserAdmin(username) Then
                'BindDropDownListEcoleByUserIdAdmin(username, ctlDropDownList, _ValueField)
                _sql = "SELECT E.pk_ecole_id,E.nom_ecole + ' ' + e.ville as ecole " & _
                    " FROM ECOLES E, UTILISATEURS_ECOLES AFF" & _
                    " WHERE(E.pk_ecole_id = AFF.FK_ECOLE_ID) " & _
                    " AND AFF.FK_UTILISATEUR_ID = '" & username & "' order by e.nom_ecole"
            Else
                '_BindDropDownListEcoleByUserID(username, ctlDropDownList, _ValueField)
                _sql = "SELECT E.pk_ecole_id,'* ' + E.nom_ecole + ' ' + e.ville as ecole " & _
                    " FROM ECOLES E, UTILISATEURS_ECOLES_EED AFF" & _
                    " WHERE(E.pk_ecole_id = AFF.FK_ECOLE_ID) " & _
                    " AND AFF.FK_UTILISATEUR_ID = '" & username & "' order by e.nom_ecole"

                _sql = "select distinct e.pk_ecole_id as pk_ecole_id,e.nom_ecole as ecole " & _
                    " from ECOLES e, VW_PEI_AFFECTATION aff" & _
                    " where aff.SCHOOL_CODE COLLATE DATABASE_DEFAULT = e.school_code COLLATE DATABASE_DEFAULT and aff.ldap_user = '" & username & "'"
            End If

            insertLog(ConfigurationSettings.AppSettings("ConnectionString"), "DEBUG", "PRINT", username, Me.DropDownListEleves.SelectedValue, "", Date.Now, "", "", "dropdownlist ecole SQL: " & _sql)

            _ds = oGen.GetDataset(_sql, ConfigurationSettings.AppSettings("ConnectionString"))
            DropDownListEcoles.DataSource = _ds
            DropDownListEcoles.DataValueField = "pk_ecole_id"
            DropDownListEcoles.DataTextField = "ecole"
            DropDownListEcoles.DataBind()

            SetDropDropListEcoleBySessionEleve()

        End Sub
        Function _IsUserAdmin(ByVal username As String) As Boolean
            Dim sql As String
            Dim ds As New DataSet
            Dim oGen As New PEIDAL.general

            sql = "select fk_utilisateur_id from utilisateurs_ecoles where fk_utilisateur_id = '" & username & "'"
            ds = oGen.GetDataset(sql, ConfigurationSettings.AppSettings("ConnectionString"))

            If ds.Tables(0).Rows.Count > 0 Then
                insertLog(ConfigurationSettings.AppSettings("ConnectionString"), "DEBUG", "PRINT", Session("user") & "/" & Page.User.Identity.Name, Me.DropDownListEleves.SelectedValue, "", Date.Now, "", "", "user " & Page.User.Identity.Name & " is ADMIN")
                Return True
            Else
                insertLog(ConfigurationSettings.AppSettings("ConnectionString"), "DEBUG", "PRINT", Session("user") & "/" & Page.User.Identity.Name, Me.DropDownListEleves.SelectedValue, "", Date.Now, "", "", "user " & Page.User.Identity.Name & " is not admin")
                Return False
            End If
        End Function
        Sub BindListeEleves(ByVal EcoleID As String)

            Dim oEleve As New PEIDAL.eleve
            Dim dr As SqlDataReader
            Dim ds As New DataSet
            Dim dt As New DataTable("ELEVES")
            Dim r As DataRow

            dt.Columns.Add("pk_eleve_id")
            dt.Columns.Add("fullname")

            Try
                dr = oEleve.GetListElevesByEcoleId(ConfigurationSettings.AppSettings("ConnectionString"), EcoleID)

                While dr.Read
                    r = dt.NewRow
                    r("pk_eleve_id") = dr.Item("pk_eleve_id")
                    r("fullname") = dr.Item("nom_eleve") & ", " & dr.Item("prenom_eleve") & " (" & dr.Item("description_niveau") & ") "
                    dt.Rows.Add(r)
                End While

                dt.AcceptChanges()

                Me.DropDownListEleves.DataSource = dt
                Me.DropDownListEleves.DataTextField = "fullname"
                Me.DropDownListEleves.DataValueField = "pk_eleve_id"
                Me.DropDownListEleves.DataBind()
                Me.DropDownListEleves.Items.Insert(0, New ListItem("Selectionnez l'élève", String.Empty))

                'Session("infoseleve") = Me.DropDownListEleves.SelectedValue
                'SetCheckbox()

                For Each i As ListItem In DropDownListEleves.Items
                    If Not i.Value <> Session("eleveid") Then
                        DropDownListEleves.SelectedValue = Session("eleveid")
                        SetCheckbox(Me.DropDownListEleves.SelectedValue)
                    End If
                Next



                'DisplayEleveToPrint()

            Catch ex As Exception
                Me.lblErrMsg.Text = "Erreure est survenue: " & ex.Message
            End Try

        End Sub
        'Sub SetCheckBoxNew(eleveId As String)
        '    Dim _context As New peireport.peiEntities

        '    Dim oEleve As peireport.ELEVE

        '    oEleve = _context.ELEVES.Where(Function(e) e.pk_eleve_id = eleveId).FirstOrDefault

        '    'Me.lblErrMsg.Text = "TEST nom eleve: " & oEleve.prenom_eleve & ", " & oEleve.nom_eleve

        '    Me.CheckBoxList1.Items.FindByValue("donnees_generales").Selected = True

        '    Me.CheckBoxList1.Items.FindByValue("donnees_evaluations").Selected = IIf(oEleve.RAPPORTS_EVALUATION Is Nothing, False, True)
        '    Me.CheckBoxList1.Items.FindByValue("ress_humaines").Selected = IIf(oEleve.RH_ELEVES Is Nothing, False, True)

        '    For Each forcesbesoins As peireport.FORCESBESOIN In oEleve.FORCESBESOINS.ToList
        '        If forcesbesoins.HAB_COGNITIVE <> "" Then
        '            Me.CheckBoxList1.Items.FindByValue("forces_besoins").Selected = True
        '        End If
        '    Next

        '    Me.CheckBoxList1.Items.FindByValue("matieres").Selected = IIf(oEleve.MATIERES_ELEVES Is Nothing, False, True)
        '    Me.CheckBoxList1.Items.FindByValue("adaptations").Selected = IIf(oEleve.SANTE_ELEVES Is Nothing, False, True)
        '    Me.CheckBoxList1.Items.FindByValue("Eval_Prov").Selected = IIf(oEleve.TESTS_PROVINCIAUX Is Nothing, False, True)
        '    Me.CheckBoxList1.Items.FindByValue("buts").Selected = IIf(oEleve.BUTS Is Nothing, False, True)
        '    Me.CheckBoxList1.Items.FindByValue("plan_transition").Selected = IIf(oEleve.BUTS_TRANSITION Is Nothing, False, True)
        '    Me.CheckBoxList1.Items.FindByValue("plan_transition2").Selected = IIf(oEleve.buts_transition2 Is Nothing, False, True)
        '    Me.CheckBoxList1.Items.FindByValue("details_consultations").Selected = IIf(oEleve.RESULTATS_CONSULTATION Is Nothing, False, True)

        '    Me.CheckBoxList1.Items.FindByValue("page_signature").Selected = True

        'End Sub
        Sub SetCheckbox(ByVal EleveID As String)

            'SetForceCheckBox(EleveID)
            SetDonneesEvaluations(EleveID)
            SetRH(EleveID)
            SetForceBesoinsNEW(EleveID)
            'setMatieresCheckbox(EleveID)
            SetMatieres(EleveID)
            setCompetences(EleveID)
            'SetAdaptationsCheckBox(EleveID)
            SetAdaptations(EleveID)
            SetEvalProvinciales(EleveID)
            SetButsAttente(EleveID)
            'SetButsTransition(EleveID)
            SetButsTransition2(EleveID)
            SetResultatsConsultations(EleveID)

            'page signature = true

        End Sub
        Sub SetButsTransition(EleveId As String)
            Dim _ds As New DataSet
            _ds = GetDataset("select * from buts_transition where fk_eleve_id='" & EleveId & "'")
            If (_ds.Tables(0).Rows.Count > 0) Then
                Me.CheckBoxList1.Items.FindByValue("plan_transition").Enabled = True
            Else
                Me.CheckBoxList1.Items.FindByValue("plan_transition").Enabled = False
            End If
        End Sub
        Sub SetButsTransition2(EleveId As String)
            Dim _ds As New DataSet
            ' _ds = GetDataset("select * from buts_transition2 where fk_eleve_id='" & EleveId & "'")
            _ds = GetDataset("select * from plan_intervention2 where fk_eleve_id='" & EleveId & "'")
            If (_ds.Tables(0).Rows.Count > 0) Then
                Me.CheckBoxList1.Items.FindByValue("plan_transition2").Enabled = True
            Else
                Me.CheckBoxList1.Items.FindByValue("plan_transition2").Enabled = False
            End If
        End Sub
        Sub SetResultatsConsultations(EleveId As String)
            Dim _ds As New DataSet
            _ds = GetDataset("select * from resultats_consultation where fk_eleve_id='" & EleveId & "'")
            If (_ds.Tables(0).Rows.Count > 0) Then
                Me.CheckBoxList1.Items.FindByValue("details_consultations").Enabled = True
            Else
                Me.CheckBoxList1.Items.FindByValue("details_consultations").Enabled = False
            End If
        End Sub
        Sub SetEvalProvinciales(EleveId As String)
            Dim _ds As New DataSet
            Dim _bool As Boolean = False
            _ds = GetDataset("select * from tests_provinciaux where fk_eleve_id='" & EleveId & "'")
            If (_ds.Tables(0).Rows.Count > 0) Then
                'Me.CheckBoxList1.Items.FindByValue("adaptations").Enabled = True
                If (_ds.Tables(0).Rows(0).Item("EVALUATIONS").ToString <> "") Then
                    _bool = True
                End If
                If (_ds.Tables(0).Rows(0).Item("ADAPTATIONS").ToString <> "") Then
                    _bool = True
                End If
                If (_ds.Tables(0).Rows(0).Item("EXEMPTIONS").ToString <> "") Then
                    _bool = True
                End If
                Me.CheckBoxList1.Items.FindByValue("Eval_Prov").Enabled = _bool
            Else
                Me.CheckBoxList1.Items.FindByValue("Eval_Prov").Enabled = False
            End If
        End Sub
        Sub SetAdaptations(EleveId As String)
            Dim _ds As New DataSet
            Dim _bool As Boolean = False
            _ds = GetDataset("select * from sante_eleves where fk_eleve_id='" & EleveId & "'")
            If (_ds.Tables(0).Rows.Count > 0) Then
                'Me.CheckBoxList1.Items.FindByValue("adaptations").Enabled = True
                If (_ds.Tables(0).Rows(0).Item("PROB_SANTE_CONN").ToString <> "") Then
                    _bool = True
                End If
                If (_ds.Tables(0).Rows(0).Item("RECOMM_PARTICULIERE").ToString <> "") Then
                    _bool = True
                End If
                If (_ds.Tables(0).Rows(0).Item("ADAPT_PEDAGOGIQUES").ToString <> "") Then
                    _bool = True
                End If
                If (_ds.Tables(0).Rows(0).Item("EQUIP_PERSONNALISE").ToString <> "") Then
                    _bool = True
                End If
                If (_ds.Tables(0).Rows(0).Item("ADAPT_ENVIRONNEMENTALES").ToString <> "") Then
                    _bool = True
                End If
                If (_ds.Tables(0).Rows(0).Item("ADAPT_MATIEREEVALUATION").ToString <> "") Then
                    _bool = True
                End If
                Me.CheckBoxList1.Items.FindByValue("adaptations").Enabled = _bool
            Else
                Me.CheckBoxList1.Items.FindByValue("adaptations").Enabled = False
            End If
        End Sub
        Sub SetRH(EleveId As String)
            Dim _ds As New DataSet
            _ds = GetDataset("select * from rh_eleves where fk_eleve_id='" & EleveId & "'")
            If (_ds.Tables(0).Rows.Count > 0) Then
                Me.CheckBoxList1.Items.FindByValue("ress_humaines").Enabled = True
            Else
                Me.CheckBoxList1.Items.FindByValue("ress_humaines").Enabled = False
            End If
        End Sub
        Sub SetDonneesEvaluations(EleveId As String)
            Dim _ds As New DataSet
            _ds = GetDataset("select * from rapports_evaluation where fk_eleve_id='" & EleveId & "'")
            If (_ds.Tables(0).Rows.Count > 0) Then
                Me.CheckBoxList1.Items.FindByValue("donnees_evaluations").Enabled = True
            Else
                Me.CheckBoxList1.Items.FindByValue("donnees_evaluations").Enabled = False
            End If
        End Sub
        Sub setCompetences(EleveId As String)
            Dim _ds As New DataSet
            _ds = GetDataset("select * from COMPETENCES_ELEVES where fk_eleve_id = '" & EleveId & "'")
            If (_ds.Tables(0).Rows.Count > 0) Then
                Me.CheckBoxList1.Items.FindByValue("competences_habiletes").Enabled = True
            Else
                Me.CheckBoxList1.Items.FindByValue("competences_habiletes").Enabled = False
            End If
        End Sub
        Sub SetMatieres(EleveId As String)
            Dim _ds As New DataSet
            _ds = GetDataset("select * from matieres_eleves where fk_eleve_id='" & EleveId & "'")
            If (_ds.Tables(0).Rows.Count > 0) Then
                Me.CheckBoxList1.Items.FindByValue("matieres").Enabled = True
            Else
                Me.CheckBoxList1.Items.FindByValue("matieres").Enabled = False
            End If
        End Sub

        Sub SetButsAttente(EleveId As String)
            Dim _ds As New DataSet
            _ds = GetDataset("select * from buts where fk_eleve_id='" & EleveId & "'")
            If (_ds.Tables(0).Rows.Count > 0) Then
                Me.CheckBoxList1.Items.FindByValue("buts").Enabled = True
            Else
                Me.CheckBoxList1.Items.FindByValue("buts").Enabled = False
            End If
        End Sub
        'Sub SetDonneeEvaluations()
        '    Dim oGen As New PEIDAL.general
        '    Dim _ds As New Data.DataSet
        '    Dim _sql As String

        '    _sql = "elect * from RAPPORTS_EVALUATION e where e.fk_eleve_id  = '" & ID & "'"
        '    _ds = oGen.GetDataset(_sql, ConfigurationSettings.AppSettings("ConnectionString"))

        '    'forces.Enabled = False
        '    Me.CheckBoxList1.Items.FindByValue("donnees_evaluations").Enabled = False

        '    For Each r As DataRow In _ds.Tables(0).Rows

        '        If r.Item("HAB_COGNITIVE").ToString <> "" Then
        '            Me.CheckBoxList1.Items.FindByValue("forces_besoins").Enabled = True
        '        End If
        '    Next
        'End Sub
        Sub SetForceBesoinsNEW(ByVal id As String)
            Dim oGen As New PEIDAL.general
            Dim _ds As New System.Data.DataSet
            Dim _sql As String

            _sql = "SELECT * from forcesbesoins where fk_eleve_id = '" & id & "'"
            _ds = oGen.GetDataset(_sql, ConfigurationSettings.AppSettings("ConnectionString"))

            'forces.Enabled = False
            Me.CheckBoxList1.Items.FindByValue("forces_besoins").Enabled = False

            For Each r As DataRow In _ds.Tables(0).Rows

                If r.Item("HAB_COGNITIVE").ToString <> "" Then
                    Me.CheckBoxList1.Items.FindByValue("forces_besoins").Enabled = True
                End If
            Next

        End Sub

        'Sub SetForceCheckBox(ByVal id As String)
        '    Dim oFB As New PEIDAL.forces_besoins
        '    Dim sqlReader As SqlDataReader
        '    Dim oFlag As Boolean

        '    Try
        '        sqlReader = oFB.GetForcesByEleveId(ConfigurationSettings.AppSettings("ConnectionString"), id)

        '        If sqlReader.HasRows Then
        '            Do While sqlReader.Read
        '                If sqlReader.GetString(0).ToString <> "" Then
        '                    forces.Enabled = True
        '                Else
        '                    forces.Enabled = False
        '                End If
        '            Loop
        '        Else
        '            forces.Enabled = False
        '        End If

        '        sqlReader.Close()

        '        sqlReader = oFB.GetBesoinsByEleveId(ConfigurationSettings.AppSettings("ConnectionString"), id)

        '        If sqlReader.HasRows Then
        '            Do While sqlReader.Read
        '                If sqlReader.GetString(0).ToString <> "" Then
        '                    forces.Enabled = True
        '                Else
        '                    forces.Enabled = False
        '                End If
        '            Loop
        '        Else
        '            forces.Enabled = False
        '        End If

        '        sqlReader.Close()

        '    Catch ex As Exception
        '        lblErrMsg.Visible = True
        '        lblErrMsg.Text = "Une erreur est survenue (case a coché points forts et besoins) " & ex.Message
        '    End Try

        'End Sub

        Sub setMatieresCheckbox(ByVal id As String)
            Dim oProg As New PEIDAL.programme
            Dim sqlReader As SqlDataReader

            Try
                sqlReader = oProg.GetReaderMatieres_ElevesOrderByDescRowID(id, ConfigurationSettings.AppSettings("ConnectionString"))

                If sqlReader.HasRows Then

                    '   matieres.Enabled = True

                    ''Do While sqlReader.Read
                    ''    If sqlReader.GetString(0).ToString <> "" Then
                    ''        matieres.Enabled = False
                    ''    Else
                    ''        matieres.Enabled = True
                    ''    End If
                    ''Loop
                Else
                    '  matieres.Enabled = False
                End If

            Catch ex As Exception
                lblErrMsg.Text = "Erreur survenu sur l'objet 'Matières'. " & ex.Message

            End Try

        End Sub
        Sub SetAdaptationsCheckBox(ByVal id As String)
            Dim oSante As New PEIDAL.sante
            Dim dsSanteEleve1 As PEIDAL.DsSanteEleves
            Dim r As PEIDAL.DsSanteEleves.SANTE_ELEVESRow

            Try
                dsSanteEleve1 = oSante.GetdsSanteEleves(ConfigurationSettings.AppSettings("ConnectionString"), id)

                If dsSanteEleve1.SANTE_ELEVES.Rows.Count < 1 Then
                    ' adaptations.Enabled = False
                Else
                    r = dsSanteEleve1.SANTE_ELEVES.FindByFK_ELEVE_ID(id)
                    If r.ADAPT_ENVIRONNEMENTALES <> "" Or r.ADAPT_MATIEREEVALUATION <> "" Or r.ADAPT_PEDAGOGIQUES <> "" Then
                        ' adaptations.Enabled = True
                    Else
                        '  adaptations.Enabled = False
                    End If
                End If

            Catch ex As Exception
                lblErrMsg.Text = "Erreur survenu sur l'objet 'Adaptations'. " & ex.Message
            End Try

        End Sub
        Sub SetDonneesEvaluation(ByVal id As String)
            Dim oFB As New PEIDAL.forces_besoins
            Dim dsDonneesEval1 As PEIDAL.dsRapportsEvaluationList
            'Dim r As PEIDAL.dsRapportsEvaluationList.RAPPORTS_EVALUATIONRow

            Try
                dsDonneesEval1 = oFB.GetdsRapportsEvaluationList(ConfigurationSettings.AppSettings("ConnectionString"), id)

                If dsDonneesEval1.RAPPORTS_EVALUATION.Rows.Count < 1 Then
                    ' evaluations.Enabled = False
                Else
                    ' evaluations.Enabled = True
                End If
            Catch ex As Exception
                lblErrMsg.Text = "Erreur survenu sur l'objet 'Évaluations'. " & ex.Message
            End Try

        End Sub
        Sub SetRHcheckBox(ByVal id As String)
            Dim oSante As New PEIDAL.sante
            Dim dsRHEleves1 As PEIDAL.DsRhEleves
            'Dim r As PEIDAL.DsRhEleves.RH_ELEVESRow

            Try
                dsRHEleves1 = oSante.GetdsRhEleves(ConfigurationSettings.AppSettings("ConnectionString"), id)

                If dsRHEleves1.RH_ELEVES.Count < 1 Then
                    ' rh.Enabled = False
                Else
                    ' rh.Enabled = True
                End If

            Catch ex As Exception

                lblErrMsg.Text = "Erreur survenu sur l'objet 'Ressources humaines'. " & ex.Message
            End Try

        End Sub
        Sub setButs_Transition(ByVal id As String)
            Dim oPlan As New PEIDAL.plan
            Dim DsButsTransition As PEIDAL.dsButsTransition

            Try
                DsButsTransition = oPlan.GetdsButsTransition(ConfigurationSettings.AppSettings("ConnectionString"), id)

                If DsButsTransition.BUTS_TRANSITION.Count < 1 Then
                    ' buts_transition.Enabled = False
                Else
                    '  buts_transition.Enabled = True
                End If


            Catch ex As Exception
                lblErrMsg.Text = "Erreur survenu sur l'objet 'Buts_Transition'. " & ex.Message

            End Try

        End Sub

        


        Protected Sub DropDownListEleves_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles DropDownListEleves.SelectedIndexChanged
            SetAllCheckBox(False)
            'DisplayEleveToPrint()
            SetCheckbox(Me.DropDownListEleves.SelectedValue)
            SetSessionVariables()
            Me.CheckBoxPrintAll.Checked = False
        End Sub

        Protected Sub DropDownListEcoles_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles DropDownListEcoles.SelectedIndexChanged
            BindListeEleves(Me.DropDownListEcoles.SelectedValue)
            SetAllCheckBox(False)
            'DisplayEleveToPrint()
            SetCheckbox(Me.DropDownListEleves.SelectedValue)
            SetSessionVariables()
            Me.CheckBoxPrintAll.Checked = False
        End Sub

        Protected Sub DropDownListEtape_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles DropDownListEtape.SelectedIndexChanged

        End Sub

        Protected Sub CheckBoxPrintAll_CheckedChanged(sender As Object, e As System.EventArgs) Handles CheckBoxPrintAll.CheckedChanged
            Dim _checked As Boolean
            _checked = Me.CheckBoxPrintAll.Checked
            SetAllCheckBox(_checked)
        End Sub
    End Class


End Namespace
