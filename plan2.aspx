﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="plan2.aspx.vb" Inherits="PeiWebAccess.plan2"  EnableSessionState="True"%>
<%@ Register TagPrefix="cc1" Namespace="sstchur.web.SmartNav" Assembly="sstchur.web.SmartNav" %>
<%@ Register TagPrefix="uc1" TagName="menu_top" Src="menu_top.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>plan</title>
		<LINK href="styles.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
            <div><uc1:menu_top id="Menu_top1" runat="server"></uc1:menu_top></div>
			<div>Plan de transition</div>
            <div>
        <asp:Label ID="lblMsgErr" runat="server" Font-Size="Larger" ForeColor="#CC3300"></asp:Label></div>
            <div>
                <div class="cbToolbar">Transitions, Buts, Mesures</div>
                <asp:DropDownList ID="DropDownListCategoriesTransition" 
                    runat="server" AutoPostBack="True" Width="600px" CssClass="fldLabel">
                </asp:DropDownList>
                <br />
                <br />
                <asp:DropDownList ID="DropDownListButs" 
                    runat="server" Width="600px" CssClass="fldLabel" AutoPostBack="True">
                </asp:DropDownList>
                <br />
                <br />
                <asp:DropDownList ID="DropDownListMesures" runat="server" CssClass="fldLabel" 
                    Width="600px">
                </asp:DropDownList>
                <asp:imagebutton id="ImageButtonAjouterBut" runat="server" 
                    ImageUrl="images/create.gif" style="width: 18px" Visible="False"></asp:imagebutton>
                <br />
                <br />
                <table>
                <tr><td>
                    <asp:Label ID="Label1" CssClass="fldLabel" runat="server" Text="Responsable(s)"></asp:Label></td>
                    <td><asp:Label ID="Label2" CssClass="fldLabel" runat="server" Text="Échéancier"></asp:Label></td></tr>
                <tr><td><asp:CheckBoxList ID="CheckBoxListResponsables" runat="server" 
                    CssClass="fldLabel">
                    <asp:ListItem>Aide-Enseignant</asp:ListItem>
                    <asp:ListItem>Direction</asp:ListItem>
                    <asp:ListItem>École</asp:ListItem>
                    <asp:ListItem>Éducateur spécialisé</asp:ListItem>
                    <asp:ListItem>Enseignante en surdité et surdité partielle</asp:ListItem>
                    <asp:ListItem>Enseignante titulaire</asp:ListItem>
                    <asp:ListItem>ER</asp:ListItem>
                    <asp:ListItem Value="ERRÉ">ERRÉ</asp:ListItem>
                    <asp:ListItem>Parent(s)</asp:ListItem>
                </asp:CheckBoxList></td>
                <td valign="top"><asp:CheckBoxList ID="CheckBoxListEcheancier" runat="server" 
                    CssClass="fldLabel">
                    <asp:ListItem>Fin de l&#39;année scolaire en cours</asp:ListItem>
                    <asp:ListItem>Juin à septembre</asp:ListItem>
                    <asp:ListItem>tout au long de l&#39;année scolaire en cours</asp:ListItem>
                    <asp:ListItem>Fin Août</asp:ListItem>
                    <asp:ListItem>Juin de l&#39;année en cours</asp:ListItem>
                </asp:CheckBoxList></td></tr>
                </table>
                
                
                <br />
                <asp:Button ID="Button1" runat="server" CssClass="fldLabel" 
                    Text="Ajouter la mesure sélectionnée" />
                <br />
                &nbsp;<br />
                <br />
            </div>
            <div>
                <div class="cbToolbar">Buts et mesures à prendre</div>
                <asp:datagrid id="DataGrid2" runat="server" Width="100%" BackColor="White" 
							DataKeyField="ROW_ID" CellPadding="3" BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC"
							AutoGenerateColumns="False" PageSize="20" CssClass="fldLabel">
							<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
							<ItemStyle ForeColor="#000066"></ItemStyle>
							<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699"></HeaderStyle>
							<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
							<Columns>
								
                                 <%--<asp:BoundColumn DataField="but_description" HeaderText="Buts Transition"></asp:BoundColumn>--%>

                                 <asp:TemplateColumn HeaderText="But transition">
									<ItemTemplate>
										<asp:Label id="lblbut_description" runat="server" Height=50px Width=300px Text='<%# DataBinder.Eval(Container, "DataItem.but_description") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label id="lblbut_description" runat="server" Height=50px Width=300px Text='<%# DataBinder.Eval(Container, "DataItem.but_description") %>'>
										</asp:Label>
									</EditItemTemplate>
								</asp:TemplateColumn>

                                <asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;img border=0 src=&quot;Images/save.gif&quot;&gt;"
									CancelText="Annuler" EditText="&lt;img border=0 src=&quot;Images/P_Draw_Lg_N.gif&quot;&gt;"></asp:EditCommandColumn>
								<asp:ButtonColumn Text="&lt;img border=0 src=&quot;Images/delete.gif&quot;&gt;" CommandName="Delete"></asp:ButtonColumn>

								<asp:TemplateColumn HeaderText="Mesures n&#233;cessaires">
									<ItemTemplate>
										<asp:Label id="lblMesures" runat="server"  Width="400px" Text='<%# DataBinder.Eval(Container, "DataItem.mesures") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="TxtMesures" TextMode="MultiLine" Width="400px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mesures") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Personnes responsables de ces mesures">
									<ItemTemplate>
										<asp:Label id="lblResonsables" runat="server"  Width="200px" Height="50px" Text='<%# DataBinder.Eval(Container, "DataItem.responsables") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="txtResponsables" TextMode="MultiLine" Width="200px" Height="50px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.responsables") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="&#201;ch&#233;anciers">
									<ItemTemplate>
										<asp:Label id="lblEcheancier" runat="server" Width="100px" Height="50px" Text='<%# DataBinder.Eval(Container, "DataItem.echeancier") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="txtEcheancier" TextMode="MultiLine" runat="server" Height="50px" Text='<%# DataBinder.Eval(Container, "DataItem.echeancier") %>' Width="100%">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
                <br />
                <br />
                <asp:datagrid id="DataGrid1" runat="server" Width="100%" 
							DataKeyField="ROW_ID"  BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC"
							AutoGenerateColumns="False" ForeColor="Black" BackColor="White" CellPadding="3" 
                            GridLines="Both" CellSpacing="1" 
                            OnItemDataBound="DataGrid1_ItemDataBound" CssClass="fldLabel" 
                    Visible="False">
                            <HeaderStyle font-bold="True"  ForeColor="White" BackColor="#006699"></HeaderStyle>
                            <ItemStyle backcolor="White"></ItemStyle>
							<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
							<Columns>
								<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;img border=0 src=&quot;Images/save.gif&quot;&gt;"
									CancelText="Annuler" EditText="&lt;img border=0 src=&quot;Images/P_Draw_Lg_N.gif&quot;&gt;"></asp:EditCommandColumn>
								<asp:ButtonColumn Text="&lt;img border=0 src=&quot;Images/delete.gif&quot;&gt;" CommandName="Delete"></asp:ButtonColumn>
                                
								<asp:TemplateColumn HeaderText="Buts et mesures n&#233;cessaires">
									<ItemTemplate>
										<asp:Label id="lblMesures0" runat="server" Height=20px Width=600px 
                                            Text='<%# DataBinder.Eval(Container, "DataItem.mesures") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="TxtMesures0" TextMode=MultiLine Height=70px Width=600px 
                                            runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mesures") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Personnes responsables de ces mesures">
									<ItemTemplate>
										<asp:Label id="lblResonsables0" runat="server" Width="200px" Height="20px" 
                                            Text='<%# DataBinder.Eval(Container, "DataItem.responsables") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="txtResponsables0" Width="200px" Height="50px" runat="server" 
                                            Text='<%# DataBinder.Eval(Container, "DataItem.responsables") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="&#201;ch&#233;anciers">
									<ItemTemplate>
										<asp:Label id="lblEcheancier0" runat="server" Width="100px" Height="20px" Text='<%# DataBinder.Eval(Container, "DataItem.echeancier") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="txtEcheancier0" runat="server" Height="50px" Text='<%# DataBinder.Eval(Container, "DataItem.echeancier") %>' Width="100%">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
                <br />
                <br />
            </div>
            
            <div>
                <asp:Panel ID="PanelButMesures" runat="server" CssClass="fldLabel">
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <div ID="mesure">
                        
                        <br />
                        <br />
                    </div>
                    
                </asp:Panel>
            </div>

    

		</form>
	    
	</body>
</HTML>
