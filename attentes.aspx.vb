Imports System.Data
Namespace PeiWebAccess

    Partial Class attentes
        Inherits System.Web.UI.Page

#Region " Code g�n�r� par le Concepteur Web Form "

        'Cet appel est requis par le Concepteur Web Form.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Dim strEleveId As String
        Dim butId As Integer
        Dim attNo As Integer


        'REMARQUE�: la d�claration d'espace r�serv� suivante est requise par le Concepteur Web Form.
        'Ne pas supprimer ou d�placer.

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN�: cet appel de m�thode est requis par le Concepteur Web Form
            'Ne le modifiez pas en utilisant l'�diteur de code.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Placez ici le code utilisateur pour initialiser la page
            'Response.Write("but id: " & DropDownListButs.SelectedValue & "    attente no: " & DropDownListAttentes.SelectedValue)

            lblMsgErr.Visible = False

            If Not (Session("eleveid")) <> "" Then
                Response.Redirect("listeeleves.aspx")
            End If
            'Dim strEleveId As String
            strEleveId = Session("eleveid")


            If Not Page.IsPostBack Then

                If (Session("etape")) <> "" Then
                    Me.DropDownListEtape.SelectedValue = Session("etape")
                End If

                'BindDropDownListeNiveauAttente(DropDownListNiveau)

                '--- pannel nouveau but
                'BindDropDownListeNiveauAttente(DropDownListNouveauButNiveauAttente)
                'BindRadioButtonListNiveauEtude(ConfigurationSettings.AppSettings("ConnectionString"), RadioButtonListNiveaux)
                '-------
                'panel infos buts
                BindDropDownListNiveauEtude(ConfigurationSettings.AppSettings("ConnectionString"), DropDownListAnneeEtude)
                BindDropDownListNiveauEtude(ConfigurationSettings.AppSettings("ConnectionString"), DropDownListNewButAnneeEtude)


                _BindDropDownListButs(strEleveId, DropDownListButs, 0)
                If DropDownListButs.SelectedValue <> "" Then
                    PanelInfosAttentes.Visible = True

                    If Request.QueryString("butid") <> "" Then
                        butId = Request.QueryString("butid")
                    Else
                        butId = DropDownListButs.SelectedValue
                    End If

                    bindInfosBut(butId)

                    BindDropDownListAttente(butId, 0)

                    BindDatagridAdaptations()

                Else
                    PanelInfosbutVisible()
                End If

                'DropDownListAdaptations.DataSource = oAttente.GetListContenus("ADAPTATION", "ADAPTATION")
                'DropDownListAdaptations.DataTextField = "CONTENU"
                'DropDownListAdaptations.DataValueField = "PK_CONTENU_ID"
                'DropDownListAdaptations.DataBind()

                Try
                    Dim oAttente As PEIDAL.attente = New PEIDAL.attente

                    DropDownListMethodes.DataSource = oAttente.GetListContenus("METHODE", "METHODE")
                    DropDownListMethodes.DataTextField = "CONTENU"
                    DropDownListMethodes.DataValueField = "PK_CONTENU_ID"
                    DropDownListMethodes.DataBind()

                Catch ex As Exception
                    lblMsgErr.Visible = True
                    lblMsgErr.Text = ex.Message
                End Try

                '----javascript --------------------
                'BindAttente()
                'string empID = "Jonathan Lurie";

            End If

            CreateJavaScriptConfirmationDelete()
            CreateJavaScriptConfirmationDeleteAttente()

        End Sub
        Sub _BindDropDownListButsNo(eleve_id As String)
            Dim oGen As New PEIDAL.general
            Dim _sql As String = "select distinct(but_no) from BUTS where fk_eleve_id = '" & eleve_id & "'"
            Dim _ds As DataSet

            _ds = oGen.GetDataset(_sql, ConfigurationSettings.AppSettings("ConnectionString"))
            Me.DropDownListButNo.DataSource = _ds
            Me.DropDownListButNo.DataTextField = "but_no"
            Me.DropDownListButNo.DataValueField = "but_no"
            Me.DropDownListButNo.DataBind()

        End Sub
        Sub _BindDropDownListButs(ByVal sEleveID As String, ByVal CTL As DropDownList, ByVal ind As Integer)
            Dim hasBut As Boolean

            _BindDropDownListButsNo(sEleveID)

            BindDropDownListButs(sEleveID, CTL, ind)
            BindDropDownListAttente(ind, 0)

            hasBut = CTL.Items.Count > 0
            LabelBut.Visible = Not hasBut
            PanelInfosBut.Visible = hasBut
            ImageButtonDeleteBut.Visible = hasBut

            If hasBut Then
                bindInfosBut(CTL.SelectedValue)
                LinkButtonNouvelleAttente.Visible = True
            Else
                PanelInfosBut.Visible = hasBut
                LinkButtonNouvelleAttente.Visible = False
            End If

        End Sub

        Sub CreateJavaScriptConfirmationDelete()
            Dim empID As String

            If DropDownListButs.Items.Count > 0 Then
                empID = DropDownListButs.SelectedItem.Text
                '// Use a StringBuilder to append strings
                Dim sb As System.Text.StringBuilder = New System.Text.StringBuilder
                'System.Text.StringBuilder sb = new System.Text.StringBuilder();
                'sb.Append ("<script language=JavaScript> ");
                sb.Append("<script language=JavaScript> ")
                sb.Append("function ConfirmDeletion() {")
                'sb.Append("return confirm('Etes-vous sur de vouloir supprimer le but suivant: " + empID + "');}")
                sb.Append("return confirm('Etes-vous sur de vouloir supprimer le but s�lectionn� ? (Tous les attentes rattach�es � ce but seront supprim�es �galement !)');}")
                sb.Append("</script>")
                'string js = sb.ToString();
                Dim js As String = sb.ToString
                'If (!IsClientScriptBlockRegistered("ConfirmDeletion")) Then
                If (Not IsClientScriptBlockRegistered("clientScript")) Then
                    RegisterClientScriptBlock("ConfirmDeletion", js)
                End If

                ImageButtonDeleteBut.Attributes.Add("onClick", "return ConfirmDeletion();")
            End If

        End Sub
        Sub CreateJavaScriptConfirmationDeleteAttente()
            Dim empID As String = ""

            If DropDownListAttentes.Items.Count > 0 Then
                empID = DropDownListAttentes.SelectedItem.Text
                '// Use a StringBuilder to append strings
                Dim sb As System.Text.StringBuilder = New System.Text.StringBuilder
                'System.Text.StringBuilder sb = new System.Text.StringBuilder();
                'sb.Append ("<script language=JavaScript> ");
                sb.Append("<script language=JavaScript> ")
                sb.Append("function ConfirmDeletionAttente() {")
                'sb.Append("return confirm('Etes-vous sur de vouloir supprimer le but suivant: " + empID + "');}")
                sb.Append("return confirm('Etes-vous sur de vouloir supprimer l attente s�lectionn�e ?');}")
                sb.Append("</script>")
                'string js = sb.ToString();
                Dim js As String = sb.ToString
                'If (!IsClientScriptBlockRegistered("ConfirmDeletion")) Then
                If (Not IsClientScriptBlockRegistered("clientScript")) Then
                    RegisterClientScriptBlock("ConfirmDeletionAttente", js)
                End If

                ImageButtonDeleteAttente.Attributes.Add("onClick", "return ConfirmDeletionAttente();")
            End If

        End Sub

        Sub BindDatagridAdaptations()
            Dim oAttente As PEIDAL.attente = New PEIDAL.attente

            Try

                'oBool = oAttente.IsMethodeExist(ConfigurationSettings.AppSettings("ConnectionString"), DropDownListButs.SelectedValue)
                'If oBool = True Then
                'DataGridAdaptations.Item()
                'If oAttente.GetCountAdaptationsByButId(ConfigurationSettings.AppSettings("ConnectionString"), DropDownListButs.SelectedValue) > 0 Then
                '    DataGridAdaptations.DataSource = oAttente.GetListAdaptationsByButID(ConfigurationSettings.AppSettings("ConnectionString"), DropDownListButs.SelectedValue)
                '    DataGridAdaptations.DataKeyField = "ID"
                'Else
                '    DataGridAdaptations.DataSource = Nothing
                'End If

                'DataGridAdaptations.DataBind()

                ''on active le bouton, un but existe
                'btnAjouterAdaptation.Enabled = True

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Erreur survenue lors de l'affichage des adaptations:" & ex.Message
            End Try



        End Sub

        Sub bindInfosBut(ByVal butId As Integer)
            Dim oButs As New PEIDAL.but
            Dim DsButs As New DataSet
            Dim strAnnee As String

            Try

                DsButs = oButs.GetDsButByButId(butId)

                ''--------------------------------
                ''bind dropdownlist but no
                'Me.DropDownListButNo.DataSource = DsButs
                'Me.DropDownListButNo.DataValueField = "but_no"
                'Me.DropDownListButNo.DataTextField = "but_no"
                'Me.DropDownListButNo.DataBind()
                Me.DropDownListButNo.SelectedValue = DsButs.Tables(0).Rows(0).Item("but_no").ToString
                ''--------------------------------


                If DsButs.Tables(0).Rows.Count < 1 Then
                    LabelBut.Visible = True
                    PanelInfosBut.Visible = False
                Else
                    PanelInfosBut.Visible = True
                    TextBoxProgramme.Text = DsButs.Tables(0).Rows(0).Item("programme")

                    'function replace appel�e ds les param�tre pass�e pour supprim� le text MOD si s agit d une attente modifi�
                    SetSelectedValueDropDownList(DropDownListAnneeEtude, Replace(DsButs.Tables(0).Rows(0).Item("annee_etude"), "MOD", ""))
                    strAnnee = DsButs.Tables(0).Rows(0).Item("annee_etude")
                    If InStr(strAnnee, "MOD") Then
                        CheckBoxAttenteMod2.Checked = True
                        'CheckBoxAttenteMod2.Visible = True
                    Else
                        CheckBoxAttenteMod2.Checked = False
                        'CheckBoxAttenteMod2.Visible = False
                    End If

                    TextBoxNiveauRendement.Text = DsButs.Tables(0).Rows(0).Item("niveau_rendement")
                    txtEditBut.Text = DsButs.Tables(0).Rows(0).Item("but_titre")
                End If
            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Errreur lors de l'affichage des informations du buts :" & ex.Message
            End Try

        End Sub

        Sub BindAttente()
            'Dim oEleve As PEI.eleves = New PEI.eleves
            Dim oAttente As PEIDAL.attente = New PEIDAL.attente
            Dim dsAttente As New DataSet
            Dim myCheck As Boolean

            'txtEditAttente.Text = DropDownListAttentes.SelectedItem.Text

            Try
                dsAttente = oAttente.GetAttente(DropDownListButs.SelectedValue, DropDownListAttentes.SelectedValue)

                'commentaire propre au but
                'dsCommentaireGeneral = oAttente.GetCommentairesGeneralesByBut(DropDownListButs.SelectedValue)

                'TextBoxProgramme.Text = dsAttente.Tables(0).Rows(0).Item("programme")
                'TextBoxNiveauRendement.Text = dsAttente.Tables(0).Rows(0).Item("niveau_rendement")

                'TextBoxAttente.Text = dsAttente.Tables(0).Rows(0).Item("attente")
                TextBoxAttente.Text = Server.HtmlDecode(dsAttente.Tables(0).Rows(0).Item("attente"))

                'TextBoxStrategie.Text = dsAttente.Tables(0).Rows(0).Item("strategie")
                TextBoxStrategie.Text = Server.HtmlDecode(dsAttente.Tables(0).Rows(0).Item("strategie"))

                'TextBoxMethode.Text = dsAttente.Tables(0).Rows(0).Item("methode")
                TextBoxMethode.Text = Server.HtmlDecode(dsAttente.Tables(0).Rows(0).Item("methode"))

                'SetSelectedValueDropDownList(DropDownListNiveau, dsAttente.Tables(0).Rows(0).Item("niveau_attente"))
                'SetSelectedValueDropDownList(DropDownListB1, dsAttente.Tables(0).Rows(0).Item("B1"))
                'txtB1.Text = Trim(dsAttente.Tables(0).Rows(0).Item("B1"))

                'SetSelectedValueDropDownList(DropDownListB2, dsAttente.Tables(0).Rows(0).Item("B2"))
                'txtB2.Text = Trim(dsAttente.Tables(0).Rows(0).Item("B2"))

                'SetSelectedValueDropDownList(DropDownListB3, dsAttente.Tables(0).Rows(0).Item("B3"))
                'txtB3.Text = Trim(dsAttente.Tables(0).Rows(0).Item("B3"))

                'SetSelectedValueDropDownList(DropDownListB4, dsAttente.Tables(0).Rows(0).Item("B4"))
                'txtB4.Text = Trim(dsAttente.Tables(0).Rows(0).Item("B4"))

                'Response.Write("<BR>COUNT ATTENTE: " & dsAttente.Tables(0).Rows.Count.MaxValue)

                'Dim tableName As String
                'Dim myCheck As Boolean

                'tableName = DropDownListEtape.SelectedValue
                'If tableName <> "" Then
                '    myCheck = IsDBNull(dsAttente.Tables(0).Rows(0).Item(tableName))
                '    If Not myCheck Then
                '        TextBoxCommentaireAttente.Text = CStr(dsAttente.Tables(0).Rows(0).Item(tableName))
                '    Else
                '        TextBoxCommentaireAttente.Text = ""
                '    End If

                '    myCheck = IsDBNull(dsCommentaireGeneral.Tables(0).Rows(0).Item(tableName))
                '    If Not myCheck Then
                '        TextBoxCommentaireGeneral.Text = dsCommentaireGeneral.Tables(0).Rows(0).Item(tableName)
                '    Else
                '        TextBoxCommentaireGeneral.Text = ""
                '    End If
                'End If

                'Dim oAtt As PEIDAL.attente = New PEIDAL.attente

                'CheckBoxListInter.DataSource = oAttente.GetListIntervenants
                'CheckBoxListInter.DataValueField = "pk_intervenant_id"
                'CheckBoxListInter.DataTextField = "pk_intervenant_id"
                'CheckBoxListInter.DataBind()

                myCheck = IsDBNull(dsAttente.Tables(0).Rows(0).Item("inter"))
                If Not myCheck Then
                    'SetListIntervenants(CheckBoxListInter, dsAttente.Tables(0).Rows(0).Item("inter"), DropDownListButs.SelectedValue)
                End If

                DisplayCommentaires() 'function inutile, commentaire par attente rejet�

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Une erreure est survenue lors de l'affichage de l'attente.<br>details de l'erreure: " & ex.Message
            End Try

            'TextBoxCaracteristiquesEleve.Text = dsEleve.Tables(0).Rows(0).Item("identifie_caracteristiques")
            'CheckBoxEleveIdentifie.Checked = dsEleve.Tables(0).Rows(0).Item("identifie")
        End Sub

        'Sub BindDropDownList(ByVal DDL As DropDownList, ByVal Value As String)
        '    Dim myCheck As Boolean
        '    Dim DropDownList As DropDownList
        '    Dim myVar As String = Value

        '    myVar = Replace(myVar, " ", "")
        '    DropDownList = DDL

        '    'myCheck = IsDBNull(Value)

        '    If myVar <> "" Then
        '        DropDownList.SelectedValue = myVar
        '    Else
        '        DropDownList.SelectedValue = DropDownList.Items(0).ToString
        '    End If

        'End Sub
        'Sub BindDropDownListButs()
        '    Dim oAttente As PEIDAL.attente = New PEIDAL.attente

        '    DropDownListButs.DataSource = oAttente.GetListButsByEleveID(Session("eleveid"))
        '    DropDownListButs.DataTextField = "but_titre"
        '    DropDownListButs.DataValueField = "pk_but_id"
        '    DropDownListButs.DataBind()
        'End Sub
        Sub BindDropDownListAttente(ByVal ButID As Integer, ByVal SelectedValue As Integer)
            Dim oAttente As PEIDAL.attente = New PEIDAL.attente

            ' DropDownListAttentes.Items.Clear()

            DropDownListAttentes.DataSource = oAttente.GetAttenteByButID(ButID)
            DropDownListAttentes.DataTextField = "attente"
            DropDownListAttentes.DataValueField = "attente_no"
            DropDownListAttentes.DataBind()

            If oAttente.GetCountAttentesByButID(ButID) > 0 Then
                ShowDetailsAttente(True)
                PanelInfosAttentes.Visible = True

                If SelectedValue <> 0 Then
                    'attNo = Request.QueryString("attno")
                    DropDownListAttentes.SelectedValue = SelectedValue
                End If

                'ImageButtonDeleteAttente.Enabled = True
                ImageButtonDeleteAttente.Visible = True

                BindAttente()

            Else
                ShowDetailsAttente(False)
                ClearDataAttentes()
                'ImageButtonDeleteAttente.Enabled = False
                ImageButtonDeleteAttente.Visible = False
                PanelInfosAttentes.Visible = False
                LabelAttente.Visible = True
            End If


        End Sub
        Sub ShowDetailsAttente(ByVal vBool As Boolean)

            LabelAttente.Visible = Not vBool

            DropDownListAttentes.Visible = vBool
            TextBoxAttente.Visible = vBool
            TextBoxStrategie.Visible = vBool
            TextBoxMethode.Visible = vBool
            DropDownListMethodes.Visible = vBool
            ImageButtonAjouterMethode.Visible = vBool

            bntEnregistrerAttente.Visible = vBool
            btnAnnulerAttente.Visible = vBool

        End Sub

        Private Sub DropDownListButs_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DropDownListButs.SelectedIndexChanged
            bindInfosBut(DropDownListButs.SelectedValue)
            BindDropDownListAttente(DropDownListButs.SelectedValue, 0)
            BindDatagridAdaptations()
            'BindAttente()
        End Sub

        Private Sub DropDownListEtape_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DropDownListEtape.SelectedIndexChanged
            'Dim oAttente As PEIDAL.attente = New PEIDAL.attente
            'Dim ds As New DataSet
            'Dim tableName As String
            'Dim myCheck As Boolean

            Session("etape") = Me.DropDownListEtape.SelectedValue

            DisplayCommentaires()

            'Try
            '    tableName = DropDownListEtape.SelectedValue

            '    If tableName <> "" Then

            '        ds = oAttente.GetAttente(DropDownListButs.SelectedValue, DropDownListAttentes.SelectedValue)
            '        myCheck = IsDBNull(ds.Tables(0).Rows(0).Item(tableName))
            '        If Not myCheck Then
            '            TextBoxCommentaireAttente.Text = ds.Tables(0).Rows(0).Item(tableName)
            '        Else
            '            TextBoxCommentaireAttente.Text = ""
            '        End If

            '        ds = oAttente.GetCommentairesGeneralesByBut(DropDownListButs.SelectedValue)
            '        myCheck = IsDBNull(ds.Tables(0).Rows(0).Item(tableName))
            '        If Not myCheck Then
            '            TextBoxCommentaireGeneral.Text = ds.Tables(0).Rows(0).Item(tableName)
            '        Else
            '            TextBoxCommentaireGeneral.Text = ""
            '        End If

            '    End If
            'Catch ex As Exception
            '    MsgErr.InnerText = ex.Message
            'End Try

        End Sub

        Private Sub btnEnregistrer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

            'BindAttente()
            'BindDropDownListAttente()

        End Sub
        Private Sub DropDownListAttentes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DropDownListAttentes.SelectedIndexChanged
            BindAttente()
        End Sub
        Private Sub btnAjouterMethode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            'Dim oAtt As PEIDAL.attente = New PEIDAL.attente
            'oAtt.InsertMethodeByButId(ConfigurationSettings.AppSettings("ConnectionString"), DropDownListButs.SelectedValue, DropDownListMethodes.SelectedItem.Text)
            'BindDatagridMethodes()

            'TextBoxMethode.Text = TextBoxMethode.Text & Chr(13) & DropDownListMethodes.SelectedItem.Text

        End Sub

        Private Sub btnAjouterAdaptation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            'Dim oAtt As PEIDAL.attente = New PEIDAL.attente
            'oAtt.InsertAdaptationByButId(ConfigurationSettings.AppSettings("ConnectionString"), DropDownListButs.SelectedValue, DropDownListAdaptations.SelectedItem.Text)
            'BindDatagridAdaptations()
        End Sub

        Private Sub DataGridAdaptations_DeleteCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
            Dim au_fnameCell As TableCell = e.Item.Cells(0)
            Dim itemIndex As Integer
            Dim index As String
            Dim index2 As String

            Try

                Dim oAtt As PEIDAL.attente = New PEIDAL.attente

                'index2 = e.Item.Cells(0).Text
                index = e.Item.Cells(1).Text
                oAtt.DeleteAdaptationById(ConfigurationSettings.AppSettings("ConnectionString"), index)

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "erreur lors de la suppression d'une adaptation..."
            End Try

            BindDatagridAdaptations()
        End Sub

        Private Sub btnEleve_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Response.Redirect("general.aspx")
        End Sub

        Private Sub btnSupprimerAttente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        End Sub

        Private Sub btnSupprimerBut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Dim oAtt As New PEIDAL.attente
            Dim oButs As New PEIDAL.but

            Try
                oAtt.DeleteAttentesByButId((ConfigurationSettings.AppSettings("ConnectionString")), DropDownListButs.SelectedValue)
                oButs.DeleteButByButId((ConfigurationSettings.AppSettings("ConnectionString")), DropDownListButs.SelectedValue)

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Une erreure est survenue lors de la suppression du but " & DropDownListButs.SelectedValue & _
                " Details de l'erreur: " & ex.Message
            End Try

            _BindDropDownListButs(strEleveId, DropDownListButs, 0)
            BindDropDownListAttente(DropDownListButs.SelectedValue, 0)

        End Sub

        Sub ClearDataAttentes()
            'DropDownListAttentes.ClearSelection
            'txtEditAttente.Text = ""
            TextBoxAttente.Text = ""
            'DropDownListNiveau.ClearSelection()
            TextBoxStrategie.Text = ""
            TextBoxMethode.Text = ""
            'CheckBoxListInter.ClearSelection()
            'DropDownListB1.ClearSelection()
            'DropDownListB2.ClearSelection()
            'DropDownListB3.ClearSelection()
            'DropDownListB4.ClearSelection()
            ' DropDownListEtape.ClearSelection()
            'TextBoxCommentaireAttente.Text = ""
            TextBoxCommentaireGeneral.Text = ""

        End Sub

        Private Sub btnNouveauBut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        End Sub

        Private Sub btnNouvelleAttente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            'Dim butId As Integer
            ''butId = DropDownListButs.SelectedValue

            'txtNouvelleAttente.Text = ""

            'PanelNouvelleAttente.Visible = True

            'Response.Redirect("newattente.aspx?butid=" & butId)
            'Server.Transfer("newattente.aspx?butid=" & butId)

        End Sub

        Private Sub DataGridAdaptations_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        End Sub
        Private Sub ImageButtonDeleteBut_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonDeleteBut.Click
            Dim oAtt As New PEIDAL.attente
            Dim oButs As New PEIDAL.but

            Try
                oAtt.DeleteAttentesByButId((ConfigurationSettings.AppSettings("ConnectionString")), DropDownListButs.SelectedValue)
                oButs.DeleteButByButId((ConfigurationSettings.AppSettings("ConnectionString")), DropDownListButs.SelectedValue)

                '======================
                ReOrderButs(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
                '======================

                _BindDropDownListButs(strEleveId, DropDownListButs, 0)
                If DropDownListButs.Items.Count > 0 Then
                    bindInfosBut(DropDownListButs.SelectedValue)
                    BindDropDownListAttente(DropDownListButs.SelectedValue, 0)
                End If
                
                'Try
                '    oAtt.DeleteAttenteByButIdAndAttenteNo(ConfigurationSettings.AppSettings("ConnectionString"), DropDownListButs.SelectedValue, DropDownListAttentes.SelectedValue)
                'Catch ex As Exception
                '    lblMsgErr.Visible = True
                '    lblMsgErr.Text = "Erreur survenue lors de la suppression de l'attente, contactez l'administrateur du syst�me"
                'End Try

                'BindDropDownListAttente(DropDownListButs.SelectedValue)


               

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Une erreure est survenue lors de la suppression du but " & DropDownListButs.SelectedValue & _
                " Details de l'erreur: " & ex.Message
            End Try

        End Sub

        Private Sub ImageButtonDeleteAttente_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonDeleteAttente.Click
            Dim oAtt As New PEIDAL.attente
            ' Dim oButs As New PEIDAL.but

            Try
                'oAtt.DeleteAttentesByButId((ConfigurationSettings.AppSettings("ConnectionString")), DropDownListButs.SelectedValue)
                oAtt.DeleteAttenteByButIdAndAttenteNo(ConfigurationSettings.AppSettings("ConnectionString"), DropDownListButs.SelectedValue, DropDownListAttentes.SelectedValue)

                BindDropDownListAttente(DropDownListButs.SelectedValue, 0)
                'BindDropDownListAttente(DropDownListButs.SelectedValue, 0)

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Une erreure est survenue lors de la suppression de l'attente " & DropDownListAttentes.SelectedItem.Text & _
                " Details de l'erreur: " & ex.Message
            End Try

            'BindDropDownListButs(strEleveId, DropDownListButs)

        End Sub
        Sub DisplayCommentaires()
            Dim oAttente As New PEIDAL.attente
            Dim dsCommentaireGeneral As DataSet

            Try
                'commentaire propre au but
                dsCommentaireGeneral = oAttente.GetCommentairesGeneralesByBut(DropDownListButs.SelectedValue)

                Dim tableName As String
                Dim myCheck As Boolean

                'tableName = DropDownListEtape.SelectedValue
                tableName = "commentaire" & Me.DropDownListEtape.SelectedValue

                If tableName <> "" Then
                    '----------------------------------------
                    ' code pour afficher les commentaires propre � l'attente 

                    'myCheck = IsDBNull(dsAttente.Tables(0).Rows(0).Item(tableName))
                    'If Not myCheck Then
                    '    TextBoxCommentaireAttente.Text = CStr(dsAttente.Tables(0).Rows(0).Item(tableName))
                    'Else
                    '    TextBoxCommentaireAttente.Text = ""
                    'End If
                    '------------------------------------------

                    myCheck = IsDBNull(dsCommentaireGeneral.Tables(0).Rows(0).Item(tableName))
                    If Not myCheck Then
                        TextBoxCommentaireGeneral.Text = dsCommentaireGeneral.Tables(0).Rows(0).Item(tableName)
                    Else
                        TextBoxCommentaireGeneral.Text = ""
                    End If
                End If

            Catch ex As Exception

                lblMsgErr.Visible = True
                lblMsgErr.Text = "Erreur lors de l'affichage des commentaire. Details --> " & ex.Message
            End Try

        End Sub

        Private Sub ImageButtonSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        End Sub

        Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
            Dim CurrentBut As Integer

            Try
                'CurrentBut = InsertNewBut(Session("eleveid"), Trim(txtNouveauBut.Text), Trim(TxtNouveauButProgramme.Text), Trim(txtNouveauButNiveauRendementActuel.Text), "", Trim(DropDownListNewButAnneeEtude.SelectedValue) & " " & CheckBoxAttenteMod.Text)
                CurrentBut = InsertNewBut(Session("eleveid"), Trim(txtNouveauBut.Text), Trim(TxtNouveauButProgramme.Text), Trim(txtNouveauButNiveauRendementActuel.Text), "", Trim(DropDownListNewButAnneeEtude.SelectedValue))

                'txtEditBut.Text = ""
                'txtEditAttente.Text = ""

                _BindDropDownListButs(Session("eleveid"), DropDownListButs, CurrentBut)
                BindDropDownListAttente(CurrentBut, 0)
                bindInfosBut(DropDownListButs.SelectedValue)
                'BindAttente()

            Catch ex As Exception
                lblMsgErr.Text = "Errreur lors de l'enregistrement du but... Detail--> " & ex.Message
                PanelNouveauBut.Visible = True
            End Try

            PanelNouveauBut.Visible = False
            ImageButtonDeleteBut.Visible = True
            DropDownListButs.Visible = True

            PanelInfosBut.Visible = True

        End Sub
        Function InsertNewBut(ByVal EleveId As String, ByVal strBut As String, ByVal Programme As String, ByVal Niveau_Rendement As String, ByVal CoteNote As String, ByVal Annee_Etude As String)
            Dim oBut As New PEIDAL.but
            Dim oAtt As New PEIDAL.attente

            Dim intButNo As Integer
            Dim ButID As Integer
            Dim strEleveId As String = Session("eleveid")
            Dim strComment As String = ""

            Try
                intButNo = oBut.GetMaxButNoByEleveID(strEleveId)
                ButID = oBut.CreateButsByEleveID(ConfigurationSettings.AppSettings("ConnectionString"), EleveId, intButNo + 1, strBut, Programme, Niveau_Rendement, CoteNote, Annee_Etude)

                'SI attente differente
                If Annee_Etude <> "" Then
                    strComment = "Les attentes modifi�es sont �valu�es sur le bulletin scolaire de l'Ontario"
                End If
                'insert commentaires
                oAtt.InsertCommentairesGenerales(ConfigurationSettings.AppSettings("ConnectionString"), ButID, strComment, strComment, strComment, strComment)

                'Response.Redirect("attentes.aspx")
                Return ButID

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Visible = "Erreur survenue lors de la cr�ation du but: " & ex.Message

            End Try


        End Function

        Private Sub btnEnregistrerNewAttente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnregistrerNewAttente.Click
            Try
                Dim attNo As Integer

                attNo = AjouterAttente(DropDownListButs.SelectedValue, txtNouvelleAttente.Text)
                'If attNo <> 0 Then
                '    Response.Redirect("attentes.aspx?attno=" & attNo & "&butid=" & DropDownListButs.SelectedValue)
                'End If
                BindDropDownListAttente(DropDownListButs.SelectedValue, attNo)

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Erreur lors de l'enregistrement de la nouvelle attente. " & ex.Message
                PanelNouvelleAttente.Visible = True

            End Try

            PanelNouvelleAttente.Visible = False
            PanelInfosAttentes.Visible = True

        End Sub
        Function AjouterAttente(ByVal ButId As Integer, ByVal strAttente As String)
            Dim intAttenteNo As Integer
            Dim oAtt As New PEIDAL.attente

            Try
                If oAtt.GetCountAttentesByButID(DropDownListButs.SelectedValue) > 0 Then
                    intAttenteNo = oAtt.GetMaxAttenteNoByButID(DropDownListButs.SelectedValue)
                    intAttenteNo = intAttenteNo + 1
                Else
                    intAttenteNo = 1
                End If

                oAtt.InsertAttente(ConfigurationSettings.AppSettings("ConnectionString"), ButId, intAttenteNo, strAttente, "", "", "", "", "", "", "", "", "", "", "", "")

            Catch ex As Exception
                'lblStatus.Text = ex.Message
                Throw New Exception("Erreur lors de l'ajout d'une attente. Details de l'erreur: " & ex.Message)
                intAttenteNo = 0

            End Try

            Return intAttenteNo

        End Function
        Sub BindDropDownListeNiveauAttente(ByVal DDL As DropDownList)
            Dim oAttente As PEIDAL.attente = New PEIDAL.attente
            Dim myDDL As DropDownList

            myDDL = DDL

            Try
                myDDL.DataSource = oAttente.GetListTypeAttente
                myDDL.DataTextField = "pk_niveau_attente_id"
                myDDL.DataValueField = "pk_niveau_attente_id"
                myDDL.DataBind()

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Erreur lors de l'affichage de la liste des niveaux d'attentes"

            End Try

        End Sub

        Private Sub btn_AnnulerNouveauBut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_AnnulerNouveauBut.Click
            PanelNouveauBut.Visible = False
            PanelInfosBut.Visible = True

        End Sub

      

        Private Sub ImageButtonCollapseInfosButs_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCollapseInfosButs.Click
            'PanelInfosBut.Visible = False

            'ImageButtonExpandInfosButs.Visible = True
            'ImageButtonCollapseInfosButs.Visible = False
        End Sub

        Private Sub ImageButtonExpandInfosButs_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonExpandInfosButs.Click
            'PanelInfosBut.Visible = True
            'ImageButtonExpandInfosButs.Visible = False
            'ImageButtonCollapseInfosButs.Visible = True
        End Sub

        Private Sub ImageButtonExpandInfosAttentes_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonExpandInfosAttentes.Click
            PanelInfosAttentes.Visible = True

            ImageButtonCollapseInfosAttentes.Visible = True
            ImageButtonExpandInfosAttentes.Visible = False

        End Sub

        Private Sub ImageButtonCollapseInfosAttentes_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCollapseInfosAttentes.Click
            PanelInfosAttentes.Visible = False

            ImageButtonCollapseInfosAttentes.Visible = False
            ImageButtonExpandInfosAttentes.Visible = True

        End Sub

        Private Sub ImageButtonAjouterMethode_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAjouterMethode.Click
            TextBoxMethode.Text = TextBoxMethode.Text & Chr(13) & DropDownListMethodes.SelectedItem.Text

        End Sub

        Private Sub btn_enregistrerBut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_enregistrerBut.Click
            SaveInfosBut()
            _BindDropDownListButs(strEleveId, DropDownListButs, Me.DropDownListButs.SelectedValue)

        End Sub

        Function _getButNoByButId(butID As Integer) As Integer
            Dim _ds As DataSet
            _ds = GetDataset("select but_no from buts where pk_but_id=" & butID)
            Return CInt(_ds.Tables(0).Rows(0).Item("but_no").ToString)
        End Function
        Function _getButIdByButNoAndEleveId(butno As Integer, eleveid As String) As Integer
            Dim _ds As DataSet
            _ds = GetDataset("select pk_but_id from buts where but_no = " & butno & " and fk_eleve_id = '" & eleveid & "'")

            If (_ds.Tables(0).Rows.Count <> 1) Then
                Throw New Exception("Erreur dans l appel _getButIdByButNoAndEleveId " & " butno=" & butno & " eleveid=" & eleveid)
            End If

            Return CInt(_ds.Tables(0).Rows(0).Item("pk_but_id").ToString)
        End Function

        Sub ReplaceButNo(currentButId As Integer, CurrentButNo As Integer, eleveid As String)
            Dim _ButNo As Integer
            Dim _ButId As Integer

            _ButNo = _getButNoByButId(currentButId)
            _ButId = _getButIdByButNoAndEleveId(CurrentButNo, eleveid)

            Dim _sql As String
            _sql = "update buts set but_no = " & _ButNo & " where pk_but_id = " & _ButId
            Dim oGen As New PEIDAL.general
            oGen.ExecuteCommand(ConfigurationSettings.AppSettings("ConnectionString"), _sql)
        End Sub

        Sub SaveInfosBut()
            Dim oBut As New PEIDAL.but
            Dim strAttente As String = " "

            If CheckBoxAttenteMod2.Checked Then
                strAttente = " MOD"
            End If

            Try
                '------------------------------------
                'interchange le numero de but
                ReplaceButNo(DropDownListButs.SelectedValue, DropDownListButNo.SelectedValue, Session("eleveid"))
                '------------------------------------
                'oBut.PK_but_id = DropDownListButs.SelectedValue
                'oBut.But_No = DropDownListButNo.SelectedValue
                'oBut.But_Titre = txtEditBut.Text
                'oBut.Programme = TextBoxProgramme.Text
                'oBut.Annee_etude = DropDownListAnneeEtude.SelectedValue & strAttente
                'oBut.Niveau_rendement = TextBoxNiveauRendement.Text
                'oBut.UpdateButByPkButId(ConfigurationSettings.AppSettings("ConnectionString"))

                Dim oGen As New PEIDAL.general
                Dim _sqlUpdate As String
                Dim _but_titre As String
                Dim _programme As String
                Dim _anneeetude As String
                Dim _niveaurendement As String

                _but_titre = Replace(txtEditBut.Text, "'", "''")
                _programme = Replace(TextBoxProgramme.Text, "'", "''")
                _anneeetude = DropDownListAnneeEtude.SelectedValue & Replace(strAttente, "'", "''")
                _niveaurendement = Replace(TextBoxNiveauRendement.Text, "'", "''")

                _sqlUpdate = "update buts set but_no=" & Me.DropDownListButNo.SelectedValue & ",but_titre='" & _but_titre & "', programme='" & _programme & "',niveau_rendement='" & _niveaurendement & "', annee_etude = '" & _anneeetude & "' where pk_but_id = " & DropDownListButs.SelectedValue
                oGen.ExecuteCommand(ConfigurationSettings.AppSettings("ConnectionString"), _sqlUpdate)

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Erreur survenue lors de la mise � jour des informations du but: " & ex.Message
            End Try

        End Sub


        Private Sub btn_AnnulerBut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_AnnulerBut.Click
            PanelInfosBut.Visible = False

            ImageButtonExpandInfosButs.Visible = True
            ImageButtonCollapseInfosButs.Visible = False

            ClearEditNouveauBut()

        End Sub
        Sub ClearEditNouveauBut()
            TextBoxProgramme.Text = ""
            TextBoxNiveauRendement.Text = ""
            txtEditBut.Text = ""
        End Sub

        Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            PanelInfosAttentes.Visible = False

        End Sub

        Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            'Dim oAttente As PEI.attente = New PEI.attente

        End Sub

        Private Sub ImageButtonExpandInfosCommentaires_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonExpandInfosCommentaires.Click
            PanelInfosCommentaires.Visible = True

            ImageButtonExpandInfosCommentaires.Visible = False
            ImageButtonCollapseInfosCommentaires.Visible = True

        End Sub

        Private Sub ImageButtonCollapseInfosCommentaires_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCollapseInfosCommentaires.Click
            PanelInfosCommentaires.Visible = False

            ImageButtonExpandInfosCommentaires.Visible = True
            ImageButtonCollapseInfosCommentaires.Visible = False
        End Sub

        Private Sub btnAnnulerCommentaire_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnnulerCommentaire.Click
            PanelInfosCommentaires.Visible = False

            ImageButtonExpandInfosCommentaires.Visible = True
            ImageButtonCollapseInfosCommentaires.Visible = False

        End Sub

        Private Sub btnAnnulerAttente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnnulerAttente.Click
            'PanelInfosAttentes.Visible = False

            ImageButtonCollapseInfosAttentes.Visible = False
            ImageButtonExpandInfosAttentes.Visible = True
        End Sub

        Private Sub bntEnregistrerCommentaire_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bntEnregistrerCommentaire.Click
            Dim oAttente As New PEIDAL.attente
            Dim strCommentaire As String

            If Not TextBoxCommentaireGeneral.Text <> "" Then
                strCommentaire = " "
            Else
                strCommentaire = TextBoxCommentaireGeneral.Text
            End If

            Try
                'Enregistrement commentaires g�n�rales au but
                Select Case "commentaire" & DropDownListEtape.SelectedValue

                    Case "commentaire1"
                        oAttente.UpdateAttenteCommentairesGenerales(ConfigurationSettings.AppSettings("ConnectionString"), DropDownListButs.SelectedValue, strCommentaire, "", "", "")
                    Case "commentaire2"
                        oAttente.UpdateAttenteCommentairesGenerales(ConfigurationSettings.AppSettings("ConnectionString"), DropDownListButs.SelectedValue, "", strCommentaire, "", "")
                    Case "commentaire3"
                        oAttente.UpdateAttenteCommentairesGenerales(ConfigurationSettings.AppSettings("ConnectionString"), DropDownListButs.SelectedValue, "", "", strCommentaire, "")
                    Case "commentaire4"
                        oAttente.UpdateAttenteCommentairesGenerales(ConfigurationSettings.AppSettings("ConnectionString"), DropDownListButs.SelectedValue, "", "", "", strCommentaire)

                End Select

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Erreur survenue lors de l'enregistrement du commentaire"
            End Try

        End Sub

        Private Sub bntEnregistrerAttente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bntEnregistrerAttente.Click
            Dim oAttente As PEIDAL.attente = New PEIDAL.attente

            'Dim oStructAtt As PEI.attente.DetailsAttente = New PEI.attente.DetailsAttente
            Dim oStructAtt As PEIDAL.attente.DetailsAttente = New PEIDAL.attente.DetailsAttente

            Try

                'oStructAtt.attente = txtEditAttente.Text
                'oStructAtt.attente = TextBoxAttente.Text
                oStructAtt.fk_but_id = DropDownListButs.SelectedValue
                oStructAtt.attente_no = DropDownListAttentes.SelectedValue

                oStructAtt.attente = TextBoxAttente.Text
                'oStructAtt.attente = Server.HtmlEncode(TextBoxAttente.Text)

                oStructAtt.strategie = TextBoxStrategie.Text
                'oStructAtt.attente = Server.HtmlEncode(TextBoxAttente.Text)

                oStructAtt.methode = TextBoxMethode.Text
                'oStructAtt.methode = Server.HtmlEncode(TextBoxMethode.Text)

                'oStructAtt.Programme = TextBoxProgramme.Text
                'oStructAtt.Niveau_Rendement = TextBoxNiveauRendement.Text

                'oStructAtt.niveau_attente = DropDownListNiveau.SelectedValue

                'oStructAtt.inter = GetItemFromCheckBoxList(CheckBoxListInter)

                'oStructAtt.B1 = DropDownListB1.SelectedValue
                'oStructAtt.B1 = Trim(txtB1.Text)

                'oStructAtt.B2 = DropDownListB2.SelectedValue
                'oStructAtt.B2 = Trim(txtB2.Text)

                'oStructAtt.B3 = DropDownListB3.SelectedValue
                'oStructAtt.B3 = Trim(txtB3.Text)

                'oStructAtt.B4 = DropDownListB4.SelectedValue
                'oStructAtt.B4 = Trim(txtB4.Text)


                oAttente.UpdateAttenteStruct(ConfigurationSettings.AppSettings("ConnectionString"), oStructAtt)

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Erreur survenue lors de l 'enregistrement de l'attente. ! Consultez l'administrateur du syst�me. ----> " & ex.Message

            End Try
        End Sub

        Private Sub DataGridMethodes_DeleteCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
            Dim au_fnameCell As TableCell = e.Item.Cells(0)
            Dim itemIndex As Integer
            Dim index As String
            Dim index2 As String

            Try
                Dim oAtt As PEIDAL.attente = New PEIDAL.attente

                'index2 = e.Item.Cells(0).Text
                index = e.Item.Cells(1).Text
                oAtt.DeleteMethodeById(ConfigurationSettings.AppSettings("ConnectionString"), index)

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "erreur lors de la suppression d'une m�thode..."
            End Try

        End Sub

        Private Sub LinkButtonNouveauBut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LinkButtonNouveauBut.Click
            'Response.Redirect("newbut.aspx")
            'Server.Transfer("newbut.aspx")
            'DropDownListButs.Visible = False
            'ImageButtonDeleteBut.Visible = False

            PanelInfosBut.Visible = False
            PanelNouveauBut.Visible = True

            TxtNouveauButProgramme.Text = ""
            txtNouveauButNiveauRendementActuel.Text = ""
            'txtNouveauButCoteNote.Text = ""
            txtNouveauBut.Text = ""

        End Sub

        Private Sub LinkButtonNouvelleAttente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LinkButtonNouvelleAttente.Click
            Dim butId As Integer
            'butId = DropDownListButs.SelectedValue

            txtNouvelleAttente.Text = ""

            PanelNouvelleAttente.Visible = True
            PanelInfosAttentes.Visible = False
        End Sub

        Private Sub btnAnnulerNewAttente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnnulerNewAttente.Click
            PanelNouvelleAttente.Visible = False
            Me.PanelInfosAttentes.Visible = True
        End Sub

        Private Sub CheckBoxAttDiff_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxAttDiff.CheckedChanged
            Try
                If CheckBoxAttDiff.Checked = True Then
                    DropDownListNewButAnneeEtude.SelectedValue = DropDownListNewButAnneeEtude.Items.FindByValue("").Value
                    DropDownListNewButAnneeEtude.Visible = False
                    LabelAnneEtude.Visible = False
                    CheckBoxAttenteMod.Visible = False
                    CheckBoxAttenteMod.Checked = False
                Else
                    DropDownListNewButAnneeEtude.Visible = True
                    LabelAnneEtude.Visible = True
                    CheckBoxAttenteMod.Visible = True
                End If
            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Erreur lors survenue dans l'�venement CheckBoxAttDiff_CheckedChanged"
            End Try

        End Sub
        Sub PanelInfosbutVisible()

            If Not DropDownListButs.SelectedValue <> "" Then
                PanelInfosBut.Visible = False
                'PanelInfosAttentes.Visible = True
            End If

        End Sub

        Private Sub CheckBoxAttenteMod2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxAttenteMod2.CheckedChanged

        End Sub

        
       
    End Class

End Namespace
