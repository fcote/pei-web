<%@ Register TagPrefix="uc1" TagName="menu_top" Src="menu_top.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" ValidateRequest="false" SmartNavigation = "true" Inherits="PeiWebAccess.general1" CodeFile="general.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="sstchur.web.SmartNav" Assembly="sstchur.web.SmartNav" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>general</title>
		<LINK href="styles.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript">
		function refresh() 
		{
			parent.frames[0].location.reload();
		}
		</script>
	    <style type="text/css">
            .style1
            {
                width: 100%;
            }
        </style>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" onload="refresh();" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE class="cbToolbar1" id="TableMain" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD><uc1:menu_top id="Menu_top1" runat="server"></uc1:menu_top></TD>
				</TR>
				<TR>
					<TD class="titleOnglet">Informations de l'�l�ve</TD>
				</TR>
				<TR>
					<TD><asp:label id="errMsg" runat="server" BackColor="Khaki" BorderColor="Firebrick" BorderWidth="1px"
							CssClass="fldlabel" Font-Size="X-Small" Font-Names="Arial" Width="100%" Visible="False"></asp:label></TD>
				</TR>
				<TR>
					<TD>
						<asp:Button id="Button1" runat="server" Width="300px" CssClass="fldTextBox" 
                            Text="Enregistrer" TabIndex="1" Visible="False"></asp:Button></TD>
				</TR>
				<TR>
					<TD borderColor="red">
						<TABLE id="TableGenerales" cellSpacing="0" cellPadding="0"  border="0">
							<TR>
								<TD style="WIDTH: 300px">
									<TABLE id="TableElevesLeft" style="WIDTH: 330px" borderColor="red" cellSpacing="2" cellPadding="2"
										width="300" border="0">
										<TR>
											<TD colSpan="2"><asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" Font-Size="XX-Small" Font-Names="Arial"
													Display="Dynamic" ControlToValidate="txtPrenom_Eleve" ErrorMessage="le champ pr�nom ne peut �tre vide" Visible="False"></asp:requiredfieldvalidator></TD>
										</TR>
										<TR>
											<TD class="fldTextBox" style="WIDTH: 129px" width="129">Pr�nom:</TD>
											<TD><asp:textbox id="txtPrenom_Eleve" runat="server" CssClass="fldTextBox" Width="200px" Enabled="False"></asp:textbox></TD>
										</TR>
										<TR>
											<TD colSpan="2"><asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" Font-Size="XX-Small" Font-Names="Arial"
													Display="Dynamic" ControlToValidate="txtNom_Eleve" ErrorMessage="le champ pr�nom ne peut �tre vide"></asp:requiredfieldvalidator></TD>
										</TR>
										<TR>
											<TD class="fldLabel" style="WIDTH: 129px" width="129">Nom:</TD>
											<TD><asp:textbox id="txtNom_Eleve" runat="server" CssClass="fldTextBox" Width="200px" Enabled="False"></asp:textbox></TD>
										</TR>
										<TR>
											<TD class="fldLabel" style="WIDTH: 129px" width="129">�cole fr�quent�e:
											</TD>
											<TD style="HEIGHT: 26px"><asp:dropdownlist id="DropDownListEcole" runat="server" 
                                                    CssClass="fldTextBox" Width="200px" AutoPostBack="True" Enabled="False"></asp:dropdownlist></TD>
										</TR>
										<TR>
											<TD class="fldLabel" style="WIDTH: 129px">Titulaire:</TD>
											<TD><asp:textbox id="txtTitulaire" runat="server" CssClass="fldTextBox" Width="200px"></asp:textbox></TD>
										</TR>
										<TR>
											<TD class="fldLabel" style="WIDTH: 31.05%">No. ID de l'�cole:</TD>
											<TD><asp:textbox id="txtNo_Identification" runat="server" CssClass="fldTextBox" Width="200px" Enabled="False"></asp:textbox></TD>
										</TR>
										<TR>
											<TD colSpan="2"><asp:requiredfieldvalidator id="RequiredFieldValidator3" runat="server" Font-Size="XX-Small" Font-Names="Arial"
													Width="264px" Display="Dynamic" ControlToValidate="txtDirecteur" ErrorMessage="Le champ Direction ne peut �tre vide"></asp:requiredfieldvalidator></TD>
										</TR>
										<TR>
											<TD class="fldLabel" style="WIDTH: 31.05%">Direction:</TD>
											<TD><asp:textbox id="txtDirecteur" runat="server" CssClass="fldTextBox" Width="200px" ForeColor="#8080FF"
													Enabled="False"></asp:textbox></TD>
										</TR>
										<TR>
											<TD class="fldLabel" style="WIDTH: 31.05%" vAlign="top">Adresse:</TD>
											<TD><asp:textbox id="txtAdresse" runat="server" CssClass="fldTextBox" Width="200px" Height="50px"
													TextMode="MultiLine" Enabled="False"></asp:textbox></TD>
										</TR>
										<TR>
											<TD class="fldLabel" style="WIDTH: 129px">T�l�phone:</TD>
											<TD><asp:textbox id="txtNo_Tel" runat="server" CssClass="fldTextBox" Enabled="False"></asp:textbox></TD>
										</TR>
										<TR>
											<TD class="fldLabel" style="WIDTH: 129px">T�l�copieur:</TD>
											<TD><asp:textbox id="txtNo_Fax" runat="server" CssClass="fldTextBox" Enabled="False"></asp:textbox></TD>
										</TR>
									</TABLE>
								</TD>
								<TD valign="top" align="left">
									<TABLE id="TableElevesRight" borderColor="green" cellSpacing="2" cellPadding="2" width="400"
										border="0">
										<TR>
											<TD class="fldLabel" style="WIDTH: 99px; HEIGHT: 13px" vAlign="top"></TD>
											<TD style="HEIGHT: 13px"></TD>
										</TR>
										
										<TR>
											<TD style="HEIGHT: 11px" vAlign="top" colSpan="2"><asp:requiredfieldvalidator id="RequiredFieldValidator4" runat="server" Font-Size="XX-Small" Font-Names="Arial"
													Width="188px" Display="Dynamic" ControlToValidate="txtDate_Naissance" ErrorMessage="la date de naissance ne peut �tre vide"></asp:requiredfieldvalidator></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 3px" vAlign="top" colSpan="2"><asp:comparevalidator id="CompareValidator1" runat="server" Font-Size="XX-Small" Font-Names="Arial" Width="172px"
													Display="Dynamic" ControlToValidate="txtDate_Naissance" ErrorMessage="Entrez une date valie (AAAA-MM-JJ)" ValueToCompare="1900-01-01" Operator="GreaterThan"
													Type="Date"></asp:comparevalidator></TD>
										</TR>
										<TR>
											<TD class="fldLabel" style="WIDTH: 99px" vAlign="top">Date de naissance:</TD>
											<TD><asp:textbox id="txtDate_Naissance" runat="server" CssClass="fldTextBox" Width="80px" Enabled="False"></asp:textbox></TD>
										</TR>
										<TR>
											<TD class="fldLabel" style="WIDTH: 99px" vAlign="top">Sexe:</TD>
											<TD><asp:dropdownlist id="DropDownListSexe" runat="server" CssClass="fldTextBox" Width="80px" Enabled="False">
													<asp:ListItem Value="  "></asp:ListItem>
													<asp:ListItem Value="M">M</asp:ListItem>
													<asp:ListItem Value="F">F</asp:ListItem>
												</asp:dropdownlist></TD>
										</TR>
										<TR>
											<TD class="fldLabel" style="WIDTH: 99px; HEIGHT: 25px" vAlign="top">&nbsp;</TD>
											<TD style="HEIGHT: 25px"><asp:dropdownlist id="DropDownListEcoleFoyer" 
                                                    runat="server" CssClass="fldTextBox" Width="200px" Visible="False"></asp:dropdownlist></TD>
										</TR>
										<TR>
											<TD class="fldLabel" vAlign="top">Ann�e d'�tudes:</TD>
											<TD vAlign="middle"><asp:radiobuttonlist id="RadioButtonListNiveaux" runat="server" 
                                                    CssClass="fldTextBox" Width="299px" Height="8px"
													RepeatColumns="3" Enabled="False"></asp:radiobuttonlist></TD>
										</TR>
										<TR>
											<TD class="fldLabel" style="WIDTH: 99px" vAlign="top">&nbsp;</TD>
											<TD><asp:textbox id="txtPk_Eleve_id" runat="server" CssClass="fldTextBox" 
                                                    Width="200px" Enabled="False" Visible="False"></asp:textbox></TD>
										</TR>
										<TR>
											<TD class="fldLabel" style="WIDTH: 99px" vAlign="top">&nbsp;</TD>
											<TD>
												<asp:TextBox id="TextBoxStudentNumber" runat="server" Width="200px" 
                                                    CssClass="fldTextBox" Enabled="False" Visible="False"></asp:TextBox></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 23px" width="100%" colSpan="2"></TD>
										</TR>
									</TABLE>
								</TD>
							<TR>
								<TD style="WIDTH: 271px"></TD>
								<TD vAlign="top"></TD>
							</TR>
							<TR>
								<TD class="cbLine" colSpan="2">.<asp:button id="btnEnregistrerInfosEleve" runat="server" CssClass="fldTextBox" Text="Enregistrer"
										Width="300px"></asp:button></TD>
							</TR>
				</TR>
			</TABLE>
			<TR>
				<TD class="cbToolbar" style="HEIGHT: 23px">D�tails sur les 
					consultations aupr�s des parents/du tuteur/de la&nbsp;tutrice/de l'�l�ve
					<asp:imagebutton id="ImageButtonAjouterResultatConsultation" runat="server" ImageUrl="images/create.gif"></asp:imagebutton></TD>
			</TR>
			<TR>
				<TD><asp:datagrid id="DataGridResultatsConsultations" runat="server" BackColor="White" BorderColor="#CCCCCC"
						BorderWidth="1px" CssClass="fldLabel" Width="100%" DataMember="RESULTATS_CONSULTATION" DataKeyField="ROW_ID"
						BorderStyle="None" CellPadding="3" AutoGenerateColumns="False">
						<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
						<ItemStyle ForeColor="#000066"></ItemStyle>
						<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699"></HeaderStyle>
						<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
						<Columns>
							<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;img border=0 src=&quot;Images/save.gif&quot;&gt;"
								CancelText="Annuler" EditText="&lt;img border=0 src=&quot;Images/P_Draw_Lg_N.gif&quot;&gt;"></asp:EditCommandColumn>
							<asp:ButtonColumn Text="&lt;img border=0 src=&quot;Images/delete.gif&quot;&gt;" CommandName="Delete"></asp:ButtonColumn>
							<asp:BoundColumn DataField="DATE_CONSULT" SortExpression="DATE_CONSULT" HeaderText="Dates" DataFormatString="{0:dd/MM/yyyy}">
								<HeaderStyle Wrap="False"></HeaderStyle>
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:TemplateColumn HeaderText="Moyens">
								<ItemTemplate>
									<asp:Label id="lblMoyen_consult" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.moyen_consult") %>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox id="txtMoyen_consult" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.moyen_consult") %>' Width="100%">
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="R&#233;sultats">
								<HeaderStyle Width="100%"></HeaderStyle>
								<ItemTemplate>
									<asp:Label id="lblResultat" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.resultat") %>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox id="txtResultat" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Resultat") %>' Width="100%">
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
						</Columns>
						<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
					</asp:datagrid></TD>
			</TR>
			<TR>
				<TD class="cbToolbar">Personnes contacts:
					<asp:imagebutton Visible="false" id="ImageButtonNouveauContact" runat="server" ImageUrl="images/create.gif"></asp:imagebutton></TD>
			</TR>
			<TR>
				<TD><asp:datagrid id="DataGrid1" Visible="false" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px"
						CssClass="fldLabel" Width="100%" DataMember="CONTACTS" DataKeyField="ROW_ID" BorderStyle="None" CellPadding="3"
						AutoGenerateColumns="False">
						<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
						<ItemStyle ForeColor="#000066"></ItemStyle>
						<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699"></HeaderStyle>
						<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
						<Columns>
							<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;img border=0 src=&quot;Images/save.gif&quot;&gt;"
								CancelText="Annuler" EditText="&lt;img border=0 src=&quot;Images/P_Draw_Lg_N.gif&quot;&gt;"></asp:EditCommandColumn>
							<asp:ButtonColumn Text="&lt;img border=0 src=&quot;Images/delete.gif&quot;&gt;" CommandName="Delete"></asp:ButtonColumn>
							<asp:BoundColumn Visible="False" DataField="ROW_ID" SortExpression="ROW_ID" HeaderText="ROW_ID"></asp:BoundColumn>
							<asp:TemplateColumn HeaderText="Type de contacts">
								<HeaderStyle Wrap="False"></HeaderStyle>
								<ItemStyle Wrap="False"></ItemStyle>
								<ItemTemplate>
									<asp:Label id=lblTypeContact Text='<%# DataBinder.Eval(Container.DataItem, "Type_Contact") %>' Runat="server">
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:Label id=lblTypeContactHold Text='<%# DataBinder.Eval(Container.DataItem, "Type_Contact") %>' Runat="server">
									</asp:Label>
									<asp:DropDownList id=ddlTypeContact runat="server" DataValueField="Description" DataTextField="Description" OnPreRender="SetDropDownIndex" DataSource="<%# BindTypeContact() %>">
									</asp:DropDownList>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:BoundColumn DataField="Nom" SortExpression="NOM" HeaderText="Nom">
								<HeaderStyle Wrap="False"></HeaderStyle>
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="Adresse" SortExpression="Adresse" HeaderText="Adresse">
								<HeaderStyle Wrap="False"></HeaderStyle>
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="TEL_MAISON" SortExpression="TEL_MAISON" HeaderText="T&#233;l. maison">
								<HeaderStyle Wrap="False"></HeaderStyle>
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
							<asp:BoundColumn DataField="TEL_TRAVAIL" SortExpression="TEL_TRAVAIL" HeaderText="T&#233;l. travail">
								<HeaderStyle Wrap="False" Width="100%"></HeaderStyle>
								<ItemStyle Wrap="False"></ItemStyle>
							</asp:BoundColumn>
						</Columns>
						<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
					</asp:datagrid></TD>
			</TR>	
			<TR>
				<TD><cc1:smartscroller id="SmartScroller1" runat="server"></cc1:smartscroller></TD>
			</TR>
			</TABLE>
            
            
            <asp:Panel ID="PanelContacts" runat="server" CssClass="fldLabel">
            <div id "infos">Les contacts suivants proviennent de la base de donn�es Trillium. Ce sont les contacts qui ont la garde de l'�l�ve. 
            <br />Si besoin, veuillez consulter la secr�taire de l'�cole pour mettre � jour.</div>
            
            <div id="contacts" runat="server" class="fldLabel"> 
            </div>
            </asp:Panel>
            
            </form>
	</body>
</HTML>
