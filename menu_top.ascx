<%@ Control Language="vb" AutoEventWireup="false" Inherits="PeiWebAccess.menu_top" CodeFile="menu_top.ascx.vb" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>liste</title>
		<LINK href="styles.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body>
	
	<telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    
    <div id="header">
			<h1>PEI Web Access 3.0</h1><br />
			<h2 runat="server" id="h2_topic" style="font-family: Arial; color: #FFFFFF; font-size: 8pt;"></h2><br />
            <h2 runat="server" id="h2_student" style="font-family: Arial; color: #FFFFFF; font-size: 8pt;"></h2>
			<div id="TDtopic" valign="bottom" align="right" runat="server" 
                style="font-family: Arial; color: #FFFFFF; font-size: 9pt;"></div>
	        <div id="TD1" style="font-family: Arial; color: #FFFFFF; font-size: 9pt;" align="right" runat="server"></div>
	
	</div>
	<div id="user"><asp:Label ID="LabelUser" align="left" runat="server" style="font-family: Arial; color: #FFFFFF; font-size: 9pt;"></asp:Label></div>
	
        <telerik:RadMenu ID="RadMenu1" Runat="server" Skin="Web20" Width="100%">
            <Items>
                <telerik:RadMenuItem runat="server" Text="[�l�ves]">
                    <Items>
                        <telerik:RadMenuItem runat="server" NavigateUrl="~/ListeEleves.aspx" 
                            Text="Liste des �l�ves">
                        </telerik:RadMenuItem>
                        <telerik:RadMenuItem runat="server" NavigateUrl="~/createEleve.aspx" 
                            Text="Ajouter un �l�ve">
                        </telerik:RadMenuItem>
                        <telerik:RadMenuItem runat="server" NavigateUrl="~/desactive_eleve.aspx" 
                            Text="D�sactiver un �l�ve">
                        </telerik:RadMenuItem>
                    </Items>
                </telerik:RadMenuItem>
                <telerik:RadMenuItem runat="server" NavigateUrl="~/general.aspx" 
                    Text="[Donn�es G�n�rales]">
                </telerik:RadMenuItem>
                <telerik:RadMenuItem runat="server" NavigateUrl="~/liste.aspx" 
                    Text="[Identification]">
                </telerik:RadMenuItem>
                <telerik:RadMenuItem runat="server" NavigateUrl="~/sante.aspx" Text="[Sant�]">
                </telerik:RadMenuItem>
                <telerik:RadMenuItem runat="server" NavigateUrl="~/forcesbesoins2.aspx" Text="[Forces et Besoins]">
                </telerik:RadMenuItem>
                <telerik:RadMenuItem runat="server" NavigateUrl="~/attentes.aspx" 
                    Text="[Attentes et strat�gies]">
                </telerik:RadMenuItem>
                <telerik:RadMenuItem runat="server" Text="[Programmes]" 
                    NavigateUrl="~/programmes.aspx">
                </telerik:RadMenuItem>
               <%-- <telerik:RadMenuItem runat="server" NavigateUrl="~/plan.aspx" 
                    Text="[Plan de transition]">
                </telerik:RadMenuItem>--%>
                <telerik:RadMenuItem runat="server" NavigateUrl="~/plan2.aspx" 
                    Text="[Plan de transition]">
                </telerik:RadMenuItem>
                <telerik:RadMenuItem runat="server" NavigateUrl="~/testsprovinciaux.aspx" 
                    Text="[Tests provinciaux]">
                </telerik:RadMenuItem>
                <telerik:RadMenuItem runat="server" NavigateUrl="~/report2c.aspx" 
                    Text="[imprimer]">
                </telerik:RadMenuItem>
                <telerik:RadMenuItem runat="server" 
                    NavigateUrl="~/login.aspx?ReturnUrl=listeeleves.aspx" 
                    Text="[se d�connecter]">
                </telerik:RadMenuItem>
            </Items>
        </telerik:RadMenu>

	</body>
</HTML>
