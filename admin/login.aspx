<%@ Page Language="vb" AutoEventWireup="false" Inherits="PeiWebAccess.login" CodeFile="login.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>authentification</title>
		<LINK href="styles.css" type="text/css" rel="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" align="center" cellSpacing="0" cellPadding="0" border="0" style="WIDTH:760px">
				<TR>
					<TD class="titleOnglet" align="center" style="height: 103px">
                        <span style="font-family: Tahoma"><span style="font-size: 24pt; color: #000099">Le plan d'enseignement individualis� 
                            <br />
                        </span><font size="4"><span style="color: #000099">version web</span></font></span></TD>
				</TR>
				<TR>
					<TD class="titleOnglet" align="center" style="height: 21px"><FONT color="#cc3300"><span style="font-size: 8pt"></span></FONT></TD>
				</TR>
				<TR>
					<TD align="center" style="height: 267px">
						<TABLE id="TableLogin" cellSpacing="1" cellPadding="1" border="0">
							<TR>
								<TD>
									<asp:Label id="Label1" runat="server" Width="88px" CssClass="fldlabel">nom d'usager :</asp:Label></TD>
								<TD style="width: 171px">
									<asp:TextBox id="txtUsername" runat="server" Width="96px" tabIndex="1" CssClass="fldlabel"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD>
									<asp:Label id="Label2" runat="server" CssClass="fldlabel">mot de passe :</asp:Label></TD>
								<TD style="width: 171px">
									<asp:TextBox id="txtPassword" runat="server" Width="96px" TextMode="Password" tabIndex="2" CssClass="fldlabel"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD style="width: 171px">
									<asp:Button id="Button1" runat="server" Width="96px" Text="connexion" BackColor="AliceBlue"
										tabIndex="3" CssClass="fldlabel"></asp:Button></TD>
							</TR>
							<TR>
								<TD colSpan="2" align="center">
									<asp:Label id="lblMsgErr" runat="server" Width="200px" CssClass="fldlabel" Visible="False"
										BackColor="Khaki" BorderWidth="1px" BorderColor="Firebrick" Height="65px"></asp:Label></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<tr>
					<TD valign="bottom" align="center"><FONT face="Arial" color="#666699" size="1">D�velopp� 
							par Fr�d�ric C�t�<BR>
							Conseil scolaire catholique centre-Sud</FONT></TD>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>
