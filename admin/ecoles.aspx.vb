Imports System
Imports System.Web
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient

Namespace PeiWebAccess

    Partial Class admin_ecoles
        Inherits System.Web.UI.Page


        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

            Me.LabelMsgErr.Text = ""

            If Not Page.IsPostBack Then
                BindDropDownListEcole("CONSEIL63", Me.DropDownList1)
                BindEcole(Me.DropDownList1.SelectedValue)
            End If
        End Sub
        Sub BindEcole(ByVal EcoleID As String)
            Dim oEcole As PEIDAL.ecole = New PEIDAL.ecole
            Dim dsEcole As New DataSet

            Try

                dsEcole = oEcole.GetInfosEcoleByEcoleID(ConfigurationSettings.AppSettings("ConnectionString"), EcoleID)
                ' = dsEcole.Tables(0).Rows(0).Item("no_identification")
                Me.TextBoxNoIdentification.Text = dsEcole.Tables(0).Rows(0).Item("no_identification")
                Me.TextBoxNomEcole.Text = dsEcole.Tables(0).Rows(0).Item("nom_ecole")
                'Me.TextBoxSchoolCode.Text = dsEcole.Tables(0).Rows(0).Item("school_code")
                Me.TextBoxDirecteur.Text = dsEcole.Tables(0).Rows(0).Item("directeur")
                Me.TextBoxAdresse.Text = dsEcole.Tables(0).Rows(0).Item("adresse")
                Me.TextBoxNoTel.Text = dsEcole.Tables(0).Rows(0).Item("no_tel")
                Me.TextBoxNoFax.Text = dsEcole.Tables(0).Rows(0).Item("no_fax")
                Me.TextBoxVille.Text = dsEcole.Tables(0).Rows(0).Item("ville")
                Me.TextBoxCodePostal.Text = dsEcole.Tables(0).Rows(0).Item("code_postal")

            Catch ex As Exception
                Me.LabelMsgErr.Text = ex.Message
            End Try

        End Sub
      
        Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.SelectedIndexChanged
            BindEcole(Me.DropDownList1.SelectedValue)
        End Sub

        Protected Sub ButtonUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonUpdate.Click
            Dim oEcole As New PEIDAL.ecole

            Try
                oEcole.UpdateEcole(ConfigurationSettings.AppSettings("ConnectionString"), Me.DropDownList1.SelectedValue, Me.TextBoxNomEcole.Text, Me.TextBoxNoIdentification.Text, Me.TextBoxDirecteur.Text, Me.TextBoxAdresse.Text, Me.TextBoxCodePostal.Text, Me.TextBoxVille.Text, Me.TextBoxNoTel.Text, Me.TextBoxNoFax.Text)

            Catch ex As Exception
                Me.LabelMsgErr.Text = ex.Message
            End Try
        End Sub
    End Class

End Namespace

