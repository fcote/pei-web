Imports System.Data

Partial Class admin_export
    Inherits System.Web.UI.Page
    Private _conn As String = ConfigurationSettings.AppSettings("ConnectionString")
    Private _sql As String = ""


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            BindListeEleves()
        End If
    End Sub
  

    Sub BindListeEleves()

        Dim oEleve As New PEIDAL.eleve
        Dim dr As SqlClient.SqlDataReader
        Dim ds As New DataSet
        Dim dt As New DataTable("ELEVES")
        Dim r As DataRow

        dt.Columns.Add("pk_eleve_id")
        dt.Columns.Add("fullname")

        Try
            'dr = oEleve.GetListElevesByEcoleId(ConfigurationSettings.AppSettings("ConnectionString"), EcoleID)
            'dr = oEleve.GetReaderEleve(ConfigurationSettings.AppSettings("ConnectionString"))
            ds = oEleve.GetDsToutesEleves(_conn)
           

            For Each r2 As DataRow In ds.Tables(0).Rows
                r = dt.NewRow
                'r("pk_eleve_id") = dr.Item("pk_eleve_id")
                r("pk_eleve_id") = r2.Item("pk_eleve_id")
                r("fullname") = r2.Item("nom_eleve") & ", " & r2.Item("prenom_eleve") & " (" & r2.Item("fk_niveau_id") & ") "
                dt.Rows.Add(r)
            Next

            'While dr.Read
            '    r = dt.NewRow
            '    r("pk_eleve_id") = dr.Item("pk_eleve_id")
            '    r("fullname") = dr.Item("nom_eleve") & ", " & dr.Item("prenom_eleve") & " (" & dr.Item("description_niveau") & ") "
            '    dt.Rows.Add(r)
            'End While

            dt.AcceptChanges()

            Me.DropDownListEleves.DataSource = dt
            Me.DropDownListEleves.DataTextField = "fullname"
            Me.DropDownListEleves.DataValueField = "pk_eleve_id"
            Me.DropDownListEleves.DataBind()

            'Session("infoseleve") = Me.DropDownListEleves.SelectedValue
            'SetCheckbox()


            'SetCheckbox(Me.DropDownListEleves.SelectedValue)
            'DisplayEleveToPrint()

        Catch ex As Exception
            Me.lblErrMsg.Text = "Erreure est survenue: " & ex.Message
        End Try

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        Dim EleveId As String
        Dim s As String
        Dim oScript As New PEIDAL.extract

        EleveId = Me.DropDownListEleves.SelectedValue

        s = oScript.GetInsertStatementPeiPart1(EleveId)

        Me.TextBox1.Text = ""
        Me.TextBox1.Text = s


    End Sub

    
    

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim oExtract As New PEIDAL.extract

        Me.TextBox1.Text = ""
        'Me.TextBox1.Text = CreateScriptButs_Attentes(Me.DropDownListEleves.SelectedValue)
        'Me.TextBox1.Text = oExtract.GetInsertStatementPeiPart2(Me.DropDownListEleves.SelectedValue)
        Me.TextBox1.Text = oExtract.GetInsertStatementPeiPart2(Me.DropDownListEleves.SelectedValue)

    End Sub

    Protected Sub ButtonImportEcoles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonImportEcoles.Click
        Dim sql As String = ""

        'sql = CreateScriptEcoles("")

        'Me.TextBox1.Text = sql


    End Sub
End Class
