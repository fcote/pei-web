Imports System.Data

Partial Class admin_matieres
    Inherits System.Web.UI.Page
    Private _conn As String = ConfigurationSettings.AppSettings("ConnectionString")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Label1.Text = ""

        If Not Page.IsPostBack Then
            PopulateMatieres()
        End If


    End Sub
    Sub PopulateMatieres()
        Dim obj As New PEIDAL.general

        Dim ds As New DataSet

        'ds = obj.GetDataset("select distinct(m.matiere),me.fk_matiere_id from matieres_eleves me,matieres m where m.pk_matiere_id = me.fk_matiere_id order by m.matiere", _conn)
        ds = obj.GetDataset("select cast(m.PK_MATIERE_ID as CHAR) + ' - ' + m.MATIERE as FULL_MATIERE, m.PK_MATIERE_ID,M.MATIERE " & _
                            "from matieres m order by m.MATIERE ", _conn)
        Me.CheckBoxList1.DataSource = ds
        Me.CheckBoxList1.DataValueField = "pk_matiere_id"
        Me.CheckBoxList1.DataTextField = "full_matiere"
        Me.CheckBoxList1.DataBind()

        Me.DropDownList1.DataSource = ds
        Me.DropDownList1.DataValueField = "pk_matiere_id"
        Me.DropDownList1.DataTextField = "full_matiere"
        Me.DropDownList1.DataBind()

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim obj As New PEIDAL.general
        Dim sql As String = ""
        Dim sql2 As String = ""

        Dim stringIDs As String = ""

        For Each i As ListItem In Me.CheckBoxList1.Items
            If i.Selected Then
                stringIDs = stringIDs & i.Value & ","
            End If
        Next

        stringIDs = stringIDs & "0"


        sql = "UPDATE matieres_eleves set fk_matiere_id=" & Me.DropDownList1.SelectedValue & " where fk_matiere_id in (" & stringIDs & ")"

        stringIDs = Replace(stringIDs, Me.DropDownList1.SelectedValue & ",", " ")
        sql2 = "DELETE from MATIERES where pk_matiere_id in (" & stringIDs & ")"

        Try
            obj.ExecuteCommand(_conn, sql)
            obj.ExecuteCommand(_conn, sql2)

        Catch ex As Exception
            Me.Label1.Text = " Une erreure est survenue :" & ex.Message
        End Try


        PopulateMatieres()


    End Sub
End Class
