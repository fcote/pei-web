Imports PEIDAL.general
Namespace PeiWebAccess

    Partial Class login
        Inherits System.Web.UI.Page

#Region " Code g�n�r� par le Concepteur Web Form "

        'Cet appel est requis par le Concepteur Web Form.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'REMARQUE�: la d�claration d'espace r�serv� suivante est requise par le Concepteur Web Form.
        'Ne pas supprimer ou d�placer.

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN�: cet appel de m�thode est requis par le Concepteur Web Form
            'Ne le modifiez pas en utilisant l'�diteur de code.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Placez ici le code utilisateur pour initialiser la page

            Response.Write(".NET Framework version: ")
            Response.Write(System.Environment.Version)

            If Not Page.IsPostBack Then
                txtUsername.Text = GetCookiesValues("user", "username")
            End If

            'If GetCookiesValues("user", "username") <> "" Then

        End Sub


        Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
            Dim oGen As New PEIDAL.general
            Dim bLogged As Boolean = False

            Try
                If oGen.CheckLogon(ConfigurationSettings.AppSettings("ConnectionString"), txtUsername.Text, txtPassword.Text) = True Then
                    'Session("username") = txtUsername.Text
                    FormsAuthentication.RedirectFromLoginPage(txtUsername.Text, True)
                    'SaveCookies("user", "username", txtUsername.Text)
                    bLogged = True
                Else
                    bLogged = False
                    lblMsgErr.Visible = True
                    lblMsgErr.Text = "Mauvais nom d'usager ou mauvais mot de passe. R�essayez."
                End If

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Erreur survenue lors de l'authentification: " & ex.Message

            End Try

            If bLogged = True Then
                Response.Redirect("security.aspx")
                'Response.Redirect("frameset1.htm")
            End If

            'Session("eleveid") = txtEleveId.Tex

        End Sub
        Sub SaveCookies(ByVal Cookiename As String, ByVal Key As String, ByVal Value As String)
            Dim Cookie As HttpCookie

            Cookie = New HttpCookie(Cookiename)
            Cookie.Values.Add(Key, Value)
            Response.AppendCookie(Cookie)
            Response.Cookies(Cookiename).Expires = DateTime.Now.AddDays(1)

        End Sub
        Function GetCookiesValues(ByVal CookieName As String, ByVal Key As String) As String

            Dim Cookie As HttpCookie
            Cookie = Request.Cookies(CookieName)

            If Not Cookie Is Nothing Then

                Select Case (Key)

                    Case "username"
                        Return Cookie.Values("username")
                End Select
            End If

            Return ""

        End Function


    End Class

End Namespace
