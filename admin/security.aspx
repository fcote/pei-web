<%@ Page Language="vb" AutoEventWireup="false" Inherits="PeiWebAccess.security" CodeFile="security.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>security</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<LINK href="../styles.css" type="text/css" rel="stylesheet">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="100%" border="1">
				<TR>
					<TD style="HEIGHT: 16px"></TD>
					<TD style="HEIGHT: 16px"></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 16px"></TD>
					<TD style="HEIGHT: 16px"></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 16px" id="user" runat="server"></TD>
					<TD style="HEIGHT: 16px"></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 16px">
						<TABLE id="Table2" borderColor="#0000ff" cellSpacing="1" cellPadding="1" width="100%" border="1">
							<TR>
								<TD>
									<TABLE id="Table3" style="WIDTH: 472px; HEIGHT: 78px" cellSpacing="1" cellPadding="1" width="472"
										border="1">
										<TR>
											<TD><asp:dropdownlist id="DropDownListConseil" runat="server" Width="417px" AutoPostBack="True" CssClass="fldLabel"></asp:dropdownlist></TD>
										</TR>
										<TR>
											<TD><asp:dropdownlist id="DropDownListEcoles" runat="server" Width="417px" AutoPostBack="True" CssClass="fldLabel"></asp:dropdownlist></TD>
										</TR>
										<TR>
											<TD>&nbsp;</TD>
										</TR>
										</TABLE>
								</TD>
								<TD>
									<TABLE id="Table4" cellSpacing="1" cellPadding="1" width="186" border="1" style="WIDTH: 186px; HEIGHT: 55px">
										<TR>
											<TD style="WIDTH: 152px"><asp:textbox id="txtSearch" runat="server" CssClass="fldLabel"></asp:textbox></TD>
											<TD>
												<asp:button id="Button1" runat="server" CssClass="fldLabel" Width="150px" Text="Rechercher un utilisateur"></asp:button></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 152px">
												<asp:TextBox id="TextBoxFirstLastName" runat="server" CssClass="fldLabel"></asp:TextBox></TD>
											<TD>
												<asp:Button id="ButtonSearchStudent" runat="server" CssClass="fldLabel" Text="Rechercher un �l�ve"></asp:Button></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							</TABLE>
					</TD>
					<TD style="HEIGHT: 16px"></TD>
				</TR>
				<TR>
					
                    <td><asp:Panel ID="Panel1" runat="server" Height="140px" ScrollBars="Vertical" 
                            Width="300px">
                    <asp:datagrid id="DataGridUtilisateur" runat="server" CssClass="fldLabel" Width="400px" AutoGenerateColumns="False"
							DataKeyField="pk_utilisateur_id">
							<Columns>
								<asp:ButtonColumn Text="S&#233;lectionner" CommandName="Select"></asp:ButtonColumn>
								<asp:BoundColumn DataField="PK_UTILISATEUR_ID" HeaderText="User ID"></asp:BoundColumn>
								<asp:BoundColumn DataField="FULLNAME" HeaderText="Nom/Prenom"></asp:BoundColumn>
							</Columns>
						</asp:datagrid>
                        </asp:Panel></td>
					<TD style="HEIGHT: 16px"></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 16px"><STRONG></STRONG></TD>
					<TD style="HEIGHT: 16px"></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 16px"><asp:datagrid id="DataGridListeEleves" runat="server" Width="100%" BackColor="White" CssClass="fldLabel"
							BorderWidth="1px" BorderColor="#CCCCCC" AutoGenerateColumns="False" BorderStyle="None" CellPadding="3" DataKeyField="pk_eleve_id"
							PageSize="100">
							<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
							<ItemStyle ForeColor="#000066"></ItemStyle>
							<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699"></HeaderStyle>
							<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
							<Columns>
								<asp:ButtonColumn Text="S&#233;lectionner" CommandName="Select"></asp:ButtonColumn>
								<asp:BoundColumn DataField="PK_ELEVE_ID" HeaderText="No Identification"></asp:BoundColumn>
								<asp:BoundColumn DataField="PRENOM_ELEVE" HeaderText="Pr&#233;nom"></asp:BoundColumn>
								<asp:BoundColumn DataField="NOM_ELEVE" HeaderText="Nom"></asp:BoundColumn>
								<asp:BoundColumn DataField="nom_ecole" HeaderText="&#201;cole">
									<HeaderStyle Width="250px"></HeaderStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="description_niveau" HeaderText="Niveau"></asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
					<TD style="HEIGHT: 16px"></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD><asp:label id="lblMsgErr" runat="server" Width="416px" Font-Size="Medium" Font-Names="Arial"
							ForeColor="Maroon"></asp:label></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD><asp:panel id="PanelUtilisateur" runat="server" Visible="False" Width="100%">
							<TABLE id="Table5" cellSpacing="1" cellPadding="1" width="100%" border="1">
								<TR>
									<TD style="WIDTH: 260px; HEIGHT: 17px"><STRONG>Infos utilisateurs</STRONG></TD>
									<TD style="HEIGHT: 17px"><STRONG>�coles affect�es</STRONG></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 260px" vAlign="top">
										<TABLE id="TableNewUser" style="HEIGHT: 278px" cellSpacing="0" cellPadding="0" width="300"
											border="1">
											<TR>
												<TD class="fldLabel" style="WIDTH: 133px">Pr�nom:</TD>
												<TD>
													<asp:TextBox id="TextBoxPrenom" runat="server" CssClass="fldLabel" Width="120px"></asp:TextBox>
													<asp:imagebutton id="ImageButtonAddUser" runat="server" ImageUrl="../images/create.gif"></asp:imagebutton>
													<asp:imagebutton id="ImageButtonDeleteUser" runat="server" ImageUrl="../images/delete.gif"></asp:imagebutton></TD>
											</TR>
											<TR>
												<TD class="fldLabel" style="WIDTH: 133px">Nom:</TD>
												<TD>
													<asp:TextBox id="TextBoxNom" runat="server" CssClass="fldLabel" Width="120px"></asp:TextBox></TD>
											</TR>
											<TR>
												<TD class="fldLabel" style="WIDTH: 133px">Nom d'usager:</TD>
												<TD>
													<asp:TextBox id="TextBoxID" runat="server" CssClass="fldLabel" Width="120px"></asp:TextBox></TD>
											</TR>
											<TR>
												<TD class="fldLabel" style="WIDTH: 133px">Mot de passe:</TD>
												<TD>
													<asp:TextBox id="TextBoxPassword" runat="server" CssClass="fldLabel" 
                                                        Width="120px"></asp:TextBox></TD>
											</TR>
											<TR>
												<TD class="fldLabel" style="WIDTH: 133px; HEIGHT: 23px">Courriel (travail):</TD>
												<TD style="HEIGHT: 23px">
													<asp:TextBox id="TextBoxEmailTravail" runat="server" CssClass="fldLabel" Width="200px"></asp:TextBox></TD>
											</TR>
											<TR>
												<TD class="fldLabel" style="WIDTH: 133px; HEIGHT: 21px">Courriel (maison):</TD>
												<TD style="HEIGHT: 21px">
													<asp:TextBox id="TextBoxEmailMaison" runat="server" CssClass="fldLabel" Width="200px"></asp:TextBox></TD>
											</TR>
											<TR>
												<TD class="fldLabel" style="WIDTH: 133px">Titre:</TD>
												<TD>
													<asp:TextBox id="TextBoxTitre" runat="server" CssClass="fldLabel" Width="200px"></asp:TextBox></TD>
											</TR>
											<TR>
												<TD class="fldLabel" style="WIDTH: 133px">Tentative de connexion:</TD>
												<TD>
													<asp:TextBox id="TextBoxTry" runat="server" CssClass="fldLabel" Width="24px"></asp:TextBox></TD>
											</TR>
											<TR>
												<TD class="fldLabel" style="WIDTH: 133px">Verrouill�:</TD>
												<TD>
													<asp:CheckBox id="CheckBoxLocked" runat="server" CssClass="fldLabel"></asp:CheckBox></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 133px">
													<asp:Button id="btn_SaveNewUser" runat="server" CssClass="fldLabel" Width="130px" Text="Ajouter Utilisateur"></asp:Button></TD>
												<TD></TD>
											</TR>
											<TR>
												<TD colspan="2">
													<asp:Button id="btn_UpateUser" runat="server" CssClass="fldLabel" Width="291px" 
                                                        Text="Sauvegarder"></asp:Button></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 133px">
													&nbsp;</TD>
											    <td>
                                                    &nbsp;</td>
											</TR>
										    <tr>
                                                <td colspan="2">
                                                    <asp:Label ID="LabelMsgErrNewUser" runat="server" CssClass="fldLabel" 
                                                        Width="400px"></asp:Label>
                                                </td>
                                            </tr>
										</TABLE>
									</TD>
									<TD valign="top">
										<asp:checkboxlist id="CheckBoxListUtilisateursEcoles" runat="server" 
                                            CssClass="fldLabel" Width="99%"
											RepeatColumns="3"></asp:checkboxlist></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 260px"></TD>
									<TD></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD><asp:checkbox id="CheckBoxSelectAll" runat="server" AutoPostBack="True" 
                            CssClass="fldLabel" Text="S�lectionner tous les �coles" Visible="False"></asp:checkbox></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD><asp:button id="btn_enregistrerUtilisateursEcoles" runat="server" Width="256px" CssClass="fldLabel"
							Text="Enregistrer affectation des �coles" Visible="False"></asp:button></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
					<TD></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
