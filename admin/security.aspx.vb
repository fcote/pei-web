Imports System
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient

Namespace PeiWebAccess

    Partial Class security
        Inherits System.Web.UI.Page

        'Private _username As String = Session("user")
        Private _userlogged As String

#Region " Code g�n�r� par le Concepteur Web Form "

        'Cet appel est requis par le Concepteur Web Form.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'REMARQUE�: la d�claration d'espace r�serv� suivante est requise par le Concepteur Web Form.
        'Ne pas supprimer ou d�placer.

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN�: cet appel de m�thode est requis par le Concepteur Web Form
            'Ne le modifiez pas en utilisant l'�diteur de code.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            _userlogged = Session("user")
            'Me.user.InnerText = Page.User.Identity.Name
            'Me.user.InnerText = System.Security.Principal.WindowsIdentity.GetCurrent().Name

            'Placez ici le code utilisateur pour initialiser la page
            If Not Page.IsPostBack Then
                Try
                    BindConseils()
                    BindEcoles(DropDownListConseil.SelectedValue)
                    BindDGUtilisateurs(DropDownListEcoles.SelectedValue)

                    BindCheckBoxListEcoles(Trim(DropDownListConseil.SelectedValue))

                Catch ex As Exception
                    lblMsgErr.Text = ex.Message

                End Try

            End If
        End Sub
        Sub BindConseils()
            Dim oEcole As New PEIDAL.ecole

            Try
                DropDownListConseil.DataSource = oEcole.GetListConseils(ConfigurationSettings.AppSettings("connectionString"))
                DropDownListConseil.DataValueField = "pk_conseil_id"
                DropDownListConseil.DataTextField = "description"
                DropDownListConseil.DataBind()
            Catch ex As Exception
                lblMsgErr.Text = ex.Message
            End Try

        End Sub
        Sub BindEcoles(ByVal conseilId As String)
            Dim oEcole As New PEIDAL.ecole

            Dim _ds As New DataSet
            Dim tbEcoles As New Data.DataTable
            Dim dcEcoleId = New DataColumn("pk_ecole_id", GetType(String))
            Dim dcFullName = New DataColumn("nom_ecole", GetType(String))
            tbEcoles.Columns.Add(dcEcoleId)
            tbEcoles.Columns.Add(dcFullName)

            _ds = oEcole.GetDsListEcole(ConfigurationSettings.AppSettings("ConnectionString"))

            Dim _fullname As String = ""
            For Each item In _ds.Tables(0).Rows
                '    'Dim row As Data.DataRow
                If Not item("fk_conseil_id") <> conseilId Then
                    _fullname = item("nom_ecole") & " (" & item("ville").ToString & ")"
                    tbEcoles.Rows.Add(item("pk_ecole_id").ToString, _fullname)
                End If   
            Next

            Try
                '    DropDownListEcoles.DataSource = oEcole.GetListEcoleByConseilId(ConfigurationSettings.AppSettings("connectionString"), conseilId)
                '    DropDownListEcoles.DataValueField = "pk_ecole_id"
                '    DropDownListEcoles.DataTextField = "nom_ecole"
                '    DropDownListEcoles.DataBind()

                DropDownListEcoles.DataSource = tbEcoles
                DropDownListEcoles.DataValueField = "pk_ecole_id"
                DropDownListEcoles.DataTextField = "nom_ecole"
                DropDownListEcoles.DataBind()

            Catch ex As Exception
                lblMsgErr.Text = ex.Message
            End Try

        End Sub

        Sub BindDGUtilisateurs(ByVal EcoleId As String)
            Dim oGen As New PEIDAL.general
            Try
                DataGridUtilisateur.DataSource = oGen.GetListUtilisateurByEcoleId(ConfigurationSettings.AppSettings("connectionString"), DropDownListEcoles.SelectedValue)
                DataGridUtilisateur.DataBind()

            Catch ex As Exception
                lblMsgErr.Text = ex.Message
            End Try
        End Sub

        Private Sub DropDownListConseil_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DropDownListConseil.SelectedIndexChanged
            Try
                BindEcoles(Trim(DropDownListConseil.SelectedValue))
                'BindCheckBoxListEcoles()
                'SetCheckBoxListEcoles()

            Catch ex As Exception
                lblMsgErr.Text = ex.Message
            End Try

        End Sub

        Private Sub DropDownListEcoles_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DropDownListEcoles.SelectedIndexChanged
            BindDGUtilisateurs(Trim(DropDownListEcoles.SelectedValue))
        End Sub
        Sub BindDGUtilisateurs()
            'Dim oGen As New PEIDAL.general

            'Try
            '    BindEcoles(Trim(DropDownListConseil.SelectedValue))
            '    BindDDLUtilisateurs(Trim(DropDownListEcoles.SelectedValue))
            '    BindCheckBoxListEcoles()
            '    SetCheckBoxListEcoles()
            'Catch ex As Exception
            '    lblMsgErr.Text = ex.Message
            'End Try

        End Sub
        Sub BindCheckBoxListEcoles(ByVal conseilid As String)
            Dim _dt As DataTable
            _dt = GetDataTableEcoles(conseilid)

            Try
                CheckBoxListUtilisateursEcoles.DataSource = _dt
                CheckBoxListUtilisateursEcoles.DataValueField = "pk_ecole_id"
                CheckBoxListUtilisateursEcoles.DataTextField = "nom_ecole"
                CheckBoxListUtilisateursEcoles.DataBind()


                'Dim oEcole As New PEIDAL.ecole


                '    CheckBoxListUtilisateursEcoles.DataSource = oEcole.GetListEcoleByConseilId(ConfigurationSettings.AppSettings("connectionString"), Trim(DropDownListConseil.SelectedValue))
                '    CheckBoxListUtilisateursEcoles.DataValueField = "pk_ecole_id"
                '    CheckBoxListUtilisateursEcoles.DataTextField = "nom_ecole"
                '    CheckBoxListUtilisateursEcoles.DataBind()

            Catch ex As Exception
                lblMsgErr.Text = ex.Message
            End Try

        End Sub
        Sub SetCheckBoxListEcoles(ByVal UtilisateurID As String)
            Dim oEcole As New PEIDAL.ecole
            Dim oReader As SqlClient.SqlDataReader

            Try
                CheckBoxListUtilisateursEcoles.ClearSelection()

                oReader = oEcole.GetListEcoleByUtilisateurId(ConfigurationSettings.AppSettings("connectionString"), Trim(UtilisateurID))
                SetCheckBoxList(CheckBoxListUtilisateursEcoles, oReader)
            Catch ex As Exception
                lblMsgErr.Text = ex.Message
            End Try

        End Sub

        Private Sub btn_enregistrerUtilisateursEcoles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_enregistrerUtilisateursEcoles.Click
            'Dim oEcole As New PEIDAL.ecole
            'Dim li As ListItem

            'Try
            '    oEcole.DeleteListUtilisateursEcolesByUtilisateurId(Trim(TextBoxID.Text))

            '    For Each li In CheckBoxListUtilisateursEcoles.Items
            '        If li.Selected Then
            '            'objIdentification.InsertListSourceInformationsByEleveId(Session("eleveid"), li.Value)
            '            oEcole.InsertUtilisateursEcoles(ConfigurationSettings.AppSettings("connectionString"), Trim(TextBoxID.Text), li.Value, 1)
            '        End If
            '    Next

            'Catch ex As Exception
            '    lblMsgErr.Text = ex.Message
            'End Try

        End Sub
        Sub SaveAffectationEcoles(ByVal UserID As String)
            Dim oEcole As New PEIDAL.ecole
            Dim li As ListItem
            Dim _sql As String

            'user = System.Security.Principal.WindowsIdentity.GetCurrent.User.ToString
            'user = System.Security.Principal.WindowsIdentity.GetCurrent.User.Value
            'user = Session("user")
            Try
                oEcole.DeleteListUtilisateursEcolesByUtilisateurId(Trim(UserID))

                For Each li In CheckBoxListUtilisateursEcoles.Items
                    If li.Selected Then
                        'objIdentification.InsertListSourceInformationsByEleveId(Session("eleveid"), li.Value)
                        _sql = "insert into utilisateurs_ecoles (fk_utilisateur_id, fk_ecole_id) values ('" & UserID & "','" & li.Value & "')"
                        ExecuteCmd(ConfigurationSettings.AppSettings("connectionString"), _sql)
                        ' oEcole.InsertUtilisateursEcoles(ConfigurationSettings.AppSettings("connectionString"), Trim(UserID), li.Value, 1)

                        ' insertLog(ConfigurationSettings.AppSettings("ConnectionString"), "ACCESS", "ADMIN_SECURITY", "user " & _userlogged, "", "", Date.Now, "", "", "access granted to " & UserID & " (" & li.Text)
                    End If
                Next

            Catch ex As Exception
                lblMsgErr.Text = ex.Message
            End Try
        End Sub

        Private Sub ImageButtonAddUser_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAddUser.Click
            ClearInfosUser()
            TextBoxID.Enabled = True
            PanelUtilisateur.Visible = True
            btn_SaveNewUser.Visible = True
            btn_UpateUser.Visible = False
        End Sub

        Private Sub btn_SaveNewUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_SaveNewUser.Click
            Dim oGen As New PEIDAL.general
            Dim DsUtilisateur1 As PEIDAL.dsUtilisateurs

            Try
                DsUtilisateur1 = oGen.GetdsUtilisateur(ConfigurationSettings.AppSettings("connectionString"), TextBoxID.Text)

                If DsUtilisateur1.UTILISATEURS.Rows.Count < 1 Then
                    Dim dr As PEIDAL.dsUtilisateurs.UTILISATEURSRow

                    dr = DsUtilisateur1.UTILISATEURS.NewUTILISATEURSRow

                    dr.prenom_utilisateur = TextBoxPrenom.Text
                    dr.nom_utilisateur = TextBoxNom.Text
                    dr.pk_utilisateur_id = TextBoxID.Text
                    dr.password = TextBoxPassword.Text
                    dr.email_travail = TextBoxEmailTravail.Text
                    dr.email_maison = TextBoxEmailMaison.Text
                    dr.titre = TextBoxTitre.Text
                    dr.locked = False
                    dr._try = 0

                    DsUtilisateur1.UTILISATEURS.Rows.InsertAt(dr, 0)

                    oGen.UpdateDsUtilisateurs(ConfigurationSettings.AppSettings("connectionString"), DsUtilisateur1)

                    LabelMsgErrNewUser.Text = "Nouvel utilisateur enregistr� avec succ�s"

                    btn_SaveNewUser.Visible = False
                    btn_UpateUser.Visible = True
                Else
                    LabelMsgErrNewUser.Text = "Nom d'utilisateur d�j� existant"

                End If

            Catch ex As Exception
                LabelMsgErrNewUser.Text = ex.Message
            End Try

            'Dim oEcole As New PEIDAL.ecole
            'oEcole.InsertUtilisateursEcoles(ConfigurationSettings.AppSettings("connectionString"), Trim(TextBoxID.Text), DropDownListEcoles.SelectedValue, 1)

            Me.SaveAffectationEcoles(Me.TextBoxID.Text)

            'BindDDLUtilisateurs(DropDownListEcoles.SelectedValue)
            SetCheckBoxListEcoles(TextBoxID.Text)

        End Sub

        Private Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
            SetCheckBoxOption()



        End Sub
        Sub SetCheckBoxOption()
            'IIf(Me.CheckBoxSelectAll.Checked, SetToUnChecked(Me.CheckBoxListUtilisateursEcoles), SetToChecked(Me.CheckBoxListUtilisateursEcoles))

            If CheckBoxSelectAll.Checked = False Then
                SetToUnChecked(Me.CheckBoxListUtilisateursEcoles)

            Else
                SetToChecked(Me.CheckBoxListUtilisateursEcoles)

            End If

        End Sub
        Sub SetToChecked(ByVal objCheckBoxList As CheckBoxList)
            Dim _obj As CheckBoxList
            _obj = objCheckBoxList
            Dim i As ListItem

            For Each i In _obj.Items
                i.Selected = True
            Next
        End Sub
        Sub SetToUnChecked(ByVal objCheckBoxList As CheckBoxList)
            Dim _obj As CheckBoxList
            _obj = objCheckBoxList
            Dim i As ListItem

            For Each i In _obj.Items
                i.Selected = False
            Next
        End Sub


        Sub DisplayInfosUser(ByVal UtilisateurId As String)

            If Not UtilisateurId <> "" Then
                Exit Sub
            End If

            Dim oGen As New PEIDAL.general
            Dim dsUtilisateur1 As New PEIDAL.dsUtilisateurs

            ClearInfosUser()
            lblMsgErr.Text = ""

            PanelUtilisateur.Visible = True
            btn_SaveNewUser.Visible = False
            btn_UpateUser.Visible = True

            Try
                dsUtilisateur1 = oGen.GetdsUtilisateur(ConfigurationSettings.AppSettings("connectionString"), UtilisateurId)
                Dim r As PEIDAL.dsUtilisateurs.UTILISATEURSRow

                'r = dsEleve1.ELEVES.FindBypk_eleve_id(UtilisateurId)
                r = dsUtilisateur1.UTILISATEURS.FindBypk_utilisateur_id(UtilisateurId)

                TextBoxID.Text = r.pk_utilisateur_id

                If Not r.Isnom_utilisateurNull Then
                    TextBoxNom.Text = r.nom_utilisateur
                End If

                If Not r.Isprenom_utilisateurNull Then
                    TextBoxPrenom.Text = r.prenom_utilisateur
                End If

                If Not r.IspasswordNull Then
                    TextBoxPassword.Text = r.password
                End If

                If Not r.Isemail_travailNull Then
                    TextBoxEmailTravail.Text = r.email_travail
                End If

                If Not r.Isemail_maisonNull Then
                    TextBoxEmailMaison.Text = r.email_maison
                End If

                If Not r.IstitreNull Then
                    TextBoxTitre.Text = r.titre
                End If

                CheckBoxLocked.Checked = r.locked
                TextBoxTry.Text = r._try

            Catch ex As Exception
                lblMsgErr.Text = ex.Message

            End Try

        End Sub
        Sub ClearInfosUser()

            TextBoxNom.Text = ""
            TextBoxPrenom.Text = ""
            TextBoxID.Text = ""
            TextBoxPassword.Text = ""
            TextBoxEmailTravail.Text = ""
            TextBoxEmailMaison.Text = ""
            TextBoxTitre.Text = ""
            CheckBoxLocked.Checked = False
            TextBoxTry.Text = ""

        End Sub

        Private Sub btn_UpateUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_UpateUser.Click
            Dim oGen As New PEIDAL.general
            Dim DsUtilisateur1 As PEIDAL.dsUtilisateurs

            TextBoxID.Enabled = False

            Try
                DsUtilisateur1 = oGen.GetdsUtilisateur(ConfigurationSettings.AppSettings("connectionString"), TextBoxID.Text)

                If DsUtilisateur1.UTILISATEURS.Rows.Count > 0 Then
                    Dim dr As PEIDAL.dsUtilisateurs.UTILISATEURSRow

                    'dr = DsUtilisateur1.UTILISATEURS.NewUTILISATEURSRow
                    dr = DsUtilisateur1.UTILISATEURS.FindBypk_utilisateur_id(TextBoxID.Text)

                    dr.prenom_utilisateur = TextBoxPrenom.Text
                    dr.nom_utilisateur = TextBoxNom.Text
                    dr.pk_utilisateur_id = TextBoxID.Text
                    dr.password = TextBoxPassword.Text
                    dr.email_travail = TextBoxEmailTravail.Text
                    dr.email_maison = TextBoxEmailMaison.Text
                    dr.titre = TextBoxTitre.Text
                    dr.locked = CheckBoxLocked.Checked
                    dr._try = TextBoxTry.Text

                    'DsUtilisateur1.UTILISATEURS.Rows.InsertAt(dr, 0)

                    oGen.UpdateDsUtilisateurs(ConfigurationSettings.AppSettings("connectionString"), DsUtilisateur1)

                    Me.SaveAffectationEcoles(Me.TextBoxID.Text)

                    LabelMsgErrNewUser.Text = "Mise � jour de l'utilisateur effectu� avec succ�s"
                Else
                    LabelMsgErrNewUser.Text = "Impossible de sauvegarder les informations. Utilisateur non existant !"

                End If

            Catch ex As Exception
                LabelMsgErrNewUser.Text = "Erreur survenue lors de la sauvegarde des informations de l'utilisateur." & ex.Message
            End Try
        End Sub

        Private Sub ImageButtonDeleteUser_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonDeleteUser.Click
            Dim oGen As New PEIDAL.general

            Try
                oGen.DeleteUtilisateuByUtilisateurId(TextBoxID.Text)
                insertLog(ConfigurationSettings.AppSettings("ConnectionString"), "ACCESS", "ADMIN_SECURITY", "user " & _userlogged, "", "", Date.Now, "", "", "user " & Me.TextBoxID.Text & " deleted from custom access")

            Catch ex As Exception
                lblMsgErr.Text = "Erreur survenue lors de la suppression de l'utilisateur. " & ex.Message
            End Try

            BindDGUtilisateurs(DropDownListEcoles.SelectedValue)
            ClearInfosUser()
            'DisplayInfosUser(TextBoxID.Text)

        End Sub

        Private Sub DataGridUtilisateur_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridUtilisateur.SelectedIndexChanged
            Dim strUtilisateurId As String = DataGridUtilisateur.DataKeys(DataGridUtilisateur.SelectedIndex)

            DisplayInfosUser(strUtilisateurId)
            SetCheckBoxListEcoles(strUtilisateurId)

        End Sub

        Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
            Me.DataGridListeEleves.Visible = False
            Me.DataGridUtilisateur.Visible = True

            Me.ClearInfosUser()

            Dim oGen As New PEIDAL.general

            Try
                DataGridUtilisateur.DataSource = oGen.GetListUtilisateurByKeyword(ConfigurationSettings.AppSettings("connectionString"), txtSearch.Text)
                DataGridUtilisateur.DataBind()

                lblMsgErr.Text = ""

            Catch ex As Exception
                lblMsgErr.Text = "Erreur survenue lors de la recherche de l'utilisateur. " & ex.Message
            End Try

        End Sub

        Private Sub ButtonSearchStudent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearchStudent.Click

            Me.DataGridListeEleves.Visible = True
            Me.DataGridUtilisateur.Visible = False

            Dim oEleve As New PEIDAL.eleve

            Try
                Me.DataGridListeEleves.DataSource = oEleve.GetListElevesByFirstOrLastname(ConfigurationSettings.AppSettings("connectionString"), Trim(Me.TextBoxFirstLastName.Text))
                Me.DataGridListeEleves.DataBind()

                lblMsgErr.Text = ""

            Catch ex As Exception
                lblMsgErr.Text = "Erreur survenue lors de la recherche de l'�l�ve. " & ex.Message
            End Try

        End Sub
    End Class

End Namespace
