<%@ Register TagPrefix="cc1" Namespace="sstchur.web.SmartNav" Assembly="sstchur.web.SmartNav" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="PeiWebAccess.printpei" CodeFile="printpei.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="menu_top" Src="menu_top.ascx" %>
<%@ OutputCache Duration="1" VaryByParam="None" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<head>
		<title>printpei</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <link href="styles.css" rel="stylesheet" type="text/css" />
        <link href="styles.css" rel="stylesheet" type="text/css" />
        <link href="styles.css" rel="stylesheet" type="text/css" />
	    <style type="text/css">
            .style1
            {
                height: 57px;
            }
            .style2
            {
                vertical-align: middle;
                font: icon;
                font-size: 10px;
                height: 12px;
                padding-left: 1px;
                padding-right: 1px;
            }
            .style3
            {
                height: 22px;
            }
        </style>
	</head>
	<body leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD><uc1:menu_top id="Menu_top1" runat="server"></uc1:menu_top></TD>
				</TR>
				<TR>
					<TD class="titleOnglet"><asp:label id="lblErrMsg" runat="server" Font-Size="X-Small" Font-Names="Arial" Visible="False"
							Width="100%" BackColor="Khaki" CssClass="fldlabel" BorderWidth="1px" BorderColor="Firebrick"></asp:label></TD>
				</TR>
				<TR>
					<TD id="tdpanel1" style="WIDTH: 708px"></TD>
				</TR>
                <tr>
                    <td id="user" runat="server" align="right" class="fldLabel">
                    </td>
                </tr>
                <tr>
                    <td id="user2" runat="server" align="left" class="style2">
                    </td>
                </tr>
				<TR>
					<TD style="height: 19px" class="fldLabel">
                        S�lectionnez l'�cole </TD>
				</TR>
				<TR>
					<TD style="height: 19px"><cc1:smartscroller id="SmartScroller1" runat="server"></cc1:smartscroller>
                        <asp:DropDownList ID="DropDownListEcoles" runat="server" AutoPostBack="True" CssClass="fldLabel" Width="300px">
                        </asp:DropDownList></TD>
				</TR>
                <tr>
                    <td style="height: 19px" class="fldLabel">
                        S�lectionnez l'�l�ve</td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList ID="DropDownListEleves" runat="server" CssClass="fldLabel" AutoPostBack="True" Width="300px">
                        </asp:DropDownList></td>
                </tr>
			</TABLE>
			<asp:panel id="Panel1" runat="server">
				<TABLE id="TablePanel1" style="WIDTH: 100%" borderColor="#00ff33" cellSpacing="0" cellPadding="1"
					border="0">
                    <tr>
                        <td class="fldLabel">
                            S�lectionnez l'�tape</td>
                    </tr>
					<TR>
						<TD>
							<asp:DropDownList id="DropDownListEtape" runat="server" CssClass="fldLabel" Width="201px" AutoPostBack="True">
								            <asp:ListItem Value="1">Bulletin de progr�s [semestre 1,terme 1]</asp:ListItem>
											<asp:ListItem Value="2">Premier bulletin [semestre 1, terme 2]</asp:ListItem>
											<asp:ListItem Value="3">Deuxi�me bulletins [semestre 2, terme 1]</asp:ListItem>
											<asp:ListItem Value="4">[semestre 2, terme 2]</asp:ListItem>
							</asp:DropDownList></TD>
					</TR>
					<TR>
						<TD class="style1">
							<asp:CheckBox id="CheckboxAll" runat="server" CssClass="fldLabel" Width="100%" AutoPostBack="True"
								Text="Imprimer tout"></asp:CheckBox></TD>
					</TR>
					<TR>
					    <td>
                            <asp:CheckBox ID="generales" runat="server" CssClass="fldLabel" 
                                Text="Donn�es G�n�rales" Width="100%" />
                        </td>
                    <tr>
                        <td>
                            <asp:CheckBox ID="forces" runat="server" CssClass="fldLabel" 
                                Text="Points forts et Besoins" Width="100%" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="matieres" runat="server" CssClass="fldLabel" 
                                Text="Mati�res" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style3">
                            <asp:CheckBox ID="competences" runat="server" CssClass="fldLabel" 
                                Text="Comp�tences" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="adaptations" runat="server" CssClass="fldLabel" 
                                Text="Adaptations" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="evaluations" runat="server" CssClass="fldLabel" 
                                Text="Donn�es d'�valuations" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="rh" runat="server" CssClass="fldLabel" 
                                Text="Ressources Humaines" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="buts" runat="server" CssClass="fldLabel" 
                                Text="Buts, Attentes et Strat�gies" Width="256px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="evaluation_prov" runat="server" CssClass="fldLabel" 
                                Text="Tests Provinciaux" Width="210px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="buts_transition" runat="server" CssClass="fldLabel" 
                                Text="Plan transition" Width="100%" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="signature" runat="server" CssClass="fldLabel" 
                                Text="Page de signature" />
                        </td>
                    </tr>
                    <tr>
                        <td class="fldLabel">
                            <hr>
                            </hr>
                        </td>
                    </tr>
                    <tr>
                        <td ID="EleveToPrint" runat="server" style="height: 22px">
                        </td>
                    </tr>
                    <tr>
                        <td runat="server" style="height: 22px">
                            <asp:Button ID="Button1" runat="server" CssClass="fldLabel" 
                                Text="Aper�u avant impression" Width="232px" />
                            <br />
                            <asp:HyperLink ID="HyperLink1" Target="_blank"  runat="server" Visible="False">visualiser le pei</asp:HyperLink>
                            <br />
                            <br />
                        </td>
                    </tr>
				</TABLE>
			</asp:panel></form>
	</body>
</HTML>
