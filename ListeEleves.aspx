<%@ Register TagPrefix="uc1" TagName="menu_top" Src="menu_top.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="PeiWebAccess.ListeEleves" CodeFile="ListeEleves.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="sstchur.web.SmartNav" Assembly="sstchur.web.SmartNav" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>- Liste des Eleves- </title>
		<LINK href="styles.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript">
		function refresh() 
		{
			parent.frames[0].location.reload();
		}
		</script>
	</HEAD>
	<body leftMargin="0" topMargin="0" onload="refresh();" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD><uc1:menu_top id="Menu_top1" runat="server"></uc1:menu_top></TD>
				</TR>
				<TR>
					<TD class="titleOnglet">Liste des �l�ves</TD>
				</TR>
				<TR>
					<TD class="titleOnglet"><asp:label id="lblErrMsg" runat="server" Visible="False" Width="100%" BackColor="Khaki" CssClass="fldlabel"
							BorderWidth="1px" BorderColor="Firebrick"></asp:label></TD>
				</TR>
				<TR>
					<TD><asp:button id="Button1" runat="server" BackColor="AliceBlue" CssClass="fldTextBox" Text="Cr�er un nouveau PEI"></asp:button></TD>
				</TR>
				<TR>
					<TD></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 40px"><asp:dropdownlist id="DropDownListEcoles" runat="server" Width="320px" CssClass="fldTextBox" AutoPostBack="True"></asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD><asp:datagrid id="DataGridListeEleves" Visible="false" runat="server" Width="100%" BackColor="White" CssClass="fldLabel"
							BorderWidth="1px" BorderColor="#CCCCCC" AutoGenerateColumns="False" BorderStyle="None" CellPadding="3"
							DataKeyField="pk_eleve_id" PageSize="100">
							<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
							<ItemStyle ForeColor="#000066"></ItemStyle>
							<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699"></HeaderStyle>
							<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
							<Columns>
								<asp:ButtonColumn Text="S&#233;lectionner" CommandName="Select"></asp:ButtonColumn>
								<asp:BoundColumn DataField="PK_ELEVE_ID" HeaderText="No Identification"></asp:BoundColumn>
								<asp:BoundColumn DataField="PRENOM_ELEVE" HeaderText="Pr&#233;nom"></asp:BoundColumn>
								<asp:BoundColumn DataField="NOM_ELEVE" HeaderText="Nom"></asp:BoundColumn>
								<asp:BoundColumn DataField="nom_ecole" HeaderText="&#201;cole">
									<HeaderStyle Width="250px"></HeaderStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="description_niveau" HeaderText="Niveau"></asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<tr>
					<td><cc1:smartscroller id="SmartScroller1" runat="server"></cc1:smartscroller>
                        <asp:datagrid id="DataGrid1" runat="server" Width="100%" BackColor="White" CssClass="fldLabel"
							BorderWidth="1px" BorderColor="#CCCCCC" AutoGenerateColumns="False" BorderStyle="None" CellPadding="3"
							DataKeyField="pk_eleve_id" PageSize="100">
							<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
							<ItemStyle ForeColor="#000066"></ItemStyle>
							<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699"></HeaderStyle>
							<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
							<Columns>
								<asp:ButtonColumn Text="S&#233;lectionner" CommandName="Select"></asp:ButtonColumn>
								<asp:BoundColumn DataField="PK_ELEVE_ID" HeaderText="No Identification"></asp:BoundColumn>
								<asp:BoundColumn DataField="PRENOM_ELEVE" HeaderText="Pr&#233;nom"></asp:BoundColumn>
								<asp:BoundColumn DataField="NOM_ELEVE" HeaderText="Nom"></asp:BoundColumn>
								
								<asp:BoundColumn DataField="fk_niveau_id" HeaderText="Niveau"></asp:BoundColumn>
								<asp:BoundColumn DataField="anomalies" HeaderText="Anomalie(s)"></asp:BoundColumn>
                                <asp:BoundColumn DataField="DATE_IDENTIFICATION" SortExpression="DATE_IDENTIFICATION" HeaderText="CIPR Initial" DataFormatString="{0:dd/MM/yyyy}">
                                    <HeaderStyle Wrap="False"></HeaderStyle>
								    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="DATE_CIPR_RECENT" SortExpression="DATE_CIPR_RECENT" HeaderText="Dernier CIPR" DataFormatString="{0:dd/MM/yyyy}">
                                        <HeaderStyle Wrap="False"></HeaderStyle>
								    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="DATE_RENONCIATION" SortExpression="DATE_RENONCIATION" HeaderText="Renonciation" DataFormatString="{0:dd/MM/yyyy}">
                                     <HeaderStyle Wrap="False"></HeaderStyle>
								    <ItemStyle Wrap="False"></ItemStyle>
                                </asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
                    </td>
				</tr>
                
                
			</TABLE>
		</form>
	</body>
</HTML>
