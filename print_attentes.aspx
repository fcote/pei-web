<%@ Page Language="vb" AutoEventWireup="false" Inherits="PeiWebAccess.print_attentes" CodeFile="print_attentes.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title></title>
		<LINK href="pei.css" type="text/css" rel="stylesheet">
		<meta content="False" name="vs_snapToGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TablePrintMenu" borderColor="black" cellSpacing="0" cellPadding="1" width="700"
				border="0">
				<TBODY>
					<tr>
						<td style="WIDTH: 706px"><asp:label id="lblMsg" runat="server" Width="100%" CssClass="fldLabel"></asp:label></td>
					</tr>
					<TR>
						<TD id="tdpanel1" style="WIDTH: 708px"><asp:panel id="Panel1" runat="server">
								<TABLE id="TablePanel1" style="WIDTH: 100%" borderColor="#00ff33" cellSpacing="0" cellPadding="1"
									border="0">
									<TR>
										<TD>
											<asp:DropDownList id="DropDownListEtape" runat="server" CssClass="fldLabel" Width="201px">
												<asp:ListItem Value="commentaire1" Selected="True">&#201;tape 1</asp:ListItem>
												<asp:ListItem Value="commentaire2">&#201;tape 2</asp:ListItem>
												<asp:ListItem Value="commentaire3">&#201;tape 3</asp:ListItem>
												<asp:ListItem Value="commentaire4">&#201;tape 4</asp:ListItem>
											</asp:DropDownList></TD>
									</TR>
									<TR>
										<TD>
											<asp:CheckBox id="CheckboxAll" runat="server" CssClass="fldLabel" Width="100%" Text="Imprimer tout"
												AutoPostBack="True"></asp:CheckBox></TD>
									</TR>
									<TR>
									<TR>
										<TD>
											<asp:CheckBox id="CheckBoxPage1" runat="server" CssClass="fldLabel" Width="100%" Text="Donn�es G�n�rales"></asp:CheckBox></TD>
									</TR>
									<TR>
										<TD>
											<asp:CheckBox id="CheckBoxForcesBesoins" runat="server" CssClass="fldLabel" Width="100%" Text="Points forts et Besoins"></asp:CheckBox></TD>
									</TR>
									<TR>
										<TD>
											<asp:CheckBox id="CheckBoxRendement" runat="server" CssClass="fldLabel" Width="100%" Text="Rendement"></asp:CheckBox></TD>
									</TR>
									<TR>
										<TD></TD>
									</TR>
									<TR>
										<TD class="fldLabel">
											<HR>
										</TD>
									</TR>
									<TR>
										<TD class="fldLabel"><STRONG>Buts</STRONG></TD>
									</TR>
									<TR>
										<TD>
											<asp:CheckBoxList id="CheckboxlistButs" runat="server" CssClass="fldLabel" Width="100%"></asp:CheckBoxList></TD>
									</TR>
									<TR>
										<TD>
											<HR>
										</TD>
									</TR>
									<TR>
										<TD>
											<asp:CheckBox id="CheckBoxPlanTransistion" runat="server" CssClass="fldLabel" Width="100%" Text="Plan transition"></asp:CheckBox></TD>
									</TR>
									<TR>
										<TD>
											<asp:CheckBox id="CheckBoxTestsProvinciaux" runat="server" CssClass="fldLabel" Width="100%" Text="Tests Provinciaux"></asp:CheckBox></TD>
									</TR>
									<TR>
										<TD></TD>
									</TR>
									<TR>
										<TD>
											<asp:HyperLink id="HyperLinkSignature" runat="server" CssClass="fldLabel" Width="100%" NavigateUrl="TEST/sign.aspx">Page de signatures et consultation</asp:HyperLink></TD>
									</TR>
									<TR>
										<TD></TD>
									</TR>
									<TR>
										<TD>
											<asp:button id="Button1" runat="server" CssClass="fldLabel" Width="232px" Text="Aper�u avant impression"></asp:button></TD>
									</TR>
								</TABLE>
							</asp:panel></TD>
					</TR>
					<TR>
						<TD id="tdHeader" style="WIDTH: 708px"><asp:panel id="PanelInfosEcole" runat="server" Width="710px" Visible="False" Height="296px">
								<TABLE id="TableHeader" borderColor="#009933" cellSpacing="0" cellPadding="3" width="100%"
									border="0">
									<TR>
										<TD style="HEIGHT: 109px">
											<P align="center"><FONT face="Arial" size="5"><STRONG>LE PLAN D'ENSEIGNEMENT INDIVIDUALIS�
														<BR>
													</STRONG><FONT size="4">(2004-2005)</FONT></FONT></P>
										</TD>
									</TR>
									<TR>
										<TD>
											<TABLE id="TableMain" borderColor="blue" width="100%" border="0">
												<TR>
													<TD style="WIDTH: 125px" vAlign="top"><IMG style="WIDTH: 114px; HEIGHT: 137px" height="137" src="images/CSDCCS_logo.gif" width="114"></TD>
													<TD style="WIDTH: 279px" vAlign="top">
														<TABLE id="TableConseil" cellSpacing="0" cellPadding="3" border="0">
															<TR>
																<TD class="fldLabel" style="HEIGHT: 10px" vAlign="bottom"><STRONG>Conseil:</STRONG></TD>
															</TR>
															<TR>
																<TD class="fldLabel" id="TDconseil" style="HEIGHT: 54px" vAlign="top" runat="server"></TD>
															</TR>
															<TR>
																<TD class="fldLabel" style="HEIGHT: 9px" vAlign="bottom"><STRONG>Adresse:</STRONG></TD>
															</TR>
															<TR>
																<TD class="fldLabel" id="TDadresseConseil" style="HEIGHT: 5px" vAlign="top" runat="server"></TD>
															</TR>
															<TR>
																<TD class="fldLabel" id="TDvilleConseil" style="HEIGHT: 12px" vAlign="top" runat="server"></TD>
															</TR>
															<TR>
																<TD class="fldLabel" id="TDCodePostalConseil" runat="server"></TD>
															</TR>
														</TABLE>
													<TD vAlign="top">
														<TABLE id="TableEcole" cellSpacing="0" cellPadding="1" border="0">
															<TR>
																<TD class="fldLabel" style="HEIGHT: 3px" vAlign="bottom"><STRONG>�cole:</STRONG></TD>
															</TR>
															<TR>
																<TD class="fldLabel" id="TDecole" style="HEIGHT: 22px" vAlign="top" runat="server"></TD>
															</TR>
															<TR>
																<TD class="fldLabel" id="TD" style="HEIGHT: 3px" vAlign="bottom"><STRONG>Direction:</STRONG></TD>
															</TR>
															<TR>
																<TD class="fldLabel" id="TDdirection" style="HEIGHT: 22px" vAlign="top" runat="server"></TD>
															</TR>
															<TR>
																<TD class="fldLabel" style="HEIGHT: 2px" vAlign="bottom"><STRONG>Adresse:</STRONG></TD>
															</TR>
															<TR>
																<TD class="fldLabel" id="TDadresseEcole" style="HEIGHT: 11px" vAlign="top" runat="server"></TD>
															</TR>
															<TR>
																<TD class="fldLabel" id="TDvilleEcole" style="HEIGHT: 11px" vAlign="top" runat="server"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
								</TABLE>
							</asp:panel></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 708px"><asp:panel id="panelGenerale" runat="server" Visible="False">
								<TABLE id="TableGenerale" borderColor="#009933" cellSpacing="0" cellPadding="1" width="100%"
									border="0">
									<TR>
										<TD>
											<HR>
										</TD>
									</TR>
									<TR>
										<TD class="titlereport">PROFIL&nbsp;DE L'�L�VE</TD>
									</TR>
									<TR>
										<TD>
											<TABLE id="TableEleve1" borderColor="#cc00cc" cellSpacing="0" cellPadding="1" width="100%"
												border="0">
												<TR>
													<TD vAlign="top" width="50%">
														<TABLE id="TableEleve1_left" cellSpacing="0" cellPadding="1" width="100%" border="0">
															<TR>
																<TD class="fldLabel" style="WIDTH: 179px">Nom de l'�l�ve:</TD>
																<TD class="fldLabel" id="TDNomEleve" runat="server"></TD>
															</TR>
															<TR>
																<TD class="fldLabel" style="WIDTH: 179px">Date de naissance:</TD>
																<TD class="fldLabel" id="TDdateNaissance" runat="server"></TD>
															</TR>
															<TR>
																<TD class="fldLabel" style="WIDTH: 179px">No d'identification:</TD>
																<TD class="fldLabel" id="TDnoIdentification" runat="server"></TD>
															</TR>
															<TR>
																<TD class="fldLabel" style="WIDTH: 179px">Sexe:</TD>
																<TD class="fldLabel" id="TDsexe" runat="server"></TD>
															</TR>
															<TR>
																<TD class="fldLabel" style="WIDTH: 179px">Ann�e d'�tude ou classe distincte</TD>
																<TD class="fldLabel" id="TDNiveau" runat="server"></TD>
															</TR>
															<TR>
																<TD class="fldLabel" style="WIDTH: 179px">Titulaire</TD>
																<TD class="fldLabel" id="TDtitulaire" runat="server"></TD>
															</TR>
														</TABLE>
													</TD>
													<TD vAlign="top" width="50%">
														<TABLE id="TableEleve1_right" cellSpacing="0" cellPadding="1" width="100%">
															<TR>
																<TD class="fldLabel" style="WIDTH: 73.77%">Date du CIPR le plus r�cent:</TD>
																<TD class="fldLabel" id="TDDateCiprRecent" runat="server"></TD>
															</TR>
															<TR>
																<TD class="fldLabel" style="WIDTH: 73.77%">Date de la renonciation de la
																	<BR>
																	R�vision annuelle par le parent/le tuteur/la tutrice</TD>
																<TD class="fldLabel" id="TDdateRenonciation" runat="server"></TD>
															</TR>
															<TR>
																<TD class="fldLabel" style="WIDTH: 73.77%">Date du d�but de placement</TD>
																<TD class="fldLabel" id="TDdateDebutPlacement" runat="server"></TD>
															</TR>
															<TR>
																<TD class="fldLabel" style="WIDTH: 73.77%">�laboration du PEI termin� le:</TD>
																<TD class="fldLabel" id="TDdatePeiTermine" runat="server"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
									<TR>
										<TD>
											<TABLE id="TableEleve2" borderColor="#cc0000" cellSpacing="0" cellPadding="1" width="100%"
												border="0">
												<TR>
													<TD vAlign="top" width="50%">
														<TABLE id="TableEleve2_left" cellSpacing="0" cellPadding="1" width="100%" border="0">
															<TR>
																<TD class="fldLabel" colSpan="2">Selon la d�cision du CIPR</TD>
															</TR>
															<TR>
																<TD class="fldLabel" style="WIDTH: 177px">Placement:</TD>
																<TD class="fldLabel" id="TDplacement" runat="server"></TD>
															</TR>
															<TR>
																<TD class="fldLabel" style="WIDTH: 177px">Anomalie(s) de l'�l�ve identifi�:</TD>
																<TD class="fldLabel" id="TDanomalie" runat="server"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 177px"></TD>
																<TD></TD>
															</TR>
														</TABLE>
													</TD>
													<TD vAlign="top" width="50%">
														<TABLE id="TableEleve2_Right" cellSpacing="0" cellPadding="1" width="100%" border="0">
															<TR>
																<TD class="fldLabel"><STRONG>PEI �labor� par:</STRONG></TD>
															</TR>
															<TR>
																<TD>
																	<asp:datagrid id="DataGridElaborations" runat="server" CssClass="fldLabel" Width="100%" BackColor="White"
																		BorderColor="White" BorderWidth="0px" CellPadding="3" AutoGenerateColumns="False" BorderStyle="None"
																		PageSize="20" ShowHeader="False">
																		<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
																		<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699"></HeaderStyle>
																		<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
																		<Columns>
																			<asp:BoundColumn Visible="False" DataField="row_id" SortExpression="row_id"></asp:BoundColumn>
																			<asp:TemplateColumn HeaderText="&#201;labor&#233; par:">
																				<ItemTemplate>
																					<asp:Label ID="lblPrenom" Text='<%# DataBinder.Eval(Container.DataItem, "prenom") %>' Runat="server"/>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:TemplateColumn HeaderText="Type d'intervenants">
																				<HeaderStyle Wrap="False" Width="200px"></HeaderStyle>
																				<ItemStyle Wrap="False"></ItemStyle>
																				<ItemTemplate>
																					<asp:Label ID="lblTitre" Text='<%# DataBinder.Eval(Container.DataItem, "titre") %>' Runat="server"/>
																					<asp:Label Visible=False ID="lbltitreID" Text='<%# DataBinder.Eval(Container.DataItem, "titre") %>' Runat="server"/>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																		</Columns>
																		<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
																	</asp:datagrid></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
											<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD class="fldLabel" id="TDformatCommunication" colSpan="3">
														<HR>
													</TD>
												</TR>
												<TR>
													<TD class="fldLabel"><STRONG>Format de communication du rendement</STRONG></TD>
													<TD class="fldLabel"><STRONG>Raison&nbsp;justifiant l'�laboration du PEI</STRONG></TD>
													<TD class="fldLabel"><STRONG>Exemption du programme au palier �l�mentaire / 
															substitutions pour les cours obligatoires au palier secondaire</STRONG></TD>
												</TR>
												<TR>
													<TD class="fldLabel">Le rendement de l'�l�ve pour les attentes modifi�e ou pour les 
														cours ou mati�res comportant des adaptations sera not� sur le Bulletin scolaire 
														de l'Ontario. Le rendement de l'�l�ve pour les attentes diff�rentes sera not� 
														sur le PEI.</TD>
													<TD class="fldLabel" id="TDraison" vAlign="top" runat="server"></TD>
													<TD class="fldLabel" id="TD_exemp_prog_scol_1TEMP" vAlign="top" runat="server">
														<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="100%" border="0">
															<TR>
																<TD class="fldLabel" id="TD_exemp_prog_scol_1" style="HEIGHT: 30px" runat="server"></TD>
															</TR>
															<TR>
																<TD class="fldLabel" id="TDdiplome" runat="server"></TD>
															</TR>
															<TR>
																<TD></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD></TD>
													<TD vAlign="top"></TD>
													<TD class="fldLabel" id="TDdiplomeTEMP" vAlign="top"></TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
									<TR>
										<TD></TD>
									</TR>
								</TABLE>
							</asp:panel></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 706px"></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 706px"><asp:panel id="PanelForcesBesoins" runat="server" Width="100%" Visible="False">
								<TABLE id="Table6" borderColor="#990099" cellSpacing="0" cellPadding="1" width="100%" border="0">
									<TR>
										<TD vAlign="top">
											<TABLE id="Table5" borderColor="#ff3366" cellSpacing="0" cellPadding="1" width="100%" border="0">
												<TR>
													<TD colSpan="2">
														<TABLE id="Table17" cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD class="titlereport" id="TDpointsForts">
																	<HR>
																</TD>
															</TR>
															<TR>
																<TD class="titlereport">POINTS FORTS ET BESOINS DE L'�L�VE</TD>
															</TR>
															<TR>
																<TD>
																	<TABLE id="TableForcesBesoins" style="PADDING-RIGHT: 1px; PADDING-LEFT: 1px; PADDING-BOTTOM: 1px; PADDING-TOP: 1px"
																		borderColor="#000033" cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD class="fldLabel" style="WIDTH: 50%"><STRONG>Points forts</STRONG></TD>
																			<TD class="fldLabel" style="WIDTH: 50%"><STRONG>Besoins</STRONG></TD>
																		</TR>
																		<TR>
																			<TD class="fldLabel" id="TDforces" vAlign="top" runat="server"></TD>
																			<TD class="fldLabel" id="TDbesoins" vAlign="top" runat="server"></TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
									<TR>
										<TD class="titleReport" vAlign="top">
											<TABLE id="Table8" cellSpacing="1" cellPadding="1" width="100%" border="0">
												<TR>
													<TD>
														<TABLE id="Table9" cellSpacing="1" cellPadding="1" width="100%" border="0">
															<TR>
																<TD class="titlereport" id="TDdonneesEvaluation" style="HEIGHT: 16px">
																	<HR>
																</TD>
															</TR>
															<TR>
																<TD class="titlereport">DONN�ES D'�VALUATION</TD>
															</TR>
															<TR>
																<TD>
																	<asp:DataGrid id="DataGridRapEval" runat="server" CssClass="fldLabel" Width="100%" AutoGenerateColumns="False"
																		GridLines="None">
																		<HeaderStyle Font-Bold="True"></HeaderStyle>
																		<Columns>
																			<asp:TemplateColumn HeaderText="Sources d'&#233;valuation">
																				<HeaderStyle Width="150px"></HeaderStyle>
																				<ItemTemplate>
																					<asp:Label id="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.type_eval") %>'>
																					</asp:Label>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:BoundColumn DataField="Date_Eval" SortExpression="DATE_EVAL" HeaderText="Date" DataFormatString="{0:yyyy/MM/dd}">
																				<HeaderStyle Wrap="False"></HeaderStyle>
																				<ItemStyle Wrap="False"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:TemplateColumn HeaderText="R&#233;sum&#233; des conclusions">
																				<ItemTemplate>
																					<asp:Label id="Label4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.sommaire") %>'>
																					</asp:Label>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																		</Columns>
																	</asp:DataGrid></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
									<TR>
										<TD class="titleReport" style="HEIGHT: 20px" vAlign="top"></TD>
									</TR>
									<TR>
										<TD>
											<TABLE id="Table19" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD>
														<TABLE id="Table20" cellSpacing="1" cellPadding="1" width="100%" border="0">
															<TR>
																<TD class="titlereport" id="TDressourcesHumaines">
																	<HR>
																</TD>
															</TR>
															<TR>
																<TD class="titlereport">RESSOURCES HUMAINES (personnel enseignant et non 
																	enseignant)</TD>
															</TR>
															<TR>
																<TD>
																	<asp:datagrid id="DatagridRH" runat="server" CssClass="fldLabel" Width="100%" Height="0px" BackColor="White"
																		BorderColor="#CCCCCC" BorderWidth="0px" CellPadding="3" AutoGenerateColumns="False" BorderStyle="None"
																		DataMember="RH_ELEVES" DataKeyField="ROW_ID">
																		<Columns>
																			<asp:BoundColumn Visible="False" DataField="ROW_ID" SortExpression="ROW_ID" HeaderText="ROW_ID"></asp:BoundColumn>
																			<asp:BoundColumn DataField="Date_Inst" SortExpression="DATE_INST" HeaderText="Date d'instauration"
																				DataFormatString="{0:yyyy/MM/dd}">
																				<HeaderStyle Wrap="False"></HeaderStyle>
																				<ItemStyle Wrap="False"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:TemplateColumn HeaderText="Type de service">
																				<HeaderStyle Wrap="False"></HeaderStyle>
																				<ItemStyle Wrap="False"></ItemStyle>
																				<ItemTemplate>
																					<asp:Label ID="lblTypEval" Text='<%# DataBinder.Eval(Container.DataItem, "FK_TYPE_SERVICE") %>' Runat="server"/>
																				</ItemTemplate>
																				<EditItemTemplate>
																					<asp:Label ID="lblTypeEvalHold" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FK_TYPE_SERVICE") %>' />
																				</EditItemTemplate>
																			</asp:TemplateColumn>
																			<asp:BoundColumn DataField="FREQ" SortExpression="FREQ" HeaderText="Fr&#233;quence">
																				<HeaderStyle Wrap="False"></HeaderStyle>
																				<ItemStyle Wrap="False"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn DataField="LIEU_INST" SortExpression="LIEU_INST" HeaderText="Lieu/endroit">
																				<HeaderStyle Wrap="False"></HeaderStyle>
																				<ItemStyle Wrap="False"></ItemStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn Visible="False" DataField="LIEU_INST" SortExpression="LIEU_INST" HeaderText="Lieu/endroit">
																				<HeaderStyle Wrap="False"></HeaderStyle>
																				<ItemStyle Wrap="False"></ItemStyle>
																			</asp:BoundColumn>
																		</Columns>
																	</asp:datagrid></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
									<TR>
										<TD class="titleReport"></TD>
									</TR>
									<TR>
										<TD></TD>
									</TR>
									<TR>
										<TD>
											<TABLE id="TableProbSanteConnexes" borderColor="#993333" cellSpacing="0" cellPadding="1"
												width="100%" border="0">
												<TR>
													<TD>
														<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD></TD>
															</TR>
															<TR>
																<TD class="fldLabel">Services auxiliaires de sant� et personnel de soutien requis:
																	<asp:CheckBoxList id="CheckBoxListProbSanteConn" runat="server" RepeatDirection="Horizontal">
																		<asp:ListItem Value="oui">oui</asp:ListItem>
																		<asp:ListItem Value="non">non</asp:ListItem>
																	</asp:CheckBoxList></TD>
															</TR>
															<TR>
																<TD class="fldLabel" id="TDProbSanteConn" runat="server"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
								</TABLE>
							</asp:panel></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 707px"><asp:panel id="PanelRendement" runat="server" Width="100%" Visible="False">
								<TABLE id="Table10" borderColor="#003366" cellSpacing="0" cellPadding="1" width="100%"
									border="0">
									<TR>
										<TD></TD>
									</TR>
								</TABLE>
							</asp:panel></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 708px"><asp:panel id="PanelCompetencesHabil" runat="server" Visible="False">
								<TABLE id="TablePanelCompetencesHabil" borderColor="green" cellSpacing="1" cellPadding="1"
									width="100%" border="0">
									<TR>
										<TD>
											<HR>
										</TD>
									</TR>
									<TR>
										<TD class="TITLEREPORT">MATIERES, COURS OU PROGRAMMES AUXQUELS S'APPLIQUE LE PEI</TD>
									</TR>
									<TR>
										<TD>
											<TABLE id="tableMainCompetencesHabilSources" cellSpacing="1" cellPadding="1" width="100%"
												border="0">
												<TR>
													<TD vAlign="top">
														<TABLE id="Table13" cellSpacing="1" cellPadding="1" border="0">
															<TR>
																<TD class="tableCompetenceNiveauRendement" vAlign="top">
																	<asp:datagrid id="DataGrid2" runat="server" CssClass="fldLabel" Width="599px" CellPadding="1"
																		AutoGenerateColumns="False" PageSize="20" GridLines="None" DataMember="COMPETENCES_ELEVES"
																		DataKeyField="PK_ROW_ID" CellSpacing="1">
																		<HeaderStyle Font-Bold="True"></HeaderStyle>
																		<Columns>
																			<asp:BoundColumn Visible="False" DataField="PK_ROW_ID" SortExpression="PK_ROW_ID" HeaderText="PK_ROW_ID"></asp:BoundColumn>
																			<asp:TemplateColumn>
																				<ItemTemplate>
																					<asp:Label visible=false ID="lblRowId2" Text='<%# DataBinder.Eval(Container.DataItem, "pk_row_id") %>' Runat="server"/>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:TemplateColumn HeaderText="Comp&#233;tences/habilet&#233;s">
																				<ItemStyle Wrap="False"></ItemStyle>
																				<ItemTemplate>
																					<asp:Label ID="lblCompetence" Text='<%# DataBinder.Eval(Container.DataItem, "competence") %>' Runat="server"/>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:TemplateColumn HeaderText="Type de l'attente">
																				<HeaderStyle Wrap="False"></HeaderStyle>
																				<ItemStyle Wrap="False"></ItemStyle>
																				<ItemTemplate>
																					<asp:Label ID="lblTypeAttente2" Text='<%# DataBinder.Eval(Container.DataItem, "type_attente") %>' Runat="server"/>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																		</Columns>
																	</asp:datagrid></TD>
															</TR>
															<TR>
																<TD class="TITLEREPORT">
																	<HR>
																	NIVEAU DE RENDEMENT
																</TD>
															</TR>
															<TR>
																<TD class="fldLabel">
																	<asp:datagrid id="DataGrid1" runat="server" CssClass="fldLabel" Width="599px" CellPadding="1"
																		AutoGenerateColumns="False" PageSize="20" GridLines="None" DataMember="MATIERES_ELEVES" DataKeyField="ROW_ID"
																		CellSpacing="1">
																		<HeaderStyle Font-Bold="True"></HeaderStyle>
																		<Columns>
																			<asp:BoundColumn Visible="False" DataField="ROW_ID" SortExpression="ROW_ID" HeaderText="*ROW_ID"></asp:BoundColumn>
																			<asp:TemplateColumn>
																				<ItemTemplate>
																					<asp:Label visible=false ID="labelRowID" Text='<%# DataBinder.Eval(Container.DataItem, "row_id") %>' Runat="server"/>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:TemplateColumn HeaderText="Mati&#232;re/cours                      ">
																				<HeaderStyle Font-Bold="True" Wrap="False"></HeaderStyle>
																				<ItemStyle Wrap="False"></ItemStyle>
																				<ItemTemplate>
																					<asp:Label ID="lblMatiere" Text='<%# DataBinder.Eval(Container.DataItem, "matiere") %>' Runat="server"/>
																					<asp:Label Visible=False ID="lblMatiereID" Text='<%# DataBinder.Eval(Container.DataItem, "fk_matiere_id") %>' Runat="server"/>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:TemplateColumn HeaderText="Type d'attente">
																				<ItemTemplate>
																					<asp:Label ID="lblTypeAttente" Text='<%# DataBinder.Eval(Container.DataItem, "type_attente") %>' Runat="server"/>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																		</Columns>
																	</asp:datagrid></TD>
															</TR>
															<TR>
																<TD>
																	<HR>
																</TD>
															</TR>
															<TR>
																<TD>
																	<TABLE id="TableSourceInformations" cellSpacing="1" cellPadding="1" width="100%" border="0">
																		<TR>
																			<TD class="fldLabel"><STRONG>Sources d'information</STRONG></TD>
																		</TR>
																		<TR>
																			<TD>
																				<asp:checkboxlist id="CheckBoxListeSources" runat="server" CssClass="fldLabel" Width="100%" RepeatColumns="2"></asp:checkboxlist></TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
															<TR>
																<TD class="fldLabel"><STRONG>�quipement personnalis�</STRONG></TD>
															</TR>
															<TR>
																<TD class="fldLabel" id="tdEquipPers" runat="server"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
									<TR>
									</TR>
								</TABLE>
								<TR>
									<TD id="TDframeAdaptatations" style="WIDTH: 707px">
										<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
											<TR>
												<TD id="TDadaptations_2">
													<HR>
												</TD>
											</TR>
											<TR>
												<TD class="TITLEREPORT">ADAPTATIONS</TD>
											</TR>
											<TR>
												<TD class="fldLabel" style="HEIGHT: 6px">(Les adaptations sont les m�mes pour 
													toutes les mati�res, � moins d'indication contraire.)</TD>
											</TR>
											<TR>
												<TD>
													<TABLE id="Table21" cellSpacing="0" cellPadding="0" width="100%" border="1">
														<TR>
															<TD class="fldLabel" style="HEIGHT: 13px"><STRONG>Adaptation p�dagogiques</STRONG></TD>
															<TD class="fldLabel" style="HEIGHT: 13px"><STRONG>Adaptations environnementales</STRONG></TD>
															<TD class="fldLabel" style="HEIGHT: 13px"><STRONG>Adaptations en mati�re d'�valuation</STRONG></TD>
														</TR>
														<TR>
															<TD class="fldLabel" id="tdAdapt_pedagogiques" vAlign="top" runat="server"></TD>
															<TD class="fldLabel" id="TDadapt_envrionnementales" vAlign="top" runat="server"></TD>
															<TD class="fldLabel" id="TDadapt_matiereevaluation" vAlign="top" runat="server"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</asp:panel></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 707px"></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 707px"><asp:datalist id="DataList2" runat="server" Width="100%" CssClass="fldLabel" Font-Names="Arial">
								<HeaderTemplate>
								</HeaderTemplate>
								<FooterTemplate>
								</FooterTemplate>
								<ItemTemplate>
									<table width="100%" border="1" bordercolor="gray">
										<!--<table width="100%" border="0" bordercolor="green">-->
										<tr>
											<td colspan="5" style="TABLE-LAYOUT: auto; BORDER-LEFT-COLOR: #ffffff; BORDER-BOTTOM-COLOR: #ffffff; BORDER-TOP-STYLE: solid; BORDER-TOP-COLOR: #ffffff; BORDER-RIGHT-STYLE: solid; BORDER-LEFT-STYLE: solid; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #ffffff; BORDER-BOTTOM-STYLE: solid">
												<asp:Label Runat=server ForeColor=black Font-Size=11 Font-Bold=True Width=100% text='<% #DataBinder.Eval(Container.DataITem, "but_titre")%>' ID="lblbut_titre">
												</asp:Label>
											<td>
										<tr />
										<tr>
											<td class="fldLabel" colspan="5">
												<asp:Label Runat=server text='<% #DataBinder.Eval(Container.DataITem, "annee_etude")%>' ID="lblbut_annee_etude">
												</asp:Label>
											</td>
										</tr>
										<tr>
											<td class="fldLabel" colspan="5">
												<asp:Label Runat=server text='<% #DataBinder.Eval(Container.DataITem, "programme")%>' ID="lblbut_programme">
												</asp:Label>
											</td>
										</tr>
										<tr>
											<td class="fldLabel" colspan="5">
												<asp:Label Runat=server text='<% #DataBinder.Eval(Container.DataITem, "niveau_rendement")%>' ID="lblbut_niveau_rendement">
												</asp:Label>
											</td>
										</tr>
										<tr>
											<td class="fldLabel" colspan="5">
												<asp:Label Runat="server" text='<% #DataBinder.Eval(Container.DataITem, "commentaire")%>' ID="lblCommentaire">
												</asp:Label>
											</td>
										</tr>
										<tr>
											<td class="fldLabelHeader" valign="top" style="TABLE-LAYOUT: auto; BORDER-LEFT-COLOR: #ffffff; BORDER-BOTTOM-COLOR: #ffffff; BORDER-TOP-STYLE: solid; BORDER-TOP-COLOR: #ffffff; BORDER-RIGHT-STYLE: solid; BORDER-LEFT-STYLE: solid; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #ffffff; BORDER-BOTTOM-STYLE: solid">
												<asp:Label Runat="server" Width="100%" text="Attentes d'apprentissage" ID="lblHeaderAttente"></asp:Label><br>
											</td>
											<td class="fldLabelHeader" valign="top" style="TABLE-LAYOUT: auto; BORDER-LEFT-COLOR: #ffffff; BORDER-BOTTOM-COLOR: #ffffff; BORDER-TOP-STYLE: solid; BORDER-TOP-COLOR: #ffffff; BORDER-RIGHT-STYLE: solid; BORDER-LEFT-STYLE: solid; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #ffffff; BORDER-BOTTOM-STYLE: solid">
												<asp:Label Runat="server" Width="100%" text="Strat�gies p�dagogiques" ID="lblHeaderStrategie"></asp:Label><br>
											</td>
											<td class="fldLabelHeader" valign="top" style="TABLE-LAYOUT: auto; BORDER-LEFT-COLOR: #ffffff; BORDER-BOTTOM-COLOR: #ffffff; BORDER-TOP-STYLE: solid; BORDER-TOP-COLOR: #ffffff; BORDER-RIGHT-STYLE: solid; BORDER-LEFT-STYLE: solid; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #ffffff; BORDER-BOTTOM-STYLE: solid">
												<asp:Label Runat="server" Width="100%" text="M�thodes d'�valuation" ID="lblHeaderMethode"></asp:Label><br>
											</td>
										</tr>
										<tr>
											<td class="fldLabel" valign="top" style="TABLE-LAYOUT: auto; BORDER-LEFT-COLOR: #ffffff; BORDER-BOTTOM-COLOR: #ffffff; BORDER-TOP-STYLE: solid; BORDER-TOP-COLOR: #ffffff; BORDER-RIGHT-STYLE: solid; BORDER-LEFT-STYLE: solid; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #ffffff; BORDER-BOTTOM-STYLE: solid"
												valign="top" width="33%">
												<asp:Label Runat=server text='<% #DataBinder.Eval(Container.DataITem, "Attente")%>' ID="lblAttente" >
												</asp:Label>
											</td>
											<td class="fldLabel" valign="top" style="TABLE-LAYOUT: auto; BORDER-LEFT-COLOR: #ffffff; BORDER-BOTTOM-COLOR: #ffffff; BORDER-TOP-STYLE: solid; BORDER-TOP-COLOR: #ffffff; BORDER-RIGHT-STYLE: solid; BORDER-LEFT-STYLE: solid; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #ffffff; BORDER-BOTTOM-STYLE: solid"
												valign="top" width="33%">
												<asp:Label Runat="server" text='<% #DataBinder.Eval(Container.DataITem, "Strategie")%>' ID="lblStrategie">
												</asp:Label>
											</td>
											<td class="fldLabel" valign="top" style="TABLE-LAYOUT: auto; BORDER-LEFT-COLOR: #ffffff; BORDER-BOTTOM-COLOR: #ffffff; BORDER-TOP-STYLE: solid; BORDER-TOP-COLOR: #ffffff; BORDER-RIGHT-STYLE: solid; BORDER-LEFT-STYLE: solid; BORDER-COLLAPSE: collapse; BORDER-RIGHT-COLOR: #ffffff; BORDER-BOTTOM-STYLE: solid"
												valign="top" width="33%">
												<asp:Label Runat=server text='<% #DataBinder.Eval(Container.DataITem, "Methode")%>' ID="lblMethode">
												</asp:Label>
											</td>
										</tr>
									</table>
								</ItemTemplate>
							</asp:datalist></TD>
					</TR>
					<TR>
						<TD><asp:panel id="panelTestsProvinciaux" runat="server" Width="100%" Visible="False" Height="58">
								<TABLE id="TableMainTestProvinciaux" cellSpacing="1" cellPadding="1" width="100%" border="0">
									<TR>
										<TD>
											<HR>
										</TD>
									</TR>
									<TR>
										<TD class="TITLEREPORT">�VALUATIONS PROVINCIALES (adaptations et exemptions)</TD>
									</TR>
									<TR>
										<TD>
											<TABLE id="Table4" cellSpacing="1" cellPadding="1" width="100%" border="0">
												<TR>
													<TD class="fldLabel" style="WIDTH: 164px">�valuations provinciales auxquelles 
														participera l'�l�ve pendant l'ann�ee scolaire:</TD>
													<TD class="fldLabel" id="TDevalProv" runat="server"></TD>
												</TR>
												<TR>
													<TD class="fldLabel" style="WIDTH: 164px">Adaptations:</TD>
													<TD class="fldLabel" id="tdAdaptationsTests" runat="server"></TD>
												</TR>
												<TR>
													<TD class="fldLabel" style="WIDTH: 164px">Exemptions:</TD>
													<TD class="fldLabel" id="tdExemptions" runat="server"></TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
									<TR>
										<TD></TD>
									</TR>
								</TABLE>
							</asp:panel></TD>
					<tr>
						<TD><asp:panel id="PanelPlanTransition" runat="server" Width="100%" Visible="False">
								<TABLE id="TablePlanTransition" borderColor="#3366ff" cellSpacing="1" cellPadding="1" width="100%"
									border="0">
									<TR>
										<TD>
											<HR>
										</TD>
									</TR>
									<TR>
										<TD class="TITLEREPORT">PLAN DE TRANSITION</TD>
									</TR>
									<TR>
										<TD>
											<asp:DataGrid id="DataGridButTransition" runat="server" CssClass="fldLabel" Width="99%" AutoGenerateColumns="False"
												PageSize="20" GridLines="None">
												<HeaderStyle Font-Bold="True"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn HeaderText="Buts particuliers pour la transition vers des activit&#233;s postsecondaires">
														<ItemTemplate>
															<asp:Label id="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
															</asp:Label>
														</ItemTemplate>
													</asp:TemplateColumn>
												</Columns>
											</asp:DataGrid></TD>
									</TR>
									<TR>
										<TD>
											<asp:DataGrid id="DataGridMesures" runat="server" CssClass="fldLabel" Width="99%" AutoGenerateColumns="False"
												GridLines="None">
												<HeaderStyle Font-Bold="True"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn HeaderText="Mesures">
														<ItemTemplate>
															<asp:Label id="Label6" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mesures") %>'>
															</asp:Label>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Responsables">
														<ItemTemplate>
															<asp:Label id="Label7" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.responsables") %>'>
															</asp:Label>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="&#201;ch&#233;anciers">
														<ItemTemplate>
															<asp:Label id="Label8" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.echeancier") %>'>
															</asp:Label>
														</ItemTemplate>
													</asp:TemplateColumn>
												</Columns>
											</asp:DataGrid></TD>
									<TR>
									</TR>
								</TABLE>
							</asp:panel></TD>
					</tr>
				</TBODY>
			</TABLE>
			</TD></TR></TD></TR></TBODY></TABLE></form>
	</body>
</HTML>
