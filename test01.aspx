﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="test01.aspx.vb" Inherits="test01" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    
   <table  id="TableMainAdaptations" borderColor="#ff66ff" cellSpacing="0" cellPadding="0" border="0">
							<TR>
							    <TD class="cbToolbar">Adaptations en matière d'évaluation</TD>
							    <TD class="cbToolbar">Adaptations environnementales</TD>
								<TD class="cbToolbar">Adaptations pédagogiques</TD>	
							</TR>
							
							<TR>
							
							
								
						    <TD align="left">
									<TABLE id="TableAdapEval" cellSpacing="0" cellPadding="0" border="0">
										<TR>
											<TD style="HEIGHT: 19px"><asp:dropdownlist id="DropDownListAdaptMatiereEval" runat="server" Width="330"   CssClass="fldLabel"></asp:dropdownlist>
											<asp:imagebutton id="ImageButtonAjouterAdaptEval" runat="server" ImageUrl="images/create.gif"></asp:imagebutton></TD>
										</TR>
										<TR>
											<TD><asp:textbox id="TextBoxAdaptMatiereEval" runat="server" Width="350px"  CssClass="fldTextBox" Height="200px"
													TextMode="MultiLine"></asp:textbox></TD>
										</TR>
									</TABLE>
								</TD>
								
							<td align="left">
									<TABLE id="TableAdaptEnv" cellSpacing="0" cellPadding="0" border="0">
										<TR>
											<TD style="HEIGHT: 19px"><asp:dropdownlist id="DropDownListAdaptationsEnv" runat="server" Width="330"  CssClass="fldLabel"></asp:dropdownlist><asp:imagebutton id="ImageButtonAjouterAdaptations" runat="server" ImageUrl="images/create.gif"></asp:imagebutton></TD>
										</TR>
										<TR>
											<TD><asp:textbox id="txtAdaptEnv" runat="server" Width="350px" CssClass="fldTextBox" Height="200px"
													TextMode="MultiLine"></asp:textbox></TD>
										</TR>
									</TABLE>
								</td>
								
							<TD align="left">
									<TABLE id="TableAdaptPedag" cellSpacing="0" cellPadding="0" border="0">
										<TR>
											<TD style="HEIGHT: 19px"><asp:dropdownlist id="DropDownListAdaptPedagogique" runat="server" Width="330"   CssClass="fldLabel"></asp:dropdownlist>
											<asp:imagebutton id="ImageButtonAjouterAdapPedag" runat="server" ImageUrl="images/create.gif"></asp:imagebutton></TD>
										</TR>
										<TR>
											<TD><asp:textbox id="txtAdaptPedagogiques" runat="server" Width="350px"  CssClass="fldTextBox" Height="200px"
													TextMode="MultiLine"></asp:textbox></TD>
										</TR>
									</TABLE>
								</TD>
								
								
								
								
								
								
								
							</TR>
						</table>
    </form>
</body>
</html>
