<%@ Register TagPrefix="cc1" Namespace="sstchur.web.SmartNav" Assembly="sstchur.web.SmartNav" %>
<%@ Page Language="vb" AutoEventWireup="false" ValidateRequest="false" SmartNavigation = "true" Inherits="PeiWebAccess.programmes" CodeFile="programmes.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="menu_top" Src="menu_top.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>programmes</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table id="TableMain" borderColor="red" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td><uc1:menu_top id="Menu_top1" runat="server"></uc1:menu_top></td>
				</tr>
                <tr>
					<td class="titleOnglet">Programme</td>
				</tr>
				<tr>
					<td class="titleOnglet">
						<asp:Button id="btnEnregistrer" runat="server" Text="Enregistrer" CssClass="fldTextBox"></asp:Button>
					</td>
				</tr>
				<tr>
					<td><asp:label id="lblMsgErr" runat="server" Width="100%" Visible="False" BorderColor="Firebrick"
							BorderWidth="1px" BackColor="Khaki" CssClass="fldlabel"></asp:label></td>
				</tr>
				<tr>
					<td class="cbToolbar">Mati�res, cours, programmes auxquels s'applique le PEI.&nbsp;
						<asp:imagebutton id="ImageButtonAjouterRendementMatiere" runat="server" ImageUrl="images/create.gif"></asp:imagebutton></td>
				</tr>
				<tr>
					<td>
						<table id="TableMatieres" borderColor="#ff3366" cellSpacing="0" cellPadding="0" width="100%"
							border="0">
							<tr>
								<td><asp:datagrid id="DataGrid1" runat="server" Width="100%" PageSize="20" AutoGenerateColumns="False"
										CellPadding="3" BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC"
										DataKeyField="ROW_ID" DataMember="MATIERES_ELEVES" CssClass="fldLabel">
										<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
										<ItemStyle ForeColor="#000066"></ItemStyle>
										<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699"></HeaderStyle>
										<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
										<Columns>
											<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;img border=0 src=&quot;Images/save.gif&quot;&gt;"
												CancelText="Annuler" EditText="&lt;img border=0 src=&quot;Images/P_Draw_Lg_N.gif&quot;&gt;"></asp:EditCommandColumn>
											<asp:ButtonColumn Text="&lt;img border=0 src=&quot;Images/delete.gif&quot;&gt;" CommandName="Delete"></asp:ButtonColumn>
											<asp:BoundColumn Visible="False" DataField="ROW_ID" SortExpression="ROW_ID" HeaderText="*ROW_ID"></asp:BoundColumn>
											<asp:TemplateColumn>
												<ItemTemplate>
													<asp:Label visible="false" ID="labelRowID" Text='<%# DataBinder.Eval(Container.DataItem, "row_id") %>' Runat="server"/>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:Label visible="true" ForeColor="White" ID="labelRowIDhold" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "row_id") %>' />
													<asp:textbox visible="false" id="txtRowID" OnPreRender="SetTextBoxRowID" runat="server" />
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn Visible="False" HeaderText="Niveau">
												<ItemTemplate>
													<asp:Label ID="lblDescription_niveau" Text='<%# DataBinder.Eval(Container.DataItem, "description_niveau") %>' Runat="server"/>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:Label ID="lblDescription_niveau_hold" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "description_niveau") %>' />
													<asp:DropDownList id="ddlNiveau" DataSource="<%# BindNiveau() %>" OnPreRender="SetDropDownIndexNiveau" DataTextField="description_niveau" DataValueField="niveau_id" runat="server" />
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Mati&#232;res/cours                      ">
												<HeaderStyle Wrap="False" Width="200px"></HeaderStyle>
												<ItemStyle Wrap="False"></ItemStyle>
												<ItemTemplate>
													<asp:Label ID="lblMatiere" Text='<%# DataBinder.Eval(Container.DataItem, "matiere") %>' Runat="server"/>
													<asp:Label Visible=False ID="lblMatiereID" Text='<%# DataBinder.Eval(Container.DataItem, "fk_matiere_id") %>' Runat="server"/>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:Label ID="lblMatiereHold" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "matiere") %>' />
													<asp:DropDownList id="ddlMatiere" DataSource="<%# BindMatiere() %>" OnPreRender="SetDropDownIndexMatiere" DataTextField="matiere" DataValueField="pk_matiere_id" runat="server" />
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn Visible="False" HeaderText="Cote/note">
												<ItemTemplate>
													<asp:Label ID="lblCoteNote" Text='<%# DataBinder.Eval(Container.DataItem, "cote_note") %>' Runat="server"/>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:Label ID="lblCoteNoteHold" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "cote_note") %>' />
													<asp:DropDownList id="ddlCoteNote" DataSource="<%# BindCoteNote() %>" OnPreRender="SetDropDownIndexCoteNote" DataTextField="pk_evaluation_id" DataValueField="pk_evaluation_id" runat="server" />
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Type d'attente">
												<HeaderStyle Width="100%"></HeaderStyle>
												<ItemTemplate>
													<asp:Label ID="lblTypeAttente" Text='<%# DataBinder.Eval(Container.DataItem, "type_attente") %>' Runat="server"/>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:Label ID="lblTypeAttenteHold" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "type_attente") %>' />
													<asp:DropDownList id="ddlTypeAttente" OnPreRender="SetDropDownIndexTypeAttente" DataTextField="type_attente"
														DataValueField="type_attente" runat="server">
														<asp:ListItem Value="AD">AD</asp:ListItem>
														<asp:ListItem Value="MOD">MOD</asp:ListItem>
														<asp:ListItem Value="AD / MOD">AD / MOD</asp:ListItem>
													</asp:DropDownList>
												</EditItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></td>
							</tr>
							<tr>
								<td></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td></td>
				</tr>
				<tr>
					<td>
						<table id="TableCompetences" borderColor="#4e6b8e" cellSpacing="0" cellPadding="0" width="100%"
							border="0">
							<tr>
								<td class="cbToolbar">Comp�tences et habilet�s
									<asp:imagebutton id="ImageButtonCompetence" runat="server" ImageUrl="images/create.gif"></asp:imagebutton></td>
							</tr>
							<tr>
								<td><asp:datagrid id="DataGrid2" runat="server" Width="100%" AutoGenerateColumns="False" CellPadding="3"
										BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC" DataKeyField="PK_ROW_ID"
										DataMember="COMPETENCES_ELEVES" CssClass="fldLabel">
										<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
										<ItemStyle ForeColor="#000066"></ItemStyle>
										<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699"></HeaderStyle>
										<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
										<Columns>
											<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;img border=0 src=&quot;Images/save.gif&quot;&gt;"
												CancelText="Annuler" EditText="&lt;img border=0 src=&quot;Images/P_Draw_Lg_N.gif&quot;&gt;"></asp:EditCommandColumn>
											<asp:ButtonColumn Text="&lt;img border=0 src=&quot;Images/delete.gif&quot;&gt;" CommandName="Delete"></asp:ButtonColumn>
											<asp:BoundColumn Visible="False" DataField="PK_ROW_ID" SortExpression="PK_ROW_ID" HeaderText="PK_ROW_ID"></asp:BoundColumn>
											<asp:TemplateColumn>
												<ItemTemplate>
													<asp:Label visible=false ID="lblRowId2" Text='<%# DataBinder.Eval(Container.DataItem, "pk_row_id") %>' Runat="server"/>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:Label visible=true ForeColor=White ID="lblRowIdHold2" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "pk_row_id") %>' />
													<asp:textbox visible="false" id="txtRowId2" OnPreRender="SetTextBoxRowID" runat="server" />
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Comp&#233;tences/habilet&#233;s">
												<HeaderStyle Width="150px"></HeaderStyle>
												<ItemStyle Wrap="False"></ItemStyle>
												<ItemTemplate>
													<asp:Label ID="lblCompetence" Text='<%# DataBinder.Eval(Container.DataItem, "competence") %>' Runat="server"/>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:Label ID="lblCompetenceHold" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "competence") %>' />
													<asp:DropDownList id="ddlCompetence" DataSource="<%# BindCompetence() %>" OnPreRender="SetDropDownIndexCompetence" DataTextField="contenu" DataValueField="contenu" runat="server" />
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Type de l'attente">
												<HeaderStyle Wrap="False" Width="100%"></HeaderStyle>
												<ItemStyle Wrap="False"></ItemStyle>
												<ItemTemplate>
													<asp:Label ID="lblTypeAttente2" Text='<%# DataBinder.Eval(Container.DataItem, "type_attente") %>' Runat="server"/>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:Label ID="lblTypeAttenteHold2" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "type_attente") %>' />
													<asp:DropDownList id="ddlTypeAttente2" OnPreRender="SetDropDownIndexTypeAttente" DataTextField="type_attente"
														DataValueField="type_attente" runat="server">
														<asp:ListItem Value="D">D</asp:ListItem>
                                                        <asp:ListItem Value=""></asp:ListItem>
													</asp:DropDownList>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn Visible="False" HeaderText="Rendement">
												<ItemStyle Wrap="False"></ItemStyle>
												<ItemTemplate>
													<asp:Label ID="lblRendement" Text='<%# DataBinder.Eval(Container.DataItem, "rendement") %>' Runat="server"/>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:Label ID="lblRendementHold" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "rendement") %>' />
													<asp:DropDownList id="ddlRendement" DataSource="<%# BindCoteNote() %>" OnPreRender="SetDropDownIndexCoteNote" DataTextField="pk_evaluation_id" DataValueField="pk_evaluation_id" runat="server" />
												</EditItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></td>
							</tr>
							<tr>
								<td></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
					</td>
				</tr>
				<tr>
					<td>
						<table id="TableExemptionProgramme" borderColor="#4e6b8e" cellSpacing="0" cellPadding="1" width="100%"
							border="0">
							<TR>
								<td class="cbToolbar" style="border-style: none">Exemptions du programme au palier �l�mentaire / 
									substitutions pour les cours obligatoires&nbsp;au palier secondaire</td>
								<td class="cbToolbar" width="100%" 
                                    style="border-style: none none none solid; border-left-width: thin; border-left-color: #4e6b8e;">A remplir pour les �l�ves du secondaire 
									seulement. L'�l�ve travaille actuellement pour obtenir le:
								</td>
								<td width="100%"></td>
							</tr>
							<tr><td class="fldTextBox" style="border-style: none">
									<asp:dropdownlist id="DropDownListExemp_prog_1" runat="server"  Font-Size="XX-Small">
										<asp:ListItem Value="OUI">OUI</asp:ListItem>
										<asp:ListItem Value="NON">NON</asp:ListItem>
									</asp:dropdownlist>&nbsp;(fournir la raison �ducationnelle)</td>
								<td style="border-style: none none none solid; border-left-width: thin; border-left-color: #4e6b8e;">
								    &nbsp;</td>
							</tr>
							<tr>
								<td valign="top" style="border-style: none"><asp:textbox id="txtExemp_Prog_Scol_2" runat="server" Width="400px" Font-Names="Arial" TextMode="MultiLine"
										Height="60px" CssClass="fldTextBox"></asp:textbox></td>
								<td style="border-style: none none none solid; border-left-width: thin; border-left-color: #4e6b8e;"><asp:radiobuttonlist id="RadioButtonListDiplomes" runat="server" Width="700px" Font-Names="Arial" CssClass="fldTextBox"></asp:radiobuttonlist></td>
								<td width="100%"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<cc1:SmartScroller id="SmartScroller1" runat="server"></cc1:SmartScroller></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
