﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="printpeirep.aspx.vb" Inherits="printpeirep" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
<form id="form1" runat="server">
    <div>
        <asp:CheckBoxList ID="CheckBoxList1" runat="server">
            <asp:ListItem Selected="True" Value="all">Imprimer tout</asp:ListItem>
            <asp:ListItem Value="donnees_generales">Données Générales</asp:ListItem>
            <asp:ListItem Value="forces_besoins">Points forts et besoins</asp:ListItem>
            <asp:ListItem Value="matieres">Matières</asp:ListItem>
            <asp:ListItem Value="adaptations">Adaptations</asp:ListItem>
            <asp:ListItem Value="donnees_evaluations">Données d&#39;évaluations</asp:ListItem>
            <asp:ListItem Value="ress_humaines">Ressources humaines</asp:ListItem>
            <asp:ListItem Value="buts">Buts, Attentes et Stratégies</asp:ListItem>
            <asp:ListItem Value="plan_transition">Plan de transition</asp:ListItem>
        </asp:CheckBoxList>
    </div>
    <div>
        <asp:Button ID="Button1" runat="server" 
            Text="Telecharger le pei au format .pdf" />
    </div>
    
    </form>
</body>
</html>
