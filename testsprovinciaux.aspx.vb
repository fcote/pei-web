Namespace PeiWebAccess

Partial Class testsprovinciaux
    Inherits System.Web.UI.Page

#Region " Code g�n�r� par le Concepteur Web Form "

    'Cet appel est requis par le Concepteur Web Form.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'REMARQUE�: la d�claration d'espace r�serv� suivante est requise par le Concepteur Web Form.
    'Ne pas supprimer ou d�placer.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN�: cet appel de m�thode est requis par le Concepteur Web Form
        'Ne le modifiez pas en utilisant l'�diteur de code.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Placez ici le code utilisateur pour initialiser la page

        If Not Session("eleveid") <> "" Then
            Response.Redirect("listeeleves.aspx")
        End If

        If Not Page.IsPostBack Then
            'BindDropDownListContenus(dropdownlistAdaptTestsProv, "ADAPTATION", "ADAPTATION")
            'BindDropDownListContenus(DropDownListMotifEducationnel, "ADAPTATION", "MOTIF_EDUCATIONNEL")
            Try
                'BindEvalTestsProv(TextBoxEvalTestProv, Session("eleveid"))
                'BindAdaptationsEvalProv(txtAdaptTestsProv, Session("eleveid"))
                'BindExemptionsEvalProv(txtMotidEducationnel, Session("eleveid"))
                bindtestprovinciaux()

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Erreur lors de la lecture des enregistrement :" & ex.Message

            End Try


        End If
    End Sub

    Private Sub ImageButtonAjouterAdaptTestsProv_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAjouterAdaptTestsProv.Click
        txtAdaptTestsProv.Text = txtAdaptTestsProv.Text & Chr(13) & dropdownlistAdaptTestsProv.SelectedValue
    End Sub

    Private Sub ImageButtonAjouterMotif_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAjouterMotif.Click
        txtMotidEducationnel.Text = txtMotidEducationnel.Text & Chr(13) & DropDownListMotifEducationnel.SelectedValue
    End Sub
    Private Sub ImagebuttonSaveEvalProv_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'SaveAdaptEvalProv()
        'SaveExmpetionsEvalProv()
    End Sub
    Sub SaveAdaptEvalProv()
        Dim oEvalProv As New PEIDAL.evalprov
        Dim dsAdaptEvalProv1 As New PEIDAL.dsAdaptEvalProv
        Dim r As PEIDAL.dsAdaptEvalProv.ADAPT_EVAL_PROVRow

        Try
            dsAdaptEvalProv1 = oEvalProv.GetdsAdaptEvalProv(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            r = dsAdaptEvalProv1.ADAPT_EVAL_PROV.FindByFK_ELEVE_ID(Session("eleveid"))

            r.ADAPTATION = txtAdaptTestsProv.Text

            oEvalProv.UpdatedsAdaptEvalProv(ConfigurationSettings.AppSettings("ConnectionString"), dsAdaptEvalProv1)

        Catch ex As Exception
            lblMsgErr.Visible = True
            lblMsgErr.Text = "Une erreure est survenu lors de l'enregistrement des adaptations. Contactez l'administrateur du system. " & Chr(13) & " D�tails de l'erreure: " & ex.Message

        End Try

    End Sub
    Sub SaveExmpetionsEvalProv()
        Dim oEvalProv As New PEIDAL.evalprov
        Dim dsExempEvalProv1 As New PEIDAL.dsExempEvalProv
        Dim r As PEIDAL.dsExempEvalProv.EXEMPTIONS_EVAL_PROVRow

        Try
            dsExempEvalProv1 = oEvalProv.GetdsExempEvalProv(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            r = dsExempEvalProv1.EXEMPTIONS_EVAL_PROV.FindByFK_ELEVE_ID(Session("eleveid"))

            r.MOTIF_EDUCATIONNEL = txtMotidEducationnel.Text


            oEvalProv.UpdatedsExempEvalProv(ConfigurationSettings.AppSettings("ConnectionString"), dsExempEvalProv1)

        Catch ex As Exception
            lblMsgErr.Visible = True
            lblMsgErr.Text = "Une erreur est survenue lors de l'enregistrement des motifs �ducationnel. Contactez l'administrateur du syst�me." & Chr(13) & _
            "details de l'erreur: " & ex.Message

        End Try
    End Sub
    

    

    Sub BindTestProvinciaux()
        Dim oEvalProv As New PEIDAL.evalprov
        Dim dsTestProv1 As PEIDAL.DsTestsProvinciaux

        dsTestProv1 = oEvalProv.GetdsTestsProvinciaux(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

        If dsTestProv1.TESTS_PROVINCIAUX.Rows.Count > 0 Then
            Dim r As PEIDAL.DsTestsProvinciaux.TESTS_PROVINCIAUXRow
            r = dsTestProv1.TESTS_PROVINCIAUX.FindByFK_ELEVE_ID(Session("eleveid"))

            TextBoxEvalTestProv.Text = r.EVALUATIONS
            txtAdaptTestsProv.Text = r.ADAPTATIONS
            txtMotidEducationnel.Text = r.EXEMPTIONS

        End If

    End Sub
    Sub SaveTestsProvinciaux()
        Dim oEvalProv As New PEIDAL.evalprov
        Dim dsTestsProvinciaux1 As New PEIDAL.DsTestsProvinciaux

        Try
            dsTestsProvinciaux1 = oEvalProv.GetdsTestsProvinciaux(ConfigurationSettings.AppSettings("connectionString"), Session("eleveid"))

            If dsTestsProvinciaux1.TESTS_PROVINCIAUX.Rows.Count < 1 Then
                Dim dr As PEIDAL.DsTestsProvinciaux.TESTS_PROVINCIAUXRow
                dr = dsTestsProvinciaux1.TESTS_PROVINCIAUX.NewTESTS_PROVINCIAUXRow

                dr.SetADAPTATIONSNull()
                dr.SetEVALUATIONSNull()
                dr.SetEXEMPTIONSNull()
                dr.FK_ELEVE_ID = Session("eleveid")

                dsTestsProvinciaux1.TESTS_PROVINCIAUX.Rows.InsertAt(dr, 0)

            End If

            Dim r As PEIDAL.DsTestsProvinciaux.TESTS_PROVINCIAUXRow
            r = dsTestsProvinciaux1.TESTS_PROVINCIAUX.FindByFK_ELEVE_ID(Session("eleveid"))

            r.EVALUATIONS = TextBoxEvalTestProv.Text
            r.ADAPTATIONS = txtAdaptTestsProv.Text
            r.EXEMPTIONS = txtMotidEducationnel.Text

            r.FK_ELEVE_ID = Session("eleveid")

            oEvalProv.UpdatedsTestsProvinciaux(ConfigurationSettings.AppSettings("connectionString"), dsTestsProvinciaux1)

        Catch ex As Exception
            lblMsgErr.Visible = True
            lblMsgErr.Text = "Erreur lors de l'enregistrement.  " & ex.Message

        End Try

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        SaveTestsProvinciaux()
    End Sub
End Class

End Namespace
