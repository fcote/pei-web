<%@ Page Language="vb" AutoEventWireup="false" Inherits="PeiWebAccess.PopUp" CodeFile="PopUp.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>PopUp</title>
		<LINK href="styles.css" type="text/css" rel="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:Calendar ID="calDate" OnSelectionChanged="Change_Date" Runat="server" CssClass="fldLabel"
				SelectionMode="DayWeekMonth" />
			<input type="hidden" id="control" runat="server">
		</form>
	</body>
</HTML>
