<%@ Register TagPrefix="cc1" Namespace="sstchur.web.SmartNav" Assembly="sstchur.web.SmartNav" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="PeiWebAccess.printpei3" CodeFile="printpei3.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="menu_top" Src="menu_top.ascx" %>
<%@ OutputCache Duration="1" VaryByParam="None" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<head>
		<title>printpei</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <link href="styles.css" rel="stylesheet" type="text/css" />
        <link href="styles.css" rel="stylesheet" type="text/css" />
        <link href="styles.css" rel="stylesheet" type="text/css" />
	</head>
	<body leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD><uc1:menu_top id="Menu_top1" runat="server"></uc1:menu_top></TD>
				</TR>
				<TR>
					<TD class="titleOnglet"><asp:label id="lblErrMsg" runat="server" Font-Size="X-Small" Font-Names="Arial" Visible="False"
							Width="100%" BackColor="Khaki" CssClass="fldlabel" BorderWidth="1px" BorderColor="Firebrick"></asp:label></TD>
				</TR>
				<TR>
					<TD id="tdpanel1" style="WIDTH: 708px"></TD>
				</TR>
                <tr>
                    <td id="user" runat="server" align="right" class="fldLabel">
                    </td>
                </tr>
                <tr>
                    <td id="user2" runat="server" align="right" class="fldLabel">
                    </td>
                </tr>
				<TR>
					<TD style="height: 19px" class="fldLabel">
                        S�lectionnez l'�cole</TD>
				</TR>
				<TR>
					<TD style="height: 19px"><cc1:smartscroller id="SmartScroller1" runat="server"></cc1:smartscroller>
                        <asp:DropDownList ID="DropDownListEcoles" runat="server" AutoPostBack="True" CssClass="fldLabel" Width="300px">
                        </asp:DropDownList></TD>
				</TR>
                <tr>
                    <td style="height: 19px" class="fldLabel">
                        S�lectionnez l'�l�ve</td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList ID="DropDownListEleves" runat="server" CssClass="fldLabel" AutoPostBack="True" Width="300px">
                        </asp:DropDownList></td>
                </tr>
			</TABLE>
			<asp:panel id="Panel1" runat="server">
				<TABLE id="TablePanel1" style="WIDTH: 100%" borderColor="#00ff33" cellSpacing="0" cellPadding="1"
					border="0">
                    <tr>
                        <td class="fldLabel">
                            S�lectionnez l'�tape</td>
                    </tr>
					<TR>
						<TD>
							<asp:DropDownList id="DropDownListEtape" runat="server" CssClass="fldLabel" Width="201px" AutoPostBack="True">
								<asp:ListItem Value="1" Selected="True">&#201;tape 1</asp:ListItem>
								<asp:ListItem Value="2">&#201;tape 2</asp:ListItem>
								<asp:ListItem Value="3">&#201;tape 3</asp:ListItem>
								<asp:ListItem Value="4">&#201;tape 4</asp:ListItem>
							</asp:DropDownList></TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 57px">
							<asp:CheckBox id="CheckboxAll" runat="server" CssClass="fldLabel" Width="100%" AutoPostBack="True"
								Text="Imprimer tout"></asp:CheckBox></TD>
					</TR>
					<TR>
					<TR>
						<TD>
							<asp:CheckBox id="generales" runat="server" CssClass="fldLabel" Width="100%" Text="Donn�es G�n�rales"></asp:CheckBox></TD>
					</TR>
					<TR>
						<TD>
							<asp:CheckBox id="forces" runat="server" CssClass="fldLabel" Width="100%" Text="Points forts et Besoins"></asp:CheckBox></TD>
					</TR>
					<TR>
						<TD>
							<asp:CheckBox id="matieres" runat="server" CssClass="fldLabel" Text="Mati�res"></asp:CheckBox></TD>
					</TR>
					<TR>
						<TD style="height: 22px">
							<asp:CheckBox id="competences" runat="server" Text="Comp�tences" CssClass="fldLabel"></asp:CheckBox></TD>
					</TR>
					<TR>
						<TD>
							<asp:CheckBox id="adaptations" runat="server" CssClass="fldLabel" Text="Adaptations"></asp:CheckBox></TD>
					</TR>
					<TR>
						<TD>
							<asp:CheckBox id="evaluations" runat="server" CssClass="fldLabel" Text="Donn�es d'�valuations"></asp:CheckBox></TD>
					</TR>
					<TR>
						<TD>
							<asp:CheckBox id="rh" runat="server" CssClass="fldLabel" Text="Ressources Humaines"></asp:CheckBox></TD>
					</TR>
					<TR>
						<TD>
							<asp:CheckBox id="buts" runat="server" CssClass="fldLabel" Width="256px" Text="Buts, Attentes et Strat�gies"></asp:CheckBox></TD>
					</TR>
					<TR>
						<TD>
							<asp:CheckBox id="evaluation_prov" runat="server" CssClass="fldLabel" Width="210px" Text="Tests Provinciaux"></asp:CheckBox></TD>
					</TR>
					<TR>
						<TD>
							<asp:CheckBox id="buts_transition" runat="server" CssClass="fldLabel" Width="100%" Text="Plan transition"></asp:CheckBox></TD>
					</TR>
					<TR>
						<TD>
							<asp:CheckBox id="signature" runat="server" CssClass="fldLabel" Text="Page de signature"></asp:CheckBox></TD>
					</TR>
					<TR>
						<TD class="fldLabel">
							<HR>
						</TD>
					</TR>
                        <tr>
                            <td id="EleveToPrint" runat="server" style="height: 22px">
                            </td>
                        </tr>
                        <tr>
                            <td id="Td1" runat="server" style="height: 22px">
							<asp:button id="Button1" runat="server" CssClass="fldLabel" Width="232px" Text="Aper�u avant impression"></asp:button></td>
                        </tr>
				</TABLE>
			</asp:panel></form>
	</body>
</HTML>
