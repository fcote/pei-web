<%@ Register TagPrefix="cc1" Namespace="sstchur.web.SmartNav" Assembly="sstchur.web.SmartNav" %>
<%@ Register TagPrefix="uc1" TagName="menu_top" Src="menu_top.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="PeiWebAccess.createEleve" CodeFile="createEleve.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>createEleve</title>
		<LINK href="pei.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
        <link href="styles.css" rel="stylesheet" type="text/css" />
        <link href="styles.css" rel="stylesheet" type="text/css" />
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<FORM id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="100%" border="0">
				<TR>
					<TD style="width: 100%"><uc1:menu_top id="Menu_top1" runat="server"></uc1:menu_top></TD>
				</TR>
				<TR>
					<TD style="width: 753px"><asp:label id="errMsg" runat="server" Font-Size="X-Small" ForeColor="Red" Font-Names="Arial"
							Width="100%"></asp:label></TD>
				</TR>
				<TR>
					<TD class="fldLabel" style="width: 753px">
                        <span style="font-size: 14pt">Ajout d'un �l�ve dans le syst�me Pei Web Access</span></TD>
				</TR>
                <tr>
                    <td class="fldLabel" style="width: 753px">
                    </td>
                </tr>
                <tr>
                    <td class="fldLabel" style="width: 753px; height: 18px">
                        1.
                        S�lectionnez d'abord l'�cole (prendre soins de choisir la bonne �cole '�l�mentaire'
                        ou 'secondaire' s'il y a lieu) :</td>
                </tr>
				<TR>
					<TD style="HEIGHT: 18px; width: 753px;"><asp:dropdownlist id="DropDownListEcole" runat="server" Width="280px" CssClass="fldLabel"></asp:dropdownlist></TD>
				</TR>
                <tr>
                    <td class="fldLabel" style="width: 753px; height: 18px">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 753px; height: 18px">
                        <asp:dropdownlist id="DropDownListAnneeScolaire" runat="server" Width="136px" 
                            CssClass="fldLabel" Visible="False">
							<asp:ListItem Value="20022003">2002-2003</asp:ListItem>
							<asp:ListItem Value="20032004">2003-2004</asp:ListItem>
							<asp:ListItem Value="20042005">2004-2005</asp:ListItem>
							<asp:ListItem Value="20052006">2005-2006</asp:ListItem>
							<asp:ListItem Value="20062007">2006-2007</asp:ListItem>
							<asp:ListItem Value="20072008">2007-2008</asp:ListItem>
							<asp:ListItem Value="20082009" Selected="True">2008-2009</asp:ListItem>
							<asp:ListItem Value="20092010">2009-2010</asp:ListItem>
							<asp:ListItem Value="20102011">2010-2011</asp:ListItem>
							<asp:ListItem Value="20112012">2011-2012</asp:ListItem>
							<asp:ListItem Value="20122013">2012-2013</asp:ListItem>
							<asp:ListItem Value="20132014">2013-2014</asp:ListItem>
						</asp:dropdownlist></td>
                </tr>
                <tr>
                    <td style="width: 753px; height: 18px">
                    </td>
                </tr>
				<TR>
					<TD style="HEIGHT: 20px; width: 753px;">
						<TABLE id="TableNomPrenom" cellSpacing="1" cellPadding="1" border="0">
							<TR>
								<TD class="fldLabel" colSpan="2" style="background-color: #6699ff">
                                    <strong>
                                        <br />
                                        Inscrire la &nbsp;partie du pr�nom et/ou nom de l'�l�ve qui ne contient pas
                                        d'accent.<br />
                                    </strong>
                                    <br />
                                    ex: pour rechercher <span style="font-size: 10pt"><em>St�phane Pich�</em></span><br />
                                    <br />
                                    <ul>
                                        <li>Entrez : "<strong>st</strong>" ou "<strong>phane</strong>" &nbsp;dans la zone Pr�nom<br />
                                    <br />
                                            Vous pouvez aussi entrer "<strong>pi</strong>" ou "<strong>pich</strong>" dans la zone nom famile</li>
                                    </ul>
                                </TD>
							</TR>
                            <tr>
                                <td class="fldLabel" style="height: 24px">
                                </td>
                                <td style="width: 172px; height: 24px">
                                </td>
                            </tr>
							<TR>
								<TD class="fldLabel" >Nom OU Pr�nom</TD>
								<TD style="width: 172px"><asp:textbox id="TextBoxPrenom" runat="server" 
                                        CssClass="fldLabel" Width="99%"></asp:textbox></TD>
							</TR>
							<%--<TR>
								<TD class="fldLabel" style="height: 23px">Nom de famille:</TD>
								<TD style="height: 23px;"><asp:textbox id="TextBoxNom" runat="server" 
                                        CssClass="fldLabel" Visible="False"></asp:textbox></TD>
							</TR>--%>
						</TABLE>
					</TD>
				</TR>
                <tr>
                    <td style="width: 753px; height: 38px">
                        <asp:button id="btnSearch" runat="server" Width="88px" CssClass="fldLabel" Text="rechercher"></asp:button></td>
                </tr>
				<TR>
					<TD style="width: 753px"></TD>
				</TR>
				<TR>
					<TD style="width: 753px"><asp:datagrid id="DataGridListeEleves" runat="server" Width="100%" CssClass="fldLabel" AutoGenerateColumns="False"
							BorderStyle="None" CellPadding="3" DataKeyField="PERSON_ID" PageSize="100" BackColor="White"
							BorderWidth="1px" BorderColor="#CCCCCC">
							<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
							<ItemStyle ForeColor="#000066"></ItemStyle>
							<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699"></HeaderStyle>
							<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
							<Columns>
								<asp:ButtonColumn Text="S&#233;lectionner" CommandName="Select"></asp:ButtonColumn>
								<asp:BoundColumn DataField="PERSON_ID" HeaderText="No Identification"></asp:BoundColumn>
								<asp:BoundColumn DataField="STUDENT_NO" HeaderText="No �tudiant"></asp:BoundColumn>
								<asp:BoundColumn DataField="LEGAL_FIRST_NAME" HeaderText="Pr&#233;nom"></asp:BoundColumn>
								<asp:BoundColumn DataField="LEGAL_SURNAME" HeaderText="Nom de famille"></asp:BoundColumn>
								<%--<asp:BoundColumn DataField="TO_CHAR(BIRTH_DATE,'YYYY-MM-DD')" HeaderText="Date de Naissance"></asp:BoundColumn>--%>
                                <asp:BoundColumn DataField="BIRTH_DATE" HeaderText="Date de Naissance"></asp:BoundColumn>
								<asp:BoundColumn DataField="SCHOOL_CODE" HeaderText="SCHOOL_CODE" Visible="False"></asp:BoundColumn>
								<asp:BoundColumn DataField="GRADE" HeaderText="Niveau"></asp:BoundColumn>
								<asp:BoundColumn DataField="GENDER" HeaderText="Sexe"></asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD style="width: 753px"></TD>
				</TR>
				<TR>
					<TD style="width: 753px">
					</TD>
				</TR>
			</TABLE>
		</FORM>
	</body>
</HTML>
