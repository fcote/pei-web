<%@ Register TagPrefix="uc1" TagName="menu_top" Src="menu_top.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" ValidateRequest="false" SmartNavigation = "false" Inherits="PeiWebAccess.attentes" CodeFile="attentes.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="sstchur.web.SmartNav" Assembly="sstchur.web.SmartNav" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>attentes</title>
		<meta content="True" name="vs_snapToGrid">
		<meta content="False" name="vs_showGrid">
		<LINK href="styles.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	    <style type="text/css">
            .style1
            {
                font: icon;
                height: 26px;
                border-bottom-style: solid;
                border-bottom-color: inherit;
                border-bottom-width: 1px;
                margin-bottom: 1px;
                padding: 0px;
                background-color: #A2C2EE;
                background-image: url('images/tool-bkgd.gif');
            }
        </style>
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE id="TableMain" borderColor="#3300cc" cellSpacing="0" cellPadding="0" width="100%"
				border="0">
				<TR>
					<TD><uc1:menu_top id="Menu_top1" runat="server"></uc1:menu_top></TD>
				</TR>
				<TR>
					<TD class="titleOnglet">Buts, attentes et strat�gies</TD>
				</TR>
				<TR>
					<TD class="fldLabel"><FONT face="Arial">Cet �nonc� d�crit ce que l'�l�ve devrait 
							raisonnablement avoir accompli � la fin de l'ann�e dans une mati�re, un cours 
							ou un programme.</FONT></TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="TableHeaderInfosButs" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD width="99%"><asp:label id="lblMsgErr" runat="server" CssClass="fldlabel" BorderWidth="1px" BorderColor="Firebrick"
										Visible="False" Width="100%" BackColor="Khaki"></asp:label></TD>
								<TD class="cbToolbar"></TD>
							</TR>
							<TR>
								<TD class="cbToolbar" width="99%">But(s) annuel(s): Le but annuel doit �tre individualis� sp�cifique, mesurable, atteignable, r�aliste et temporel.</TD>
								<TD class="cbToolbar"><asp:imagebutton id="ImageButtonExpandInfosButs" runat="server" ImageUrl="images/navbar-expand.gif"></asp:imagebutton><asp:imagebutton id="ImageButtonCollapseInfosButs" runat="server" Visible="False" ImageUrl="images/navbar-collapse.gif"></asp:imagebutton></TD>
							</TR>
						</TABLE>
						<asp:linkbutton id="LinkButtonNouveauBut" runat="server" Font-Names="Arial" Font-Size="X-Small">Cr�er un nouveau but</asp:linkbutton></TD>
				</TR>
				<TR>
					<TD>
						<asp:Label ID="LabelBut" runat="server" 
                            Text="il n'y a pas de but pour cet �l�ve."></asp:Label>
                    </TD>
				</TR>
				<TR>
					<TD></TD>
				</TR>
				<tr>
					<td><asp:panel id="PanelNouveauBut" runat="server" Visible="False">
							<TABLE id="TableNouveauBut" borderColor="#990066" cellSpacing="1" cellPadding="1" width="100%"
								border="0">
								<TR>
									<TD class="fldLabel">
										<asp:CheckBox id="CheckBoxAttDiff" runat="server" AutoPostBack="True" Text="Attente diff�rente"></asp:CheckBox></TD>
								</TR>
								<TR>
									<TD class="fldLabel">
										Mati�re, cours ou programme:</TD>
								</TR>
								<TR>
									<TD class="fldLabel">
										<asp:TextBox ID="TxtNouveauButProgramme" runat="server" Width="230px"></asp:TextBox>
                                    </TD>
								</TR>
								<TR>
									<TD class="fldLabel">
										<asp:Label ID="lblNiveauRendementActuel" runat="server" Width="224px">Niveau de 
                                        rendement actuel:</asp:Label>
                                    </TD>
								</TR>
								<TR>
									<TD class="fldLabel">
										<asp:TextBox ID="txtNouveauButNiveauRendementActuel" runat="server" 
                                            CssClass="fldLabel" Width="230px"></asp:TextBox>
                                    </TD>
								</TR>
								<TR>
									<TD class="fldLabel">
										<asp:Label ID="LabelAnneEtude" runat="server" Width="144px">Ann�e d&#39;�tude au 
                                        sein du curriculum:</asp:Label>
                                    </TD>
								</TR>
								<TR>
									<TD class="fldLabel">
                                        <asp:DropDownList ID="DropDownListNewButAnneeEtude" runat="server" 
                                            Width="232px">
                                        </asp:DropDownList>
                                        <asp:CheckBox ID="CheckBoxAttenteMod" runat="server" Font-Names="Arial" 
                                            Font-Size="Smaller" Text="MOD" />
                                    </TD>
								</TR>
								<TR>
									<TD class="fldLabel">
										�nonc� du nouveau but:</TD>
								</TR>
								<TR>
									<TD>
										<asp:TextBox ID="txtNouveauBut" runat="server" CssClass="fldTextBox" 
                                            Height="40px" TextMode="MultiLine" Width="230px"></asp:TextBox>
                                    </TD>
								</TR>
							    <tr>
                                    <td align="left">
                                        <asp:Button ID="Button1" runat="server" CssClass="fldLabel" 
                                            Text="Ajouter le nouveau but" Width="144px" />
                                        <asp:Button ID="btn_AnnulerNouveauBut" runat="server" CssClass="fldLabel" 
                                            Text="Annuler" />
                                    </td>
                                </tr>
							</TABLE>
						</asp:panel></td>
				</tr>
				<TR>
					<TD><asp:panel id="PanelInfosBut" runat="server" Width="100%">
							<TABLE id="TableinfosBut" borderColor="red" cellSpacing="0" cellPadding="1" width="100%"
								border="0">
								<TR>
									<TD class="fldLabel" style="HEIGHT: 15px">Liste des buts:</TD>
									<TD style="HEIGHT: 15px">
										<asp:dropdownlist id="DropDownListButs" runat="server" Width="600px" CssClass="fldTextBox" AutoPostBack="True"></asp:dropdownlist>
										<asp:imagebutton id="ImageButtonDeleteBut" runat="server" 
                                            ImageUrl="images/icon-unresolved.gif" Width="16px"></asp:imagebutton></TD>
								</TR>
								<TR>
									<TD class="fldLabel" style="HEIGHT: 15px">But no:</TD>
									<TD style="HEIGHT: 15px">
										<asp:DropDownList ID="DropDownListButNo" runat="server" CssClass="fldTextBox" 
                                            Width="232px">
                                        </asp:DropDownList>
                                    </TD>
								</TR>
								<TR>
									<TD class="fldLabel">Mati�re, cours ou programme:</TD>
									<TD>
										<asp:TextBox ID="TextBoxProgramme" runat="server" CssClass="fldTextBox" 
                                            Width="232px"></asp:TextBox>
                                    </TD>
								</TR>
								<TR>
									<TD class="fldLabel" height="18" style="HEIGHT: 18px">Ann�e d&#39;�tude au sein du 
                                        curriculum:</TD>
									<TD height="18" style="HEIGHT: 18px">
										<asp:DropDownList ID="DropDownListAnneeEtude" runat="server" 
                                            CssClass="fldTextBox" Width="232px">
                                        </asp:DropDownList>
                                        <asp:CheckBox ID="CheckBoxAttenteMod2" runat="server" Font-Names="Arial" 
                                            Font-Size="Smaller" Text="MOD" />
                                    </TD>
								</TR>
								<TR>
									<TD class="fldLabel">Niveau de rendement actuel:</TD>
									<TD>
										<asp:textbox id="TextBoxNiveauRendement" runat="server" Width="360px" 
                                            CssClass="fldTextBox" TextMode="MultiLine"
											Height="40px"></asp:textbox></TD>
								</TR>
								<TR>
									<TD class="fldLabel">
										But annuel:</TD>
									<TD>
                                        <asp:TextBox ID="txtEditBut" runat="server" CssClass="fldTextBox" Height="60px" 
                                            TextMode="MultiLine" Width="360px"></asp:TextBox>
                                    </TD>
								</TR>
								<TR>
									<TD class="fldLabel">
                                        <asp:Button ID="btn_enregistrerBut" runat="server" CssClass="fldTextBox" 
                                            Text="Enregistrer" />
                                        <asp:Button ID="btn_AnnulerBut" runat="server" CssClass="fldTextBox" 
                                            Text="Annuler" />
                                    </TD>
									<TD align="left"></TD>
								</TR>
							    <tr>
                                    <td class="fldLabel">
                                    </td>
                                    <td align="left">
                                    </td>
                                </tr>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="TableHeaderInfosAttentes" borderColor="#993300" cellSpacing="0" cellPadding="0"
							width="100%" border="0">
							<TR>
								<TD class="style1" width="99%">Informations sur les attentes</TD>
								<TD class="style1"><asp:imagebutton id="ImageButtonExpandInfosAttentes" runat="server" ImageUrl="images/navbar-expand.gif"></asp:imagebutton><asp:imagebutton id="ImageButtonCollapseInfosAttentes" runat="server" Visible="False" ImageUrl="images/navbar-collapse.gif"></asp:imagebutton></TD>
							</TR>
							<TR>
							  <TD class="fldTextBox">
													<asp:Label ID="LabelAttente" runat="server" 
                                                        Text="Il n'y a pas d'attente pour le but s�lectionn�"></asp:Label>
                            </td></TR>
						</TABLE>
						<asp:LinkButton ID="LinkButtonNouvelleAttente" runat="server">Cr�er une nouvelle 
                                                    attente</asp:LinkButton>
						<asp:panel id="PanelInfosAttentes" runat="server" Visible="False">
							<TABLE id="TableMainInfosAttentes" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<TD>
										<TABLE id="TableListeAttentes" style="WIDTH: 100%; HEIGHT: 54px" cellSpacing="0" cellPadding="0"
											width="100%" border="0">
											
											<tr>
                                                <td class="fldTextBox" valign="middle" width="187">
                                                   
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
											<TR>
												<TD class="fldTextBox" vAlign="middle" width="187">Liste des attentes:</TD>
												<TD>
													<asp:dropdownlist id="DropDownListAttentes" runat="server" Width="650px" 
                                                        CssClass="fldTextBox" AutoPostBack="True"></asp:dropdownlist>
                                                    <asp:ImageButton ID="ImageButtonDeleteAttente" runat="server" 
                                                        ImageUrl="images/icon-unresolved.gif" />
                                                </TD>
												<TD>
													&nbsp;</TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
								<TR>
									<TD>
										<TABLE id="TableInfosAttentes" borderColor="#00ff00" cellSpacing="0" cellPadding="0" width="100%"
											border="0">
											<TR>
												<TD class="fldLabel" id="tdAttente">Attentes d'apprentissage&nbsp;</TD>
												<TD class="fldLabel" id="tdStrategie">Strat�gies p�dagogiques</TD>
												<TD class="fldLabel" id="tdMethode">M�thodes d'�valuation</TD>
												<TD id="tdOffset" width="100%"></TD>
											</TR>
											<TR>
												<TD vAlign="top" width="81">
													<asp:textbox id="TextBoxAttente" runat="server" Width="300px" 
                                                        CssClass="fldTextBox" TextMode="MultiLine"
														Height="180px"></asp:textbox></TD>
												<TD vAlign="top">
													<asp:textbox id="TextBoxStrategie" runat="server" Width="300px" 
                                                        CssClass="fldTextBox" TextMode="MultiLine"
														Height="180px"></asp:textbox></TD>
												<TD vAlign="top">
													<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD>
																<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD width="245">
																			<asp:DropDownList id="DropDownListMethodes" runat="server" Width="240px" CssClass="fldLabel"></asp:DropDownList></TD>
																		<TD align="left">
																			<asp:ImageButton id="ImageButtonAjouterMethode" runat="server" ImageUrl="images/create.gif"></asp:ImageButton></TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
														<TR>
															<TD>
																<asp:textbox id="TextBoxMethode" runat="server" Width="240px" 
                                                                    CssClass="fldTextBox" TextMode="MultiLine"
																	Height="160px"></asp:textbox></TD>
														</TR>
														<TR>
															<TD></TD>
														</TR>
													</TABLE>
												</TD>
												<TD></TD>
											</TR>
											<TR>
												<TD width="81"></TD>
												<TD></TD>
												<TD></TD>
												<TD></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
								<TR>
									<TD align="left">
										<asp:Button id="bntEnregistrerAttente" runat="server" CssClass="fldLabel" Text="Enregistrer"></asp:Button>
										<asp:Button id="btnAnnulerAttente" runat="server" CssClass="fldLabel" Text="Annuler"></asp:Button></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD></TD>
				</TR>
				<TR>
					<TD></TD>
				</TR>
				<tr>
					<td><asp:panel id="PanelInfosAttentes_OLD" runat="server" Visible="False" Width="100%"></asp:panel></td>
				</tr>
				
				<TR>
					<TD><asp:panel id="PanelNouvelleAttente" runat="server" Visible="False">
							<TABLE id="TableNouvelleAttente" borderColor="#339933" cellSpacing="1" cellPadding="1"
								width="100%" border="0">
								<TR>
									<TD class="fldLabel">Entrer le contenu de l'attente:</TD>
								</TR>
								<TR>
									<TD>
										<asp:textbox id="txtNouvelleAttente" runat="server" Width="320px" CssClass="fldTextBox" TextMode="MultiLine"
											Height="120px"></asp:textbox></TD>
								</TR>
								<TR>
									<TD>
										<asp:button id="btnEnregistrerNewAttente" runat="server" Width="88px" CssClass="fldTextBox"
											Text="Enregistrer"></asp:button>
										<asp:Button id="btnAnnulerNewAttente" runat="server" CssClass="fldTextBox" Text="Annuler"></asp:Button></TD>
								</TR>
							</TABLE>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD height="18">
						<TABLE id="TableHeaderCommentaires" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="cbToolbar">Commentaires</TD>
								<TD class="cbToolbar"><asp:imagebutton id="ImageButtonExpandInfosCommentaires" runat="server" ImageUrl="images/navbar-expand.gif"></asp:imagebutton><asp:imagebutton id="ImageButtonCollapseInfosCommentaires" runat="server" Visible="False" ImageUrl="images/navbar-collapse.gif"></asp:imagebutton></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD><asp:panel id="PanelInfosCommentaires" runat="server" CssClass="fldTextBox"
							Width="100%">
							<TABLE id="TableInfosCommentaires" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<TD vAlign="top" width="250">
										<asp:dropdownlist id="DropDownListEtape" runat="server" Width="250px" Font-Size="XX-Small" AutoPostBack="True">
											<asp:ListItem Value="1">Bulletin de progr�s [semestre 1,terme 1]</asp:ListItem>
											<asp:ListItem Value="2">Premier bulletin [semestre 1, terme 2]</asp:ListItem>
											<asp:ListItem Value="3">Deuxi�me bulletins [semestre 2, terme 1]</asp:ListItem>
											<asp:ListItem Value="4">[semestre 2, terme 2]</asp:ListItem>
										</asp:dropdownlist></TD>
									<TD>
										<asp:textbox id="TextBoxCommentaireGeneral" runat="server" Width="600px" CssClass="fldTextBox"
											TextMode="MultiLine" Height="80px"></asp:textbox></TD>
								</TR>
								<TR>
									<TD width="80"></TD>
									<TD align="left">
										<asp:Button id="bntEnregistrerCommentaire" runat="server" CssClass="fldTextBox" Text="Enregistrer"></asp:Button>
										<asp:Button id="btnAnnulerCommentaire" runat="server" CssClass="fldTextBox" Text="Annuler"></asp:Button></TD>
								</TR>
								<TR>
									<TD width="80"></TD>
									<TD></TD>
								</TR>
							</TABLE>
						</asp:panel><cc1:smartscroller id="SmartScroller1" runat="server"></cc1:smartscroller></TD>
				</TR>
				<TR>
					<TD width="853" height="18"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
