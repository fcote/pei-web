Namespace PeiWebAccess

    Partial Class report3
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            'ReportViewer1.SetQueryParameter("fk_eleve_id", "BK19979619")
            'ReportViewer1.SetQueryParameter("fk_eleve_id", Session("eleveid"))
            ReportViewer1.SetQueryParameter("fk_eleve_id", Request.QueryString("fk_eleve_id"))
            ReportViewer1.SetQueryParameter("person_id", Request.QueryString("person_id"))
            ReportViewer1.SetQueryParameter("DDN", Request.QueryString("DDN"))
            ReportViewer1.SetQueryParameter("nom", Request.QueryString("nom"))
            ReportViewer1.SetQueryParameter("rh", Request.QueryString("rh"))
            ReportViewer1.SetQueryParameter("forces", Request.QueryString("forces"))
            ReportViewer1.SetQueryParameter("attentes", Request.QueryString("attentes"))
            ReportViewer1.SetQueryParameter("buts_transition", Request.QueryString("buts_transition"))
            'ReportViewer1.SetQueryParameter("buts_transition", Request.QueryString("buts_transition"))
            ReportViewer1.SetQueryParameter("matieres", Request.QueryString("matieres"))
            ReportViewer1.SetQueryParameter("competences", Request.QueryString("competences"))
            ReportViewer1.SetQueryParameter("signature", Request.QueryString("signature"))
            ReportViewer1.SetQueryParameter("adaptations", Request.QueryString("adaptations"))
            ReportViewer1.SetQueryParameter("generales", Request.QueryString("generales"))
            ReportViewer1.SetQueryParameter("evaluations", Request.QueryString("evaluations"))
            ReportViewer1.SetQueryParameter("evaluation_prov", Request.QueryString("evaluation_prov"))
            ReportViewer1.SetQueryParameter("Etape", Request.QueryString("Etape"))
            ReportViewer1.SetQueryParameter("year", Request.QueryString("year"))

            'param�tre servant a forcer le refresh du .pdf. curtime donne une valeur (date) diff�rente a chaque fois
            ReportViewer1.SetQueryParameter("curtime", Date.Now)

            'Response.Write("<script language=javascript>window.close(''+self.location,'mywin','left=20,top=20,width=500,height=500,toolbar=1,resizable=0');</script>")

        End Sub

    End Class

End Namespace
