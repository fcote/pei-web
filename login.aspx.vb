Namespace PeiWebAccess


    Partial Class login
        Inherits System.Web.UI.Page

#Region " Code g�n�r� par le Concepteur Web Form "

        'Cet appel est requis par le Concepteur Web Form.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'REMARQUE�: la d�claration d'espace r�serv� suivante est requise par le Concepteur Web Form.
        'Ne pas supprimer ou d�placer.

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN�: cet appel de m�thode est requis par le Concepteur Web Form
            'Ne le modifiez pas en utilisant l'�diteur de code.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Placez ici le code utilisateur pour initialiser la page

            'Response.Write(".net framework version: ")
            'Response.Write(System.Environment.Version)
            'With Request.Browser
            '    'display browser....
            '    Response.Write("Navigateur " & .Browser.ToString)
            '    If UCase(.Browser.ToString) <> "CHROME" Then
            '        lblMsgErr.Text = "Pour des raisons de compatibilit�, vous devez d'utiliser le navigateur Google Chrome"
            '        lblMsgErr.Visible = True
            '        Button1.Enabled = False
            '    Else
            '        lblMsgErr.Text = String.Empty
            '        'lblMsgErr.Visible = True
            '        Button1.Enabled = True
            '    End If

            'End With

            If Not Page.IsPostBack Then
                txtUsername.Text = GetCookiesValues("user", "username")
            End If


            'If GetCookiesValues("user", "username") <> "" Then

        End Sub


        Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
            Dim oGen As New PEIDAL.general
            Dim bLogged As Boolean = False
            Dim _username As String
            Dim _date As Date
            Try
                'Dim auth = New AuthenticationServiceManager
                _username = Replace(txtUsername.Text, "@csdccs.edu.on.ca", "")
                _username = Replace(txtUsername.Text, "@cscmonavenir.ca", "")
                Session("user") = _username
                
                'If oGen.CheckLogon(ConfigurationSettings.AppSettings("ConnectionString"), txtUsername.Text, txtPassword.Text) = True Then
                If ADSIlogin(_username, txtPassword.Text, DropDownListDomaine.SelectedValue, True) Then
                    'Session("username") = txtUsername.Text
                    If Not Request.QueryString("u") Is Nothing Then
                        _username = Request.QueryString("u").ToString
                    End If
                    _date = System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
                    insertLog(ConfigurationSettings.AppSettings("ConnectionString"), "LOGIN", "AUTHENTICATION", Session("user") & "/" & _username, "", "", _date, "", "", "")

                    Dim _msg As String
                    '_msg = hasAccess(_username)
                    _msg = hasAccess2(_username)

                    If _msg = "succes" Then
                        FormsAuthentication.RedirectFromLoginPage(_username, True)
                        bLogged = True
                    Else
                        lblMsgErr.Visible = True
                        lblMsgErr.Text = _msg
                    End If

                    'SaveCookies("user", "username", txtUsername.Text)

                Else
                    bLogged = False
                    lblMsgErr.Visible = True
                    lblMsgErr.Text = "Mauvais nom d'usager ou mauvais mot de passe. R�essayez."
                End If

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message

            End Try

            If bLogged = True Then
                'Response.Redirect("listeeleves.aspx")

                'Response.Redirect("frameset1.htm")
            End If

            'Session("eleveid") = txtEleveId.Tex

        End Sub
        Sub SaveCookies(ByVal Cookiename As String, ByVal Key As String, ByVal Value As String)
            Dim Cookie As HttpCookie

            Cookie = New HttpCookie(Cookiename)
            Cookie.Values.Add(Key, Value)
            Response.AppendCookie(Cookie)
            Response.Cookies(Cookiename).Expires = DateTime.Now.AddDays(1)

        End Sub
        Function GetCookiesValues(ByVal CookieName As String, ByVal Key As String) As String

            Dim Cookie As HttpCookie
            Cookie = Request.Cookies(CookieName)

            If Not Cookie Is Nothing Then

                Select Case (Key)

                    Case "username"
                        Return Cookie.Values("username")
                End Select
            End If

            Return ""

        End Function


    End Class

End Namespace
