Imports System.Data
Imports System.Diagnostics
Imports System.Data.SqlClient
Imports System.Drawing
Imports VwdCms.TitleCheckBoxList


Namespace PeiWebAccess


Partial Class liste
    Inherits System.Web.UI.Page

#Region " Code g�n�r� par le Concepteur Web Form "

    'Cet appel est requis par le Concepteur Web Form.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Public strUtilisateur As String
    Public strTitre As String
    Public strNom As String
    Public strTypeIntervenant As String
    Public strPrenom As String

    Protected WithEvents dsElaborePar1 As PEIDAL.dsElaborePar

    'REMARQUE�: la d�claration d'espace r�serv� suivante est requise par le Concepteur Web Form.
    'Ne pas supprimer ou d�placer.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN�: cet appel de m�thode est requis par le Concepteur Web Form
        'Ne le modifiez pas en utilisant l'�diteur de code.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Placez ici le code utilisateur pour initialiser la page
        ' Obtain products and databind to an asp:datalist control
            'Dim productCatalogue As ASPNET.StarterKit.Commerce.ProductsDB = New ASPNET.StarterKit.Commerce.ProductsDB

            'Me.DisplayAnomaliesFromWebConfigFile3("")

        Dim EleveId As String = Session("eleveid")

        If Not Session("eleveid") <> "" Then
            Response.Redirect("listeeleves.aspx")
        Else
            Session("eleveid") = Session("eleveid")
        End If

        lblMsgErr.Visible = False

        'V�rifie la session
            'If Not (Session("eleveid")) <> "" Then
            '    Response.Redirect("login.aspx")
            'End If

        If Not Page.IsPostBack Then
            Dim s As Style = New Style
            s.BorderColor = Color.Red
            s.BackColor = Color.LightGray
            s.ForeColor = Color.Green
            's.Font.Name = System.Web.UI.WebControls.FontNamesConverter.StandardValuesCollection
            s.Font.Name = "arial"
            s.Font.Size = System.Web.UI.WebControls.FontUnit.Point(8)

            ''Dim ctl As New RadioButtonList
            'Dim ctl As Control
            'Dim ctl2 As Control

            'For Each ctl In Page.Controls
            '    For Each ctl2 In ctl.Controls
            '        Dim ctlName As String
            '        Dim mType As Type
            '        ctlName = ctl2.ID
            '        mType = ctl2.
            '    Next

            '    'ctl.ApplyStyle(s)
            'Next
            'RadioButtonListPlacement.ApplyStyle(s)

            'Dim oIdentification As PEI.identification = New PEI.identification
            Dim oIdentification As New PEIDAL.identification
            'Try
            'CheckBoxListeSources.DataSource = oIdentification.GetListSourceInformations
            'CheckBoxListeSources.DataValueField = "source_id"
            'CheckBoxListeSources.DataTextField = "source"
            'CheckBoxListeSources.DataBind()
            Dim oReader As SqlDataReader

            oReader = oIdentification.GetListSourceInformations
            LoadCheckBoxList(oReader, CheckBoxListeSources)

            oReader = oIdentification.GetListSourceInformationsByEleveId(Session("eleveid"))
            SetCheckBoxList(CheckBoxListeSources, oReader)


        'oIdentification.SetListSourceInformationsByEleveId(CheckBoxListeSources, Session("eleveid"))

        'RadioButtonListPlacement.DataSource = oIdentification.GetListPlacements
        'RadioButtonListPlacement.DataValueField = "pk_placement_id"
        'RadioButtonListPlacement.DataTextField = "description"
        'RadioButtonListPlacement.DataBind()
        oReader = oIdentification.GetListPlacements
        LoadRadioButtonList(oReader, RadioButtonListPlacement)
            oReader = oIdentification.GetListPlacementByEleveId(Session("eleveid"))
            If oReader.HasRows Then
                    SetRadioButtonList(RadioButtonListPlacement, oReader)
            Else
                RadioButtonNothing.Checked = True
            End If




            'oIdentification.SetListPlacementsByEleveId(RadioButtonListPlacement, Session("eleveid"))

            'liste intervenant (�labor� par..)
            Dim oAtt As New PEIDAL.attente

            'DropDownListTitre.DataSource = oAtt.GetListIntervenants
            'DropDownListTitre.DataValueField = "pk_intervenant_id"
            'DropDownListTitre.DataTextField = "pk_intervenant_id"
            'DropDownListTitre.DataBind()


            'Anomalies
            RadioButtonListAnomalies.DataSource = oIdentification.GetListAnomalies
            RadioButtonListAnomalies.DataValueField = "pk_anomalie_id"
            RadioButtonListAnomalies.DataTextField = "description"
            RadioButtonListAnomalies.DataBind()

            'oIdentification.SetListAnomaliesByEleveId(RadioButtonListAnomalies, Session("eleveid"))
            SetListAnomaliesByEleveId(RadioButtonListAnomalies, Session("eleveid"))

                'oReader = oIdentification.GetListAnomalies
                'LoadCheckBoxList(oReader, CheckBoxListAnomalies)
                'oReader = oIdentification.GetListAnomaliesByEleveId(Session("eleveid"))
                'SetCheckBoxList(CheckBoxListAnomalies, oReader)


                Me.DisplayAnomalieGroup(Session("eleveid"), ConfigurationSettings.AppSettings("ConnectionString"))

            RadioButtonListRaisons.DataSource = oIdentification.GetListRaisons
            RadioButtonListRaisons.DataValueField = "pk_raison_id"
            RadioButtonListRaisons.DataTextField = "description"
            RadioButtonListRaisons.DataBind()

            'oIdentification.SetListRaisonsByEleveId(RadioButtonListRaisons, Session("eleveid"))
            SetListRaisonsByEleveId(RadioButtonListRaisons, Session("eleveid"))

            CheckBoxListeFormat.DataSource = oIdentification.GetListFormatsCommunications
            CheckBoxListeFormat.DataValueField = "pk_format_communication_id"
            CheckBoxListeFormat.DataTextField = "description"
            CheckBoxListeFormat.DataBind()

            'oIdentification.SetListFormatsCommunicationsByEleveId(CheckBoxListeFormat, Session("eleveid"))
            SetListFormatsCommunicationsByEleveId(CheckBoxListeFormat, Session("eleveid"))


            BindDatagridElaborations()

            BindCaracteristiques()

                BindDates()

                'bind checkbox financement
                Me.CheckBoxFincancement.Checked = IsFinancementMinister(Session("eleveid"))

                'plan de securite
                Me.CheckBoxPlanSecurite.Checked = IsStudentHasPlanSecurity(Session("eleveid"))

            'Catch ex As Exception
            'lblMsgErr.Visible = True
            'lblMsgErr.Text = ex.Message & " ex de base: " & ex.GetBaseException.ToString
            'End Try

            'Dim row As DataRow
            'row = ds.Tables(0).Rows.Fin
            'If Not (row Is Nothing) Then
            '    lblMsg.Text = "Row Found !!!"
            'Else
            '    lblMsg.Text = "Row  N O T  Found !!!"
            'End If

            'dssourceseleves.Tables(0).

            'If dsSourcesEleves.Tables(0).

            'End If

        End If

        End Sub
        
        Sub DisplayAnomaliesFromWebConfigFile3(ByVal strAnomalies As String)
            Dim delimStr As String = ";"
            Dim delimStrAno As String = ","
            Dim delimiter As Char() = delimStr.ToCharArray()
            Dim delimiterStrAno As Char() = delimStrAno.ToCharArray
            Dim words As String = "comportement:1,2,3;physique:4,5,6;intellectuelle:7,8,9;multiples:14"
            Dim _split As String() = Nothing
            Dim _ano As String
            Dim _grp As String
            Dim _splitAno As String() = Nothing
            Dim s As String
            Dim a As String


            Dim charColl As System.Text.RegularExpressions.MatchCollection = System.Text.RegularExpressions.Regex.Matches(words, ":")
            'MessageBox.Show(charColl.Count.ToString())
            Response.Write("<br>nombre groupe:" & charColl.Count.ToString)

            _split = words.Split(delimiter, charColl.Count)

            For Each s In _split
                'Console.WriteLine("-{0}-", s)
                Response.Write("<br><br>s=" & s.ToString)
                _ano = Replace(Mid(s.ToString, InStr(s.ToString, ":")), ":", "")
                _grp = Mid(s.ToString, 1, InStr(s.ToString, ":"))
                Response.Write("<br>groupe:" & _grp)
                Response.Write("<br>anomalies:" & _ano)
                Dim charCollAno As System.Text.RegularExpressions.MatchCollection = System.Text.RegularExpressions.Regex.Matches(_ano, ",")
                _splitAno = _ano.Split(delimiterStrAno, charCollAno.Count + 1)
                Response.Write("<br>=================<br>anomalie du groupe '" & _grp & "'")
                For Each a In _splitAno
                    Response.Write("<br>anomalie: " & a.ToString)
                Next

            Next s


        End Sub

        Sub DisplayAnomalies()
            'Dim _anomalies() As String = Nothing
            'Dim _str As String
            'Dim _cnt As Integer
            ''Dim split() As String
            ''_str = ConfigurationSettings.AppSettings("anomalies").ToString
            '_str = "comportement:1,2,3;physique:4,5,6;intellectuelle:7,8,9;multiples:14"
            ''_str = "aaa;bb;ccc;dddd;eeee"

            ''_anomalies = _str.Split(";")

            'Dim s As String
            'For _cnt = 1 To 5
            '    _anomalies = _str.Split(";", _cnt)
            '    _anomalies = _str.Split(";")
            'Next




        End Sub
        Function IsStudentHasPlanSecurity(ByVal EleveID As String) As Boolean
            Dim oGen As New PEIDAL.general
            Dim ds As New DataSet

            ds = oGen.GetDataset("Select plan_securite from ELEVES where pk_eleve_id='" & EleveID & "'", ConfigurationSettings.AppSettings("ConnectionString"))

            If Trim(ds.Tables(0).Rows(0).Item("plan_securite").ToString) <> "OUI" Then
                Return False
            Else
                Return True
            End If


        End Function
        Function IsFinancementMinister(ByVal EleveID As String) As Boolean
            Dim oGen As New PEIDAL.general
            Dim ds As New DataSet

            ds = oGen.GetDataset("Select demande_financement_ministere from ELEVES where pk_eleve_id='" & EleveID & "'", ConfigurationSettings.AppSettings("ConnectionString"))

            If Trim(ds.Tables(0).Rows(0).Item("demande_financement_ministere").ToString) <> "OUI" Then
                Return False
            Else
                Return True
            End If


        End Function
        Private Sub DataGridElaborations_DeleteCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)

            Dim au_fnameCell As TableCell = e.Item.Cells(0)
            Dim itemIndex As Integer
            Dim index As String
            Dim index2 As String
            Dim key As String
            Dim oIdentification As New PEIDAL.identification

            Try
                key = Trim(DataGrid1.DataKeys(e.Item.ItemIndex).ToString)

                oIdentification.DeleteElaborationByEleveId(Session("eleveid"), key)

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message
            End Try

            BindDatagridElaborations()

        End Sub

        Public Sub SetDropDownListTitre(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim ed As System.Web.UI.WebControls.DropDownList
            ed = sender
            ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strTitre))
            'ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strVal))
        End Sub

        Sub BindDatagridElaborations()
            'Dim oIdentification As PEI.identification = New PEI.identification
            Dim oIdentification As New PEIDAL.identification

            DataGrid1.DataSource = oIdentification.GetdsElaborePar(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            DataGrid1.DataKeyField = "row_id"
            Try
                DataGrid1.DataBind()
            Catch ex As Exception
                'Response.Write("Erreur de l'aplication: " & ex.Message & " --- source: " & ex.Source)
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Erreur survenue lors de l'affichage des �laborations" & ex.Message & " --- source: " & ex.Source

            End Try
        End Sub
        Public Sub SetDropDownIndex(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim ed As System.Web.UI.WebControls.DropDownList
            ed = sender
            ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strUtilisateur))
        End Sub
        Public Function BindListUtilisateur()
            Try
                Dim oIden As New PEIDAL.identification

                Return oIden.GetListEnseignantsByEcole(Session("eleveid"))

            Catch ex As Exception
                lblMsgErr.Text = ex.Message
            End Try

        End Function
        'Public Function BindListIntervenant() 'cr�er control, car fonctions doubl� dans liste.vb
        '    Try
        '        Dim oAtt As New PEIDAL.attente

        '        Return oAtt.GetListIntervenants

        '    Catch ex As Exception
        '        Throw New Exception("Erreur survenue lors du remplissage de la liste des intervenants")
        '    End Try

        'End Function
        Public Function BindListIntervenant() As SqlDataReader 'cr�er control, car fonctions doubl� dans liste.vb
            Try

                Return GetSqlDataReader("select pk_contenu_id,contenu as description from CONTENUS where type_contenu = 'IDENTIFICATION' and categorie = 'ELABORE_PAR' order by contenu", ConfigurationSettings.AppSettings("ConnectionString"))

            Catch ex As Exception
                Throw New Exception("Erreur survenue lors du remplissage de la liste des intervenants")
            End Try

        End Function

        Sub BindCaracteristiques()
            Dim oEleve As New PEIDAL.eleve
            Dim oGen As New PEIDAL.general
            Dim dsEleve As New DataSet

            Try
                dsEleve = oGen.GetDsInfosEleveEleveId(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

                TextBoxCaracteristiquesEleve.Text = dsEleve.Tables(0).Rows(0).Item("identifie_caracteristiques")
                'CheckBoxEleveIdentifie.Checked = dsEleve.Tables(0).Rows(0).Item("identifie")

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Erreur lors de l'affichage des caract�ristiques de l'�l�ve: " & ex.Message
            End Try


        End Sub


        Private Sub btnAjouterElaboration_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


        End Sub

        Private Sub DataGridElaborations_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        End Sub

        Private Sub ImageButtonSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)


        End Sub

        Private Sub DataGridElaborations_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs)

        End Sub

        Private Sub ImageButtonAjouterElaboration_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAjouterElaboration.Click
            Dim oSante As New PEIDAL.sante
            Dim oIden As New PEIDAL.identification

            'dsRhEleves1 = New PEIDAL.dsRhEleves
            dsElaborePar1 = New PEIDAL.DsElaborePar

            'Dim dr As DataRow = DsContactsList1.CONTACTS.NewRow
            'Dim dr As DataRow = dsRhEleves1.RH_ELEVES.NewRow
            Dim dr As DataRow = dsElaborePar1.ELABORE_PAR.NewRow

            Try
                'dr("fk_eleve_id") = Session("fk_eleve_id")
                'dr("prenom") = txtPrenom.Text
                'dr("nom") = TxtNom.Text
                'dr("titre") = DropDownListTitre.SelectedItem.Value
                'dr("fk_eleve_id") = Session("eleveid")

                'dr("fk_eleve_id") = Session("fk_eleve_id")
                dr("prenom") = ""
                dr("nom") = ""
                dr("titre") = ""
                dr("fk_eleve_id") = Session("eleveid")

                'DsContactsList1.CONTACTS.Rows.InsertAt(dr, 0)
                'dsElaborePar1.RH_ELEVES.Rows.InsertAt(dr, 0)
                dsElaborePar1.ELABORE_PAR.Rows.InsertAt(dr, 0)

                'oGen.UpdateDsContactsList(DsContactsList1)
                'oSante.UpdateDsRhEleves(ConfigurationSettings.AppSettings("ConnectionString"), dsRhEleves1)
                oIden.UpdateDsElaborePar(ConfigurationSettings.AppSettings("ConnectionString"), dsElaborePar1)

                DataGrid1.EditItemIndex = 0

                DataGrid1.DataSource = oIden.GetDataReaderElaboreParOrderByDescRowId(Session("eleveid"), ConfigurationSettings.AppSettings("ConnectionString"))
                DataGrid1.DataBind()

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message

            End Try

            'BindDropDownListElaborations()
            'BindDatagridElaborations()
        End Sub
        Sub BindDates()
            Dim oIden As New PEIDAL.identification
            Dim dsDatesEleve1 As New PEIDAL.dsDatesEleve
            Dim r As PEIDAL.dsDatesEleve.DATESRow

            Try
                dsDatesEleve1 = oIden.GetdsDatesEleve(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

                If dsDatesEleve1.DATES.Rows.Count > 0 Then

                    r = dsDatesEleve1.DATES.FindByFK_ELEVE_ID(Session("eleveid"))

                    If Not r.IsDATE_CIPR_RECENTNull Then
                        Me.TxtDernierCipr.Text = r.DATE_CIPR_RECENT.ToString("dd/MM/yyyy")
                    End If

                    If Not r.IsDATE_DEBUT_PLACEMENTNull Then
                        Me.TxtDatePlacement.Text = r.DATE_DEBUT_PLACEMENT.ToString("dd/MM/yyyy")
                    End If

                    If Not r.IsDATE_PEI_TERMINENull Then
                        Me.txtPeiDetermine.Text = r.DATE_PEI_TERMINE.ToString("dd/MM/yyyy")
                    End If

                    If Not r.IsDATE_RENONCIATIONNull Then
                        Me.txtDateRenonciation.Text = r.DATE_RENONCIATION.ToString("dd/MM/yyyy")
                    End If

                    If Not r.IsDATE_IDENTIFICATIONNull Then
                        Me.TxtDateIdentification.Text = r.DATE_IDENTIFICATION.ToString("dd/MM/yyyy")
                    End If

                    If Not r.IsDATE_ETAPE_1Null Then
                        Me.txtDateBulletin1.Text = r.DATE_ETAPE_1.ToString("dd/MM/yyyy")
                    End If

                    If Not r.IsDATE_ETAPE_2Null Then
                        Me.txtDateBulletin2.Text = r.DATE_ETAPE_2.ToString("dd/MM/yyyy")
                    End If

                    If Not r.IsDATE_ETAPE_3Null Then
                        Me.txtDateBulletin3.Text = r.DATE_ETAPE_3.ToString("dd/MM/yyyy")
                    End If

                    If Not r.IsDATE_ETAPE_4Null Then
                        Me.txtDateBulletin4.Text = r.DATE_ETAPE_4.ToString("dd/MM/yyyy")
                    End If

                Else

                    Dim dr As PEIDAL.dsDatesEleve.DATESRow
                    dr = dsDatesEleve1.DATES.NewDATESRow

                    dr.SetDATE_CIPR_RECENTNull()
                    dr.SetDATE_DEBUT_PLACEMENTNull()
                    dr.SetDATE_ETAPE_1Null()
                    dr.SetDATE_ETAPE_2Null()
                    dr.SetDATE_ETAPE_3Null()
                    dr.SetDATE_ETAPE_4Null()
                    dr.SetDATE_MODIFICATIONNull()
                    dr.SetDATE_PEI_TERMINENull()
                    dr.SetDATE_RENONCIATIONNull()
                    dr.SetDATE_IDENTIFICATIONNull()

                    dr.FK_ELEVE_ID = (Session("eleveid"))
                    dsDatesEleve1.DATES.Rows.InsertAt(dr, 0)

                    oIden.UpdateDsDatesEleve(ConfigurationSettings.AppSettings("ConnectionString"), dsDatesEleve1)

                End If

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Erreur survenu lors de l'afichage des dates. Details:" & ex.Message

            End Try

        End Sub

        Private Sub DataGrid1_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.DeleteCommand
            Dim Key As Integer
            Dim oSante As New PEIDAL.sante
            Dim oIdent As New PEIDAL.identification

            Try
                Key = DataGrid1.DataKeys(e.Item.ItemIndex).ToString
                oIdent.DeleteElaborePar(ConfigurationSettings.AppSettings("ConnectionString"), Key)

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message

            End Try

            'DataGrid1.DataBind()
            BindDatagridElaborations()

        End Sub

        Private Sub DataGrid1_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.EditCommand
            DataGrid1.EditItemIndex = e.Item.ItemIndex

            'strNom = CType(e.Item.FindControl("lblNom"), Label).Text
            strPrenom = CType(e.Item.FindControl("lblPrenom"), Label).Text
            strTypeIntervenant = Trim(CType(e.Item.FindControl("lblTitre"), Label).Text)

            BindDatagridElaborations()

        End Sub

        Private Sub DataGrid1_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.UpdateCommand
            Dim Prenom, Nom, Titre As String

            ' Gets the value of the key field of the row being updated

            Dim key As String = DataGrid1.DataKeys(e.Item.ItemIndex).ToString()

            ' Gets get the value of the controls (textboxes) that the user
            ' updated. The DataGrid columns are exposed as the Cells collection.
            ' Each cell has a collection of controls. In this case, there is only one 
            ' control in each cell -- a TextBox control. To get its value,
            ' you copy the TextBox to a local instance (which requires casting)
            ' and extract its Text property.
            '
            ' The first column -- Cells(0) -- contains the Update and Cancel buttons.
            Dim tb As TextBox

            Dim temp As String

            Try

                ' Gets the value the TextBox control in the third column
                'tb = CType(e.Item.Cells(3).Controls(0), TextBox)
                'Prenom = tb.Text

                'Prenom = CType(e.Item.FindControl("lblPrenom"), Label).Text
                Prenom = CType(e.Item.FindControl("textboxPrenom"), TextBox).Text

                'Nom = CType(e.Item.FindControl("lblNom"), Label).Text

                'tb = CType(e.Item.Cells(5).Controls(0), TextBox)
                'Freq = tb.Text

                Titre = CType(e.Item.FindControl("DropDownListIntervenant"), DropDownList).SelectedValue


                ' Finds the row in the dataset table that matches the 
                ' one the user updated in the grid. This example uses a 
                ' special Find method defined for the typed dataset, which
                ' returns a reference to the row.
                'SqlDataAdapter1.Fill(DsRapportsEvaluationList1)

                'Dim objFB As New PEIDAL.forces_besoins
                'Dim oSante As New PEIDAL.sante
                Dim oIden As New PEIDAL.identification

                'DsRapportsEvaluationList1 = objFB.GetdsRapportsEvaluationList(Session("eleveid"))
                'dsRhEleves1 = oSante.GetdsRhEleves(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
                dsElaborePar1 = oIden.GetdsElaborePar(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

                'Dim r As PEIDAL.dsRapportsEvaluationList.RAPPORTS_EVALUATIONRow
                'Dim r As PEIDAL.dsRhEleves.RH_ELEVESRow
                Dim r As PEIDAL.DsElaborePar.ELABORE_PARRow
                'r = DsRapportsEvaluationList1.RAPPORTS_EVALUATION.FindByROW_ID(key)
                'r = dsRhEleves1.RH_ELEVES.FindByROW_ID(key)
                r = dsElaborePar1.ELABORE_PAR.FindByROW_ID(key)


                'temp = r.CategoryName
                'temp = r.Description()

                ' Updates the dataset table.
                r.nom = ""
                r.prenom = Prenom
                ' r.nom = ""
                r.NO_ORDER = 100
                r.titre = Titre
                r.fk_eleve_id = Session("eleveid")

                'r.TYPE_EVAL = TypeEval
                'r.DATE_EVAL = DateEval
                'r.SOMMAIRE = Sommaire

                ' Calls a SQL statement to update the database from the dataset
                'SqlDataAdapter1.Update(DsRapportsEvaluationList1)
                'Dim oFB As New PEIDAL.forces_besoins

                'Debug.Listeners.Add(New TextWriterTraceListener("peilog.log"))
                'Debug.WriteLine("Starting tests !!!")

                Trace.Write("erreur d'application", "une erreur est arriv� dans....")


                'objFB.UpdateDsRapportsEvaluation(DsRapportsEvaluationList1)
                'oSante.UpdateDsRhEleves(ConfigurationSettings.AppSettings("ConnectionString"), dsRhEleves1)
                oIden.UpdateDsElaborePar(ConfigurationSettings.AppSettings("ConnectionString"), dsElaborePar1)

                ' Takes the DataGrid row out of editing mode
                DataGrid1.EditItemIndex = -1

                ' Refreshes the grid
                'DataGrid1.DataBind()
                '******should be binding with specific sql query (order by no DESC) 
                BindDatagridElaborations()

            Catch ex As Exception
                'Debug.Listeners.Add(New TextWriterTraceListener("peilog.log"))
                'Debug.WriteLine("Starting tests in Catch sub..")
                'Debug.Flush()
                'Debug.Assert(True, "Erreur de mise � jour des �laborations!")
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message
                'Debug.Assert(False, "Erreur de mise � jour des �laborations!")


            End Try

            'Debug.Flush()
        End Sub



        Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender

        End Sub
        Public Sub SetDropDownListIntervenant(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim ed As System.Web.UI.WebControls.DropDownList
            ed = sender

            Try
                ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByValue(strTypeIntervenant))
            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Erreur survenue lors de l'affichage de l'intervenant (liste d�roulante)"
            End Try

        End Sub

        Private Sub DataGrid1_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.CancelCommand
            Try
                DataGrid1.EditItemIndex = -1

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Erreur lors de l'annulation de la ligne"

            End Try

            BindDatagridElaborations()

        End Sub

        Private Sub DataGrid1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DataGrid1.ItemDataBound
            Dim tb As TextBox

            'tb = CType(e.Item.Cells(2).Controls(0), TextBox)
            'strTypeIntervenant = tb.Text

            'CType(e.Item.Cells(1).Controls(0), TextBox).Text = "CC"
            'CType(e.Item.Cells(2).Controls(0), DropDownList).SelectedValue = "CC"

        End Sub

        Private Sub btnEnregistrer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEnregistrer.Click
            SavePage()
        End Sub

        Private Sub RadioButtonNothing_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButtonNothing.CheckedChanged
            If RadioButtonNothing.Checked = True Then
                For Each I As ListItem In RadioButtonListPlacement.Items
                    I.Selected = False
                Next
            End If
        End Sub

        Private Sub RadioButtonListPlacement_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButtonListPlacement.SelectedIndexChanged
            RadioButtonNothing.Checked = False
        End Sub
        Sub SavePage()
            'update source d informations
            'Dim objIdentification As New PEI.identification
            Dim objIdentification As New PEIDAL.identification
            Dim oGen As New PEIDAL.general

            Dim dValue As Date
            Dim sDate As String
            Dim eleveID As String = Session("eleveid")

            Try

                'ficancement
                Dim valFinancement As String
                If Me.CheckBoxFincancement.Checked Then
                    valFinancement = "OUI"
                Else
                    valFinancement = "NON"
                End If

                oGen.ExecuteCommand(ConfigurationSettings.AppSettings("ConnectionString"), "UPDATE ELEVES SET demande_financement_ministere='" & valFinancement & "' WHERE pk_eleve_id='" & eleveID & "'")

                'plan de securit�

                Dim valplanSecurite As String
                If Me.CheckBoxPlanSecurite.Checked Then
                    valplanSecurite = "OUI"
                Else
                    valplanSecurite = "NON"
                End If

                oGen.ExecuteCommand(ConfigurationSettings.AppSettings("ConnectionString"), "UPDATE ELEVES SET plan_securite='" & valplanSecurite & "' WHERE pk_eleve_id='" & eleveID & "'")

                objIdentification.DeleteListSourceInformationsByEleveId(Session("eleveid"))
                Dim ch As New CheckBox
                Dim li As ListItem
                'Dim dr As DataRow
                For Each li In CheckBoxListeSources.Items
                    If li.Selected Then
                        objIdentification.InsertListSourceInformationsByEleveId(Session("eleveid"), li.Value)
                    End If
                Next

                'update placements
                objIdentification.DeleteListPlacementsElevesByEleveId(Session("eleveid"))
                For Each li In RadioButtonListPlacement.Items
                    If li.Selected Then
                        objIdentification.InsertListPlacementsElevesByEleveId(Session("eleveid"), li.Value)
                    End If
                Next

                '==============================================
                'update Anomalies
                objIdentification.DeleteListAnomaliesByEleveId(Session("eleveid"))

                'For Each li In CheckBoxListAnomalies.Items
                '    If li.Selected Then
                '        objIdentification.InsertListAnomaliesElevesByEleveId(Session("eleveid"), li.Value)
                '    End If
                'Next

                For Each li In Me.cblTitles.Items

                    If li.Selected Then
                        objIdentification.InsertListAnomaliesElevesByEleveId(Session("eleveid"), li.Value)
                    End If
                Next
                '=============================================


                'update Raisons
                objIdentification.DeleteListRaisonsElevesByEleveId(Session("eleveid"))
                For Each li In RadioButtonListRaisons.Items
                    If li.Selected Then
                        objIdentification.InsertListRaisonsElevesByEleveId(Session("eleveid"), li.Value)
                    End If
                Next

                'update Caracteristiques
                'Dim oEleve As New PEI.eleves
                Dim oEle As New PEIDAL.eleve

                'Dim oDetailsEleves As New PEI.DetailsEleve

                oEle.UpdateDetailsEleve(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"), "", "", "", "", "", "", "", "", "", "", "", "", Date.MinValue, "", "", TextBoxCaracteristiquesEleve.Text, "")

                'oDetailsEleves.Prenom_Eleve = "newYvon"
                'oDetailsEleves.Pk_Eleve_ID = Session("eleveid")
                'oDetailsEleves.Identifie = CheckBoxEleveIdentifie.Checked
                'oDetailsEleves.IdentifieCaracteristiques = TextBoxCaracteristiquesEleve.Text

                'oEleve.UpdateDetailsEleve(oDetailsEleves)

                'update Formats communications du rendement
                objIdentification.DeleteListFormatsCommunicationsByEleveId(Session("eleveid"))
                For Each li In CheckBoxListeFormat.Items
                    If li.Selected Then
                        objIdentification.InsertListFormatsCommunicationsByEleveId(Session("eleveid"), li.Value)
                    End If
                Next

                'Update Dates
                Dim objIden As New PEIDAL.identification
                Dim dsDatesEleve1 As New PEIDAL.dsDatesEleve

                dsDatesEleve1 = objIden.GetdsDatesEleve(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

                If dsDatesEleve1.DATES.Rows.Count > 0 Then

                    Dim r As PEIDAL.dsDatesEleve.DATESRow
                    r = dsDatesEleve1.DATES.FindByFK_ELEVE_ID(Session("eleveid"))

                    If Me.TxtDernierCipr.Text <> "" Then

                        'r.DATE_CIPR_RECENT.ToString("yyyy/MM/dd")
                        r.DATE_CIPR_RECENT = SetFrenchCanDate(Me.TxtDernierCipr.Text)

                        ''d()
                        ''dValue.ToShortDateString()

                        'dValue.ToString("yyyy/MM/dd")
                        'dValue = Me.TxtDernierCipr.Text
                        'r.DATE_CIPR_RECENT = dValue


                        'r.DATE_CIPR_RECENT = Date.Now

                        'r.DATE_CIPR_RECENT = Date.Now.ToString("yyyy/MM/dd")
                        ''r.DATE_CIPR_RECENT = Date.Now

                        'sDate = Format(Me.TxtDernierCipr.Text, ")

                        'r.DATE_CIPR_RECENT = sDate
                        'r.DATE_CIPR_RECENT = dValue
                        'r.DATE_CIPR_RECENT = dValue.ToString("yyyy/MM/dd")

                        ''Date.Now.ToString("yyyy/MM/dd")

                        ''r.DATE_CIPR_RECENT = String.Format("{0:d}", Me.TxtDernierCipr.Text)

                        'r.DATE_CIPR_RECENT = dValue.ToString

                    Else
                        r.SetDATE_CIPR_RECENTNull()
                    End If

                    If Me.TxtDatePlacement.Text <> "" Then
                        r.DATE_DEBUT_PLACEMENT = SetFrenchCanDate(Me.TxtDatePlacement.Text)
                    Else
                        r.SetDATE_DEBUT_PLACEMENTNull()
                    End If

                    If Me.txtPeiDetermine.Text <> "" Then
                        r.DATE_PEI_TERMINE = SetFrenchCanDate(Me.txtPeiDetermine.Text)
                    Else
                        r.SetDATE_PEI_TERMINENull()
                    End If

                    If Me.txtDateRenonciation.Text <> "" Then
                        r.DATE_RENONCIATION = SetFrenchCanDate(Me.txtDateRenonciation.Text)
                    Else
                        r.SetDATE_RENONCIATIONNull()
                    End If

                    If Me.TxtDateIdentification.Text <> "" Then
                        r.DATE_IDENTIFICATION = SetFrenchCanDate(Me.TxtDateIdentification.Text)
                    Else
                        r.SetDATE_IDENTIFICATIONNull()
                    End If

                    'date bulletin
                    If Me.txtDateBulletin1.Text <> "" Then
                        r.DATE_ETAPE_1 = SetFrenchCanDate(Me.txtDateBulletin1.Text)
                    Else
                        r.SetDATE_ETAPE_1Null()
                    End If

                    If Me.txtDateBulletin2.Text <> "" Then
                        r.DATE_ETAPE_2 = SetFrenchCanDate(Me.txtDateBulletin2.Text)
                    Else
                        r.SetDATE_ETAPE_2Null()
                    End If

                    If Me.txtDateBulletin3.Text <> "" Then
                        r.DATE_ETAPE_3 = SetFrenchCanDate(Me.txtDateBulletin3.Text)
                    Else
                        r.SetDATE_ETAPE_3Null()
                    End If

                    If Me.txtDateBulletin4.Text <> "" Then
                        r.DATE_ETAPE_4 = SetFrenchCanDate(Me.txtDateBulletin4.Text)
                    Else
                        r.SetDATE_ETAPE_4Null()
                    End If

                    'objIden.UpdateDsDatesEleve(ConfigurationSettings.AppSettings("ConnectionString"), dsDatesEleve1)
                Else

                    Dim dr As PEIDAL.dsDatesEleve.DATESRow
                    dr = dsDatesEleve1.DATES.NewDATESRow

                    dr.SetDATE_CIPR_RECENTNull()
                    dr.SetDATE_DEBUT_PLACEMENTNull()
                    dr.SetDATE_ETAPE_1Null()
                    dr.SetDATE_ETAPE_2Null()
                    dr.SetDATE_ETAPE_3Null()
                    dr.SetDATE_ETAPE_4Null()
                    dr.SetDATE_MODIFICATIONNull()
                    dr.SetDATE_PEI_TERMINENull()
                    dr.SetDATE_RENONCIATIONNull()
                    dr.SetDATE_IDENTIFICATIONNull()


                    dr.FK_ELEVE_ID = Session("eleveid")

                    dsDatesEleve1.DATES.Rows.InsertAt(dr, 0)

                End If

                objIden.UpdateDsDatesEleve(ConfigurationSettings.AppSettings("ConnectionString"), dsDatesEleve1)


            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Une erreur est survenue lors de l'enregistrement. Contactez l'administrateur du syst�me. D�tails de l'erreur:" & ex.Message

            End Try
        End Sub

        Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
            SavePage()
        End Sub

        Sub DisplayAnomalieGroup(ByVal EleveID As String, ByVal Conn As String)
            Dim oIden As New PEIDAL.identification
            Dim _ds As New DataSet
            Dim itm As ListItem
            Dim _groupe As String = ""
            'Dim _nbrGroupe As Integer = 0
            'Dim _strGroupe As String = ""


            _ds = oIden.GetDsAnomaliesByEleveID(Session("eleveid"), Conn)

            For Each r As Data.DataRow In _ds.Tables(0).Rows

                If _groupe <> Trim(r.Item("groupe").ToString) Then
                    itm = New ListItem("[ " & r.Item("groupe").ToString & " ]", VwdCms.TitleCheckBoxList.TitleValue)
                    Me.cblTitles.Items.Add(itm)

                End If

                itm = New ListItem(r.Item("description").ToString, r.Item("pk_anomalie_id").ToString)

                If Not IsDBNull(r.Item("fk_eleve_id")) Then
                    itm.Selected = True
                End If

                Me.cblTitles.Items.Add(itm)

                _groupe = Trim(r.Item("groupe").ToString)

            Next

            '---------------------------------
            '-- coche la case 'anomalies multiples' si plus de deux groupes coch�s...'

            Me.cblTitles.Items.FindByText("Anomalies multiples").Selected = oIden.IsAnomaliesMultiples(EleveID, Conn)
            Me.cblTitles.Items.FindByText("Anomalies multiples").Enabled = False

            '-----------------------------------

        End Sub

        
        Protected Sub RadioButtonListRaisons_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioButtonListRaisons.SelectedIndexChanged
            If Me.RadioButtonListRaisons.Items.FindByText("�l�ve identifi�(e) comme un �l�ve en difficult� par un CIPR.").Selected Then
                Me.TextBoxCaracteristiquesEleve.Enabled = False
                Me.TextBoxCaracteristiquesEleve.Text = " "
            Else
                Me.TextBoxCaracteristiquesEleve.Enabled = True

            End If
        End Sub

        Protected Sub ButtonDesactiver_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonDesactiver.Click

            Me.LabelValidationRetirerEleve.Visible = True
            Me.CheckBoxValidationRetirerEleve.Visible = True
        End Sub

        Protected Sub CheckBoxValidationRetirerEleve_CheckedChanged(sender As Object, e As System.EventArgs) Handles CheckBoxValidationRetirerEleve.CheckedChanged

            If CheckBoxValidationRetirerEleve.Checked Then
                Dim oEle As New PEIDAL.eleve

                oEle.UpdateEleveByOneField(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"), "FK_ECOLE_ID", "9999")

                Response.Redirect("listeeleves.aspx")
            End If
        End Sub
    End Class

End Namespace
