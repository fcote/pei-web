﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="report2c.aspx.vb" Inherits="PeiWebAccess.report2" %>
<%@ Register assembly="Telerik.ReportViewer.WebForms, Version=6.0.12.215, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" namespace="Telerik.ReportViewer.WebForms" tagprefix="telerik" %>
<%@ Register TagPrefix="uc1" TagName="menu_top" Src="menu_top.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div><uc1:menu_top id="Menu_top1" runat="server"></uc1:menu_top></div>
    <asp:Label ID="lblErrMsg" runat="server" Font-Bold="True" ForeColor="#CC0000"></asp:Label>
    <div>

    </div>
    <div>
        <asp:DropDownList ID="DropDownListEcoles" runat="server" CssClass="fldLabel" AutoPostBack="True" 
            Width="307px">
        </asp:DropDownList>
        <br />
        <asp:DropDownList ID="DropDownListEleves" runat="server" CssClass="fldLabel" AutoPostBack="True" 
            Width="307px">
        </asp:DropDownList><br />
      <asp:DropDownList id="DropDownListEtape" runat="server" CssClass="fldLabel" Width="201px" AutoPostBack="True">
								            <asp:ListItem Value="1">Bulletin de progrès [semestre 1,terme 1]</asp:ListItem>
											<asp:ListItem Value="2">Premier bulletin [semestre 1, terme 2]</asp:ListItem>
											<asp:ListItem Value="3">Deuxième bulletins [semestre 2, terme 1]</asp:ListItem>
											<asp:ListItem Value="4">[semestre 2, terme 2]</asp:ListItem>
	 </asp:DropDownList>
        <br />
    </div>
    <div>
       <br />
        <asp:CheckBox ID="CheckBoxPrintAll" runat="server" AutoPostBack="True" 
            Text="Imprimer tout" />
        <br />
        <br />
    </div>
    <div>
        <asp:CheckBoxList ID="CheckBoxList1" runat="server">
            <asp:ListItem Value="donnees_generales">Données Générales</asp:ListItem>
            <asp:ListItem Value="donnees_evaluations">Données d&#39;évaluations</asp:ListItem>
            <asp:ListItem Value="ress_humaines">Ressources humaines</asp:ListItem>
            <asp:ListItem Value="forces_besoins">Points forts et besoins</asp:ListItem>
            <asp:ListItem Value="matieres">Matières</asp:ListItem>
            <asp:ListItem Value="competences_habiletes">Compétences et habiletés</asp:ListItem>
            <asp:ListItem Value="adaptations">Adaptations</asp:ListItem>
            <asp:ListItem Value="Eval_Prov">Évaluations Provinciales</asp:ListItem>
            <asp:ListItem Value="buts">Buts, Attentes et Stratégies</asp:ListItem>
            <%--<asp:ListItem Value="plan_transition">Plan de transition</asp:ListItem>--%>
            <asp:ListItem Value="plan_transition2">Plan de transition</asp:ListItem>
            <asp:ListItem Value="details_consultations">Détails sur les consultations</asp:ListItem>
            <asp:ListItem Value="page_signature">Page de signature</asp:ListItem>
        </asp:CheckBoxList>
        <!--<asp:ListItem Value="plan_transition2">Plan transition (nouveau)</asp:ListItem>-->
    </div>
<br />
    <div>
        <asp:Button ID="Button1" runat="server" Text="Visualiser le PEI (.pdf)" />
    </div>
    <telerik:ReportViewer Visible="false" ID="ReportViewer1" runat="server" Height="1200px" 
        Report="peireport.pei, peireport, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" 
        Width="933px" ShowDocumentMapButton="False" ShowHistoryButtons="False" 
        ShowNavigationGroup="False" ToolbarVisible="False">
    </telerik:ReportViewer>

    <telerik:ReportViewer ID="ReportViewer2" Visible=false runat="server">
    </telerik:ReportViewer>
    </form>
</body>
</html>
