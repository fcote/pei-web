﻿' Copyright (c) 2007 Jeffrey Bazinet, VWD-CMS, http://www.vwd-cms.com 
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.IO

Namespace VwdCms
    Public Class TitleCheckBoxList
        Inherits CheckBoxList
        Public Const TitleValue As String = "###title###"
        Public Const SpacerValue As String = "###spacer###"

        Private _titleWidth As Unit

        Public Property TitleWidth() As Unit
            Get
                Return _titleWidth
            End Get
            Set(ByVal value As Unit)
                _titleWidth = value
            End Set
        End Property

        Protected Overrides Sub RenderItem(ByVal itemType As ListItemType, ByVal repeatIndex As Integer, ByVal repeatInfo As RepeatInfo, ByVal writer As HtmlTextWriter)
            Dim itm As ListItem = Me.Items(repeatIndex)
            Dim val As String = itm.Value

            If val = TitleValue OrElse val = SpacerValue Then
                If repeatIndex = 0 Then
                    Dim sw As New StringWriter()
                    Dim dummyWriter As New HtmlTextWriter(sw)
                    MyBase.RenderItem(itemType, repeatIndex, repeatInfo, dummyWriter)
                End If

                If val = TitleValue Then
                    Dim lbl As New Label()
                    lbl.Font.CopyFrom(Me.Font)
                    lbl.Font.Bold = True
                    lbl.Text = itm.Text
                    If Me.TitleWidth <> Nothing Then
                        lbl.Width = Me.TitleWidth
                    End If
                    lbl.RenderControl(writer)
                ElseIf val = SpacerValue Then
                    writer.Write(HtmlTextWriter.SpaceChar)
                End If
            Else
                MyBase.RenderItem(itemType, repeatIndex, repeatInfo, writer)
            End If
        End Sub
    End Class
End Namespace


