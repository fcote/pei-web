<%@ Register TagPrefix="cc1" Namespace="sstchur.web.SmartNav" Assembly="sstchur.web.SmartNav" %>
<%@ Page Language="vb" AutoEventWireup="false" ValidateRequest="false" Inherits="PeiWebAccess.testsprovinciaux" smartNavigation="True" CodeFile="testsprovinciaux.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="menu_top" Src="menu_top.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>testsprovinciaux</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE class="cbToolbar_2 tabBkgd_2" id="tableMain" borderColor="#ff3300" cellSpacing="0"
				cellPadding="0" width="100%" border="0">
				<TR>
					<TD colSpan="4"><uc1:menu_top id="Menu_top1" runat="server"></uc1:menu_top></TD>
				</TR>
				<TR>
					<TD class="titleOnglet">�valuations provinciales</TD>
				</TR>
				<TR>
					<TD class="titleOnglet"><asp:label id="lblMsgErr" runat="server" BorderWidth="1px" BorderColor="Firebrick" BackColor="Khaki"
							CssClass="fldlabel" Visible="False" Width="100%"></asp:label></TD>
				</TR>
				<TR>
					<TD borderColor="#0000ff">
						<asp:Button id="Button1" runat="server" Width="300px" CssClass="fldLabel" Text="Enregistrer"></asp:Button></TD>
				</TR>
				<TR>
					<TD class="cbToolbar" borderColor="#0000ff">�valuations</TD>
				</TR>
				<TR>
					<TD class="fldLabel" borderColor="#0000ff">�valuations provinciales auxquelles 
						participera l'�l�ve pendant l'ann�e scolaire:</TD>
				</TR>
				<TR>
					<TD borderColor="#0000ff"><asp:textbox id="TextBoxEvalTestProv" runat="server" CssClass="fldLabel" Width="312px" TextMode="MultiLine"
							Height="80px"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="cbToolbar" borderColor="#0000ff">Adaptations pour la participation aux 
						�valuations provinciales</TD>
				</TR>
				<TR>
					<TD class="tbButton2" borderColor="#0000ff"><asp:textbox id="txtAdaptTestsProv" runat="server" CssClass="fldTextBox" Width="312px" TextMode="MultiLine"
							Height="150px"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="tbButton2" borderColor="#0000ff"><asp:dropdownlist id="dropdownlistAdaptTestsProv" runat="server" CssClass="fldLabel" Visible="False"
							Width="600px"></asp:dropdownlist><asp:imagebutton id="ImageButtonAjouterAdaptTestsProv" runat="server" Visible="False" ImageUrl="images/create.gif"></asp:imagebutton></TD>
				</TR>
				<TR>
					<TD class="tbButton2" borderColor="#0000ff"><asp:panel id="PanelTypesExemptions" runat="server" Visible="False">
							<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<TD></TD>
								</TR>
								<TR>
									<TD>
										<asp:CheckBox id="CheckBox1" runat="server" Width="100%" Visible="False" CssClass="fldLabel" Text="Type A - Participation en classe � toutes les composantes de l'unit� d'�valuation (activit� collective et t�ches individuelle). La t�che d'�valuation individuelle ne sera pas  not�e."></asp:CheckBox></TD>
								</TR>
								<TR>
									<TD>
										<asp:CheckBox id="CheckBox2" runat="server" Visible="False" CssClass="fldLabel" Text="Type B - Participation en classe aux activit�s collectives. L'�l�ve ne re�oit aucune t�che individuelle."></asp:CheckBox></TD>
								</TR>
								<TR>
									<TD>
										<asp:CheckBox id="CheckBox3" runat="server" Visible="False" CssClass="fldLabel" Text="Type C - Aucune participation en classe pendant toute la dur�e de l'�valuation"></asp:CheckBox></TD>
								</TR>
							</TABLE>
							<asp:CheckBox id="CheckBox4" runat="server" Width="584px" Visible="False" CssClass="fldLabel"
								Text="Type D - L'�l�ve n'�tudie pas en vue de l'obtention du dipl�me d'�tudes secondaires de l'Ontario"></asp:CheckBox>
						</asp:panel></TD>
				</TR>
				<TR>
					<TD class="cbToolbar" borderColor="#0000ff">Exemptions: Inscrire l'�nonc� tir� du 
						document de l'OQRE.</TD>
				</TR>
				<TR>
					<TD class="tbButton2" style="HEIGHT: 19px" borderColor="#0000ff"><asp:textbox id="txtMotidEducationnel" runat="server" CssClass="fldTextBox" Width="312px" TextMode="MultiLine"
							Height="80px"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="tbButton2" style="HEIGHT: 19px" borderColor="#0000ff"><asp:dropdownlist id="DropDownListMotifEducationnel" runat="server" CssClass="fldLabel" Visible="False"
							Width="552px"></asp:dropdownlist><asp:imagebutton id="ImageButtonAjouterMotif" runat="server" Visible="False" ImageUrl="images/create.gif"></asp:imagebutton></TD>
				</TR>
				<TR>
					<TD class="tbButton2" borderColor="#0000ff">&nbsp;</TD>
				</TR>
				<TR>
					<TD class="tbButton2" borderColor="#0000ff">
						<cc1:SmartScroller id="SmartScroller1" runat="server"></cc1:SmartScroller></TD>
				</TR>
			</TABLE>
		</form>
		</TABLE>
	</body>
</HTML>
