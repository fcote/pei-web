Imports System.Data
Imports System.Data.SqlClient

Namespace PeiWebAccess

    Partial Class sante
        Inherits System.Web.UI.Page

#Region " Code g�n�r� par le Concepteur Web Form "

        'Cet appel est requis par le Concepteur Web Form.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        'Public strTypeEval As String
        Public strTypeIntervenant As String

        Protected WithEvents dsRhEleves1 As PEIDAL.DsRhEleves

        'REMARQUE�: la d�claration d'espace r�serv� suivante est requise par le Concepteur Web Form.
        'Ne pas supprimer ou d�placer.

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN�: cet appel de m�thode est requis par le Concepteur Web Form
            'Ne le modifiez pas en utilisant l'�diteur de code.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Placez ici le code utilisateur pour initialiser la page

            If Not Session("eleveid") <> "" Then
                Response.Redirect("listeeleves.aspx")
            End If

            lblMsgErr.Visible = False

            If Not Page.IsPostBack Then

                DisplaySanteEleves()
                'GetDataFromTableContenuEleves()
                BindDropDownListContenus(DropDownListProbSanteConnexes, "SANTE", "PROBLEME_SANTE")
                BindDropDownListContenus(DropDownListAdaptPedagogique, "ADAPTATION", "ADAPT_PEDAGOGIQUE")
                BindDropDownListContenus(DropDownListEquipement, "SANTE", "EQUIP_PERSONNALISE")
                BindDropDownListContenus(DropDownListAdaptationsEnv, "ADAPTATION", "ADAPT_ENVIRONNEMENTALE")
                BindDropDownListContenus(DropDownListAdaptMatiereEval, "ADAPTATION", "ADAPT_MATIEREEVALUATION")
                BindDG()
            End If

        End Sub

        Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            ' txtProblemesSante.Text = txtProblemesSante.Text & txtNew.Text

        End Sub
        Sub BindEleve(ByVal EleveId As String)
            'Dim oEleve As PEI.eleves = New PEI.eleves
            'Dim oAttente As PEIDAL.attente = New PEIDAL.attente
            Dim oEleve As PEIDAL.eleve = New PEIDAL.eleve
            Dim dsEleve As New DataSet
            'Dim dsAttente As New DataSet
            'Dim dsCommentaireGeneral As DataSet

            On Error Resume Next

            dsEleve = oEleve.GetEleveByID(ConfigurationSettings.AppSettings("ConnectionString"), EleveId)

            txtProbSanteConnexes.Text = dsEleve.Tables(0).Rows(0).Item("prob_sante_connexe")
            'txtRecommPart.Text = dsEleve.Tables(0).Rows(0).Item("prob_sante_recom")
            txtAdaptPedagogiques.Text = dsEleve.Tables(0).Rows(0).Item("adaptations_pedagogiques")


            'Catch ex As Exception
            'errMsg.Text = ex.Message
            'End Try

        End Sub
        Sub GetDataFromTableContenuEleves()
            Try
                Dim oGen As New PEIDAL.general

                txtEquipPers.Text = oGen.GetContenuByTypeAndCategory(Session("eleveid"), "SANTE", "EQUIP_PERSONNALISE", ConfigurationSettings.AppSettings("ConnectionString"))
                txtAdaptEnv.Text = oGen.GetContenuByTypeAndCategory(Session("eleveid"), "SANTE", "ADAPT_ENVIRONNEMENTALES", ConfigurationSettings.AppSettings("ConnectionString"))

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message
            End Try
        End Sub

        Private Sub btnEnregistrer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        End Sub
        Public Function BindTypeInter()
            Try
                Dim oAtt As New PEIDAL.attente
                Return oAtt.GetListIntervenants

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message
            End Try

        End Function
        Public Sub SetDropDownIndex(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim ed As System.Web.UI.WebControls.DropDownList
            ed = sender
            ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strTypeIntervenant))
        End Sub

        Sub BindDG()
            ' get dataset
            Dim oSante As New PEIDAL.sante
            Try
                DataGrid1.DataSource = oSante.GetdsRhEleves(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
                DataGrid1.DataBind()

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message

            End Try

        End Sub

        Private Sub DataGrid1_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.EditCommand
            DataGrid1.EditItemIndex = e.Item.ItemIndex
            strTypeIntervenant = CType(e.Item.FindControl("lblTypEval"), Label).Text

            BindDG()
        End Sub

        'Function GetDateFormatted(Val As String) As Date
        '    Dim format = "dd/MM/yyyy"
        '    Return Date.ParseExact(Val, format, System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.None)
        'End Function



        Private Sub DataGrid1_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.UpdateCommand
            Dim DateInst, TypeService, LieuInst, Freq As String
            Dim dDateInst As Date
            Dim DateEval As Date
            Dim Test As String

            ' Gets the value of the key field of the row being updated
            Test = DataGrid1.DataKeyField
            Test = DataGrid1.DataKeyField.ToString

            Dim key As String = DataGrid1.DataKeys(e.Item.ItemIndex).ToString()

            ' Gets get the value of the controls (textboxes) that the user
            ' updated. The DataGrid columns are exposed as the Cells collection.
            ' Each cell has a collection of controls. In this case, there is only one 
            ' control in each cell -- a TextBox control. To get its value,
            ' you copy the TextBox to a local instance (which requires casting)
            ' and extract its Text property.
            '
            ' The first column -- Cells(0) -- contains the Update and Cancel buttons.
            Dim tb As TextBox
            Dim temp As String

            Try

                ' Gets the value the TextBox control in the third column
                tb = CType(e.Item.Cells(3).Controls(0), TextBox)
                'DateInst = tb.Text
                'dDateInst = CDate(tb.Text)
                'Dim format() = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy"}
                'Dim format = "dd/MM/yyyy"
                'dDateInst = Date.ParseExact(tb.Text, format, System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.None)
                dDateInst = GetDateFormatted(tb.Text)


                TypeService = CType(e.Item.FindControl("ddlTypeEval"), DropDownList).SelectedItem.Value

                tb = CType(e.Item.Cells(5).Controls(0), TextBox)
                Freq = tb.Text

                tb = CType(e.Item.Cells(6).Controls(0), TextBox)
                LieuInst = tb.Text

                ' Finds the row in the dataset table that matches the 
                ' one the user updated in the grid. This example uses a 
                ' special Find method defined for the typed dataset, which
                ' returns a reference to the row.
                'SqlDataAdapter1.Fill(DsRapportsEvaluationList1)

                'Dim objFB As New PEIDAL.forces_besoins
                Dim oSante As New PEIDAL.sante

                'DsRapportsEvaluationList1 = objFB.GetdsRapportsEvaluationList(Session("eleveid"))
                dsRhEleves1 = oSante.GetdsRhEleves(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

                'Dim r As PEIDAL.dsRapportsEvaluationList.RAPPORTS_EVALUATIONRow
                Dim r As PEIDAL.DsRhEleves.RH_ELEVESRow
                'r = DsRapportsEvaluationList1.RAPPORTS_EVALUATION.FindByROW_ID(key)
                r = dsRhEleves1.RH_ELEVES.FindByROW_ID(key)

                'temp = r.CategoryName
                'temp = r.Description()

                ' Updates the dataset table.
                r.DATE_INST = dDateInst ' temporairement commented  ********************************************
                'r.DATE_INST=Date.Now

                r.FK_TYPE_SERVICE = TypeService
                r.FREQ = Freq
                r.LIEU_INST = LieuInst
                r.FK_ELEVE_ID = Session("eleveid")

                Dim inLen As Integer
                inLen = dsRhEleves1.RH_ELEVES.FREQColumn.MaxLength

                'r.TYPE_EVAL = TypeEval
                'r.DATE_EVAL = DateEval
                'r.SOMMAIRE = Sommaire

                ' Calls a SQL statement to update the database from the dataset
                'SqlDataAdapter1.Update(DsRapportsEvaluationList1)
                'Dim oFB As New PEIDAL.forces_besoins

                'objFB.UpdateDsRapportsEvaluation(DsRapportsEvaluationList1)

                '=============================
                'oSante.UpdateDsRhEleves(ConfigurationSettings.AppSettings("ConnectionString"), dsRhEleves1)
                'oSante.UpdateRHByEleveId(ConfigurationSettings.AppSettings("ConnectionString"), 9, Session("eleveid"), TypeService, Freq, dDateInst, LieuInst)

                Dim retValue As Integer
                retValue = oSante.UpdateRHEleveByRowID(ConfigurationSettings.AppSettings("ConnectionString"), CInt(key), 9, Session("eleveid"), TypeService, Freq, dDateInst, LieuInst)
                '===================================

                ' Takes the DataGrid row out of editing mode
                DataGrid1.EditItemIndex = -1

                ' Refreshes the grid
                'DataGrid1.DataBind()
                '******should bind with specific sql query (order by no DESC) 
                BindDG()

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message

            End Try
        End Sub

        Private Sub ImageButtonSaveSante_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)


        End Sub

        Private Sub DataGrid1_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.DeleteCommand
            Dim Key As Integer
            Dim oSante As New PEIDAL.sante

            Try
                Key = DataGrid1.DataKeys(e.Item.ItemIndex).ToString
                oSante.DeleteRHEleves(ConfigurationSettings.AppSettings("ConnectionString"), Key)

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Erreur survenue lors de la suppression (tableau ressources humaines)"

            End Try

            'DataGrid1.DataBind()
            BindDG()
        End Sub

        Private Sub ImageButtonAjouterRHeleves_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAjouterRHeleves.Click
            Dim oSante As New PEIDAL.sante

            dsRhEleves1 = New PEIDAL.DsRhEleves

            'Dim dr As DataRow = DsContactsList1.CONTACTS.NewRow
            Dim dr As DataRow = dsRhEleves1.RH_ELEVES.NewRow

            Try
                dr("fk_type_service") = ""
                dr("freq") = ""
                dr("date_inst") = Date.Now
                dr("lieu_inst") = ""
                dr("fk_eleve_id") = Session("eleveid")

                'DsContactsList1.CONTACTS.Rows.InsertAt(dr, 0)
                dsRhEleves1.RH_ELEVES.Rows.InsertAt(dr, 0)

                'oGen.UpdateDsContactsList(DsContactsList1)
                oSante.UpdateDsRhEleves(ConfigurationSettings.AppSettings("ConnectionString"), dsRhEleves1)

                DataGrid1.EditItemIndex = 0

                DataGrid1.DataSource = oSante.GetDataReaderRHElevesOrderByDescRowId(Session("eleveid"), ConfigurationSettings.AppSettings("ConnectionString"))
                DataGrid1.DataBind()

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message

            End Try
        End Sub

        Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
            txtEquipPers.Text = txtEquipPers.Text & Chr(13) & DropDownListEquipement.SelectedValue
        End Sub

        Private Sub ImageButtonAjouterAdaptations_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAjouterAdaptations.Click
            txtAdaptEnv.Text = txtAdaptEnv.Text & Chr(13) & DropDownListAdaptationsEnv.SelectedValue
        End Sub
        Sub DisplaySanteEleves()
            Dim oSante As New PEIDAL.sante
            Dim dsSanteEleves1 As New PEIDAL.DsSanteEleves
            Dim r As PEIDAL.DsSanteEleves.SANTE_ELEVESRow

            Try
                dsSanteEleves1 = oSante.GetdsSanteEleves(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

                If dsSanteEleves1.SANTE_ELEVES.Rows.Count < 1 Then
                    Dim dr As DataRow = dsSanteEleves1.SANTE_ELEVES.NewRow

                    dr("prob_sante_conn") = ""
                    dr("recomm_particuliere") = ""
                    dr("adapt_pedagogiques") = ""
                    dr("equip_personnalise") = ""
                    dr("adapt_environnementales") = ""
                    dr("adapt_matiereevaluation") = ""
                    dr("FK_ELEVE_ID") = Session("eleveid")

                    dsSanteEleves1.SANTE_ELEVES.Rows.InsertAt(dr, 0)

                    oSante.UpdateDsSanteEleves(ConfigurationSettings.AppSettings("ConnectionString"), dsSanteEleves1)

                Else

                    r = dsSanteEleves1.SANTE_ELEVES.FindByFK_ELEVE_ID(Session("eleveid"))

                    txtProbSanteConnexes.Text = r.PROB_SANTE_CONN.ToString

                    If txtProbSanteConnexes.Text <> "" Then
                        RadioButtonListProbSanteConn.SelectedValue = "oui"
                        PanelProbSanteConn.Visible = True
                    Else
                        RadioButtonListProbSanteConn.SelectedValue = "non"
                        PanelProbSanteConn.Visible = False
                    End If

                    'txtRecommPart.Text = r.RECOMM_PARTICULIERE.ToString
                    txtAdaptPedagogiques.Text = r.ADAPT_PEDAGOGIQUES.ToString
                    txtEquipPers.Text = r.EQUIP_PERSONNALISE.ToString
                    txtAdaptEnv.Text = r.ADAPT_ENVIRONNEMENTALES.ToString
                    TextBoxAdaptMatiereEval.Text = r.ADAPT_MATIEREEVALUATION.ToString

                End If

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message
            End Try

        End Sub

        Private Sub ImageButtonAjouterProbSanteConn_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAjouterProbSanteConn.Click
            txtProbSanteConnexes.Text = txtProbSanteConnexes.Text & Chr(13) & DropDownListProbSanteConnexes.SelectedValue

        End Sub

        Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
            txtProbSanteConnexes.Visible = True
        End Sub

        Private Sub RadioButtonListProbSanteConn_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButtonListProbSanteConn.SelectedIndexChanged
            If RadioButtonListProbSanteConn.SelectedValue = "oui" Then
                PanelProbSanteConn.Visible = True
            Else
                PanelProbSanteConn.Visible = False
            End If
        End Sub

        Private Sub ImageButtonAjouterAdapPedag_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAjouterAdapPedag.Click
            txtAdaptPedagogiques.Text = txtAdaptPedagogiques.Text & Chr(13) & DropDownListAdaptPedagogique.SelectedValue
        End Sub

        Private Sub ImageButtonAjouterAdaptEval_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAjouterAdaptEval.Click
            TextBoxAdaptMatiereEval.Text = TextBoxAdaptMatiereEval.Text & Chr(13) & DropDownListAdaptMatiereEval.SelectedValue
        End Sub

        Public Function BindListIntervenant() 'cr�er control, car fonctions doubl� dans liste.vb
            Try
                Dim oAtt As New PEIDAL.attente

                'Return oFB.GetTypeEval(ConfigurationSettings.AppSettings("ConnectionString"))

                Return oAtt.GetListIntervenants

            Catch ex As Exception
                Throw New Exception("Erreur survenue lors du remplissage de la liste des intervenants")
            End Try

        End Function
        Public Sub SetDropDownListIntervenant(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim ed As System.Web.UI.WebControls.DropDownList
            ed = sender

            Try
                ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByValue(strTypeIntervenant))
            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Erreur survenue lors de l'affichage de l'intervenant (liste d�roulante)"
            End Try

        End Sub




        Private Sub DataGrid1_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.CancelCommand
            DataGrid1.EditItemIndex = -1
            BindDG()
        End Sub
        Sub SaveSante()
            Dim oSante As New PEIDAL.sante
            Dim dsSanteEleves1 As New PEIDAL.DsSanteEleves

            dsSanteEleves1 = oSante.GetdsSanteEleves(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

            Dim r As PEIDAL.DsSanteEleves.SANTE_ELEVESRow
            r = dsSanteEleves1.SANTE_ELEVES.FindByFK_ELEVE_ID(Session("eleveid"))

            If RadioButtonListProbSanteConn.SelectedValue = "oui" Then
                r.PROB_SANTE_CONN = txtProbSanteConnexes.Text
            Else
                r.PROB_SANTE_CONN = ""
            End If

            'r.RECOMM_PARTICULIERE = txtRecommPart.Text
            r.EQUIP_PERSONNALISE = txtEquipPers.Text
            r.ADAPT_PEDAGOGIQUES = txtAdaptPedagogiques.Text
            r.ADAPT_MATIEREEVALUATION = TextBoxAdaptMatiereEval.Text
            r.ADAPT_ENVIRONNEMENTALES = txtAdaptEnv.Text

            Try
                oSante.UpdateDsSanteEleves(ConfigurationSettings.AppSettings("ConnectionString"), dsSanteEleves1)
            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = "Erreur survenue lors de l'enregistrement des adaptations...  " & ex.Message
            End Try

            DisplaySanteEleves()
        End Sub

        Private Sub Button1_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
            SaveSante()
        End Sub
    End Class

End Namespace
