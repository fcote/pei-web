Imports System.Data
Imports System.Data.SqlClient


Namespace PeiWebAccess

Partial Class programmes
    Inherits System.Web.UI.Page

#Region " Code g�n�r� par le Concepteur Web Form "

    'Cet appel est requis par le Concepteur Web Form.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Public intRowID As Integer ' use for the two datagrids

    Public strNiveau As String
    Public strNiveauID As String

    Public strMatiere As String
    Public intMatiereID As Integer

    Public strCoteNote As String
    Public strTypeAttente As String 'use for the two datagrids
    Public strCompetence As String
    Public strRendement As String


    Protected WithEvents ddl1 As System.Web.UI.WebControls.DropDownList

    Protected WithEvents dsMatieresElevesByEleveId1 As PEIDAL.dsMatieresElevesByEleveID
    Protected WithEvents dsMatieresEleves2 As PEIDAL.dsMatieresEleves
    Protected WithEvents dsCompetencesEleves1 As PEIDAL.dsCompetences_Eleves

    Protected WithEvents TextBox1 As System.Web.UI.WebControls.TextBox

    'REMARQUE�: la d�claration d'espace r�serv� suivante est requise par le Concepteur Web Form.
    'Ne pas supprimer ou d�placer.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN�: cet appel de m�thode est requis par le Concepteur Web Form
        'Ne le modifiez pas en utilisant l'�diteur de code.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Placez ici le code utilisateur pour initialiser la page
        If Not Session("eleveid") <> "" Then
            Response.Redirect("listeeleves.aspx")
        End If

            lblMsgErr.Visible = False

        If Not Page.IsPostBack Then
            BindDG()
            BindDGcompetencesEleves()
            BindCommentairesRendement() 'from ELEVE table
            BindDiplome()

        End If

    End Sub
    Sub BindDG()
        ' get dataset
        Try
            Dim oProg As New PEIDAL.programme

            'DataGrid1.DataSource = oProg.GetdsMatieresElevesByEleveID(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            'DataGrid1.DataBind()

            DataGrid1.DataSource = oProg.GetReaderMatieres_ElevesOrderByDescRowID(Session("eleveid"), ConfigurationSettings.AppSettings("ConnectionString"))
            DataGrid1.DataBind()

        Catch ex As Exception
            lblMsgErr.Text = ex.Message
            lblMsgErr.Visible = True
            'lblHelpLink.Text = ex.HelpLink
        End Try

    End Sub
    Sub BindDGcompetencesEleves()
        ' get dataset
        Try
            Dim oProg As New PEIDAL.programme

            DataGrid2.DataSource = oProg.GetdsCompetences_Eleves(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

            'DataGrid1.DataSource = oProg.GetReaderMatieresElevesByEleveID(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            DataGrid2.DataBind()

        Catch ex As Exception
            lblMsgErr.Visible = True
            lblMsgErr.Text = ex.Message
        End Try

    End Sub
    Public Function BindNiveau()
        Try
            Dim oProg As New PEIDAL.programme

            'Return oFB.GetTypeEval(ConfigurationSettings.AppSettings("ConnectionString"))
            Return oProg.GetListNiveauEtude


        Catch ex As Exception
            lblMsgErr.Text = ex.Message
        End Try

    End Function
        Public Function Bindmatiere()
            Dim _sql As String
            Dim _ds As Data.DataSet
            Dim _niveau As String
            Dim _colname As String = ""

            'Dim oEle As New PEIDAL.eleve
            '_ds = oEle.GetDsEleve(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            '_niveau = Trim(_ds.Tables(0).Rows(0).Item("fk_niveau_id"))

            'Select Case _niveau
            '    Case "M", "J"
            '        _colname = "MJ"
            '    Case "1", "2", "3", "4", "5", "6", "7", "8"
            '        _colname = "ELE"
            '    Case "9", "10", "11", "12"
            '        _colname = "SEC"

            '    Case Else
            '        Throw New Exception("Erreur survenu lors de l'affichage des matieres: niveau d''�tude de l'�l�ve inconnu")
            'End Select

            '_sql = "SELECT PK_MATIERE_ID,MATIERE FROM MATIERES WHERE " & _colname & " = 1"
            _sql = "select PK_MATIERE_ID,MATIERE from MATIERES where (ordre Is Not null)order by ordre"

            Try
                Dim oProg As New PEIDAL.programme

                'Return oFB.GetTypeEval(ConfigurationSettings.AppSettings("ConnectionString"))
                'Return oProg.GetListMatiere
                Return GetSqlDataReader(_sql, ConfigurationSettings.AppSettings("ConnectionString"))


            Catch ex As Exception
                lblMsgErr.Text = ex.Message
            End Try

        End Function
    Public Function BindCompetence()
        Try
            Dim oGeneral As New PEIDAL.general

            'Return oFB.GetTypeEval(ConfigurationSettings.AppSettings("ConnectionString"))
            Return oGeneral.GetListContenuByTypeAndCategorie("COMPETENCES", "Habilet�")


        Catch ex As Exception
            lblMsgErr.Text = ex.Message
        End Try

    End Function
    Public Function GetMatiere(ByVal MatiereId As Integer)
        Dim oProg As New PEIDAL.programme

        Return oProg.GetMatiereByMatiereID(ConfigurationSettings.AppSettings("ConnectionString"), MatiereId)

    End Function
    Public Function BindTypeAttente()
        Try
            Dim oProg As New PEIDAL.programme

            'Return oFB.GetTypeEval(ConfigurationSettings.AppSettings("ConnectionString"))
            'Return oProg.GetListMatiere


        Catch ex As Exception
            lblMsgErr.Text = ex.Message
        End Try

    End Function
    Public Function BindCoteNote()
        Try
            Dim oAtt As New PEIDAL.attente

            'Return oFB.GetTypeEval(ConfigurationSettings.AppSettings("ConnectionString"))

            Return oAtt.GetListTypeEvaluation()


        Catch ex As Exception
            lblMsgErr.Text = ex.Message
        End Try

    End Function
    Public Sub SetDropDownIndex(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ed As System.Web.UI.WebControls.DropDownList
        ed = sender
        ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strNiveau))
        'ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strVal))
    End Sub
    Public Sub SetDropDownIndexNiveau(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ed As System.Web.UI.WebControls.DropDownList
        ed = sender
        ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strNiveau))
        'ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strVal))
    End Sub
    Public Sub SetDropDownIndexMatiere(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ed As System.Web.UI.WebControls.DropDownList
        ed = sender
        ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strMatiere))
        'ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strVal))
    End Sub
    Public Sub SetDropDownIndexCoteNote(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ed As System.Web.UI.WebControls.DropDownList
        ed = sender
        ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strRendement))
        'ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strVal))
    End Sub
    Public Sub SetDropDownIndexCompetence(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ed As System.Web.UI.WebControls.DropDownList
        ed = sender
        ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strCompetence))
        'ed.Items.Add(strCompetence)
        'ed.Items.FindByText(strCompetence).Selected = True

    End Sub
    Public Sub SetTextBoxRowID(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ed As System.Web.UI.WebControls.TextBox
        ed = sender
        'ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(intRowID))
        ed.Text = intRowID
        'ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strVal))
    End Sub
    Public Sub SetDropDownIndexTypeAttente(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ed As System.Web.UI.WebControls.DropDownList
        ed = sender
        ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strTypeAttente))
        'ed.SelectedIndex = ed.Items.IndexOf(ed.Items.FindByText(strVal))
    End Sub

    Private Sub DataGrid1_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.EditCommand
        DataGrid1.EditItemIndex = e.Item.ItemIndex

        intRowID = CType(e.Item.FindControl("labelRowID"), Label).Text
        strNiveau = CType(e.Item.FindControl("lblDescription_niveau"), Label).Text
        strMatiere = CType(e.Item.FindControl("lblMatiere"), Label).Text
        intMatiereID = CType(e.Item.FindControl("lblMatiereID"), Label).Text
        strCoteNote = Trim(CType(e.Item.FindControl("lblCoteNote"), Label).Text)
        strTypeAttente = Trim(CType(e.Item.FindControl("lblTypeAttente"), Label).Text)

        BindDG()
    End Sub

    Private Sub DataGrid1_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.UpdateCommand
        Dim TypeEval, Sommaire As String
        Dim DateEval As Date
        Dim Test As String

        Try

            ' Gets the value of the key field of the row being updated
            Test = DataGrid1.DataKeyField
            Test = DataGrid1.DataKeyField.ToString

            'Dim key2 As Integer
            DataGrid1.DataKeyField = "ROW_ID"
            'Dim key2 As String = DataGrid1.SelectedItem.ItemIndex.ToString()
            'Dim key As String = e.Item.ItemIndex

            'Dim key As String = DataGrid1.DataKeys(e.Item.ItemIndex).ToString()
            'Dim key As String
            Dim key As Integer
            'key = CType(e.Item.FindControl("lblRowId"), Label).Text

            'Dim key As String = DataGrid1.DataKeys(e.Item

            ' Gets get the value of the controls (textboxes) that the user
            ' updated. The DataGrid columns are exposed as the Cells collection.
            ' Each cell has a collection of controls. In this case, there is only one 
            ' control in each cell -- a TextBox control. To get its value,
            ' you copy the TextBox to a local instance (which requires casting)
            ' and extract its Text property.
            '
            ' The first column -- Cells(0) -- contains the Update and Cancel buttons.
            Dim tb As TextBox
            Dim temp As String

            'tb = CType(e.Item.Cells(0).Controls(0), TextBox)
            'key = tb.Text

            'key = CType(e.Item.FindControl("txtRowId"), TextBox).Text
            key = CType(e.Item.FindControl("labelRowIDhold"), Label).Text

            ' here it works !!!!-----------
            'tb = CType(e.Item.Cells(2).Controls(0), TextBox)
            'key = tb.Text
            '---------------------------------------

            strNiveauID = CType(e.Item.FindControl("ddlNiveau"), DropDownList).SelectedItem.Value
            intMatiereID = CType(e.Item.FindControl("ddlmatiere"), DropDownList).SelectedItem.Value
            strCoteNote = CType(e.Item.FindControl("ddlCoteNote"), DropDownList).SelectedItem.Value
            strTypeAttente = CType(e.Item.FindControl("ddlTypeAttente"), DropDownList).SelectedItem.Value

            ' Finds the row in the dataset table that matches the 
            ' one the user updated in the grid. This example uses a 
            ' special Find method defined for the typed dataset, which
            ' returns a reference to the row.
            'SqlDataAdapter1.Fill(DsRapportsEvaluationList1)

            'Dim objFB As New PEIDAL.forces_besoins
            Dim objProg As New PEIDAL.programme

            'DsRapportsEvaluationList1 = objFB.GetdsRapportsEvaluationList("az199511185_2")
            'dsMatieresElevesByEleveId1 = objProg.GetdsMatieresElevesByEleveID(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

            'Dim dsMatieres_Eleves1 As New PEIDAL.dsMatieres_Eleves
            'dsMatieres_Eleves1 = objProg.GetdsMatieres_Eleves(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

            dsMatieresEleves2 = objProg.GetdsMatieres_Eleves(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            'Response.Write("<br>DATE:<br>" & dsMatieres_Eleves2.MATIERES_ELEVES.Columns.Item(0).ToString)

            'Dim r As PEIDAL.dsMatieresElevesByEleveID.MATIERESRow
            'Dim r As New PEIDAL.dsMatieres_Eleves.MATIERES_ELEVESRow
            Dim r As PEIDAL.dsMatieresEleves.MATIERES_ELEVESRow

            'r = dsMatieresElevesByEleveId1.MATIERES.FindByROW_ID(key) ' matieres row contient champ de la table 'matieres_eleves'
            'r = dsMatieres_Eleves1.MATIERES_ELEVES.FindByROW_ID(key)

            r = dsMatieresEleves2.MATIERES_ELEVES.FindByROW_ID(key)
            'r = dsMatieres_Eleves2.MATIERES_ELEVES.FindByROW_ID(intRowID)

            ' Updates the dataset table.
            r.FK_NIVEAU_ID = strNiveauID
            r.FK_MATIERE_ID = intMatiereID
            r.COTE_NOTE = strCoteNote
            r.TYPE_ATTENTE = strTypeAttente

            'r.TYPE_EVAL = TypeEval
            'r.DATE_EVAL = DateEval
            'r.SOMMAIRE = Sommaire

            ' Calls a SQL statement to update the database from the dataset
            'SqlDataAdapter1.Update(DsRapportsEvaluationList1)
            'Dim oFB As New PEIDAL.forces_besoins
            'objFB.UpdateDsRapportsEvaluation(DsRapportsEvaluationList1)

            'objProg.UpdateDsMatieresEleveByEleveId(dsMatieresElevesByEleveId1)
            'objProg.UpdateDsMatieres_Eleves(dsMatieres_Eleves1)
            objProg.UpdateDsMatieres_Eleves(dsMatieresEleves2, ConfigurationSettings.AppSettings("ConnectionString"))

            ' Takes the DataGrid row out of editing mode
            DataGrid1.EditItemIndex = -1

            ' Refreshes the grid
            'DataGrid1.DataBind()
            BindDG()

        Catch ex As Exception
            lblMsgErr.Visible = True
            lblMsgErr.Text = ex.Message

        End Try
    End Sub


    Private Sub DataGrid2_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid2.ItemCommand

    End Sub

    Private Sub DataGrid2_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid2.EditCommand
        DataGrid2.EditItemIndex = e.Item.ItemIndex

        intRowID = CType(e.Item.FindControl("lblRowID2"), Label).Text
        strCompetence = CType(e.Item.FindControl("lblCompetence"), Label).Text
        strTypeAttente = Trim(CType(e.Item.FindControl("lblTypeAttente2"), Label).Text)
        strRendement = Trim(CType(e.Item.FindControl("lblRendement"), Label).Text)

        BindDGcompetencesEleves()
    End Sub
    Private Sub DataGrid2_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid2.UpdateCommand

        Try

            'Dim key2 As Integer
            DataGrid2.DataKeyField = "PK_ROW_ID"
            'Dim key2 As String = DataGrid1.SelectedItem.ItemIndex.ToString()
            'Dim key As String = e.Item.ItemIndex

            'Dim key As String = DataGrid1.DataKeys(e.Item.ItemIndex).ToString()
            'Dim key As String
            Dim key As Integer
            'key = CType(e.Item.FindControl("lblRowId"), Label).Text

            'Dim key As String = DataGrid1.DataKeys(e.Item

            ' Gets get the value of the controls (textboxes) that the user
            ' updated. The DataGrid columns are exposed as the Cells collection.
            ' Each cell has a collection of controls. In this case, there is only one 
            ' control in each cell -- a TextBox control. To get its value,
            ' you copy the TextBox to a local instance (which requires casting)
            ' and extract its Text property.
            '
            ' The first column -- Cells(0) -- contains the Update and Cancel buttons.
            Dim tb As TextBox
            Dim temp As String

            'tb = CType(e.Item.Cells(0).Controls(0), TextBox)
            'key = tb.Text

            'key = CType(e.Item.FindControl("txtRowId"), TextBox).Text
            key = CType(e.Item.FindControl("lblRowIDhold2"), Label).Text

            ' here it works !!!!-----------
            'tb = CType(e.Item.Cells(2).Controls(0), TextBox)
            'key = tb.Text
            '---------------------------------------

            strCompetence = CType(e.Item.FindControl("ddlCompetence"), DropDownList).SelectedItem.Value
            strCoteNote = CType(e.Item.FindControl("ddlRendement"), DropDownList).SelectedItem.Value
            strTypeAttente = CType(e.Item.FindControl("ddlTypeAttente2"), DropDownList).SelectedItem.Value

            ' Finds the row in the dataset table that matches the 
            ' one the user updated in the grid. This example uses a 
            ' special Find method defined for the typed dataset, which
            ' returns a reference to the row.
            'SqlDataAdapter1.Fill(DsRapportsEvaluationList1)

            'Dim objFB As New PEIDAL.forces_besoins
            Dim objProg As New PEIDAL.programme

            'DsRapportsEvaluationList1 = objFB.GetdsRapportsEvaluationList("az199511185_2")
            'dsMatieresElevesByEleveId1 = objProg.GetdsMatieresElevesByEleveID(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

            'Dim dsMatieres_Eleves1 As New PEIDAL.dsMatieres_Eleves
            Dim dsCompetences_Eleves1 As New PEIDAL.dsCompetences_Eleves
            'dsMatieres_Eleves1 = objProg.GetdsMatieres_Eleves(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

            'dsMatieres_Eleves2 = objProg.GetdsMatieres_Eleves(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            dsCompetences_Eleves1 = objProg.GetdsCompetences_Eleves(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))


            'Response.Write("<br>DATE:<br>" & dsMatieres_Eleves2.MATIERES_ELEVES.Columns.Item(0).ToString)

            'Dim r As PEIDAL.dsMatieresElevesByEleveID.MATIERESRow
            'Dim r As New PEIDAL.dsMatieres_Eleves.MATIERES_ELEVESRow
            'Dim r As PEIDAL.dsMatieres_Eleves.MATIERES_ELEVESRow
            Dim r As PEIDAL.dsCompetences_Eleves.COMPETENCES_ELEVESRow

            'r = dsMatieresElevesByEleveId1.MATIERES.FindByROW_ID(key) ' matieres row contient champ de la table 'matieres_eleves'
            'r = dsMatieres_Eleves1.MATIERES_ELEVES.FindByROW_ID(key)

            'r = dsMatieres_Eleves2.MATIERES_ELEVES.FindByROW_ID(key)
            r = dsCompetences_Eleves1.COMPETENCES_ELEVES.FindBypk_row_id(key)

            ' Updates the dataset table.
            r.competence = strCompetence
            r.type_attente = strTypeAttente
            r.rendement = strCoteNote

            ' Calls a SQL statement to update the database from the dataset
            'SqlDataAdapter1.Update(DsRapportsEvaluationList1)
            'Dim oFB As New PEIDAL.forces_besoins
            'objFB.UpdateDsRapportsEvaluation(DsRapportsEvaluationList1)

            'objProg.UpdateDsMatieres_Eleves(dsMatieres_Eleves2, ConfigurationSettings.AppSettings("ConnectionString"))
            objProg.UpdateDsCompetences_Eleves(dsCompetences_Eleves1, ConfigurationSettings.AppSettings("ConnectionString"))
            'objProg.UpdateDsCompetences_Eleves(dsCompetences_Eleves1, "fad")

            ' Takes the DataGrid row out of editing mode
            DataGrid2.EditItemIndex = -1

            ' Refreshes the grid

            BindDGcompetencesEleves()

        Catch ex As Exception
            lblMsgErr.Text = ex.Message

        End Try
    End Sub

    Private Sub DataGrid2_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid2.DeleteCommand
        Dim oProg As New PEIDAL.programme
        Dim key As Integer

        Try
            key = DataGrid2.DataKeys(e.Item.ItemIndex).ToString

            oProg.DeleteCompetencesEleve(ConfigurationSettings.AppSettings("ConnectionString"), key)

        Catch ex As Exception
            lblMsgErr.Visible = True
            lblMsgErr.Text = ex.Message
        End Try

        BindDGcompetencesEleves()

    End Sub

    Private Sub btnAjouterMatiere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnAjouterCompetence_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Sub BindCommentairesRendement()
        Dim oEleve As PEIDAL.eleve = New PEIDAL.eleve
        Dim dsEleve1 As New PEIDAL.dsEleve
        Dim r As PEIDAL.dsEleve.ELEVESRow

        Try
            dsEleve1 = oEleve.GetDsEleve(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

            If dsEleve1.ELEVES.Rows.Count > 0 Then
                r = dsEleve1.ELEVES.Rows(0)

                'txtCommentairesRendement.Text = r.Commentaires_rendement
                DropDownListExemp_prog_1.SelectedValue = Trim(r.Exemp_prog_scol_1)
                txtExemp_Prog_Scol_2.Text = r.Exemp_prog_scol_2

            End If

        Catch ex As Exception
            lblMsgErr.Text = ex.Message
        End Try

    End Sub

    Private Sub btnEngistrerCommentaires_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


    End Sub

    Private Sub DataGrid1_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.DeleteCommand
        Dim oProg As New PEIDAL.programme
        Dim key As Integer

        Try
            key = DataGrid1.DataKeys(e.Item.ItemIndex).ToString

            oProg.DeleteMatiereEleve(ConfigurationSettings.AppSettings("ConnectionString"), key)

        Catch ex As Exception
            lblMsgErr.Text = ex.Message
        End Try

        BindDG()

    End Sub

    Private Sub ImageButtonAjouterRendementMatiere_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAjouterRendementMatiere.Click
        Dim objFB As New PEIDAL.forces_besoins
        'Dim ds As New PEIDAL.dsRapportsEvaluationList

        'DsRapportsEvaluationList1 = objFB.GetdsRapportsEvaluationList("az199511185_2")
        'BindDG()

        'DsRapportsEvaluationList1 = New PEIDAL.dsRapportsEvaluationList
        dsMatieresEleves2 = New PEIDAL.dsMatieresEleves

        'Dim dr As DataRow = DsRapportsEvaluationList1.RAPPORTS_EVALUATION.NewRow
        Dim dr As DataRow = dsMatieresEleves2.MATIERES_ELEVES.NewRow

        Try
            dr("fk_matiere_id") = 6
            dr("fk_niveau_id") = 1
            dr("type_attente") = ""
            dr("cote_note") = ""
            dr("fk_eleve_id") = Session("eleveid")

            'DsRapportsEvaluationList1.RAPPORTS_EVALUATION.Rows.InsertAt(dr, 0)
            dsMatieresEleves2.MATIERES_ELEVES.Rows.InsertAt(dr, 0)

            'ds.RAPPORTS_EVALUATION.Rows.InsertAt(dr, 0)

            'Session("ds") = DsRapEval1
            'SqlDataAdapter1.Update(DsRapportsEvaluationList1)

            'Dim oFB As New PEIDAL.forces_besoins
            Dim oProg As New PEIDAL.programme
            'oFB.UpdateDsRapportsEvaluation(DsRapportsEvaluationList1)
            oProg.UpdateDsMatieres_Eleves(dsMatieresEleves2, ConfigurationSettings.AppSettings("ConnectionString"))
            'oFB.UpdateDsRapportsEvaluation(ds)

            DataGrid1.EditItemIndex = 0

            'DataGrid1.DataSource = objFB.GetdsRapportsEvaluationListOrderByDescRowID(Session("eleveid"), ConfigurationSettings.AppSettings("ConnectionString"))
            DataGrid1.DataSource = oProg.GetReaderMatieres_ElevesOrderByDescRowID(Session("eleveid"), ConfigurationSettings.AppSettings("ConnectionString"))
            DataGrid1.DataBind()
            'BindDG()

            'DataGrid1.DataBind()

        Catch ex As Exception
            lblMsgErr.Visible = True
            lblMsgErr.Text = ex.Message
        End Try
    End Sub

    Private Sub ImageButtonCompetence_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCompetence.Click
        'Dim objFB As New PEIDAL.forces_besoins
        Dim objProg As New PEIDAL.programme
        'Dim ds As New PEIDAL.dsRapportsEvaluationList

        'DsRapportsEvaluationList1 = objFB.GetdsRapportsEvaluationList("az199511185_2")
        'BindDG()

        'DsRapportsEvaluationList1 = New PEIDAL.dsRapportsEvaluationList

        '* dsMatieres_Eleves2 = New PEIDAL.dsMatieres_Eleves
        dsCompetencesEleves1 = New PEIDAL.dsCompetences_Eleves
        'Dim dsCompetencesEleves1 As New PEIDAL.dsCompetences_Eleves


        'Dim dr As DataRow = DsRapportsEvaluationList1.RAPPORTS_EVALUATION.NewRow
        '*Dim dr As DataRow = dsMatieres_Eleves2.MATIERES_ELEVES.NewRow
        Dim dr As DataRow = dsCompetencesEleves1.COMPETENCES_ELEVES.NewRow

        Try
                dr("competence") = "Autonomie au travail"
            dr("type_attente") = ""
            dr("rendement") = "A"
            dr("fk_eleve_id") = Session("eleveid")

            'DsRapportsEvaluationList1.RAPPORTS_EVALUATION.Rows.InsertAt(dr, 0)
            '*dsMatieres_Eleves2.MATIERES_ELEVES.Rows.InsertAt(dr, 0)
            dsCompetencesEleves1.COMPETENCES_ELEVES.Rows.InsertAt(dr, 0)

            'ds.RAPPORTS_EVALUATION.Rows.InsertAt(dr, 0)

            'Session("ds") = DsRapEval1
            'SqlDataAdapter1.Update(DsRapportsEvaluationList1)

            'Dim oFB As New PEIDAL.forces_besoins
            'Dim oProg As New PEIDAL.programme
            'oFB.UpdateDsRapportsEvaluation(DsRapportsEvaluationList1)
            '*oProg.UpdateDsMatieres_Eleves(dsMatieres_Eleves2, ConfigurationSettings.AppSettings("ConnectionString"))
            objProg.UpdateDsCompetences_Eleves(dsCompetencesEleves1, ConfigurationSettings.AppSettings("ConnectionString"))

            'oFB.UpdateDsRapportsEvaluation(ds)

            DataGrid2.EditItemIndex = 0

            'DataGrid1.DataSource = objFB.GetdsRapportsEvaluationListOrderByDescRowID(Session("eleveid"), ConfigurationSettings.AppSettings("ConnectionString"))
            'DataGrid2.DataSource = objProg.GetdsCompetences_Eleves(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            DataGrid2.DataSource = objProg.GetdsCompetences_ElevesOrderByDescRowID(Session("eleveid"), ConfigurationSettings.AppSettings("ConnectionString"))
            DataGrid2.DataBind()
            'BindDG()

            'DataGrid1.DataBind()

        Catch ex As Exception
            lblMsgErr.Visible = True
            lblMsgErr.Text = ex.Message
        End Try
    End Sub

    Private Sub DataGrid2_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid2.CancelCommand
        DataGrid2.EditItemIndex = -1
        BindDGcompetencesEleves()
    End Sub

    Private Sub DataGrid1_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.CancelCommand
        DataGrid1.EditItemIndex = -1
        BindDG()
    End Sub
    Sub BindDiplome()
        Dim oProg As New PEIDAL.programme
        Dim oReader As SqlDataReader

        Try
            oReader = oProg.GetListDiplomes(ConfigurationSettings.AppSettings("ConnectionString"))
            LoadRadioButtonList(oReader, RadioButtonListDiplomes)

            oReader = oProg.GetListDiplomeByEleveId(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            SetRadioButtonList(RadioButtonListDiplomes, oReader)

        Catch ex As Exception
            lblMsgErr.Visible = True
            lblMsgErr.Text = ex.Message

        End Try

    End Sub
    Sub SaveCommentaireAndDiplome()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnEnregistrer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnregistrer.Click
        Dim oEleve As PEIDAL.eleve = New PEIDAL.eleve
            'Dim dsEleve1 As New PEIDAL.DsEleve
            'Dim r As PEIDAL.DsEleve.ELEVESRow

        Try
                'dsEleve1 = oEleve.GetDsEleve(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

                'If dsEleve1.ELEVES.Rows.Count > 0 Then
                '    r = dsEleve1.ELEVES.Rows(0)

                '        r.Exemp_prog_scol_1 = DropDownListExemp_prog_1.SelectedValue
                '        r.Exemp_prog_scol_2 = txtExemp_Prog_Scol_2.Text

                '    oEleve.UpdateDsEleve(dsEleve1)

                'End If

                Dim _eleveid As String = Session("eleveid")
                Dim _exemp_prog_Scol_2 As String
                _exemp_prog_Scol_2 = IIf(txtExemp_Prog_Scol_2.Text = "", " ", txtExemp_Prog_Scol_2.Text)
                _exemp_prog_Scol_2 = Replace(_exemp_prog_Scol_2, "'", "''")

                Dim _sql As String = "update eleves set exemp_prog_scol_1 = '" & DropDownListExemp_prog_1.SelectedValue & "', exemp_prog_Scol_2 = '" & _exemp_prog_Scol_2 & "' where pk_eleve_id = '" & _eleveid & "'"

                ExecuteCmd(ConfigurationSettings.AppSettings("ConnectionString"), _sql)


            Dim oProg As New PEIDAL.programme
            Dim rd As New RadioButton
            Dim li As ListItem

            oProg.DeleteDiplomesElevesByEleveId(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))
            For Each li In RadioButtonListDiplomes.Items
                If li.Selected Then
                    oProg.InsertListDiplomesElevesByEleveId(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"), li.Value)
                End If
            Next

        Catch ex As Exception
            lblMsgErr.Visible = True
            lblMsgErr.Text = "Erreur lors de l'enregistrement des commentaires de l'�l�ve et/ou Dipl�me.."

        End Try
    End Sub
End Class

End Namespace
