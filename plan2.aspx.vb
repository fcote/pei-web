﻿Imports System.Data
Imports System.Drawing.Color
Namespace PeiWebAccess

    Partial Class plan2
        Inherits System.Web.UI.Page

        Private _update As Boolean = False
        Private but As String = ""
        Private currentBut As String = ""
        'Public _eleveid As String = Session("eleveid")


        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

            Me.lblMsgErr.Text = ""

            If Not (Session("eleveid")) <> "" Then
                Response.Redirect("listeeleves.aspx")
            End If

            If Not Page.IsPostBack Then
                'bind drop down list categorie
                'BindDropDownListCategorie(DropDownListCategoriesTransition, "PLAN")
                BindDDLtransition()

                setDropDownListButs(Me.DropDownListCategoriesTransition.SelectedValue)

                BindDDLMesureByButid(DropDownListButs.SelectedValue)
                'LoadButTransitionEleve(Session("eleveid"))

                BindGridviewMesure(Session("eleveid"))
                bindGrid1(Session("eleveid"))

            End If

            If _update Then
                BindGridviewMesure(Session("eleveid"))
            End If

            'CreateJavaScriptConfirmationDelete()
            CreateJavaScriptConfirmationDeleteAttente()
        End Sub
        Sub BindDDLtransition()
            Dim sql As String = "select * from transitions order by transition_id"
            Dim ds As DataSet
            ds = GetDataset(sql)

            Me.DropDownListCategoriesTransition.DataSource = ds
            Me.DropDownListCategoriesTransition.DataTextField = "transition"
            Me.DropDownListCategoriesTransition.DataValueField = "transition_id"
            Me.DropDownListCategoriesTransition.DataBind()

        End Sub
        Sub BindDDLMesureByButid(ByVal butid As Integer)
            Dim sql As String = "select * from mesures_transition where but_transition_id = " & butid
            Dim _ds As DataSet
            _ds = GetDataset(sql)

            With DropDownListMesures
                .DataSource = _ds
                .DataValueField = "mesure_id"
                .DataTextField = "mesure"
                .DataBind()
            End With

        End Sub
        Sub setDropDownListButs(ByVal transitionid As Integer)
            Dim _ds As DataSet
            Dim _sql As String = "select * from butstransition where transition_id = " & transitionid & " order by but_transition_id"
            '_ds = GetlstButsByCategorie()
            _ds = GetDataset(_sql)

            Me.DropDownListButs.DataSource = _ds
            Me.DropDownListButs.DataValueField = "but_transition_id"
            Me.DropDownListButs.DataTextField = "but_description"
            Me.DropDownListButs.DataBind()

        End Sub
        Sub bindGrid1(ByVal eleveid As String)
            Dim ds As DataSet
            Dim sql As String = "select b.but_description, p.row_id,p.NO_ORDER, t.transition, b.but_transition_id,  p.mesure_id,p.mesures, m.mesure, p.RESPONSABLES,p.ECHEANCIER " & _
                " from plan_intervention2 p, mesures_transition m, butstransition b, transitions t " & _
                " where p.mesure_id = m.mesure_id and m.but_transition_id = b.but_transition_id and t.transition_id = b.transition_id and " & _
                " p.fk_eleve_id = '" & eleveid & "'" & _
                " order by b.but_transition_id,p.NO_ORDER"

            sql = "select b.but_description, p.mesures, p.RESPONSABLES,p.ECHEANCIER, p.row_id,p.NO_ORDER, t.transition, b.but_transition_id,p.mesures, m.mesure " & _
                " from plan_intervention2 p, mesures_transition m, butstransition b, transitions t " & _
                " where p.mesure_id = m.mesure_id and m.but_transition_id = b.but_transition_id and t.transition_id = b.transition_id and " & _
                " p.fk_eleve_id = '" & eleveid & "'" & _
                " order by b.but_transition_id,p.NO_ORDER"
            ds = GetDataset(sql)

            Dim curCat As String 'The Current Category we are on
            Dim prevCat As String 'The category of the Previous Item
            Dim row As TableRow 'the New Row we need to Insert
            Dim i As Integer = 0 'An Index for specifying the Insertion Point.

            'Loop through the rows of the DataTable
            Do While i <= ds.Tables(0).Rows.Count - 1
                curCat = ds.Tables(0).Rows(i).Item("but_description") 'categoryName
                If curCat <> prevCat Then
                    prevCat = curCat
                    Dim shRow As DataRow = ds.Tables(0).NewRow
                    shRow("mesures") = ds.Tables(0).Rows(i).Item(0)
                    shRow("responsables") = "SubHead"
                    ds.Tables(0).Rows.InsertAt(shRow, i)

                    i += 1
                End If

                i += 1
            Loop

            Me.DataGrid1.DataSource = ds
            Me.DataGrid1.DataBind()

        End Sub
        Sub BindGridviewMesure(ByVal eleveid As String)
            Dim ds As New DataSet
            'Dim _sql As String = "select * from plan_intervention2 where fk_eleve_id = '" & eleveid & "'"
            Dim _sql As String = "select p.row_id,p.NO_ORDER, t.transition, b.but_transition_id, b.but_description, p.mesure_id,p.mesures, m.mesure, p.RESPONSABLES,p.ECHEANCIER " & _
                " from plan_intervention2 p, mesures_transition m, butstransition b, transitions t where " & _
                " p.mesure_id = m.mesure_id and " & _
                " m.but_transition_id = b.but_transition_id and " & _
                " t.transition_id = b.transition_id and p.fk_eleve_id = '" & eleveid & "'" & _
                " order by b.but_transition_id,p.NO_ORDER"

            ds = GetDataset(_sql)

            Me.DataGrid2.DataSource = ds
            Me.DataGrid2.DataBind()

            Me.DataGrid1.DataSource = ds
            Me.DataGrid1.DataBind()
        End Sub
        Public Function GetlstButsByCategorie() As Data.DataSet
            Try

                Dim oGen As New PEIDAL.general
                Dim dr As Data.SqlClient.SqlDataReader
                Dim ds As New DataSet
                Dim _sql As String
                _sql = "select  rtrim(cast(contenu as varchar(100))) as contenu, contenu as contenuValue from contenus where type_contenu='PLAN' and categorie = '" & Replace(DropDownListCategoriesTransition.SelectedValue, "'", "''") & "'"

                'dr = oGen.GetListContenuByTypeAndCategorie("PLAN", DropDownListCategorie.SelectedValue)
                'dr = GetSqlDataReader(_sql, ConfigurationSettings.AppSettings("ConnectionString"))
                Return oGen.GetDataset(_sql, ConfigurationSettings.AppSettings("ConnectionString"))

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message
            End Try

        End Function

        Protected Sub DropDownListCategories_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownListCategoriesTransition.SelectedIndexChanged
            setDropDownListButs(Me.DropDownListCategoriesTransition.SelectedValue)
            BindDDLMesureByButid(DropDownListButs.SelectedValue)
        End Sub

        Protected Sub ImageButtonAjouterBut_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAjouterBut.Click
            'InsertButTransition(Session("eleveid"), Me.DropDownListButs.Text)

            'Dim lstResponsable As String = ""
            'For Each Item As ListItem In Me.CheckBoxListResponsables.Items
            '    If Item.Selected Then
            '        If Not lstResponsable <> "" Then
            '            lstResponsable = Item.Text
            '        Else
            '            lstResponsable = lstResponsable & ", " & Item.Text
            '        End If

            '    End If
            'Next

            'InsertPlan2(Session("eleveid"), Me.DropDownListMesures.SelectedValue, GetMaxOrderMesure(Session("eleveid")), DropDownListMesures.SelectedItem.Text, lstResponsable, "")

            'BindGridviewMesure(Session("eleveid"))
            'CreateJavaScriptConfirmationDelete()

            'Me.DropDownListButsEleve.SelectedValue = Me.DropDownListButsEleve.i
            'Me.DropDownListButsEleve.SelectedValue = Me.DropDownListButsEleve.Items.Cast(listitem(
        End Sub

        'Sub InsertButTransition(fk_eleve_id As String, description As String)
        '    Dim _order As Integer = 1
        '    _order = GetMaxOrderBut(fk_eleve_id)
        '    Dim _description As String = Replace(description, "'", "''")
        '    Dim _sql As String = "insert into buts_transition2 (no_order,fk_eleve_id, description) values (" & _order & ",'" & fk_eleve_id & "','" & _description & "')"
        '    Dim oGen As New PEIDAL.general
        '    oGen.ExecuteCommand(ConfigurationSettings.AppSettings("ConnectionString"), _sql)
        '    ExecuteSQL(_sql)
        '    LoadButTransitionEleve(Session("eleveid"))

        'End Sub
        Function GetMaxOrderBut(ByVal fk_eleve_id As String) As Integer
            'Dim _sql As String = "select max(no_order) from buts_transition2 where fk_eleve_id = '" & fk_eleve_id & "'"
            Dim _sql As String = "select max(no_order) no_order" & _
            " from buts_transition2 where fk_eleve_id = '" & fk_eleve_id & "' and no_order in (Select max(b.no_order) from buts_transition2 b)"

            _sql = "select max(no_order) no_order" & _
            " from buts_transition2 where fk_eleve_id = '" & fk_eleve_id & "'"

            Dim oGen As New PEIDAL.general
            Dim _ds As New DataSet
            Dim _max As Integer = 1

            _ds = oGen.GetDataset(_sql, ConfigurationSettings.AppSettings("ConnectionString"))
            If _ds.Tables(0).Rows.Count > 0 Then
                '_max = CInt(_ds.Tables(0).Rows(0).Item("no_order").ToString) + 1
                If Not (String.IsNullOrEmpty(_ds.Tables(0).Rows(0).Item("no_order").ToString)) Then
                    _max = CInt(_ds.Tables(0).Rows(0).Item("no_order").ToString) + 1
                End If
            End If

            Return _max

        End Function
        Function GetMaxOrderMesure(ByVal eleveid As String) As Integer
            'Dim _sql As String = "select max(no_order) from buts_transition2 where fk_eleve_id = '" & fk_eleve_id & "'"
            'Dim _sql As String = "select max(no_order) no_order" & _
            '" from plan_intervention2 where fk_but_transition_id = " & but_transition & " and no_order in (Select max(b.no_order) from plan_intervention2 b where b.fk_but_transition_id = " & but_transition & ")"

            Dim _sql As String = "select max(no_order) no_order from plan_intervention2 where fk_eleve_id = '" & eleveid & "'"
            Dim oGen As New PEIDAL.general
            Dim _ds As New DataSet
            Dim _max As Integer = 1

            _ds = oGen.GetDataset(_sql, ConfigurationSettings.AppSettings("ConnectionString"))
            If _ds.Tables(0).Rows.Count > 0 Then
                If Not (String.IsNullOrEmpty(_ds.Tables(0).Rows(0).Item("no_order").ToString)) Then
                    _max = CInt(_ds.Tables(0).Rows(0).Item("no_order").ToString) + 1
                End If
            End If

            Return _max

        End Function
        Sub DeleteButTransition(ByVal buttransitionid As Integer, ByVal eleve_id As String)

            Dim _sql As String = "delete from buts_transition2 where fk_eleve_id = " & eleve_id & "' and buts_transition = " & buttransitionid
            'Dim oGen As New PEIDAL.general
            'oGen.ExecuteCommand(ConfigurationSettings.AppSettings("ConnectionString"), _sql)
            ExecuteSQL(_sql)
        End Sub
        Sub DeletePlan2(ByVal row_id As Integer)

            Dim _sql As String = "delete from plan_intervention2 where row_id = " & row_id
            'Dim oGen As New PEIDAL.general
            ExecuteSQL(_sql)
            'oGen.ExecuteCommand(ConfigurationSettings.AppSettings("ConnectionString"), _sql)
            BindGridviewMesure(Session("eleveid"))
        End Sub
        Sub InsertPlan2(ByVal fk_eleve_id As String, ByVal mesure_id As Integer, ByVal no_order As Integer, ByVal mesure As String, ByVal responsable As String, ByVal echeancier As String)
            Dim _maxorder As Integer
            Dim _mesure As String
            Dim _responsable As String
            Dim _echeancier As String

            _mesure = Replace(mesure, "'", "''")
            _responsable = Replace(responsable, "'", "''")
            _echeancier = Replace(echeancier, "'", "''")
            _maxorder = GetMaxOrderMesure(fk_eleve_id)
            'Dim _sql As String = "insert into plan_intervention2 (no_order,mesures,responsables,echeancier,fk_but_transition_id) values (" & _maxorder & ",'" & mesure & "','" & responsable & "','" & echeancier & "'," & fk_but_transition_id & ")"
            Dim _sql As String = "insert into plan_intervention2 (no_order,fk_eleve_id,mesure_id,mesures,responsables,echeancier) values (" & _maxorder & ",'" & fk_eleve_id & "'," & mesure_id & ",'" & _mesure & "','" & _responsable & "','" & _echeancier & "'" & ")"
            ExecuteSQL(_sql)
        End Sub
        Sub UpdatePlan2(ByVal rowid As Integer, ByVal mesure As String, ByVal responsable As String, ByVal echeancier As String)
            Dim _sql As String = "update plan_intervention2 set mesures = '" & mesure & "',responsables = '" & responsable & "', echeancier = '" & echeancier & "' where row_id = " & rowid
            ExecuteSQL(_sql)
        End Sub

        Sub ExecuteSQL(ByVal sql As String)
            Dim _sql As String = sql
            Dim oGen As New PEIDAL.general

            Try
                oGen.ExecuteCommand(ConfigurationSettings.AppSettings("ConnectionString"), _sql)
            Catch ex As Exception
                Me.lblMsgErr.Text = "Une erreur est survenue: " & ex.Message
            End Try

        End Sub


        Sub UpdateMesure(ByVal rowid As Integer, ByVal mesure As String, ByVal echeancier As String, ByVal responsables As String)
            Dim _mesure As String
            Dim _responsable As String
            Dim _echeancier As String

            _mesure = Replace(mesure, "'", "''")
            _responsable = Replace(responsables, "'", "''")
            _echeancier = Replace(echeancier, "'", "''")

            Dim _sql As String
            _sql = "update plan_intervention2 set mesures = '" & _mesure & "', echeancier = '" & _echeancier & "', responsables = '" & _responsable & "' where row_id = " & rowid
            ExecuteSQL(_sql)

        End Sub


        'Sub DisplayDetailsMesures(ByVal rowid As Integer)
        '    Dim _ds As New DataSet
        '    Dim _sql As String = " select * from plan_intervention2 where row_id = " & rowid
        '    Dim oGen As New PEIDAL.general
        '    _ds = oGen.GetDataset(_sql, ConfigurationSettings.AppSettings("ConnectionString"))

        '    If _ds.Tables(0).Rows.Count = 1 Then
        '        Me.TextBoxMesure.Text = _ds.Tables(0).Rows(0).Item("mesures").ToString
        '        Me.TextBoxResponsable.Text = _ds.Tables(0).Rows(0).Item("responsables").ToString
        '        Me.TextBoxEcheancier.Text = _ds.Tables(0).Rows(0).Item("echeancier").ToString
        '    End If
        'End Sub



        'Protected Sub DropDownListMesureByBut_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownListMesureByBut.SelectedIndexChanged
        '    DisplayDetailsMesures(Me.DropDownListMesureByBut.SelectedValue)
        'End Sub

        Protected Sub DropDownListButs_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownListButs.SelectedIndexChanged
            BindDDLMesureByButid(DropDownListButs.SelectedValue)
        End Sub














        Sub CreateJavaScriptConfirmationDeleteAttente()
            'Dim empID As String = ""

            'If DropDownListMesureByBut.Items.Count > 0 Then
            '    empID = DropDownListMesureByBut.SelectedItem.Text
            '    '// Use a StringBuilder to append strings
            '    Dim sb As System.Text.StringBuilder = New System.Text.StringBuilder
            '    'System.Text.StringBuilder sb = new System.Text.StringBuilder();
            '    'sb.Append ("<script language=JavaScript> ");
            '    sb.Append("<script language=JavaScript> ")
            '    sb.Append("function ConfirmDeletionAttente() {")
            '    'sb.Append("return confirm('Etes-vous sur de vouloir supprimer le but suivant: " + empID + "');}")
            '    sb.Append("return confirm('Etes-vous sur de vouloir supprimer la mesure sélectionnée ?');}")
            '    sb.Append("</script>")
            '    'string js = sb.ToString();
            '    Dim js As String = sb.ToString
            '    'If (!IsClientScriptBlockRegistered("ConfirmDeletion")) Then
            '    If (Not IsClientScriptBlockRegistered("clientScript")) Then
            '        RegisterClientScriptBlock("ConfirmDeletionAttente", js)
            '    End If

            '    ImageButtonDeleteMesure.Attributes.Add("onClick", "return ConfirmDeletionAttente();")
            'End If

        End Sub

        Protected Sub DataGrid2_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid2.CancelCommand
            DataGrid2.EditItemIndex = -1
            BindGridviewMesure(Session("eleveid"))
        End Sub

        Protected Sub DataGrid2_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid2.DeleteCommand
            Dim Key As Integer
            Dim oPlan As New PEIDAL.plan
            Dim _sql As String

            Try
                Key = DataGrid2.DataKeys(e.Item.ItemIndex).ToString
                _sql = "delete from plan_intervention2 where row_id = " & CInt(Key)
                ExecuteSQL(_sql)

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message

            End Try

            BindGridviewMesure(Session("eleveid"))
        End Sub

        Protected Sub DataGrid2_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid2.EditCommand
            DataGrid2.EditItemIndex = e.Item.ItemIndex
            BindGridviewMesure(Session("eleveid"))
        End Sub

        Protected Sub DataGrid2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DataGrid2.ItemDataBound

            Select Case e.Item.ItemType
                Case ListItemType.AlternatingItem, ListItemType.Item 'Check the Row Type

                    'test--------
                    currentBut = CType(e.Item.FindControl("lblbut_description"), Label).Text
                    If but <> currentBut Then

                        e.Item.Cells(0).BackColor = System.Drawing.Color.AliceBlue

                        'Format the data, and then align the text of each cell to the left.

                        'Set the cell to a ColSpan of 3
                        'e.Item.Cells(2).ColumnSpan = 5

                        'Remove the two right cells

                        'e.Item.Cells.RemoveAt(3)


                        '---------------
                        'fcote
                        'e.Item.Cells(0).Text = "le but...."
                        'e.Item.Font.Size = 12
                        'e.Item.Cells(0).Visible = False
                        'e.Item.Cells(1).Visible = False
                        '--------------

                        'Align the cell to the left, make its text bold, and set
                        'its background color
                        'e.Item.Cells(0).Attributes.Add("align", "Left")
                        'e.Item.Cells(0).Font.Bold = True
                        'e.Item.BackColor = Color.FromArgb(204, 204, 255)
                        'e.Item.BackColor = System.Drawing.Color.AliceBlue

                    Else
                        e.Item.Cells(0).Text = ""
                    End If

                    but = currentBut


            End Select
        End Sub

        Protected Sub DataGrid2_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid2.UpdateCommand
            Dim TypeEval, Sommaire As String
            Dim ButTransition As String
            Dim DateEval As Date
            Dim Test As String
            Dim Mesures, Responsables, Echeancier As String
            Dim key As String = DataGrid2.DataKeys(e.Item.ItemIndex).ToString()

            ' The first column -- Cells(0) -- contains the Update and Cancel buttons.
            Dim tb As TextBox
            Dim temp As String
            Dim _sql As String
            Try

                ' Gets the value the TextBox control in the third column
                'strButDescription = CType(e.Item.FindControl("textboxDescription"), TextBox).Text
                Mesures = CType(e.Item.FindControl("txtMesures"), TextBox).Text
                Mesures = Replace(Mesures, "'", "''")

                Responsables = CType(e.Item.FindControl("txtResponsables"), TextBox).Text
                Responsables = Replace(Responsables, "'", "''")

                Echeancier = CType(e.Item.FindControl("txtEcheancier"), TextBox).Text
                Echeancier = Replace(Echeancier, "'", "''")

                ' do update here
                UpdatePlan2(CInt(key), Mesures, Responsables, Echeancier)
                '' Takes the DataGrid row out of editing mode
                DataGrid2.EditItemIndex = -1

                '' Refreshes the grid
                BindGridviewMesure(Session("eleveid"))

            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message

            End Try
        End Sub

        Protected Sub DataGrid1_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.CancelCommand
            DataGrid1.EditItemIndex = -1
            BindGridviewMesure(Session("eleveid"))
        End Sub

        Protected Sub DataGrid1_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.EditCommand
            DataGrid1.EditItemIndex = e.Item.ItemIndex
            BindGridviewMesure(Session("eleveid"))
        End Sub

        Protected Sub DataGrid1_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.ItemCommand

        End Sub

        Protected Sub DataGrid1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DataGrid1.ItemDataBound
            Dim Responsables As String = ""
            Select Case e.Item.ItemType
                Case ListItemType.AlternatingItem, ListItemType.Item 'Check the Row Type

                    'test--------
                    Responsables = CType(e.Item.FindControl("lblResonsables0"), Label).Text
                    If Not Responsables <> "SubHead" Then
                        'Format the data, and then align the text of each cell to the left.

                        'Set the cell to a ColSpan of 3
                        e.Item.Cells(2).ColumnSpan = 5

                        'Remove the two right cells
                        'e.Item.Cells.RemoveAt(4)
                        e.Item.Cells.RemoveAt(3)
                        'e.Item.Cells.RemoveAt(2)
                        'e.Item.Cells.RemoveAt(1)

                        '---------------
                        'fcote
                        'e.Item.Cells(0).Text = "le but...."
                        e.Item.Font.Size = 12
                        e.Item.Cells(0).Visible = False
                        e.Item.Cells(1).Visible = False
                        '--------------

                        'Align the cell to the left, make its text bold, and set
                        'its background color
                        e.Item.Cells(0).Attributes.Add("align", "Left")
                        e.Item.Cells(0).Font.Bold = True
                        'e.Item.BackColor = Color.FromArgb(204, 204, 255)
                        e.Item.BackColor = System.Drawing.Color.AliceBlue

                    End If
                    '------------------------
                    ''See if we have a Subheader!
                    'If e.Item.Cells(1).Text.Equals("SubHead") Then
                    '    'Format the data, and then align the text of each cell to the left.

                    '    'Set the cell to a ColSpan of 3
                    '    e.Item.Cells(0).ColumnSpan = 3

                    '    'Remove the two right cells
                    '    e.Item.Cells.RemoveAt(2)
                    '    e.Item.Cells.RemoveAt(1)

                    '    'Align the cell to the left, make its text bold, and set
                    '    'its background color
                    '    e.Item.Cells(0).Attributes.Add("align", "Left")
                    '    e.Item.Cells(0).Font.Bold = True
                    '    'e.Item.BackColor = Color.FromArgb(204, 204, 255)
                    '    e.Item.BackColor = System.Drawing.Color.Beige
                    'End If
            End Select
        End Sub

        Protected Sub DataGrid1_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.UpdateCommand
            Dim TypeEval, Sommaire As String
            Dim ButTransition As String
            Dim DateEval As Date
            Dim Test As String
            Dim Mesures, Responsables, Echeancier As String
            Dim key As String = DataGrid1.DataKeys(e.Item.ItemIndex).ToString()

            ' The first column -- Cells(0) -- contains the Update and Cancel buttons.
            Dim tb As TextBox
            Dim temp As String
            Dim _sql As String
            Try

                ' Gets the value the TextBox control in the third column
                'strButDescription = CType(e.Item.FindControl("textboxDescription"), TextBox).Text
                Mesures = CType(e.Item.FindControl("txtMesures0"), TextBox).Text
                Mesures = Replace(Mesures, "'", "''")

                Responsables = CType(e.Item.FindControl("txtResponsables0"), TextBox).Text
                Responsables = Replace(Responsables, "'", "''")

                Echeancier = CType(e.Item.FindControl("txtEcheancier0"), TextBox).Text
                Echeancier = Replace(Echeancier, "'", "''")

                ' do update here
                UpdatePlan2(CInt(key), Mesures, Responsables, Echeancier)
                '' Takes the DataGrid row out of editing mode
                DataGrid1.EditItemIndex = -1

                'Page.Response.Redirect("plan2.aspx")

                '-----------------------------------------------
                '' Refreshes the grid
                BindGridviewMesure(Session("eleveid"))

                _update = True
                'Dim ds As New DataSet
                '_sql = "select * from plan_intervention2 where fk_eleve_id = '" & Session("eleveid") & "'"
                'ds = GetDataset(_sql)
                'Me.DataGrid1.DataSource = ds
                'Me.DataGrid1.DataBind()
                '--------------------------------------------------------


            Catch ex As Exception
                lblMsgErr.Visible = True
                lblMsgErr.Text = ex.Message

            End Try
        End Sub

        Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
            'BindGridviewMesure(Session("eleveid"))
        End Sub

        Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
            Dim lstResponsable As String = ""
            For Each Item As ListItem In Me.CheckBoxListResponsables.Items
                If Item.Selected Then
                    If Not lstResponsable <> "" Then
                        lstResponsable = Item.Text
                    Else
                        lstResponsable = lstResponsable & ", " & Item.Text
                    End If

                End If
            Next

            Dim lstEcheancier As String = ""
            For Each Item As ListItem In Me.CheckBoxListEcheancier.Items
                If Item.Selected Then
                    If Not lstEcheancier <> "" Then
                        lstEcheancier = Item.Text
                    Else
                        lstEcheancier = lstEcheancier & ", " & Item.Text
                    End If

                End If
            Next

            'lstResponsable = Replace(lstResponsable, "'", "''")
            'lstEcheancier = Replace(lstEcheancier, "'", "''")

            InsertPlan2(Session("eleveid"), Me.DropDownListMesures.SelectedValue, GetMaxOrderMesure(Session("eleveid")), DropDownListMesures.SelectedItem.Text, lstResponsable, lstEcheancier)

            BindGridviewMesure(Session("eleveid"))
        End Sub
    End Class
End Namespace
