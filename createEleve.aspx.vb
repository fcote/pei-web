Imports System
Imports System.Web
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient


Namespace PeiWebAccess

Partial Class createEleve
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            Me.errMsg.Text = ""

        If Not Page.IsPostBack Then
            'BindDropDownListEcoleByUserID(Session("username"), DropDownListEcole)
                'BindDropDownListTrillSchoolCode()
                BindDropDownListEcoleByUserID(Page.User.Identity.Name, DropDownListEcole, "school_code")
        End If

    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim oEleve As New PEIDAL.eleve
            Dim dsPersonTrill As New DataSet

            Dim _school_year As String = ""
            _school_year = ConfigurationSettings.AppSettings("school_year").ToString

            Try



                'dsPersonTrill = oEleve.GetListElevesTrillOLEDB(ConfigurationSettings.AppSettings("ConnectionStringTrillium"), DropDownListEcole.SelectedValue, TextBoxPrenom.Text, TextBoxNom.Text, DropDownListAnneeScolaire.SelectedValue, Date.MinValue, "", "")

                'new
                'dsPersonTrill = oEleve.GetListElevesTrillOLEDB(ConfigurationSettings.AppSettings("ConnectionStringTrillium"), DropDownListEcole.SelectedValue, TextBoxPrenom.Text, TextBoxNom.Text, _school_year, Date.MinValue, "", "")


         
                Dim _sql As String = "select STD_OID PERSON_ID,'' AS STUDENT_NO, LEGAL_FIRST_NAME,LEGAL_SURNAME,PSN_DOB AS BIRTH_DATE, " & _
                    " STD_GRADE_LEVEL AS GRADE, PSN_GENDER_CODE AS GENDER, SCHOOL_CODE" & _
                    " from VW_ASPEN_ELEVES_ELE v" & _
                    " where SCHOOL_CODE = '" & DropDownListEcole.SelectedValue & "'" & _
                    " and upper(STD_NAME_VIEW) LIKE '%" & TextBoxPrenom.Text.ToUpper & "%'"

                dsPersonTrill = GetDatasetMonAvenir(_sql)

                DataGridListeEleves.DataSource = dsPersonTrill
                DataGridListeEleves.DataBind()

            Catch ex As Exception
                errMsg.Text = "Erreur: " & ex.Message

            End Try


    End Sub
    Sub BindDropDownListTrillSchoolCode()
        Dim oEcole As New PEIDAL.ecole
        Dim oEc As New PEIDAL.ecole

        'Dim oReader As OracleClient.OracleDataReader
        Dim oReader As SqlDataReader
            Dim strUsername As String = Session("username")

            Dim tbEcoles As New Data.DataTable
            Dim dcEcoleId = New DataColumn("school_code", GetType(String))
            Dim dcFullName = New DataColumn("nom_ecole", GetType(String))
            tbEcoles.Columns.Add(dcEcoleId)
            tbEcoles.Columns.Add(dcFullName)

        Try

                'oReader = oEc.GetListSchoolCodeTrillFromDBpei(ConfigurationSettings.AppSettings("connectionString"), strUsername)

                oReader = oEc.GetListSchoolCodeTrillFromDBpei(ConfigurationSettings.AppSettings("connectionString"), Page.User.Identity.Name)



                If oReader.HasRows Then
                    Do While oReader.Read
                        'tbEcoles.Rows.Add(oReader.Item("school_code").ToString, oReader.Item("nom_ecole").ToString & " (" & oReader.Item("ville").ToString & ")")
                        'tbEcoles.Rows.Add(oReader.GetString(0), oReader.GetString(1))
                        tbEcoles.Rows.Add(oReader.Item("school_code"), oReader.Item("nom_ecole") & "(" & oReader.Item("school_code") & ")")
                    Loop
                Else
                    errMsg.Text = "Aucune �cole disponible"
                End If

                DropDownListEcole.DataSource = tbEcoles
                DropDownListEcole.DataTextField = "nom_ecole"
                DropDownListEcole.DataValueField = "school_code"
                DropDownListEcole.DataBind()

            Catch ex As Exception
                errMsg.Text = "Erreur liste d�roulante �cole. " & ex.Message
            End Try
        

        End Sub
        'Sub BindDropDownListTrillSchoolCode2()
        '    Dim oEcole As New PEIDAL.ecole

        '    Dim _ds As New DataSet
        '    Dim tbEcoles As New Data.DataTable
        '    Dim dcEcoleId = New DataColumn("pk_ecole_id", GetType(String))
        '    Dim dcFullName = New DataColumn("nom_ecole", GetType(String))
        '    tbEcoles.Columns.Add(dcEcoleId)
        '    tbEcoles.Columns.Add(dcFullName)

        '    _ds = oEcole.GetDsListEcole(ConfigurationSettings.AppSettings("ConnectionString"))

        '    Dim _fullname As String = ""
        '    For Each item In _ds.Tables(0).Rows
        '        '    'Dim row As Data.DataRow
        '        If Not item("fk_conseil_id") <> conseilId Then
        '            _fullname = item("nom_ecole") & " (" & item("ville").ToString & ")"
        '            tbEcoles.Rows.Add(item("pk_ecole_id").ToString, _fullname)
        '        End If
        '    Next

        '    Try


        '        DropDownListEcole.DataSource = tbEcoles
        '        DropDownListEcole.DataValueField = "school_code"
        '        DropDownListEcole.DataTextField = "nom_ecole"
        '        DropDownListEcole.DataBind()

        '    Catch ex As Exception
        '        'lblMsgErr.Text = ex.Message

        '    End Try
        'End Sub

        Private Sub DataGridListeEleves_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridListeEleves.SelectedIndexChanged
            Dim Person_Id As String = DataGridListeEleves.DataKeys(DataGridListeEleves.SelectedIndex)
            'Session("eleveid") = Person_Id
            Dim ds As New DataSet
            Dim _ecole_id As String
            Dim _nom_ecole As String
            Dim _pk_eleve_id As String

            Dim oEleve As New PEIDAL.eleve

            'ds = oEleve.GetEleveByPersonID(ConfigurationSettings.AppSettings("ConnectionString"), Person_Id)
            Dim _sql As String = "select * from eleves where STD_OID = '" & Person_Id & "'"
            ds = GetDataset(_sql)

            '_pk_eleve_id = ds.Tables(0).Rows(0).Item("pk_eleve_id").ToString

            ' si �l�ve non pr�sent dans BD, on cr�er l'�l�ve
            If ds.Tables(0).Rows.Count < 1 Then
                'CreateEleve()
                CreateEleve2()
                'InsertEleve(ds.Tables(0).Rows(0))
                CreateDates(ConfigurationSettings.AppSettings("ConnectionString"), DataGridListeEleves.SelectedItem.Cells(1).Text)

                Response.Redirect("general.aspx")

            Else
                _ecole_id = ds.Tables(0).Rows(0).Item("fk_ecole_id").ToString
                _pk_eleve_id = ds.Tables(0).Rows(0).Item("pk_eleve_id").ToString

                ' si �leve nest pas affect� a �cole temp, donc d�ja assign� a une �cole
                If _ecole_id <> "9999" Then

                    _nom_ecole = ds.Tables(0).Rows(0).Item("nom_ecole").ToString
                    errMsg.Text = "L'�l�ve a d�ja un pei � l'�cole " & _nom_ecole & " (person id:" & Person_Id & ")"

                    ' si non, on UPDATE l'�l�ve avec le bon fk_ecole_id
                Else
                    Dim _FK_ECOLE_ID As String
                    _FK_ECOLE_ID = GetFK_ECOLE_ID_bySchoolCode(DataGridListeEleves.SelectedItem.Cells(6).Text)
                    oEleve.UpdateEleveByOneField(ConfigurationSettings.AppSettings("ConnectionString"), _pk_eleve_id, "FK_ECOLE_ID", _FK_ECOLE_ID)

                    Response.Redirect("general.aspx")

                End If

            End If

        End Sub
        Sub CreateEleve()
            Dim oEleve As New PEIDAL.eleve
            Dim dsEleve1 As New PEIDAL.dsEleve
            Dim r As PEIDAL.dsEleve

            Dim dr As DataRow = dsEleve1.ELEVES.NewRow
            Dim NIV As String = ""

            Try
                dr("sexe") = Trim(DataGridListeEleves.SelectedItem.Cells(8).Text)
                dr("prenom_eleve") = DataGridListeEleves.SelectedItem.Cells(3).Text
                dr("nom_eleve") = DataGridListeEleves.SelectedItem.Cells(4).Text
                dr("titulaire") = ""
                dr("date_naissance") = DataGridListeEleves.SelectedItem.Cells(5).Text
                'dr("fk_ecole_id") = DropDownListEcoles.SelectedValue
                'dr("fk_ecole_foyer_id") = DropDownListEcoles.SelectedValue
                dr("identifie_caracteristiques") = " "
                dr("identifie") = 0
                dr("exemp_prog_scol_1") = "NON"
                'dr("EvaluationsTestsProv") = ""

                'cr�er un dropdownlist pour le niveau de l'�l�ve
                ' dr("fk_niveau_id") = DataGridListeEleves.SelectedItem.Cells(7).Text
                'dr("fk_niveau_id") = 1

                Select Case DataGridListeEleves.SelectedItem.Cells(7).Text

                    Case "JK"
                        NIV = "Maternelle"
                    Case "SK"
                        NIV = "Jardin"
                    Case "01"
                        NIV = "1"
                    Case "02"
                        NIV = "2"
                    Case "03"
                        NIV = "3"
                    Case "04"
                        NIV = "4"
                    Case "05"
                        NIV = "5"
                    Case "06"
                        NIV = "6"
                    Case "07"
                        NIV = "7"
                    Case "08"
                        NIV = "8"
                    Case "09"
                        NIV = "9"
                    Case "10"
                        NIV = "10"
                    Case "11"
                        NIV = "11"
                    Case "12"
                        NIV = "12"
                    Case Else
                        Throw New Exception("Erreur: niveau �tude non stantard")


                End Select

                dr("fk_niveau_id") = Trim(NIV)

                Dim strTemp As String

                'strTemp = DataGridListeEleves.SelectedItem.Cells(0).Text

                'dr("fk_ecole_id") = GetFK_ECOLE_ID_bySchoolCode("121")
                dr("fk_ecole_id") = GetFK_ECOLE_ID_bySchoolCode(DataGridListeEleves.SelectedItem.Cells(6).Text)
                '            DataGridListeEleves.SelectedItem.Cells(3)

                dr("pk_eleve_id") = DataGridListeEleves.SelectedItem.Cells(1).Text
                dr("fk_person_id") = DataGridListeEleves.SelectedItem.Cells(1).Text
                dr("std_oid") = DataGridListeEleves.SelectedItem.Cells(1).Text

                Session("eleveid") = dr("pk_eleve_id")

                dsEleve1.ELEVES.Rows.InsertAt(dr, 0)

                oEleve.UpdateDsEleve(dsEleve1)

            Catch ex As Exception
                errMsg.Text = "Erreur lors de la cr�ation du PEI. DETAIL: " & ex.Message
                errMsg.Visible = True
            End Try


        End Sub
        Sub CreateEleve2()

            Dim _sexe As String = Trim(DataGridListeEleves.SelectedItem.Cells(8).Text)
            Dim _prenom_eleve As String = Replace(DataGridListeEleves.SelectedItem.Cells(3).Text, "'", "''")
            Dim _nom_eleve As String = Replace(DataGridListeEleves.SelectedItem.Cells(4).Text, "'", "''")
            Dim _titulaire As String = ""
            Dim _date_naissance As String = DataGridListeEleves.SelectedItem.Cells(5).Text
            'dr("fk_ecole_id") = DropDownListEcoles.SelectedValue
            'dr("fk_ecole_foyer_id") = DropDownListEcoles.SelectedValue
            Dim _identifie_caracteristiques As String = " "
            Dim _identifie As Integer = 0
            Dim _exemp_prog_scol_1 As String = "NON"
            Dim NIV As String = ""

            Try
                Select Case DataGridListeEleves.SelectedItem.Cells(7).Text

                    Case "JK"
                        NIV = "Maternelle"
                    Case "SK"
                        NIV = "Jardin"
                    Case "01"
                        NIV = "1"
                    Case "02"
                        NIV = "2"
                    Case "03"
                        NIV = "3"
                    Case "04"
                        NIV = "4"
                    Case "05"
                        NIV = "5"
                    Case "06"
                        NIV = "6"
                    Case "07"
                        NIV = "7"
                    Case "08"
                        NIV = "8"
                    Case "09"
                        NIV = "9"
                    Case "10"
                        NIV = "10"
                    Case "11"
                        NIV = "11"
                    Case "12"
                        NIV = "12"
                    Case Else
                        Throw New Exception("Erreur: niveau �tude non stantard")


                End Select

                Dim _fk_niveau_id As String = Trim(NIV)

                Dim strTemp As String

               
                Dim _fk_ecole_id As String = GetFK_ECOLE_ID_bySchoolCode(DataGridListeEleves.SelectedItem.Cells(6).Text)
                '            DataGridListeEleves.SelectedItem.Cells(3)

                'Dim _pk_eleve_id As String = DataGridListeEleves.SelectedItem.Cells(1).Text
                'Dim _fk_person_id As String = DataGridListeEleves.SelectedItem.Cells(1).Text
                'Dim _std_oid As String = DataGridListeEleves.SelectedItem.Cells(1).Text
                Dim _id As String = DataGridListeEleves.SelectedItem.Cells(1).Text

                Session("eleveid") = _id

                'Dim iString As String = _date_naissance
                ''Dim oDate As DateTime = DateTime.ParseExact(iString, "yyyy-MM-dd HH:mm tt", Nothing)
                'Dim _oDateNaissane As DateTime = DateTime.ParseExact(iString, "yyyy-MM-dd", Nothing)

                Dim iDate As String = _date_naissance
                Dim _oDateNaissane As DateTime = Convert.ToDateTime(iDate)

                Dim _sql As String
                Dim _ConnString As String = ConfigurationSettings.AppSettings("ConnectionString")
                Dim objConn As New SqlConnection(_ConnString)
                Dim oCmd As New SqlCommand


                ' insert eleve into db
                _sql = "INSERT INTO ELEVES (pk_eleve_id,std_oid, fk_ecole_id, prenom_eleve,nom_eleve,date_naissance,fk_niveau_id,sexe,identifie_caracteristiques,identifie,exemp_prog_scol_1) VALUES (" & _
             "'" & _id & "','" & _id & "','" & _fk_ecole_id & "','" & _prenom_eleve & "','" & _nom_eleve & "','" & _oDateNaissane & "','" & _fk_niveau_id & "','" & _sexe & "','" & _identifie & "','" & _identifie_caracteristiques & "','" & _exemp_prog_scol_1 & "')"

                objConn.Open()
                oCmd.Connection = objConn
                oCmd.CommandText = _sql
                oCmd.ExecuteNonQuery()

            Catch ex As Exception
                errMsg.Text = "Erreur lors de la cr�ation du PEI. DETAIL: " & ex.Message
                errMsg.Visible = True
            End Try


        End Sub
        Function GetFK_ECOLE_ID_bySchoolCode(ByVal School_Code As String) As String
            Dim oEcole As New PEIDAL.ecole
            Dim EcoleId As String

            Try
                'dsEcoles1 = oEcole.GetdsEcoles(ConfigurationSettings.AppSettings("ConnectionString"), "")
                EcoleId = oEcole.GetEcoleIdBySchoolCode(ConfigurationSettings.AppSettings("ConnectionString"), School_Code)
                'oEcole.gete()

                If EcoleId <> "" Then
                    GetFK_ECOLE_ID_bySchoolCode = EcoleId
                Else
                    Throw New Exception("EcoleId non trouv� pour school_code=" & School_Code)

                End If

            Catch ex As Exception
                Throw New Exception(ex.Message)

            End Try

        End Function
        Private Sub DataGridListeEleves_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DataGridListeEleves.ItemDataBound
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='AliceBlue'")
                e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='white'")
            End If
        End Sub
    End Class
End Namespace
