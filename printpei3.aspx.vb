Imports System
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient


Namespace PeiWebAccess

    Partial Class printpei3
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            Me.lblErrMsg.Text = ""

            If Not Session("eleveid") <> "" Then
                Response.Redirect("listeeleves.aspx")
            End If

            'If Not Session("username") <> "" Then
            '    Response.Redirect("login.aspx")
            'End If

            'Me.user.InnerText = "Utilisateur en cours : " & Session("username")
            Me.user2.InnerText = Page.User.Identity.Name

            If Not Page.IsPostBack Then

                Dim ds As New DataSet
                Dim oEle As New PEIDAL.eleve

                ds = oEle.GetEleveByID(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

                'BindDropDownListEcoleByUserID(Session("username"), DropDownListEcoles)
                BindDropDownListEcoleByUserID(Page.User.Identity.Name, DropDownListEcoles)
                Me.DropDownListEcoles.SelectedValue = ds.Tables(0).Rows(0).Item("fk_ecole_id").ToString

                BindListeEleves(Me.DropDownListEcoles.SelectedValue)
                Me.DropDownListEleves.SelectedValue = ds.Tables(0).Rows(0).Item("pk_eleve_id").ToString

                SetCheckbox(Me.DropDownListEleves.SelectedValue)

                If (Session("etape")) <> "" Then
                    Me.DropDownListEtape.SelectedValue = Session("etape")
                End If

                'SetForceCheckBox()
                'setMatieresCheckbox()
                'SetAdaptationsCheckBox()
                'SetDonneesEvaluation()
                'SetRHcheckBox()
                'setButs_Transition()

            End If


        End Sub

        Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

            Dim _http As String
            _http = "http://ss-sql-01/ReportServer/?/CSDCCS/EED/PEI_CSDCCS_09&rs:Command=Render&rc:Parameters=false&rs:Command=Render&matieres=True&Etape=1&adaptations=True&evaluation_prov=True&forces=True&signature=True&evaluations=True&rc:toolbar=false&attentes=True&nom=Stephane                                           Balogh                                            &generales=True&rs:Format=PDF&rh=True&year=2015 - 2016&person_id=0001126156&competences=True&fk_eleve_id=0001126156&DDN=12/4/2003&buts_transition=True&curtime=2/3/2016 10:25:51 AM"
            Response.Redirect(_http)

            ''Dim strEleveId As String = Session("eleveid")
            'Dim strEleveId As String = Me.DropDownListEleves.SelectedValue
            'Dim strPersonID As String
            'Dim Etape As String
            'Dim DDN As String
            'Dim strNom As String
            'Dim oEleve As New PEIDAL.eleve
            'Dim ds As New DataSet
            'Dim Url As String

            'Dim year As String = System.Configuration.ConfigurationManager.AppSettings("annee_scolaire")

            'Etape = DropDownListEtape.SelectedValue

            'Try
            '    ds = oEleve.GetEleveByID(ConfigurationSettings.AppSettings("ConnectionString"), strEleveId)
            '    Session("infoseleve") = ds.Tables(0).Rows(0).Item("prenom_eleve") & " " & ds.Tables(0).Rows(0).Item("nom_eleve") & ",         " & ds.Tables(0).Rows(0).Item("date_naissance")
            '    strNom = ds.Tables(0).Rows(0).Item("prenom_eleve") & " " & ds.Tables(0).Rows(0).Item("nom_eleve")
            '    DDN = ds.Tables(0).Rows(0).Item("date_naissance")
            '    strPersonID = ds.Tables(0).Rows(0).Item("fk_person_id")

            'Catch ex As Exception
            '    lblErrMsg.Visible = True
            '    lblErrMsg.Text = ex.Message

            'End Try
            ''window.open('report2.aspx','mywindow','menubar=yes,scrollbars=yes')

            ''<script language=javascript>window.open('report2.aspx?fk_eleve_id=" & strEleveId & "&DDN=" & DDN & "&nom=" & strNom & "&rh=" & ReverseBooleanValue(rh.Checked) & "&forces=" & ReverseBooleanValue(forces.Checked) & "&attentes=" & ReverseBooleanValue(buts.Checked) & "&matieres=" & ReverseBooleanValue(matieres.Checked) & "&adaptations=" & ReverseBooleanValue(adaptations.Checked) & "&evaluations=" & ReverseBooleanValue(evaluations.Checked) & "&signature=" & ReverseBooleanValue(signature.Checked) & "&generales=" & ReverseBooleanValue(generales.Checked)','new_Win');</script>
            'Url = "<script language=javascript>window.open('"
            'Url = Url & "report3.aspx?fk_eleve_id=" & strEleveId & "&person_id=" & strPersonID & "&DDN=" & DDN & "&nom=" & strNom & "&rh=" & ReverseBooleanValue(rh.Checked) & "&forces=" & ReverseBooleanValue(forces.Checked) & "&attentes=" & ReverseBooleanValue(buts.Checked) & "&matieres=" & ReverseBooleanValue(matieres.Checked) & "&adaptations=" & ReverseBooleanValue(adaptations.Checked) & "&evaluations=" & ReverseBooleanValue(evaluations.Checked) & "&signature=" & ReverseBooleanValue(signature.Checked) & "&generales=" & ReverseBooleanValue(generales.Checked) & "&buts_transition=" & ReverseBooleanValue(buts_transition.Checked) & "&evaluation_prov=" & ReverseBooleanValue(evaluation_prov.Checked)
            'Url = Url & "','new_Win');</script>"

            ''Response.Write(Url)

            ''liens original
            ''Response.Redirect("report2.aspx?fk_eleve_id=" & strEleveId & "&person_id=" & strPersonID & "&Etape=" & Etape & "&DDN=" & DDN & "&nom=" & strNom & "&rh=" & ReverseBooleanValue(rh.Checked) & "&forces=" & ReverseBooleanValue(forces.Checked) & "&attentes=" & ReverseBooleanValue(buts.Checked) & "&matieres=" & ReverseBooleanValue(matieres.Checked) & "&adaptations=" & ReverseBooleanValue(adaptations.Checked) & "&evaluations=" & ReverseBooleanValue(evaluations.Checked) & "&signature=" & ReverseBooleanValue(signature.Checked) & "&generales=" & ReverseBooleanValue(generales.Checked) & "&buts_transition=" & ReverseBooleanValue(buts_transition.Checked) & "&evaluation_prov=" & ReverseBooleanValue(evaluation_prov.Checked))

            ''liens modifi�, ajouter parmat�tre datenow afin de g�n�rer un liens diff�rents a chaque fois et evitez la mise en cache...
            'Response.Redirect("report3.aspx?fk_eleve_id=" & strEleveId & _
            '"&person_id=" & strPersonID & _
            '"&Etape=" & Etape & _
            '"&DDN=" & DDN & _
            '"&nom=" & strNom & _
            '"&rh=" & ReverseBooleanValue(rh.Checked) & _
            '"&forces=" & ReverseBooleanValue(forces.Checked) & _
            '"&attentes=" & ReverseBooleanValue(buts.Checked) & _
            '"&matieres=" & ReverseBooleanValue(matieres.Checked) & _
            '"&competences=" & ReverseBooleanValue(competences.Checked) & _
            '"&adaptations=" & ReverseBooleanValue(adaptations.Checked) & _
            '"&evaluations=" & ReverseBooleanValue(evaluations.Checked) & _
            '"&signature=" & ReverseBooleanValue(signature.Checked) & _
            '"&generales=" & ReverseBooleanValue(generales.Checked) & _
            '"&buts_transition=" & ReverseBooleanValue(buts_transition.Checked) & _
            '"&evaluation_prov=" & ReverseBooleanValue(evaluation_prov.Checked) & _
            '"&year=" & year & _
            '"&currenttime=" & Date.Now)

            ''response.Write("<script language=javascript>window.open('report2.aspx?fk_eleve_id=" & strEleveId & "&DDN=" & DDN & "&nom=" & strNom & "&rh=" & ReverseBooleanValue(rh.Checked) & "&forces=" & ReverseBooleanValue(forces.Checked) & "&attentes=" & ReverseBooleanValue(buts.Checked) & "&matieres=" & ReverseBooleanValue(matieres.Checked) & "&adaptations=" & ReverseBooleanValue(adaptations.Checked) & "&evaluations=" & ReverseBooleanValue(evaluations.Checked) & "&signature=" & ReverseBooleanValue(signature.Checked) & "&generales=" & ReverseBooleanValue(generales.Checked)','new_Win');</script>")

            ''Response.Redirect("report2.aspx?fk_eleve_id=" & strEleveId & "&DDN=" & DDN & "&nom=" & strNom & "&rh=" & ReverseBooleanValue(rh.Checked) & "&forces=" & ReverseBooleanValue(forces.Checked) & "&attentes=" & ReverseBooleanValue(buts.Checked) & "&matieres=" & ReverseBooleanValue(matieres.Checked) & "&adaptations=" & ReverseBooleanValue(adaptations.Checked) & "&evaluations=" & ReverseBooleanValue(evaluations.Checked) & "&signature=" & ReverseBooleanValue(signature.Checked) & "&generales=" & ReverseBooleanValue(generales.Checked))

        End Sub
        Function ReverseBooleanValue(ByVal bValue As Boolean) As Boolean
            If bValue Then
                Return False
            Else
                Return True
            End If
        End Function

        Private Sub CheckboxAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckboxAll.CheckedChanged
            Dim Ctl As Control

            'If CheckboxAll.Checked Then
            '    For Each chBx In Page.Controls
            '        chBx.Checked = True
            '    Next
            'Else
            '    For Each Ctl In Page.Controls
            '        If Ctl.Then Then
            '            chBx.Checked = False
            '    Next
            'End If

            If CheckboxAll.Checked Then
                generales.Checked = True

                If forces.Enabled Then
                    forces.Checked = True
                End If

                If matieres.Enabled Then
                    matieres.Checked = True
                End If

                If competences.Enabled Then
                    competences.Checked = True
                End If

                If adaptations.Enabled Then
                    adaptations.Checked = True
                End If

                If evaluations.Enabled Then
                    evaluations.Checked = True
                End If

                If rh.Enabled Then
                    rh.Checked = True
                End If

                If buts_transition.Enabled Then
                    buts_transition.Checked = True
                End If

                If evaluation_prov.Enabled Then
                    evaluation_prov.Checked = True
                End If

                If buts.Enabled Then
                    buts.Checked = True
                End If

                
                If signature.Enabled Then
                    signature.Checked = True
                End If

            Else

                generales.Checked = False
                forces.Checked = False
                matieres.Checked = False
                competences.Checked = False
                adaptations.Checked = False
                evaluations.Checked = False
                rh.Checked = False
                buts_transition.Checked = False
                evaluation_prov.Checked = False
                buts.Checked = False
                signature.Checked = False

            End If

        End Sub
        Sub SetAllCheckBoxToFalse()
            generales.Checked = False
            forces.Checked = False
            matieres.Checked = False
            competences.Checked = False
            adaptations.Checked = False
            evaluations.Checked = False
            rh.Checked = False
            buts_transition.Checked = False
            evaluation_prov.Checked = False
            buts.Checked = False
            signature.Checked = False
        End Sub

        Sub setButsCheckBox(ByVal id As String)
            Dim oAtt As New PEIDAL.but
            Dim sqlReader As SqlDataReader
            Dim oFlag As Boolean
            Dim nBut As Integer

            Try
                nBut = oAtt.GetCountButByEleveID(id)
                If nBut > 0 Then
                    buts.Enabled = True
                Else
                    buts.Enabled = False
                End If
            Catch ex As Exception
                lblErrMsg.Visible = True
                lblErrMsg.Text = "Une erreur est survenue (case a coch� buts/attentes) " & ex.Message
            End Try

        End Sub

        Sub SetForceCheckBox(ByVal id As String)
            Dim oGen As New PEIDAL.general
            Dim _ds As New Data.DataSet
            Dim _sql As String

            _sql = "SELECT * from forcesbesoins where fk_eleve_id = '" & id & "'"
            _ds = oGen.GetDataset(_sql, ConfigurationSettings.AppSettings("ConnectionString"))

            forces.Enabled = False

            For Each r As DataRow In _ds.Tables(0).Rows

                If r.Item("HAB_COGNITIVE").ToString <> "" Then
                    forces.Enabled = True
                End If
            Next

        End Sub
        Sub setMatieresCheckbox(ByVal id As String)
            Dim oProg As New PEIDAL.programme
            Dim sqlReader As SqlDataReader

            Try
                sqlReader = oProg.GetReaderMatieres_ElevesOrderByDescRowID(id, ConfigurationSettings.AppSettings("ConnectionString"))

                If sqlReader.HasRows Then

                    matieres.Enabled = True

                    'Do While sqlReader.Read
                    '    If sqlReader.GetString(0).ToString <> "" Then
                    '        matieres.Enabled = False
                    '    Else
                    '        matieres.Enabled = True
                    '    End If
                    'Loop
                Else
                    matieres.Enabled = False
                End If

            Catch ex As Exception
                lblErrMsg.Text = "Erreur survenu sur l'objet 'Mati�res'. " & ex.Message

            End Try

        End Sub
        Sub SetAdaptationsCheckBox(ByVal id As String)
            Dim oSante As New PEIDAL.sante
            Dim dsSanteEleve1 As PEIDAL.DsSanteEleves
            Dim r As PEIDAL.DsSanteEleves.SANTE_ELEVESRow

            Try
                dsSanteEleve1 = oSante.GetdsSanteEleves(ConfigurationSettings.AppSettings("ConnectionString"), id)

                If dsSanteEleve1.SANTE_ELEVES.Rows.Count < 1 Then
                    adaptations.Enabled = False
                Else
                    r = dsSanteEleve1.SANTE_ELEVES.FindByFK_ELEVE_ID(id)
                    If r.ADAPT_ENVIRONNEMENTALES <> "" Or r.ADAPT_MATIEREEVALUATION <> "" Or r.ADAPT_PEDAGOGIQUES <> "" Then
                        adaptations.Enabled = True
                    Else
                        adaptations.Enabled = False
                    End If
                End If

            Catch ex As Exception
                lblErrMsg.Text = "Erreur survenu sur l'objet 'Adaptations'. " & ex.Message
            End Try

        End Sub
        Sub SetCompetences(ByVal id As String)
            Dim oProg As New PEIDAL.programme
            Dim ds As New DataSet
            ds = oProg.GetdsCompetences_Eleves(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

            If ds.Tables(0).Rows.Count > 0 Then
                competences.Enabled = True
            Else
                competences.Enabled = False
            End If

        End Sub
        Sub SetEval_Provinciaux(ByVal id As String)
            Dim oEvalProv As New PEIDAL.evalprov
            Dim dsTestProv1 As New PEIDAL.DsTestsProvinciaux

            dsTestProv1 = oEvalProv.GetdsTestsProvinciaux(ConfigurationSettings.AppSettings("ConnectionString"), Session("eleveid"))

            If (dsTestProv1.Tables(0).Rows.Count > 0) Then
                If ((dsTestProv1.TESTS_PROVINCIAUX.Rows.Count > 0) And (dsTestProv1.TESTS_PROVINCIAUX(0).ADAPTATIONS.ToString <> "" Or dsTestProv1.TESTS_PROVINCIAUX(0).EVALUATIONS <> "" Or dsTestProv1.TESTS_PROVINCIAUX(0).EXEMPTIONS <> "")) Then
                    evaluation_prov.Enabled = True
                Else
                    evaluation_prov.Enabled = False
                End If
            Else
                evaluation_prov.Enabled = False
            End If

            
        End Sub
        Sub SetDonneesEvaluation(ByVal id As String)
            Dim oFB As New PEIDAL.forces_besoins
            Dim dsDonneesEval1 As PEIDAL.dsRapportsEvaluationList

            Try
                dsDonneesEval1 = oFB.GetdsRapportsEvaluationList(ConfigurationSettings.AppSettings("ConnectionString"), id)
                If dsDonneesEval1.RAPPORTS_EVALUATION.Rows.Count < 1 Then
                    evaluations.Enabled = False
                Else
                    evaluations.Enabled = True
                End If
            Catch ex As Exception
                lblErrMsg.Text = "Erreur survenu sur l'objet '�valuations'. " & ex.Message
            End Try
        End Sub
        Sub SetRHcheckBox(ByVal id)
            Dim oSante As New PEIDAL.sante
            Dim dsRHEleves1 As PEIDAL.DsRhEleves
            Try
                dsRHEleves1 = oSante.GetdsRhEleves(ConfigurationSettings.AppSettings("ConnectionString"), id)

                If dsRHEleves1.RH_ELEVES.Count < 1 Then
                    rh.Enabled = False
                Else
                    rh.Enabled = True
                End If
            Catch ex As Exception
                lblErrMsg.Text = "Erreur survenu sur l'objet 'Ressources humaines'. " & ex.Message
            End Try
        End Sub
        Sub setButs_Transition(ByVal id)
            Dim oPlan As New PEIDAL.plan
            Dim DsButsTransition As PEIDAL.dsButsTransition
            Try
                DsButsTransition = oPlan.GetdsButsTransition(ConfigurationSettings.AppSettings("ConnectionString"), id)

                If DsButsTransition.BUTS_TRANSITION.Count < 1 Then
                    buts_transition.Enabled = False
                Else
                    buts_transition.Enabled = True
                End If
            Catch ex As Exception
                lblErrMsg.Text = "Erreur survenu sur l'objet 'Buts_Transition'. " & ex.Message
            End Try
        End Sub

        Private Sub DropDownListEtape_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DropDownListEtape.SelectedIndexChanged
            Session("etape") = Me.DropDownListEtape.SelectedValue
        End Sub
        Sub BindListeEleves(ByVal EcoleID As String)

            Dim oEleve As New PEIDAL.eleve
            Dim dr As SqlDataReader
            Dim ds As New DataSet
            Dim dt As New DataTable("ELEVES")
            Dim r As DataRow

            dt.Columns.Add("pk_eleve_id")
            dt.Columns.Add("fullname")

            Try
                dr = oEleve.GetListElevesByEcoleId(ConfigurationSettings.AppSettings("ConnectionString"), EcoleID)
                While dr.Read
                    r = dt.NewRow
                    r("pk_eleve_id") = dr.Item("pk_eleve_id")
                    r("fullname") = dr.Item("nom_eleve") & ", " & dr.Item("prenom_eleve") & " (" & dr.Item("description_niveau") & ") "
                    dt.Rows.Add(r)
                End While
                dt.AcceptChanges()
                Me.DropDownListEleves.DataSource = dt
                Me.DropDownListEleves.DataTextField = "fullname"
                Me.DropDownListEleves.DataValueField = "pk_eleve_id"
                Me.DropDownListEleves.DataBind()

                SetCheckbox(Me.DropDownListEleves.SelectedValue)

            Catch ex As Exception
                Me.lblErrMsg.Text = "Erreure est survenue: " & ex.Message
            End Try
        End Sub

        'Protected Sub DropDownListEeleves_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownListEcoles.SelectedIndexChanged
        '    BindListeEleves(Me.DropDownListEcoles.SelectedValue)
        '    Session("infoseleve") = Me.DropDownListEleves.SelectedValue
        '    SetCheckbox()

        'End Sub
        'Protected Sub DropDownListEcoles_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownListEcoles.SelectedIndexChanged
        '    BindListeEleves(Me.DropDownListEcoles.SelectedValue)
        '    Session("infoseleve") = Me.DropDownListEleves.SelectedValue
        '    SetCheckbox()

        'End Sub

        Sub SetCheckbox(ByVal EleveID As String)

            SetForceCheckBox(EleveID)
            setMatieresCheckbox(EleveID)
            SetAdaptationsCheckBox(EleveID)
            SetDonneesEvaluation(EleveID)
            SetCompetences(EleveID)
            SetRHcheckBox(EleveID)
            setButsCheckBox(EleveID)
            setButs_Transition(EleveID)
            SetEval_Provinciaux(EleveID)

        End Sub

        Protected Sub DropDownListEleves_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownListEleves.SelectedIndexChanged
            'Me.EleveToPrint.InnerHtml = Me.DropDownListEleves.SelectedItem.ToString
            SetCheckbox(Me.DropDownListEleves.SelectedValue)
            'DisplayEleveToPrint()

            SetSessionVariables()

        End Sub

        Protected Sub DropDownListEcoles_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownListEcoles.SelectedIndexChanged
            BindListeEleves(Me.DropDownListEcoles.SelectedValue)
            'DisplayEleveToPrint()

            SetSessionVariables()
        End Sub

        Sub DisplayEleveToPrint()
            Me.EleveToPrint.InnerHtml = "�l�ve: " & Me.DropDownListEleves.SelectedItem.ToString
        End Sub
        Sub SetSessionVariables()
            Session("eleveid") = Me.DropDownListEleves.SelectedValue
            Session("infoseleve") = Me.DropDownListEleves.SelectedItem.ToString
        End Sub
    End Class

End Namespace

