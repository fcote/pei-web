<%@ Page Language="vb" AutoEventWireup="false" ValidateRequest="false" SmartNavigation = "true" Inherits="PeiWebAccess.plan" CodeFile="plan.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="sstchur.web.SmartNav" Assembly="sstchur.web.SmartNav" %>
<%@ Register TagPrefix="uc1" TagName="menu_top" Src="menu_top.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>plan</title>
		<LINK href="styles.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">

    <table>
    <tr></tr>
    </table>

		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" borderColor="#0066ff" cellSpacing="-" cellPadding="0" width="100%" border="0">
				<TR>
					<TD><uc1:menu_top id="Menu_top1" runat="server"></uc1:menu_top></TD>
				</TR>
				<TR>
					<TD class="titleOnglet">Plan de transition</TD>
				</TR>
				<TR>
				<TR>
					<TD><asp:label id="lblMsgErr" runat="server" Visible="False" Width="100%" CssClass="fldlabel" BorderColor="Firebrick"
							BorderWidth="1px" BackColor="Khaki"></asp:label></TD>
				</TR>
				<TR>
					<TD class="cbToolbar">Choisissez la catégorie:
						<asp:dropdownlist id="DropDownListCategorie" runat="server" Width="240px"></asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD class="cbToolbar">Buts de transition:
						<asp:imagebutton id="ImageButtonAjouterBut" runat="server" ImageUrl="images/create.gif"></asp:imagebutton></TD>
				</TR>
				<tr>
					<td><asp:datagrid id="Datagrid3" runat="server" Width="100%" BackColor="White" DataMember="BUTS_TRANSITION"
							DataKeyField="BUTS_TRANSITION" CellPadding="3" BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC"
							AutoGenerateColumns="False" CssClass="fldLabel">
							<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
							<ItemStyle ForeColor="#000066"></ItemStyle>
							<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699"></HeaderStyle>
							<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
							<Columns>
								<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;img border=0 src=&quot;Images/save.gif&quot;&gt;"
									CancelText="Annuler" EditText="&lt;img border=0 src=&quot;Images/P_Draw_Lg_N.gif&quot;&gt;"></asp:EditCommandColumn>
								<asp:ButtonColumn Text="&lt;img border=0 src=&quot;Images/delete.gif&quot;&gt;" CommandName="Delete"></asp:ButtonColumn>
								<asp:TemplateColumn HeaderText="Buts de transition">
									<HeaderStyle Width="100%"></HeaderStyle>
									<ItemTemplate>
										<!--<asp:Label id="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'></asp:Label>-->
										<asp:Label id="lblTypeContact" Text='<%# DataBinder.Eval(Container.DataItem, "description") %>' Runat="server">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="textboxDescription2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>' Width="100%">
										</asp:TextBox>
										<!--<asp:Label id=lblTypeContactHold Text='<%# DataBinder.Eval(Container.DataItem, "description") %>' Runat="server"></asp:Label>-->
										<asp:DropDownList id="ddlTypeContact" AutoPostBack="True" OnSelectedIndexChanged="SetNewTextDescription" runat="server" DataValueField="CONTENUVALUE" DataTextField="CONTENU" OnPreRender="SetDropDownIndex" DataSource="<%# BindTypeContact() %>">
										</asp:DropDownList>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></td>
				</tr>
				<!--<TR>
					<TD><asp:datagrid id="DataGrid1" runat="server" Width="100%" BackColor="White" Font-Size="XX-Small"
							Font-Names="Arial" Height="62px" DataMember="BUTS_TRANSITION" DataKeyField="BUTS_TRANSITION"
							CellPadding="3" BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC" AutoGenerateColumns="False">
							<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
							<ItemStyle ForeColor="#000066"></ItemStyle>
							<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699"></HeaderStyle>
							<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
							<Columns>
								<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Mettre &#224; jour" CancelText="Annuler" EditText="Modifier">
									<HeaderStyle Width="40px"></HeaderStyle>
								</asp:EditCommandColumn>
								<asp:ButtonColumn Text="Supprimer" CommandName="Delete">
									<HeaderStyle Width="40px"></HeaderStyle>
								</asp:ButtonColumn>
								<asp:TemplateColumn HeaderText="Buts de transition">
									<ItemTemplate>
										<asp:Label id=Label1 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id=textboxDescription runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.description") %>' Width="100%">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>-->
				<TR>
					<TD class="cbToolbar">Mesures nécessaires
						<asp:imagebutton id="ImageButtonAjouterMesure" runat="server" ImageUrl="images/create.gif"></asp:imagebutton></TD>
				</TR>
				<TR>
					<TD><asp:datagrid id="DataGrid2" runat="server" Width="100%" BackColor="White" DataMember="PLAN_INTERVENTION"
							DataKeyField="ROW_ID" CellPadding="3" BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC"
							AutoGenerateColumns="False" PageSize="20" CssClass="fldLabel">
							<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
							<ItemStyle ForeColor="#000066"></ItemStyle>
							<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#006699"></HeaderStyle>
							<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
							<Columns>
								<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;img border=0 src=&quot;Images/save.gif&quot;&gt;"
									CancelText="Annuler" EditText="&lt;img border=0 src=&quot;Images/P_Draw_Lg_N.gif&quot;&gt;"></asp:EditCommandColumn>
								<asp:ButtonColumn Text="&lt;img border=0 src=&quot;Images/delete.gif&quot;&gt;" CommandName="Delete"></asp:ButtonColumn>
								<asp:TemplateColumn HeaderText="Mesures n&#233;cessaires">
									<ItemTemplate>
										<asp:Label id="lblMesures" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mesures") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="TxtMesures" TextMode=MultiLine Height=40px Width=150px runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.mesures") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Personnes responsables de ces mesures">
									<ItemTemplate>
										<asp:Label id="lblResonsables" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.responsables") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="txtResponsables" Width="300px" Height="40px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.responsables") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="&#201;ch&#233;anciers">
									<ItemTemplate>
										<asp:Label id="lblEcheancier" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.echeancier") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id="txtEcheancier" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.echeancier") %>' Width="100%">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD>
						<cc1:SmartScroller id="SmartScroller1" runat="server"></cc1:SmartScroller></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
