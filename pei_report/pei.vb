Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms
Imports Telerik.Reporting
Imports Telerik.Reporting.Drawing
'Imports System.Data.EntityClient
Imports System.Globalization



Public Class pei
    Inherits Telerik.Reporting.Report

    Dim _reportManager As ReportManager

    Public Sub New()
        InitializeComponent()
        _reportManager = New ReportManager
    End Sub
    Private _peiname As String = "default_pei"
    Private _annee_scolaire As String = ""
    Private _etape As Integer = 1
    Private _donnees_generales As Boolean = True
    Private _forces_besoins As Boolean = True
    Private _matieres As Boolean = True
    Private _competences_habiletes As Boolean = True
    Private _adaptations As Boolean = True
    Private _ress_humaines As Boolean = False
    Private _donnees_evaluations As Boolean = True
    Private _buts As Boolean = True
    Private _tests_provinciaux As Boolean = True
    Private _plan_transition As Boolean = True
    Private _plan_transition2 As Boolean = True
    Private _details_consultations As Boolean = True
    Private _page_signature As Boolean = True
    Private _logo As String = "https://www.cscmonavenir.ca/publications/logo/cscmonavenir_logo-NOIR.png"

    Public Property peiname As String
        Get
            Return _peiname
        End Get
        Set(ByVal value As String)
            _peiname = value
        End Set
    End Property
    Public Property etape As Integer
        Get
            Return _etape
        End Get
        Set(ByVal value As Integer)
            _etape = value
        End Set
    End Property
    Public Property logo As String
        Get
            Return _logo
        End Get
        Set(ByVal value As String)
            _logo = value
        End Set
    End Property
    Public Property annee_scolaire As String
        Get
            Return _annee_scolaire
        End Get
        Set(ByVal value As String)
            _annee_scolaire = value
        End Set
    End Property
    Public Property donnees_generales As Boolean
        Get
            Return _donnees_generales
        End Get
        Set(ByVal value As Boolean)
            _donnees_generales = value
        End Set
    End Property
    Public Property forces_besoins As Boolean
        Get
            Return _forces_besoins
        End Get
        Set(ByVal value As Boolean)
            _forces_besoins = value
        End Set
    End Property
    Public Property matieres As Boolean
        Get
            Return _matieres
        End Get
        Set(ByVal value As Boolean)
            _matieres = value
        End Set
    End Property
    Public Property competences_habiletes As Boolean
        Get
            Return _competences_habiletes
        End Get
        Set(ByVal value As Boolean)
            _competences_habiletes = value
        End Set
    End Property
    Public Property adaptations As Boolean
        Get
            Return _adaptations
        End Get
        Set(ByVal value As Boolean)
            _adaptations = value
        End Set
    End Property
    Public Property ress_humaines As Boolean
        Get
            Return _ress_humaines
        End Get
        Set(ByVal value As Boolean)
            _ress_humaines = value
        End Set
    End Property
    Public Property buts As Boolean
        Get
            Return _buts
        End Get
        Set(ByVal value As Boolean)
            _buts = value
        End Set
    End Property
    Public Property tests_provinciaux As Boolean
        Get
            Return _tests_provinciaux
        End Get
        Set(ByVal value As Boolean)
            _tests_provinciaux = value
        End Set
    End Property
    Public Property donnees_evaluations As Boolean
        Get
            Return _donnees_evaluations
        End Get
        Set(ByVal value As Boolean)
            _donnees_evaluations = value
        End Set
    End Property
    Public Property plan_transition As Boolean
        Get
            Return _plan_transition
        End Get
        Set(value As Boolean)
            _plan_transition = value
        End Set
    End Property
    Public Property plan_transition2 As Boolean
        Get
            Return _plan_transition2
        End Get
        Set(value As Boolean)
            _plan_transition2 = value
        End Set
    End Property
    Public Property details_consultations As Boolean
        Get
            Return _details_consultations
        End Get
        Set(value As Boolean)
            _details_consultations = value
        End Set
    End Property
    Public Property page_signature As Boolean
        Get
            Return _page_signature
        End Get
        Set(value As Boolean)
            _page_signature = value
        End Set
    End Property

    Public Sub SetDataSource(eleveid As String)
        Dim _context As New peiEntities
        '_context.ELEVES.Include("ECOLES").MergeOption = Objects.MergeOption.NoTracking
        '_context.ELEVES.Include("PLACEMENTS").MergeOption = Objects.MergeOption.NoTracking
        '_context.ELEVES.Include("RH_ELEVES").MergeOption = Objects.MergeOption.NoTracking
        '_context.ELEVES.Include("PLACEMENTS").MergeOption = Objects.MergeOption.NoTracking
        '_context.ELEVES.Include("BUTS").MergeOption = Objects.MergeOption.NoTracking
        _context.ELEVES.Include("BUT").MergeOption = Objects.MergeOption.NoTracking
        _context.ELEVES.Include("ATTENTES").MergeOption = Objects.MergeOption.NoTracking
        _context.ELEVES.Include("buts_transition2").MergeOption = Objects.MergeOption.NoTracking
        _context.ELEVES.Include("plan_intervention2").MergeOption = Objects.MergeOption.NoTracking

        _context.ELEVES.Include("butstransition").MergeOption = Objects.MergeOption.NoTracking
        _context.ELEVES.Include("mesures_transition").MergeOption = Objects.MergeOption.NoTracking
        '_context.ELEVES.Include("but_transitions2").MergeOption = Objects.MergeOption.NoTracking
        '_context.ELEVES.Include("buts_transition2").Include("plan_intervention2").ToList()
        '_context.ELEVES.Include("plan_intervention2").Include("buts_transition2").ToList()

        '_context.buts_transition2.Include("plan_intervention2")


        Dim _oEleve As ELEVE
        _oEleve = _context.ELEVES.Where(Function(e) e.pk_eleve_id = eleveid).FirstOrDefault

        '--------------------------
        'header nom eleve
        Me.TextBoxHeaderNomEleve.Value = RTrim(_oEleve.prenom_eleve) & " " & RTrim(_oEleve.nom_eleve) & "  (DDN:" & _oEleve.date_naissance.Value.ToString("d", CultureInfo.CreateSpecificCulture("fr-FR")) & ")"
        Me.TextBoxHeaderNomEcole.Value = RTrim(_oEleve.ECOLE.nom_ecole)
        '------
        ' set school year
        Me.TextBoxAnneeScolaire.Value = _annee_scolaire

        'infos conseil
        'Me.PictureBox1.Value = "https://www.cscmonavenir.ca/publications/logo/cscmonavenir_logo-NOIR.png"
        'Me.PictureBox1.Value = _logo

        'Infos �cole
        Me.TextBox1NomEcole.Value = _oEleve.ECOLE.nom_ecole
        Me.TextBoxDirection.Value = _oEleve.ECOLE.directeur
        Me.TextBoxAdresseEcole.Value = _oEleve.ECOLE.adresse
        Me.TextBoxTelephone.Value = _oEleve.ECOLE.no_tel
        Me.TextBoxTelecopieur.Value = _oEleve.ECOLE.no_fax

        ' date bulletins
        If _oEleve.DATE.DATE_ETAPE_1.HasValue Then Me.TextBoxEtape1.Value = _oEleve.DATE.DATE_ETAPE_1.Value.ToString("d", CultureInfo.CreateSpecificCulture("fr-FR"))
        If _oEleve.DATE.DATE_ETAPE_2.HasValue Then Me.TextBoxEtape2.Value = _oEleve.DATE.DATE_ETAPE_2.Value.ToString("d", CultureInfo.CreateSpecificCulture("fr-FR"))
        If _oEleve.DATE.DATE_ETAPE_3.HasValue Then Me.TextBoxEtape3.Value = _oEleve.DATE.DATE_ETAPE_3.Value.ToString("d", CultureInfo.CreateSpecificCulture("fr-FR"))
        If _oEleve.DATE.DATE_ETAPE_4.HasValue Then Me.TextBoxEtape4.Value = _oEleve.DATE.DATE_ETAPE_4.Value.ToString("d", CultureInfo.CreateSpecificCulture("fr-FR"))

        'profile �l�ve cot� GAUCHE
        Me.TextBoxPrenomEleve.Value = _oEleve.prenom_eleve
        Me.TextBoxNomEleve.Value = _oEleve.nom_eleve
        Me.TextBoxDateNaissance.Value = _oEleve.date_naissance.Value.ToString("d", CultureInfo.CreateSpecificCulture("fr-FR"))
        'niso
        Me.TextBoxSexe.Value = _oEleve.sexe
        Me.TextBoxAnneeEtude.Value = _oEleve.fk_niveau_id
        'Me.TextBoxClasseDistinct.Value = _oEleve.PLACEMENTS.Select(Function(p) p.description).FirstOrDefault.ToString

        If Not IsNothing(_oEleve.PLACEMENTS.FirstOrDefault) Then
            Me.TextBoxClasseDistinct.Value = _oEleve.PLACEMENTS.Select(Function(p) p.description).FirstOrDefault.ToString
        End If

        Me.TextBoxTitulaire.Value = IIf(IsDBNull(_oEleve.titulaire), "", _oEleve.titulaire.ToString)
        'If Not String.IsNullOrEmpty(_oEleve.PLACEMENTS.Select(Function(p) p.description).FirstOrDefault.ToString) Then
        '    Me.TextBoxClasseDistinct.Value = _oEleve.PLACEMENTS.Select(Function(p) p.description).FirstOrDefault.ToString
        'End If
        'If Not IsNothing(_oEleve.PLACEMENTS.Select(Function(p) p.description).FirstOrDefault.ToString) Then
        '    Me.TextBoxClasseDistinct.Value = _oEleve.PLACEMENTS.Select(Function(p) p.description).FirstOrDefault.ToString
        'End If

        'anomalies
        Me.TableAnomalies.DataSource = _oEleve.ANOMALIES

        'source d'informations
        Me.TableSourcesInformations.DataSource = _oEleve.SOURCES

        'format communication
        Me.TableFormatCommunication.DataSource = _oEleve.FORMATS_COMMUNICATIONS

        'raisons
        Me.TableRaison.DataSource = _oEleve.RAISONS

        Me.TextBoxExemptions.Value = _oEleve.Exemp_prog_scol_2

        Me.lblDemandeFinancement.Visible = IIf(String.Compare(_oEleve.DEMANDE_FINANCEMENT_MINISTERE.ToString, "OUI") = 1, True, False)

        'ressources humaines
        Me.TableRessourceHumaines.DataSource = _oEleve.RH_ELEVES

        'profile �l�ve cot� DROIT
        'dates
        If _oEleve.DATE.DATE_IDENTIFICATION.HasValue Then Me.TextBoxDateIdentification.Value = _oEleve.DATE.DATE_IDENTIFICATION.Value.ToString("d", CultureInfo.CreateSpecificCulture("fr-FR"))
        If _oEleve.DATE.DATE_CIPR_RECENT.HasValue Then Me.TextBoxDateCIPR.Value = _oEleve.DATE.DATE_CIPR_RECENT.Value.ToString("d", CultureInfo.CreateSpecificCulture("fr-FR"))
        If _oEleve.DATE.DATE_RENONCIATION.HasValue Then Me.TextBoxDateRenonciation.Value = _oEleve.DATE.DATE_RENONCIATION.Value.ToString("d", CultureInfo.CreateSpecificCulture("fr-FR"))
        If _oEleve.DATE.DATE_DEBUT_PLACEMENT.HasValue Then Me.TextBoxDatePlacement.Value = _oEleve.DATE.DATE_DEBUT_PLACEMENT.Value.ToString("d", CultureInfo.CreateSpecificCulture("fr-FR"))
        If _oEleve.DATE.DATE_PEI_TERMINE.HasValue Then Me.TextBoxDateElaborationTermine.Value = _oEleve.DATE.DATE_PEI_TERMINE.Value.ToString("d", CultureInfo.CreateSpecificCulture("fr-FR"))

        Dim listElaborePar As List(Of ELABORE_PAR)
        listElaborePar = _context.ELABORE_PAR.Where(Function(e) e.fk_eleve_id = eleveid).ToList
        Me.TableElaborePar.DataSource = listElaborePar
        'Me.TableElaborePar.DataSource = _oEleve.ELABORE_PAR

        '--------------------------------------
        'Donn�e �valuaions
        Me.TableDonneesEvaluation.DataSource = _oEleve.RAPPORTS_EVALUATION

        'FORCES ET BESOINS
        'Dim lst = _oEleve.FORCESBESOINS.ToList
        'Dim lst As List(Of FORCESBESOIN)
        'lst = _oEleve.FORCESBESOINS.ToList
        Dim lstForcesBesoins As List(Of FORCESBESOIN)
        lstForcesBesoins = _context.FORCESBESOINS.Where(Function(fb) fb.FK_ELEVE_ID = eleveid).ToList

        For Each fb As FORCESBESOIN In lstForcesBesoins
            If Trim(fb.TYPE_FB) = "F" Then
                Me.TextBoxForces.Value = fb.HAB_COGNITIVE.ToString
            End If
            If Trim(fb.TYPE_FB) = "B" Then
                Me.TextBoxBesoins.Value = fb.HAB_COGNITIVE.ToString
            End If
            If Trim(fb.TYPE_FB) = "BP" Then
                Me.TextBoxBesoinsPrioritaires.Value = fb.HAB_COGNITIVE.ToString
            End If
        Next

        'mati�res, cours ou programmes
        Me.TableMatieres.DataSource = _oEleve.MATIERES_ELEVES
        Me.TableCompetences.DataSource = _oEleve.COMPETENCES_ELEVES

        'ADAPTATION
        Me.TableAdaptations.DataSource = _oEleve.SANTE_ELEVES
        Me.TableAdaptations2.DataSource = _oEleve.SANTE_ELEVES

        '�VALUATIONS PROVINCIALES
        Me.TableEvaluationsProvinciales.DataSource = _oEleve.TESTS_PROVINCIAUX

        'PLAN DE TRANSITION
        Me.TablePlanTransition.DataSource = _oEleve.BUTS_TRANSITION

        ''MESURE N�CESSAIRES
        Me.TablePlanInterventions.DataSource = _oEleve.PLAN_INTERVENTION


        ' buts transition et mesures/plan intervention
        'Dim _lstButs As New List(Of boButTransition)
        'Dim _but_transition As boButTransition
        'Dim _lst_plan As New List(Of boPlanIntervention)
        'Dim _index As Integer = 0

        'For Each BUT As buts_transition2 In _oEleve.buts_transition2
        '    _but_transition = New boButTransition
        '    _but_transition.but_transition_id = BUT.BUTS_TRANSITION ' id du but
        '    _but_transition.description = BUT.DESCRIPTION
        '    For Each plan As plan_intervention2 In BUT.plan_intervention2
        '        Dim _plan As New boPlanIntervention
        '        _plan.but_transition_id = plan.FK_BUT_TRANSITION_ID
        '        _plan.no_order = plan.NO_ORDER
        '        _plan.mesures = plan.MESURES
        '        _plan.responsables = plan.RESPONSABLES
        '        _plan.echeancier = plan.ECHEANCIER

        '        _lst_plan.Add(_plan)
        '    Next
        '    _but_transition.plan_intervention = _lst_plan
        '    _lstButs.Add(_but_transition)
        'Next

        ''-------------------------------------------------
        ''but transition2 et plan transition2
        'Dim lstPlanIntervention2 As New List(Of plan_intervention2)
        'Dim lstButsTransition2 As New List(Of buts_transition2)
        'lstButsTransition2 = _context.buts_transition2.Where(Function(b) b.FK_ELEVE_ID = eleveid).OrderBy(Function(o) o.NO_ORDER).ToList

        'For Each _but As buts_transition2 In lstButsTransition2

        '    If (_but.plan_intervention2.Count < 1) Then
        '        Dim _planInterEmpty As New plan_intervention2
        '        _planInterEmpty.buts_transition2 = _but
        '        lstPlanIntervention2.Add(_planInterEmpty)
        '    Else
        '        For Each _planintervention2 As plan_intervention2 In _but.plan_intervention2
        '            Dim _planInter2 As New plan_intervention2
        '            _planInter2 = _planintervention2
        '            lstPlanIntervention2.Add(_planInter2)
        '        Next
        '    End If
        'Next

        Dim SubReportPT As New SubReportPlanTransition2
        SubReportPT.eleveid = eleveid
        'SubReportPT.SetDataSource(_lstButs)
        'SubReportPT.SetDataSource(_lst_plan)
        SubReportPT.SetDatasourcePT(_context.plan_intervention2.Where(Function(el) el.FK_ELEVE_ID = eleveid).ToList)
        SubReportPanTrans.ReportSource = SubReportPT

        '-----------------------------------------------------



        'RESULTATS CONSULTATION
        Me.TableResultatsConsultation.DataSource = _oEleve.RESULTATS_CONSULTATION

        'buts et attentes
        Dim lstAttentes As New List(Of ATTENTE)
        Dim lstButs As New List(Of BUT)
        lstButs = _context.BUTS.Where(Function(b) b.fk_eleve_id = eleveid).OrderBy(Function(o) o.but_no).ToList
        Dim lstId As New List(Of Integer)
        lstId.Add(70248)
        lstId.Add(70249)
        lstId.Add(70250)

        For Each _but As BUT In lstButs

            If (_but.ATTENTES.Count < 1) Then
                Dim _attenteEmpty As New ATTENTE
                _attenteEmpty.BUT = _but
                lstAttentes.Add(_attenteEmpty)
            Else
                For Each _att As ATTENTE In _but.ATTENTES
                    Dim _attente As New ATTENTE
                    _attente = _att
                    lstAttentes.Add(_attente)
                Next
            End If
        Next

        ''Me.ObjectDataSourceAttentes.DataSource = lstAttentes

        'Dim subButAttente As New SubRepButsAttentes
        ''Dim subButAttente As New ButsAttentesStrategies

        ''SubReport1.Parameters.Add("etape", "3")
        ''SubReport1.Parameters("etape").Value = 3
        'subButAttente.etape = 2
        'subButAttente.eleveid = eleveid
        ''subButAttente.eleveid = "0001221854"
        'subButAttente.attentes = lstAttentes
        'subButAttente.FormatReport()

        'SubReport1.ReportSource = subButAttente
        'Me.TableAttentes.DataSource = lstAttentes.ToList
        'Me.ObjectDataSourceAttNew.DataSource = lstAttentes
        '================================================

        '============================================
        'Dim subButAttente As New ButAttStrat

        ' ''SubReport1.Parameters.Add("etape", "3")
        ' ''SubReport1.Parameters("etape").Value = 3
        ''subButAttente.etape = 2
        ''subButAttente.eleveid = eleveid
        ' ''subButAttente.eleveid = "0001221854"
        'subButAttente.attentes = lstAttentes
        'subButAttente.SetDataSource()
        ''subButAttente.FormatReport()

        'SubReport1.ReportSource = subButAttente
        '====================================================
        'Dim subReportBut As New SubReportBut
        'subReportBut.PopulateReport(lstButs)
        'SubReportButs.ReportSource = subReportBut

        '===================================================================================
        Dim SubReportAttentes1 As New SubReportAttentes
        SubReportAttentes1.attentes = lstAttentes
        SubReportAttentes1.etape = etape
        SubReportAttentes1.SetDataSource()
        SubReportAtt.ReportSource = SubReportAttentes1

        'For Each b As BUT In lstButs
        '    Dim rep As New SubReportBut
        '    rep.PopulateReport(b)
        '    Me.GroupHeaderSectionButs.Items.Add(rep)
        'Next

        '---------------------
        'temporary deactivated
        'Dim SubRepPlanTransition As New SubReportPlanTransition2
        'SubReport2.ReportSource = SubRepPlanTransition
        'SubRepPlanTransition.eleveid = eleveid


        'Dim lstplanInter As New List(Of plan_intervention2)
        'For Each butTrans In _oEleve.buts_transition2
        '    Dim butT As New buts_transition2
        '    butT.BUTS_TRANSITION = butTrans.BUTS_TRANSITION
        '    butT.DESCRIPTION = butTrans.DESCRIPTION
        '    For Each plan In butTrans.plan_intervention2
        '        Dim planinter As New plan_intervention2
        '        planinter.FK_BUT_TRANSITION_ID = butTrans.BUTS_TRANSITION
        '        planinter.NO_ORDER = plan.NO_ORDER
        '        planinter.MESURES = plan.MESURES
        '        planinter.RESPONSABLES = plan.RESPONSABLES
        '        planinter.buts_transition2 = butT
        '        lstplanInter.Add(planinter)
        '    Next
        'Next
        'SubRepPlanTransition.SetObjectDatasource(lstplanInter)

        '------------------

        'format
        _reportManager.FormatFieldsH1(Me.lblTitreRapport)
        _reportManager.FormatFieldsH2(Me.lblProfileEleveTitre, Me.lblDonneesEvaluationTitre, Me.lblRessourcesHumainesTitre, Me.lblForcesTitre, Me.lblBesoinsTitre, Me.lblBesoinsPrioritaires, Me.lblMatieresCoursProgrammesTitre, Me.lblAdaptationsTitre, Me.lblEvaluationsProvincialesTitre, Me.lblPlanTransitionTitre, Me.lblMesuresNecessairesTitre, Me.lblDetailsSurLesConsultationsTitre)
        _reportManager.FormatFieldsBold(Me.lblAnomalies, Me.lblSourcesInformations, Me.lblElaborePar, Me.lblDateBulletins, Me.lblFormatDeCommunication, Me.lblRaisons, Me.lblExemptions, Me.lblSources, Me.lblDateDonneesEvaluation, Me.lblResumeConclusion, Me.lblTypeService, Me.lblDateInstauration, Me.lblFrequence, Me.lblLieuEndroit, Me.lblMatieresCours, Me.lblTypeAttente, Me.lblCompetence, Me.lblTypeAttenteCompetence, Me.lblAdapPedagogiques, Me.lblAdapEnvironnementales, Me.lblAdapMatiereEvaluations, Me.lblAdaptEquipementPersonalise, Me.lblAdaptProbSanteConnexes, Me.lblTestProvEvaluations, Me.lblTestProvinciauxAdaptations, Me.lblTestProvExemptions, Me.lblResponsables, Me.lblMesures, Me.lblEcheancier, Me.lblDateConsultation, Me.lblMoyenConsultation, Me.lblResultatsConsultation)

        'center title

        '===========================================================
        'Show/Hide section reports
        'donn�es �coles/conseils, donn�es g�n�rales et tableau 3 col (format communication,Raisons justifiant pei, Exemmption...)
        'Me.GroupHeaderSectionEcoleConseil.Visible = _donnees_generales
        Me.GroupHeaderProfileEleve.Visible = _donnees_generales
        Me.GroupHeaderFormatRaisonExemptions.Visible = _donnees_generales

        'donn�e evaluation
        Me.GroupHeaderSectionDonneeEvaluations.Visible = _donnees_evaluations

        'Ress humaines
        Me.GroupHeaderSectionRH.Visible = _ress_humaines

        'Force et Besoins
        Me.GroupHeaderSectionForcesBesoins.Visible = _forces_besoins

        'Mati�res et comp�tences
        Me.GroupHeaderSectionMatieres.Visible = IIf(_matieres Or _competences_habiletes, True, False)
        Me.TableMatieres.Visible = _matieres
        Me.TableCompetences.Visible = _competences_habiletes

        'Adaptations 
        Me.GroupHeaderSectionAdaptations.Visible = _adaptations

        'test(provinciaux)
        Me.GroupHeaderSectionEvaluationsProvinciales.Visible = _tests_provinciaux

        'buts, attente et strategies, commentaires
        'Me.GroupButs.Visible = _buts
        Me.GroupHeaderSectionButs.Visible = _buts
        'Me.GroupButs.Visible = False

        'plan transition, mesures n�ecessaires
        Me.GroupHeaderSectionPlanTransition.Visible = _plan_transition
        'Me.lblNomEleve, Me.lblPrenomEleve, Me.lblDateNaissance, Me.lblSexe, Me.lblTitulaire, Me.lblNiso, Me.lblClasse

        'plan transtion 2 (nouveau)
        'Me.GroupHeaderSectionPlantTransition2.Visible = _plan_transition2
        Me.GroupHeaderSectionPlantTransition2.Visible = _plan_transition2

        'details sur les consultation
        Me.GroupHeaderSectionDetailsConsultations.Visible = _details_consultations

        'page signature
        Me.GroupHeaderSectionPageSignature.Visible = _page_signature
        '=====================================================================


        'Dim newlst As List(Of plan_intervention2)
        ''newlst = _oEleve.buts_transition2.SelectMany((Function(buts) buts.plan_intervention2))
        'newlst = _oEleve.buts_transition2.Select(Function(plan) plan.plan_intervention2.ToList)
        'SubRepPlanTransition.SetObjectDatasource(newlst)

    End Sub
End Class

