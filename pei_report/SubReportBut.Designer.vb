Partial Class SubReportBut
    
    'NOTE: The following procedure is required by the telerik Reporting Designer
    'It can be modified using the telerik Reporting Designer.  
    'Do not modify it using the code editor.
    Private Sub InitializeComponent()
        Me.pageHeaderSectionBut = New Telerik.Reporting.PageHeaderSection()
        Me.detail = New Telerik.Reporting.DetailSection()
        Me.pageFooterSection1 = New Telerik.Reporting.PageFooterSection()
        Me.ObjectDataSourceBut = New Telerik.Reporting.ObjectDataSource()
        Me.Group1But = New Telerik.Reporting.Group()
        Me.GroupHeaderSection1 = New Telerik.Reporting.GroupHeaderSection()
        Me.GroupFooterSection1 = New Telerik.Reporting.GroupFooterSection()
        Me.TextBox5 = New Telerik.Reporting.TextBox()
        Me.TextBoxBut = New Telerik.Reporting.TextBox()
        Me.TextBox1 = New Telerik.Reporting.TextBox()
        Me.TextBox2 = New Telerik.Reporting.TextBox()
        Me.TextBox3 = New Telerik.Reporting.TextBox()
        Me.TextBox4 = New Telerik.Reporting.TextBox()
        Me.TextBoxProgramme = New Telerik.Reporting.TextBox()
        Me.TextBoxAnneEtude = New Telerik.Reporting.TextBox()
        Me.TextBoxNiveauRendement = New Telerik.Reporting.TextBox()
        Me.Group2Attentes = New Telerik.Reporting.Group()
        Me.GroupHeaderSection2 = New Telerik.Reporting.GroupHeaderSection()
        Me.GroupFooterSection2 = New Telerik.Reporting.GroupFooterSection()
        Me.SubReportAttentes = New Telerik.Reporting.SubReport()
        Me.TextBox6 = New Telerik.Reporting.TextBox()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'pageHeaderSectionBut
        '
        Me.pageHeaderSectionBut.Height = Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433R)
        Me.pageHeaderSectionBut.Name = "pageHeaderSectionBut"
        '
        'detail
        '
        Me.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(0.19999994337558746R)
        Me.detail.Name = "detail"
        '
        'pageFooterSection1
        '
        Me.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.299999862909317R)
        Me.pageFooterSection1.Name = "pageFooterSection1"
        '
        'ObjectDataSourceBut
        '
        Me.ObjectDataSourceBut.DataSource = GetType(peireport.BUT)
        Me.ObjectDataSourceBut.Name = "ObjectDataSourceBut"
        '
        'Group1But
        '
        Me.Group1But.GroupFooter = Me.GroupFooterSection1
        Me.Group1But.GroupHeader = Me.GroupHeaderSection1
        Me.Group1But.Groupings.AddRange(New Telerik.Reporting.Data.Grouping() {New Telerik.Reporting.Data.Grouping("=Fields.but_titre")})
        Me.Group1But.Name = "Group1But"
        '
        'GroupHeaderSection1
        '
        Me.GroupHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(10.715415954589844R)
        Me.GroupHeaderSection1.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.TextBox5, Me.TextBoxBut, Me.TextBox1, Me.TextBox2, Me.TextBox3, Me.TextBox4, Me.TextBoxProgramme, Me.TextBoxAnneEtude, Me.TextBoxNiveauRendement, Me.SubReportAttentes, Me.TextBox6})
        Me.GroupHeaderSection1.Name = "GroupHeaderSection1"
        Me.GroupHeaderSection1.PageBreak = Telerik.Reporting.PageBreak.Before
        '
        'GroupFooterSection1
        '
        Me.GroupFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.30000025033950806R)
        Me.GroupFooterSection1.Name = "GroupFooterSection1"
        '
        'TextBox5
        '
        Me.TextBox5.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.97895830869674683R), Telerik.Reporting.Drawing.Unit.Cm(3.4999995231628418R))
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.300000190734863R), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634R))
        Me.TextBox5.Value = "But(s) annuel(s): Il s'agit d'un �nonc� qui d�crit ce qu'un �l�ve devrait raisonn" & _
    "ablement avoir accompli � la fin de l'ann�e scolaire dans une mati�re, un cours " & _
    "ou un programme."
        '
        'TextBoxBut
        '
        Me.TextBoxBut.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.97895830869674683R), Telerik.Reporting.Drawing.Unit.Cm(4.1085410118103027R))
        Me.TextBoxBut.Name = "TextBoxBut"
        Me.TextBoxBut.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.300000190734863R), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634R))
        Me.TextBoxBut.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14.0R)
        Me.TextBoxBut.Value = "= Fields.but_titre"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.97895830869674683R), Telerik.Reporting.Drawing.Unit.Cm(0.43083286285400391R))
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.339166641235352R), Telerik.Reporting.Drawing.Unit.Cm(0.60000008344650269R))
        Me.TextBox1.Value = "PROGRAMME D'ENSEIGNEMENT � L'ENFANCE EN DIFFICULT�"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.97895830869674683R), Telerik.Reporting.Drawing.Unit.Cm(1.330416202545166R))
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5999999046325684R), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791R))
        Me.TextBox2.Value = "Mati�re, cours ou programme"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.97895830869674683R), Telerik.Reporting.Drawing.Unit.Cm(2.0183327198028564R))
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5999999046325684R), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791R))
        Me.TextBox3.Value = "Ann�e d'�tudes au sein du curriculum"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.97895830869674683R), Telerik.Reporting.Drawing.Unit.Cm(2.7327077388763428R))
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5999999046325684R), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791R))
        Me.TextBox4.Value = "Niveau de rendement actule"
        '
        'TextBoxProgramme
        '
        Me.TextBoxProgramme.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.8683333396911621R), Telerik.Reporting.Drawing.Unit.Cm(1.330416202545166R))
        Me.TextBoxProgramme.Name = "TextBoxProgramme"
        Me.TextBoxProgramme.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.44979190826416R), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791R))
        Me.TextBoxProgramme.Value = "= Fields.programme"
        '
        'TextBoxAnneEtude
        '
        Me.TextBoxAnneEtude.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.8683333396911621R), Telerik.Reporting.Drawing.Unit.Cm(2.0183327198028564R))
        Me.TextBoxAnneEtude.Name = "TextBoxAnneEtude"
        Me.TextBoxAnneEtude.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.44979190826416R), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791R))
        Me.TextBoxAnneEtude.Value = "= Fields.annee_etude"
        '
        'TextBoxNiveauRendement
        '
        Me.TextBoxNiveauRendement.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.8683333396911621R), Telerik.Reporting.Drawing.Unit.Cm(2.7327077388763428R))
        Me.TextBoxNiveauRendement.Name = "TextBoxNiveauRendement"
        Me.TextBoxNiveauRendement.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.44979190826416R), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791R))
        Me.TextBoxNiveauRendement.Value = "= Fields.niveau_rendement"
        '
        'Group2Attentes
        '
        Me.Group2Attentes.GroupFooter = Me.GroupFooterSection2
        Me.Group2Attentes.GroupHeader = Me.GroupHeaderSection2
        Me.Group2Attentes.Name = "Group2Attentes"
        '
        'GroupHeaderSection2
        '
        Me.GroupHeaderSection2.Height = Telerik.Reporting.Drawing.Unit.Cm(0.30000025033950806R)
        Me.GroupHeaderSection2.Name = "GroupHeaderSection2"
        '
        'GroupFooterSection2
        '
        Me.GroupFooterSection2.Height = Telerik.Reporting.Drawing.Unit.Cm(0.29999944567680359R)
        Me.GroupFooterSection2.Name = "GroupFooterSection2"
        '
        'SubReportAttentes
        '
        Me.SubReportAttentes.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.0181242227554321R), Telerik.Reporting.Drawing.Unit.Cm(5.1999998092651367R))
        Me.SubReportAttentes.Name = "SubReportAttentes"
        Me.SubReportAttentes.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.300000190734863R), Telerik.Reporting.Drawing.Unit.Cm(1.4999996423721313R))
        '
        'TextBox6
        '
        Me.TextBox6.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.2999999523162842R), Telerik.Reporting.Drawing.Unit.Cm(7.2999997138977051R))
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.2999997138977051R), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634R))
        Me.TextBox6.Value = "TextBox6"
        '
        'SubReportBut
        '
        Me.DataSource = Me.ObjectDataSourceBut
        Me.Groups.AddRange(New Telerik.Reporting.Group() {Me.Group1But, Me.Group2Attentes})
        Me.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.pageHeaderSectionBut, Me.detail, Me.pageFooterSection1, Me.GroupHeaderSection1, Me.GroupFooterSection1, Me.GroupHeaderSection2, Me.GroupFooterSection2})
        Me.Name = "SubReportBut"
        Me.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Mm(5.0R)
        Me.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Mm(5.0R)
        Me.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Mm(5.0R)
        Me.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Mm(5.0R)
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4
        Me.Style.BackgroundColor = System.Drawing.Color.White
        Me.Style.Visible = True
        Me.Width = Telerik.Reporting.Drawing.Unit.Cm(16.299999237060547R)
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents pageHeaderSectionBut As Telerik.Reporting.PageHeaderSection
    Friend WithEvents detail As Telerik.Reporting.DetailSection
    Friend WithEvents pageFooterSection1 As Telerik.Reporting.PageFooterSection
    Friend WithEvents ObjectDataSourceBut As Telerik.Reporting.ObjectDataSource
    Friend WithEvents Group1But As Telerik.Reporting.Group
    Friend WithEvents GroupFooterSection1 As Telerik.Reporting.GroupFooterSection
    Friend WithEvents GroupHeaderSection1 As Telerik.Reporting.GroupHeaderSection
    Friend WithEvents TextBox5 As Telerik.Reporting.TextBox
    Friend WithEvents TextBoxBut As Telerik.Reporting.TextBox
    Friend WithEvents TextBox1 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox2 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox3 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox4 As Telerik.Reporting.TextBox
    Friend WithEvents TextBoxProgramme As Telerik.Reporting.TextBox
    Friend WithEvents TextBoxAnneEtude As Telerik.Reporting.TextBox
    Friend WithEvents TextBoxNiveauRendement As Telerik.Reporting.TextBox
    Friend WithEvents Group2Attentes As Telerik.Reporting.Group
    Friend WithEvents GroupFooterSection2 As Telerik.Reporting.GroupFooterSection
    Friend WithEvents GroupHeaderSection2 As Telerik.Reporting.GroupHeaderSection
    Friend WithEvents SubReportAttentes As Telerik.Reporting.SubReport
    Friend WithEvents TextBox6 As Telerik.Reporting.TextBox
End Class