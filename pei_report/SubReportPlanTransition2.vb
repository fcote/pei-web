Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms
Imports Telerik.Reporting
Imports Telerik.Reporting.Drawing

Partial Public Class SubReportPlanTransition2
    Inherits Telerik.Reporting.Report
    Dim _reportManager As ReportManager

    Public Sub New()
        InitializeComponent()
        _reportManager = New ReportManager
    End Sub
    Private _eleveid As String = ""

    Public Property eleveid As String
        Get
            Return _eleveid
        End Get
        Set(ByVal value As String)
            _eleveid = value
        End Set
    End Property
    Public Property planIntervention2
    Private _planIntervention As List(Of plan_intervention2) = Nothing
    Public Property attentes2 As List(Of plan_intervention2)
        Get
            Return _planIntervention2
        End Get
        Set(ByVal value As List(Of plan_intervention2))
            _planIntervention2 = value
        End Set
    End Property

    Sub SetDataSource()
        Dim _sql As String = "SELECT   buts_transition2.BUTS_TRANSITION, buts_transition2.DESCRIPTION, buts_transition2.NO_ORDER AS BUT_NO_ORDER, plan_intervention2.NO_ORDER AS NO_ORDER, " & _
        " plan_intervention2.MESURES, plan_intervention2.RESPONSABLES, plan_intervention2.ECHEANCIER, buts_transition2.FK_ELEVE_ID " & _
        " FROM buts_transition2 INNER JOIN " & _
        " plan_intervention2 ON buts_transition2.BUTS_TRANSITION = plan_intervention2.FK_BUT_TRANSITION_ID " & _
        " WHERE        (buts_transition2.FK_ELEVE_ID = '" & _eleveid & "')"

        'Me.SqlDataSourcePlanTransition2.SelectCommand = _sql


        '_reportManager.FormatFieldsH2(Me.lblPlanTransitionTitre2, Me.lblMesuresNecessairesTitre2)
        '_reportManager.FormatFieldsBold(Me.lblDateConsultations2, Me.lblMoyenConsultation2, Me.lblResultatsConsultation2)


    End Sub
    'Sub SetObjectDatasource(_butTrans As List(Of buts_transition2))
    '    Me.ObjectDataSource1.DataSource = _butTrans
    'End Sub
    Sub SetObjectDatasource(_planInter As List(Of plan_intervention2))
        Me.ObjectDataSource1.DataSource = _planInter

        _reportManager.FormatFieldsH2(Me.lblPlanTransitionTitre2)
        '_reportManager.FormatFieldsBold(Me.lblDateConsultations2, Me.lblMoyenConsultation2, Me.lblResultatsConsultation2)
    End Sub
    Sub SetDataSource(lst As List(Of boButTransition))
        Dim _sql As String = "SELECT   buts_transition2.BUTS_TRANSITION, buts_transition2.DESCRIPTION, buts_transition2.NO_ORDER AS BUT_NO_ORDER, plan_intervention2.NO_ORDER AS NO_ORDER, " & _
        " plan_intervention2.MESURES, plan_intervention2.RESPONSABLES, plan_intervention2.ECHEANCIER, buts_transition2.FK_ELEVE_ID " & _
        " FROM buts_transition2 INNER JOIN " & _
        " plan_intervention2 ON buts_transition2.BUTS_TRANSITION = plan_intervention2.FK_BUT_TRANSITION_ID " & _
        " WHERE        (buts_transition2.FK_ELEVE_ID = '" & _eleveid & "')"


        'Me.SqlDataSourcePlanTransition2.SelectCommand = _sql
        'Me.DataSource = 
        Me.ObjectDataSource1.DataSource = lst

        '_reportManager.FormatFieldsH2(Me.lblPlanTransitionTitre2, Me.lblMesuresNecessairesTitre2)
        '_reportManager.FormatFieldsBold(Me.lblDateConsultations2, Me.lblMoyenConsultation2, Me.lblResultatsConsultation2)

        'Me.TextBoxButTransition2.Value = lst.FirstOrDefault.description

    End Sub
    Sub SetDatasourcePT(ByVal lst As List(Of plan_intervention2))
        Me.ObjectDataSource1.DataSource = lst
        _reportManager.FormatFieldH2(Me.lblPlanTransitionTitre2)
        '_reportManager.FormatFieldH3(Me.TextBoxButTransition2)
        ' _reportManager.FormatFieldH4(Me.lblPlanIntervention2)
    End Sub
End Class