﻿Imports Telerik.Reporting.Processing
Partial Class printpeirep
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click
        Dim rep As New peireport.pei
        'Dim rep2 As New peireport.pei


        '0001162055"
        '"0001180704"
        '0001180704
        Dim peiname As String = "Jacob-Lessard"

        rep.peiname = Trim(peiname) & "_0001226619"
        rep.annee_scolaire = "2017-2018"
        rep.donnees_generales = Me.CheckBoxList1.Items.FindByValue("donnees_generales").Selected
        rep.donnees_evaluations = Me.CheckBoxList1.Items.FindByValue("donnees_evaluations").Selected
        rep.forces_besoins = Me.CheckBoxList1.Items.FindByValue("forces_besoins").Selected
        rep.matieres = Me.CheckBoxList1.Items.FindByValue("matieres").Selected

        rep.competences_habiletes = Me.CheckBoxList1.Items.FindByValue("adaptations").Selected
        rep.adaptations = Me.CheckBoxList1.Items.FindByValue("adaptations").Selected

        rep.ress_humaines = Me.CheckBoxList1.Items.FindByValue("ress_humaines").Selected

        rep.buts = Me.CheckBoxList1.Items.FindByValue("buts").Selected


        rep.plan_transition = Me.CheckBoxList1.Items.FindByValue("plan_transition").Selected
        rep.tests_provinciaux = Me.CheckBoxList1.Items.FindByValue("plan_transition").Selected

        rep.page_signature = False

        rep.SetDataSource("0001226619")

        ExportToPDF(rep, rep.peiname)
    End Sub
    Sub ExportToPDF(ByVal reportToExport As Telerik.Reporting.Report, docname As String)
        Dim reportProcessor As New Telerik.Reporting.Processing.ReportProcessor()
        'Dim result As RenderingResult = reportProcessor.RenderReport("PDF", reportToExport, Nothing)
        Dim result As RenderingResult = reportProcessor.RenderReport("PDF", reportToExport, Nothing)

        'Dim fileName As String = result.DocumentName + ".pdf"
        Dim fileName As String = docname + ".pdf"

        Response.Clear()
        Response.ContentType = result.MimeType
        Response.Cache.SetCacheability(HttpCacheability.Private)
        Response.Expires = -1
        Response.Buffer = True
        Response.AddHeader("Content-Disposition", String.Format("{0};FileName=""{1}""", "attachment", fileName))
        Response.BinaryWrite(result.DocumentBytes)
        Response.End()
    End Sub
End Class
