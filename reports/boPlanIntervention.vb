﻿<Serializable()>
Public Class boPlanIntervention
    Private _but_transition_id As Integer
    Private _no_order As Integer
    Private _mesures As String
    Private _responsables As String
    Private _echeancier As String

    Public Property but_transition_id As Integer
        Get
            Return _but_transition_id
        End Get
        Set(value As Integer)
            Me._but_transition_id = value
        End Set
    End Property

    Private _But_Transition As boButTransition
    Public Overridable Property but_transition As boButTransition
        Get
            Return Me._But_Transition
        End Get
        Set(ByVal value As boButTransition)
            Me._but_transition = value
        End Set
    End Property

    Public Property no_order As Integer
        Get
            Return _no_order
        End Get
        Set(value As Integer)
            Me._no_order = value
        End Set
    End Property

    Public Property mesures As String
        Get
            Return _mesures
        End Get
        Set(value As String)
            Me._mesures = value
        End Set
    End Property

    Public Property responsables As String
        Get
            Return _responsables
        End Get
        Set(value As String)
            Me._responsables = value
        End Set
    End Property

    Public Property echeancier As String
        Get
            Return _echeancier
        End Get
        Set(value As String)
            Me._echeancier = value
        End Set
    End Property
End Class
