﻿Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms
Imports Telerik.Reporting
Imports Telerik.Reporting.Drawing

Imports System.Drawing.Imaging
Imports System.Runtime.CompilerServices
Imports System.Collections.Specialized
Imports System.Web
Imports System.Drawing.Drawing2D



Public Class ReportManager

#Region "Formattage"
    Public Sub FormatColumnTitles(ParamArray txtColumnTitles() As Telerik.Reporting.TextBox)
        Array.ForEach(txtColumnTitles, Sub(txtColumnTitle) FormatColumnTitle(txtColumnTitle))
    End Sub
    Public Sub FormatColumnTitlesVertical(ByVal ParamArray txtColumnTitles() As Telerik.Reporting.TextBox)
        Array.ForEach(txtColumnTitles, Sub(txtColumnTitle) FormatColumnTitleVertical(txtColumnTitle))
    End Sub

    Public Sub HidePicture(ByVal ParamArray picturesBox() As Telerik.Reporting.PictureBox)
        Array.ForEach(picturesBox, Sub(picturebox) HidePicture(picturebox))
    End Sub
    Public Sub HidePicture(ByVal pictureBox As Telerik.Reporting.PictureBox)
        pictureBox.Visible = False
    End Sub
    Public Sub FormatColumnTitle(ByVal txtColumn As Telerik.Reporting.TextBox)
        txtColumn.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid
        txtColumn.Style.Font.Bold = True
        txtColumn.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.0R)
        txtColumn.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        txtColumn.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        txtColumn.Style.BackgroundColor = System.Drawing.Color.Lavender
    End Sub
    Public Sub FormatColumnTitleVertical(ByVal txtColumn As Telerik.Reporting.TextBox)
        txtColumn.Angle = 270
        txtColumn.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0.3)
        txtColumn.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid
        txtColumn.Style.Font.Bold = True
        txtColumn.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.0R)
        txtColumn.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        txtColumn.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        txtColumn.Style.BackgroundColor = System.Drawing.Color.Lavender
    End Sub
    Public Sub FormatFields(ParamArray txtFields() As Telerik.Reporting.TextBox)
        Array.ForEach(txtFields, Sub(txtField) FormatField(txtField))
    End Sub
    Public Sub FormatFieldsH1(ParamArray txtFields() As Telerik.Reporting.TextBox)
        Array.ForEach(txtFields, Sub(txtField) FormatFieldH1(txtField))
    End Sub
    Public Sub FormatFieldsH2(ParamArray txtFields() As Telerik.Reporting.TextBox)
        Array.ForEach(txtFields, Sub(txtField) FormatFieldH2(txtField))
    End Sub
    Public Sub FormatFieldsBold(ParamArray txtFields() As Telerik.Reporting.TextBox)
        Array.ForEach(txtFields, Sub(txtField) FormatFieldBold(txtField))
    End Sub
    Public Sub FormatFieldsItalic(ParamArray txtFields() As Telerik.Reporting.TextBox)
        Array.ForEach(txtFields, Sub(txtField) FormatFieldItalic(txtField))
    End Sub
    Public Sub FormatPicturesBoxField(ByVal ParamArray picturesBox() As Telerik.Reporting.PictureBox)
        Array.ForEach(picturesBox, Sub(imgPictureBox) FormatPictureBoxField(imgPictureBox))
    End Sub
    Public Sub FormatPictureBoxField(ByVal imgPictureBox As Telerik.Reporting.PictureBox)
        imgPictureBox.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid
        imgPictureBox.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        imgPictureBox.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        'imgPictureBox.Style.TextAlign =HorizontalAlign.Center
        imgPictureBox.Style.VerticalAlign = VerticalAlign.Middle
        imgPictureBox.Sizing = ImageSizeMode.Center

        'imgPictureBox.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0.25)
        'imgPictureBox.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.25)
    End Sub

    Public Sub FormatField(ByVal txtField As Telerik.Reporting.TextBox)
        'txtField.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid
        txtField.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.0R)
        txtField.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        txtField.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
    End Sub
    Public Sub FormatFieldH1(ByVal txtField As Telerik.Reporting.TextBox)
        'txtField.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid
        txtField.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(22.0R)
        txtField.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        txtField.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
    End Sub
    Public Sub FormatFieldH2(ByVal txtField As Telerik.Reporting.TextBox)
        'txtField.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid
        txtField.Style.Font.Bold = True
        txtField.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12.0R)
        txtField.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        txtField.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
    End Sub
    Public Sub FormatFieldH3(ByVal txtField As Telerik.Reporting.TextBox)
        'txtField.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid
        txtField.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14.0R)
        txtField.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        txtField.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
    End Sub
    Public Sub FormatFieldH4(ByVal txtField As Telerik.Reporting.TextBox)
        'txtField.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid
        txtField.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12.0R)
        txtField.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        txtField.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
    End Sub
    Public Sub FormatFieldItalic(ByVal txtField As Telerik.Reporting.TextBox)
        'txtField.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid
        txtField.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.0R)
        txtField.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        txtField.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        txtField.Style.Font.Italic = True
    End Sub
    Public Sub FormatFieldBold(ByVal txtField As Telerik.Reporting.TextBox)
        'txtField.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid
        txtField.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.0R)
        txtField.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        txtField.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        txtField.Style.Font.Bold = True
    End Sub


    Public Sub InitializeGridFormat(ByVal txtColumnDateEvaluation As Telerik.Reporting.TextBox, _
                  ByVal txtFieldDateEvaluation As Telerik.Reporting.TextBox, _
                    ByVal txtColumnAnneeEtude As Telerik.Reporting.TextBox, _
                  ByVal txtFieldAnneeEtude As Telerik.Reporting.TextBox, _
                  Optional ByVal evaluationName As String = "")

        If Not String.IsNullOrWhiteSpace(evaluationName) Then evaluationName = evaluationName & "."

        '
        '
        'Date_Evaluation
        '
        txtFieldDateEvaluation.Format = "{0:dd MMM yy}"
        FormatField(txtFieldDateEvaluation)
        txtFieldDateEvaluation.Value = "= Fields." + evaluationName + "Date_Evaluation"
        '
        'Année Étude
        '
        FormatField(txtFieldAnneeEtude)
        txtFieldAnneeEtude.Value = "= Fields." + evaluationName + "Niveau_Etude"
        '
        '
        'Column 3 : Date"
        '
        FormatColumnTitle(txtColumnDateEvaluation)
        txtColumnDateEvaluation.Value = "Date"
        '
        'Column 4 : A/É
        '
        FormatColumnTitle(txtColumnAnneeEtude)
        txtColumnAnneeEtude.Value = "A/É*"
    End Sub
#End Region
End Class


