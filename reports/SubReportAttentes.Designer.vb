Partial Class SubReportAttentes
    
    'NOTE: The following procedure is required by the telerik Reporting Designer
    'It can be modified using the telerik Reporting Designer.  
    'Do not modify it using the code editor.
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SubReportAttentes))
        Dim ReportParameter1 As Telerik.Reporting.ReportParameter = New Telerik.Reporting.ReportParameter()
        Me.pageHeaderSection1 = New Telerik.Reporting.PageHeaderSection()
        Me.detail = New Telerik.Reporting.DetailSection()
        Me.ObjectDataSourceAttentes = New Telerik.Reporting.ObjectDataSource()
        Me.pageFooterSection1 = New Telerik.Reporting.PageFooterSection()
        Me.Group1 = New Telerik.Reporting.Group()
        Me.GroupFooterSection1 = New Telerik.Reporting.GroupFooterSection()
        Me.lblCommentaires = New Telerik.Reporting.TextBox()
        Me.TextBoxCommentaires = New Telerik.Reporting.TextBox()
        Me.GroupHeaderSection1 = New Telerik.Reporting.GroupHeaderSection()
        Me.TextBox1 = New Telerik.Reporting.TextBox()
        Me.lblMatiereCoursProgramme = New Telerik.Reporting.TextBox()
        Me.TextBoxMatiereCoursProgramme = New Telerik.Reporting.TextBox()
        Me.lblAnneEtude = New Telerik.Reporting.TextBox()
        Me.TextBoxAnneeEtude = New Telerik.Reporting.TextBox()
        Me.TextBox6 = New Telerik.Reporting.TextBox()
        Me.TextBoxNiveauRendement = New Telerik.Reporting.TextBox()
        Me.lblButInfo = New Telerik.Reporting.TextBox()
        Me.TextBox7 = New Telerik.Reporting.TextBox()
        Me.TextBox5 = New Telerik.Reporting.TextBox()
        Me.TextBox4 = New Telerik.Reporting.TextBox()
        Me.TextBox2 = New Telerik.Reporting.TextBox()
        Me.TextBox10 = New Telerik.Reporting.TextBox()
        Me.TextBox11 = New Telerik.Reporting.TextBox()
        Me.lblButEntete = New Telerik.Reporting.TextBox()
        Me.Shape2 = New Telerik.Reporting.Shape()
        Me.Group2Attentes = New Telerik.Reporting.Group()
        Me.GroupFooterSection2 = New Telerik.Reporting.GroupFooterSection()
        Me.GroupHeaderSection2 = New Telerik.Reporting.GroupHeaderSection()
        Me.TextBox8 = New Telerik.Reporting.TextBox()
        Me.TextBox9 = New Telerik.Reporting.TextBox()
        Me.TextBox3 = New Telerik.Reporting.TextBox()
        Me.Shape1 = New Telerik.Reporting.Shape()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'pageHeaderSection1
        '
        Me.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.20000000298023224R)
        Me.pageHeaderSection1.Name = "pageHeaderSection1"
        '
        'detail
        '
        Me.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(0.20000007748603821R)
        Me.detail.Name = "detail"
        '
        'ObjectDataSourceAttentes
        '
        Me.ObjectDataSourceAttentes.DataSource = GetType(peireport.ATTENTE)
        Me.ObjectDataSourceAttentes.Name = "ObjectDataSourceAttentes"
        '
        'pageFooterSection1
        '
        Me.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.30000025033950806R)
        Me.pageFooterSection1.Name = "pageFooterSection1"
        '
        'Group1
        '
        Me.Group1.GroupFooter = Me.GroupFooterSection1
        Me.Group1.GroupHeader = Me.GroupHeaderSection1
        Me.Group1.Groupings.AddRange(New Telerik.Reporting.Data.Grouping() {New Telerik.Reporting.Data.Grouping("=Fields.BUT.but_titre")})
        Me.Group1.Name = "Group1"
        '
        'GroupFooterSection1
        '
        Me.GroupFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(1.8999994993209839R)
        Me.GroupFooterSection1.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.lblCommentaires, Me.TextBoxCommentaires})
        Me.GroupFooterSection1.Name = "GroupFooterSection1"
        Me.GroupFooterSection1.PageBreak = Telerik.Reporting.PageBreak.After
        '
        'lblCommentaires
        '
        Me.lblCommentaires.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.099999949336051941R), Telerik.Reporting.Drawing.Unit.Cm(0.19999989867210388R))
        Me.lblCommentaires.Name = "lblCommentaires"
        Me.lblCommentaires.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.9999990463256836R), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634R))
        Me.lblCommentaires.Value = "Commentaires"
        '
        'TextBoxCommentaires
        '
        Me.TextBoxCommentaires.CanShrink = True
        Me.TextBoxCommentaires.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.099999949336051941R), Telerik.Reporting.Drawing.Unit.Cm(0.80019986629486084R))
        Me.TextBoxCommentaires.Name = "TextBoxCommentaires"
        Me.TextBoxCommentaires.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.499998092651367R), Telerik.Reporting.Drawing.Unit.Cm(0.89979970455169678R))
        Me.TextBoxCommentaires.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.TextBoxCommentaires.Value = resources.GetString("TextBoxCommentaires.Value")
        '
        'GroupHeaderSection1
        '
        Me.GroupHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(6.8000006675720215R)
        Me.GroupHeaderSection1.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.TextBox1, Me.lblMatiereCoursProgramme, Me.TextBoxMatiereCoursProgramme, Me.lblAnneEtude, Me.TextBoxAnneeEtude, Me.TextBox6, Me.TextBoxNiveauRendement, Me.lblButInfo, Me.TextBox7, Me.TextBox5, Me.TextBox4, Me.TextBox2, Me.TextBox10, Me.TextBox11, Me.lblButEntete, Me.Shape2})
        Me.GroupHeaderSection1.KeepTogether = False
        Me.GroupHeaderSection1.Name = "GroupHeaderSection1"
        Me.GroupHeaderSection1.PageBreak = Telerik.Reporting.PageBreak.None
        '
        'TextBox1
        '
        Me.TextBox1.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.099999949336051941R), Telerik.Reporting.Drawing.Unit.Cm(3.8000001907348633R))
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.499998092651367R), Telerik.Reporting.Drawing.Unit.Cm(0.59999990463256836R))
        Me.TextBox1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox1.Style.Font.Bold = True
        Me.TextBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11.0R)
        Me.TextBox1.Value = "= Fields.BUT.but_titre"
        '
        'lblMatiereCoursProgramme
        '
        Me.lblMatiereCoursProgramme.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.099999949336051941R), Telerik.Reporting.Drawing.Unit.Cm(0.7999998927116394R))
        Me.lblMatiereCoursProgramme.Name = "lblMatiereCoursProgramme"
        Me.lblMatiereCoursProgramme.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.1997995376586914R), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448R))
        Me.lblMatiereCoursProgramme.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.lblMatiereCoursProgramme.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.lblMatiereCoursProgramme.Style.Font.Bold = True
        Me.lblMatiereCoursProgramme.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.lblMatiereCoursProgramme.Value = "Mati�re, cours au programme"
        '
        'TextBoxMatiereCoursProgramme
        '
        Me.TextBoxMatiereCoursProgramme.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.099999949336051941R), Telerik.Reporting.Drawing.Unit.Cm(1.2000000476837158R))
        Me.TextBoxMatiereCoursProgramme.Name = "TextBoxMatiereCoursProgramme"
        Me.TextBoxMatiereCoursProgramme.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.1997995376586914R), Telerik.Reporting.Drawing.Unit.Cm(1.0R))
        Me.TextBoxMatiereCoursProgramme.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBoxMatiereCoursProgramme.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None
        Me.TextBoxMatiereCoursProgramme.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.TextBoxMatiereCoursProgramme.Value = "= Fields.BUT.programme"
        '
        'lblAnneEtude
        '
        Me.lblAnneEtude.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.099999949336051941R), Telerik.Reporting.Drawing.Unit.Cm(2.1999998092651367R))
        Me.lblAnneEtude.Name = "lblAnneEtude"
        Me.lblAnneEtude.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.1997995376586914R), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448R))
        Me.lblAnneEtude.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.lblAnneEtude.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.lblAnneEtude.Style.Font.Bold = True
        Me.lblAnneEtude.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.lblAnneEtude.Value = "Ann�e d'�tude au sein du curriculum"
        '
        'TextBoxAnneeEtude
        '
        Me.TextBoxAnneeEtude.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.099999949336051941R), Telerik.Reporting.Drawing.Unit.Cm(2.6000003814697266R))
        Me.TextBoxAnneeEtude.Name = "TextBoxAnneeEtude"
        Me.TextBoxAnneeEtude.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.1997995376586914R), Telerik.Reporting.Drawing.Unit.Cm(0.60000008344650269R))
        Me.TextBoxAnneeEtude.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBoxAnneeEtude.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBoxAnneeEtude.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None
        Me.TextBoxAnneeEtude.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.TextBoxAnneeEtude.Value = "= Fields.BUT.annee_etude"
        '
        'TextBox6
        '
        Me.TextBox6.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.2999997138977051R), Telerik.Reporting.Drawing.Unit.Cm(0.79979985952377319R))
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.299999237060547R), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448R))
        Me.TextBox6.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox6.Style.Font.Bold = True
        Me.TextBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.TextBox6.Value = "Niveau de rendement actuel"
        '
        'TextBoxNiveauRendement
        '
        Me.TextBoxNiveauRendement.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.2999997138977051R), Telerik.Reporting.Drawing.Unit.Cm(1.2000001668930054R))
        Me.TextBoxNiveauRendement.Name = "TextBoxNiveauRendement"
        Me.TextBoxNiveauRendement.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.299999237060547R), Telerik.Reporting.Drawing.Unit.Cm(2.0000002384185791R))
        Me.TextBoxNiveauRendement.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBoxNiveauRendement.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None
        Me.TextBoxNiveauRendement.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.TextBoxNiveauRendement.Value = "= Fields.BUT.niveau_rendement"
        '
        'lblButInfo
        '
        Me.lblButInfo.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.099999949336051941R), Telerik.Reporting.Drawing.Unit.Cm(3.2999999523162842R))
        Me.lblButInfo.Name = "lblButInfo"
        Me.lblButInfo.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.499998092651367R), Telerik.Reporting.Drawing.Unit.Cm(0.40000024437904358R))
        Me.lblButInfo.Style.Font.Italic = True
        Me.lblButInfo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.lblButInfo.Value = "But(s) annuel(s): Il s'agit d'un �nonc� qui d�crit ce qu'un �l�ve devrait raisonn" & _
    "ablement avoir accompli � la fin de l'ann�e scolaire dans une"
        '
        'TextBox7
        '
        Me.TextBox7.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.20000000298023224R), Telerik.Reporting.Drawing.Unit.Cm(5.3000001907348633R))
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.1056323051452637R), Telerik.Reporting.Drawing.Unit.Cm(1.2999997138977051R))
        Me.TextBox7.Style.Font.Italic = True
        Me.TextBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.TextBox7.Value = "(�num�rer les attentes modifi�es ou diff�rentes qui d�crivent les connaissances e" & _
    "t les habilet�s � �valuer � chaque �tape du bulletin. Indiquer l'ann�e d'�tudes," & _
    " le cas �ch�ant)."
        '
        'TextBox5
        '
        Me.TextBox5.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.59999942779541R), Telerik.Reporting.Drawing.Unit.Cm(4.5999999046325684R))
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.0R), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634R))
        Me.TextBox5.Style.Font.Bold = True
        Me.TextBox5.Style.Font.Italic = True
        Me.TextBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.TextBox5.Value = "M�thodes d'�valuation"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.5000004768371582R), Telerik.Reporting.Drawing.Unit.Cm(4.5999999046325684R))
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.8999996185302734R), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634R))
        Me.TextBox4.Style.Font.Bold = True
        Me.TextBox4.Style.Font.Italic = True
        Me.TextBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.TextBox4.Value = "Strat�gies p�dagogiques"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.20583368837833405R), Telerik.Reporting.Drawing.Unit.Cm(4.5999999046325684R))
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.0997991561889648R), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634R))
        Me.TextBox2.Style.Font.Bold = True
        Me.TextBox2.Style.Font.Italic = True
        Me.TextBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.TextBox2.Value = "Attentes d'apprentissage"
        '
        'TextBox10
        '
        Me.TextBox10.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.5000004768371582R), Telerik.Reporting.Drawing.Unit.Cm(5.3000001907348633R))
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.90000057220459R), Telerik.Reporting.Drawing.Unit.Cm(1.2999997138977051R))
        Me.TextBox10.Style.Font.Italic = True
        Me.TextBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.TextBox10.Value = "(�num�rer les strat�gies qui sont sp�cifiques � l'�l�ve et celles qui se rapporte" & _
    "nt aux attentes d'apprentissage)."
        '
        'TextBox11
        '
        Me.TextBox11.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.59999942779541R), Telerik.Reporting.Drawing.Unit.Cm(5.3000001907348633R))
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.0R), Telerik.Reporting.Drawing.Unit.Cm(1.2999997138977051R))
        Me.TextBox11.Style.Font.Italic = True
        Me.TextBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.TextBox11.Value = "(Indiquer la m�thode d'�valuation � utiliser pour chaque attente d'apprentissage)" & _
    "."
        '
        'lblButEntete
        '
        Me.lblButEntete.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.099999949336051941R), Telerik.Reporting.Drawing.Unit.Cm(0.20000000298023224R))
        Me.lblButEntete.Name = "lblButEntete"
        Me.lblButEntete.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.499996185302734R), Telerik.Reporting.Drawing.Unit.Cm(0.59999990463256836R))
        Me.lblButEntete.Value = "PROGRAMMES D'ENSEIGNEMENT � L'ENFANCE EN DIFFICULT�"
        '
        'Shape2
        '
        Me.Shape2.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.29104167222976685R), Telerik.Reporting.Drawing.Unit.Cm(6.7000002861022949R))
        Me.Shape2.Name = "Shape2"
        Me.Shape2.ShapeType = New Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW)
        Me.Shape2.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.399999618530273R), Telerik.Reporting.Drawing.Unit.Cm(0.10000000149011612R))
        '
        'Group2Attentes
        '
        Me.Group2Attentes.GroupFooter = Me.GroupFooterSection2
        Me.Group2Attentes.GroupHeader = Me.GroupHeaderSection2
        Me.Group2Attentes.Groupings.AddRange(New Telerik.Reporting.Data.Grouping() {New Telerik.Reporting.Data.Grouping("=Fields.attente1")})
        Me.Group2Attentes.Name = "Group2Attentes"
        '
        'GroupFooterSection2
        '
        Me.GroupFooterSection2.Height = Telerik.Reporting.Drawing.Unit.Cm(0.19999989867210388R)
        Me.GroupFooterSection2.Name = "GroupFooterSection2"
        '
        'GroupHeaderSection2
        '
        Me.GroupHeaderSection2.Height = Telerik.Reporting.Drawing.Unit.Cm(1.0000001192092896R)
        Me.GroupHeaderSection2.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.TextBox8, Me.TextBox9, Me.TextBox3, Me.Shape1})
        Me.GroupHeaderSection2.KeepTogether = True
        Me.GroupHeaderSection2.Name = "GroupHeaderSection2"
        '
        'TextBox8
        '
        Me.TextBox8.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.20000000298023224R), Telerik.Reporting.Drawing.Unit.Cm(0.099999144673347473R))
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.0997991561889648R), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634R))
        Me.TextBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.TextBox8.Value = "= Fields.attente1"
        '
        'TextBox9
        '
        Me.TextBox9.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.5R), Telerik.Reporting.Drawing.Unit.Cm(0.099999144673347473R))
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.8999996185302734R), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611R))
        Me.TextBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.TextBox9.Value = "= Fields.strategie"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.59999942779541R), Telerik.Reporting.Drawing.Unit.Cm(0.099999949336051941R))
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9999985694885254R), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611R))
        Me.TextBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.TextBox3.Value = "= Fields.methode"
        '
        'Shape1
        '
        Me.Shape1.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.20000000298023224R), Telerik.Reporting.Drawing.Unit.Cm(0.79999959468841553R))
        Me.Shape1.Name = "Shape1"
        Me.Shape1.ShapeType = New Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW)
        Me.Shape1.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.399999618530273R), Telerik.Reporting.Drawing.Unit.Cm(0.10000000149011612R))
        '
        'SubReportAttentes
        '
        Me.DataSource = Me.ObjectDataSourceAttentes
        Me.Groups.AddRange(New Telerik.Reporting.Group() {Me.Group1, Me.Group2Attentes})
        Me.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.GroupHeaderSection1, Me.GroupFooterSection1, Me.GroupHeaderSection2, Me.GroupFooterSection2, Me.pageHeaderSection1, Me.detail, Me.pageFooterSection1})
        Me.Name = "SubReportAttentes"
        Me.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Mm(5.0R)
        Me.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Mm(5.0R)
        Me.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Mm(5.0R)
        Me.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Mm(5.0R)
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4
        ReportParameter1.Name = "etape"
        Me.ReportParameters.Add(ReportParameter1)
        Me.Style.BackgroundColor = System.Drawing.Color.White
        Me.Width = Telerik.Reporting.Drawing.Unit.Cm(19.005632400512695R)
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents pageHeaderSection1 As Telerik.Reporting.PageHeaderSection
    Friend WithEvents detail As Telerik.Reporting.DetailSection
    Friend WithEvents pageFooterSection1 As Telerik.Reporting.PageFooterSection
    Friend WithEvents ObjectDataSourceAttentes As Telerik.Reporting.ObjectDataSource
    Friend WithEvents Group1 As Telerik.Reporting.Group
    Friend WithEvents GroupFooterSection1 As Telerik.Reporting.GroupFooterSection
    Friend WithEvents GroupHeaderSection1 As Telerik.Reporting.GroupHeaderSection
    Friend WithEvents TextBox1 As Telerik.Reporting.TextBox
    Friend WithEvents Group2Attentes As Telerik.Reporting.Group
    Friend WithEvents GroupFooterSection2 As Telerik.Reporting.GroupFooterSection
    Friend WithEvents GroupHeaderSection2 As Telerik.Reporting.GroupHeaderSection
    Friend WithEvents TextBox8 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox9 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox3 As Telerik.Reporting.TextBox
    Friend WithEvents lblMatiereCoursProgramme As Telerik.Reporting.TextBox
    Friend WithEvents TextBoxMatiereCoursProgramme As Telerik.Reporting.TextBox
    Friend WithEvents lblAnneEtude As Telerik.Reporting.TextBox
    Friend WithEvents TextBoxAnneeEtude As Telerik.Reporting.TextBox
    Friend WithEvents TextBox6 As Telerik.Reporting.TextBox
    Friend WithEvents TextBoxNiveauRendement As Telerik.Reporting.TextBox
    Friend WithEvents lblCommentaires As Telerik.Reporting.TextBox
    Friend WithEvents TextBoxCommentaires As Telerik.Reporting.TextBox
    Friend WithEvents lblButInfo As Telerik.Reporting.TextBox
    Friend WithEvents Shape1 As Telerik.Reporting.Shape
    Friend WithEvents TextBox7 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox5 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox4 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox2 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox10 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox11 As Telerik.Reporting.TextBox
    Friend WithEvents lblButEntete As Telerik.Reporting.TextBox
    Friend WithEvents Shape2 As Telerik.Reporting.Shape
End Class