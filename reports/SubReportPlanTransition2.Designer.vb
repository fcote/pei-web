Partial Class SubReportPlanTransition2
    
    'NOTE: The following procedure is required by the telerik Reporting Designer
    'It can be modified using the telerik Reporting Designer.  
    'Do not modify it using the code editor.
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SubReportPlanTransition2))
        Me.detail = New Telerik.Reporting.DetailSection()
        Me.pageFooterSection1 = New Telerik.Reporting.PageFooterSection()
        Me.GroupButTransition = New Telerik.Reporting.Group()
        Me.GroupFooterSection1 = New Telerik.Reporting.GroupFooterSection()
        Me.GroupHeaderSectionButTransition = New Telerik.Reporting.GroupHeaderSection()
        Me.TextBoxButTransition2 = New Telerik.Reporting.TextBox()
        Me.Shape1 = New Telerik.Reporting.Shape()
        Me.lblResponsable = New Telerik.Reporting.TextBox()
        Me.TextBox1 = New Telerik.Reporting.TextBox()
        Me.lblEcheancier = New Telerik.Reporting.TextBox()
        Me.SqlDataSourcePlanTransition2 = New Telerik.Reporting.SqlDataSource()
        Me.GroupPlanIntervention = New Telerik.Reporting.Group()
        Me.GroupFooterSection2 = New Telerik.Reporting.GroupFooterSection()
        Me.GroupHeaderPlanIntervention = New Telerik.Reporting.GroupHeaderSection()
        Me.TextBoxPersResponsables2 = New Telerik.Reporting.TextBox()
        Me.TextBoxMesureNecessaires2 = New Telerik.Reporting.TextBox()
        Me.TextBoxEcheance = New Telerik.Reporting.TextBox()
        Me.ObjectDataSource1 = New Telerik.Reporting.ObjectDataSource()
        Me.PageHeaderSection1 = New Telerik.Reporting.PageHeaderSection()
        Me.lblPlanTransitionTitre2 = New Telerik.Reporting.TextBox()
        Me.Group1Titre = New Telerik.Reporting.Group()
        Me.GroupFooterSection3 = New Telerik.Reporting.GroupFooterSection()
        Me.GroupHeaderSection1 = New Telerik.Reporting.GroupHeaderSection()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'detail
        '
        Me.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(0.19999989867210388R)
        Me.detail.Name = "detail"
        '
        'pageFooterSection1
        '
        Me.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.19999989867210388R)
        Me.pageFooterSection1.Name = "pageFooterSection1"
        '
        'GroupButTransition
        '
        Me.GroupButTransition.GroupFooter = Me.GroupFooterSection1
        Me.GroupButTransition.GroupHeader = Me.GroupHeaderSectionButTransition
        Me.GroupButTransition.Groupings.AddRange(New Telerik.Reporting.Data.Grouping() {New Telerik.Reporting.Data.Grouping("=Fields.MESURES_TRANSITION.but_transition_id")})
        Me.GroupButTransition.GroupKeepTogether = Telerik.Reporting.GroupKeepTogether.All
        Me.GroupButTransition.Name = "GroupButTransition"
        '
        'GroupFooterSection1
        '
        Me.GroupFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.19999989867210388R)
        Me.GroupFooterSection1.Name = "GroupFooterSection1"
        '
        'GroupHeaderSectionButTransition
        '
        Me.GroupHeaderSectionButTransition.Height = Telerik.Reporting.Drawing.Unit.Cm(1.7999999523162842R)
        Me.GroupHeaderSectionButTransition.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.TextBoxButTransition2, Me.Shape1, Me.lblResponsable, Me.TextBox1, Me.lblEcheancier})
        Me.GroupHeaderSectionButTransition.Name = "GroupHeaderSectionButTransition"
        '
        'TextBoxButTransition2
        '
        Me.TextBoxButTransition2.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.19999989867210388R), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448R))
        Me.TextBoxButTransition2.Name = "TextBoxButTransition2"
        Me.TextBoxButTransition2.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.0R), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448R))
        Me.TextBoxButTransition2.Style.Font.Bold = True
        Me.TextBoxButTransition2.Value = "= Fields.MESURES_TRANSITION.BUTSTRANSITION.BUT_DESCRIPTION"
        '
        'Shape1
        '
        Me.Shape1.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.20000000298023224R), Telerik.Reporting.Drawing.Unit.Cm(0.099999949336051941R))
        Me.Shape1.Name = "Shape1"
        Me.Shape1.ShapeType = New Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW)
        Me.Shape1.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.0R), Telerik.Reporting.Drawing.Unit.Cm(0.10000000149011612R))
        '
        'lblResponsable
        '
        Me.lblResponsable.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.20000000298023224R), Telerik.Reporting.Drawing.Unit.Cm(0.85000002384185791R))
        Me.lblResponsable.Name = "lblResponsable"
        Me.lblResponsable.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6000001430511475R), Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209R))
        Me.lblResponsable.Style.Font.Bold = True
        Me.lblResponsable.Style.Font.Italic = True
        Me.lblResponsable.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.lblResponsable.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        Me.lblResponsable.Value = "Personnes responsable de ces mesures"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.0999999046325684R), Telerik.Reporting.Drawing.Unit.Cm(0.85000002384185791R))
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.899999618530273R), Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209R))
        Me.TextBox1.Style.Font.Bold = True
        Me.TextBox1.Style.Font.Italic = True
        Me.TextBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.TextBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        Me.TextBox1.Value = "Mesure nécessaires"
        '
        'lblEcheancier
        '
        Me.lblEcheancier.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.30000114440918R), Telerik.Reporting.Drawing.Unit.Cm(1.0677083730697632R))
        Me.lblEcheancier.Name = "lblEcheancier"
        Me.lblEcheancier.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9495890140533447R), Telerik.Reporting.Drawing.Unit.Cm(0.60000008344650269R))
        Me.lblEcheancier.Style.Font.Bold = True
        Me.lblEcheancier.Style.Font.Italic = True
        Me.lblEcheancier.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.lblEcheancier.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        Me.lblEcheancier.Value = "Échéancier"
        '
        'SqlDataSourcePlanTransition2
        '
        Me.SqlDataSourcePlanTransition2.ConnectionString = "peireport.My.MySettings.pei"
        Me.SqlDataSourcePlanTransition2.Name = "SqlDataSourcePlanTransition2"
        Me.SqlDataSourcePlanTransition2.SelectCommand = resources.GetString("SqlDataSourcePlanTransition2.SelectCommand")
        '
        'GroupPlanIntervention
        '
        Me.GroupPlanIntervention.GroupFooter = Me.GroupFooterSection2
        Me.GroupPlanIntervention.GroupHeader = Me.GroupHeaderPlanIntervention
        Me.GroupPlanIntervention.Groupings.AddRange(New Telerik.Reporting.Data.Grouping() {New Telerik.Reporting.Data.Grouping("= Fields.MESURES")})
        Me.GroupPlanIntervention.Name = "GroupPlanIntervention"
        '
        'GroupFooterSection2
        '
        Me.GroupFooterSection2.Height = Telerik.Reporting.Drawing.Unit.Cm(0.20000030100345612R)
        Me.GroupFooterSection2.Name = "GroupFooterSection2"
        '
        'GroupHeaderPlanIntervention
        '
        Me.GroupHeaderPlanIntervention.Height = Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791R)
        Me.GroupHeaderPlanIntervention.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.TextBoxPersResponsables2, Me.TextBoxMesureNecessaires2, Me.TextBoxEcheance})
        Me.GroupHeaderPlanIntervention.Name = "GroupHeaderPlanIntervention"
        '
        'TextBoxPersResponsables2
        '
        Me.TextBoxPersResponsables2.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.20000000298023224R), Telerik.Reporting.Drawing.Unit.Cm(0.099999949336051941R))
        Me.TextBoxPersResponsables2.Name = "TextBoxPersResponsables2"
        Me.TextBoxPersResponsables2.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6000001430511475R), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448R))
        Me.TextBoxPersResponsables2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.TextBoxPersResponsables2.Value = "= Fields.RESPONSABLES"
        '
        'TextBoxMesureNecessaires2
        '
        Me.TextBoxMesureNecessaires2.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.0999999046325684R), Telerik.Reporting.Drawing.Unit.Cm(0.099999949336051941R))
        Me.TextBoxMesureNecessaires2.Name = "TextBoxMesureNecessaires2"
        Me.TextBoxMesureNecessaires2.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.899998664855957R), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448R))
        Me.TextBoxMesureNecessaires2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.TextBoxMesureNecessaires2.Value = "= Fields.MESURES"
        '
        'TextBoxEcheance
        '
        Me.TextBoxEcheance.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.30000114440918R), Telerik.Reporting.Drawing.Unit.Cm(0.099999949336051941R))
        Me.TextBoxEcheance.Name = "TextBoxEcheance"
        Me.TextBoxEcheance.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0495901107788086R), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448R))
        Me.TextBoxEcheance.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.TextBoxEcheance.Value = "= Fields.ECHEANCIER"
        '
        'ObjectDataSource1
        '
        Me.ObjectDataSource1.DataSource = GetType(peireport.plan_intervention2)
        Me.ObjectDataSource1.Name = "ObjectDataSource1"
        '
        'PageHeaderSection1
        '
        Me.PageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.13229165971279144R)
        Me.PageHeaderSection1.Name = "PageHeaderSection1"
        '
        'lblPlanTransitionTitre2
        '
        Me.lblPlanTransitionTitre2.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.20000000298023224R), Telerik.Reporting.Drawing.Unit.Cm(0.05000000074505806R))
        Me.lblPlanTransitionTitre2.Name = "lblPlanTransitionTitre2"
        Me.lblPlanTransitionTitre2.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.999998092651367R), Telerik.Reporting.Drawing.Unit.Cm(0.5R))
        Me.lblPlanTransitionTitre2.Value = "PLAN TRANSITION"
        '
        'Group1Titre
        '
        Me.Group1Titre.GroupFooter = Me.GroupFooterSection3
        Me.Group1Titre.GroupHeader = Me.GroupHeaderSection1
        Me.Group1Titre.Name = "Group1Titre"
        '
        'GroupFooterSection3
        '
        Me.GroupFooterSection3.Height = Telerik.Reporting.Drawing.Unit.Cm(0.13229165971279144R)
        Me.GroupFooterSection3.Name = "GroupFooterSection3"
        '
        'GroupHeaderSection1
        '
        Me.GroupHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.59999990463256836R)
        Me.GroupHeaderSection1.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.lblPlanTransitionTitre2})
        Me.GroupHeaderSection1.Name = "GroupHeaderSection1"
        '
        'SubReportPlanTransition2
        '
        Me.DataSource = Me.ObjectDataSource1
        Me.Groups.AddRange(New Telerik.Reporting.Group() {Me.Group1Titre, Me.GroupButTransition, Me.GroupPlanIntervention})
        Me.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.GroupHeaderSection1, Me.GroupFooterSection3, Me.GroupHeaderSectionButTransition, Me.GroupFooterSection1, Me.GroupHeaderPlanIntervention, Me.GroupFooterSection2, Me.detail, Me.pageFooterSection1, Me.PageHeaderSection1})
        Me.Name = "SubReportPlanTransition2"
        Me.PageSettings.Margins.Bottom = Telerik.Reporting.Drawing.Unit.Mm(15.399999618530273R)
        Me.PageSettings.Margins.Left = Telerik.Reporting.Drawing.Unit.Mm(15.399999618530273R)
        Me.PageSettings.Margins.Right = Telerik.Reporting.Drawing.Unit.Mm(15.399999618530273R)
        Me.PageSettings.Margins.Top = Telerik.Reporting.Drawing.Unit.Mm(15.399999618530273R)
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4
        Me.Style.BackgroundColor = System.Drawing.Color.White
        Me.Width = Telerik.Reporting.Drawing.Unit.Cm(18.5R)
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents detail As Telerik.Reporting.DetailSection
    Friend WithEvents pageFooterSection1 As Telerik.Reporting.PageFooterSection
    Friend WithEvents GroupButTransition As Telerik.Reporting.Group
    Friend WithEvents GroupFooterSection1 As Telerik.Reporting.GroupFooterSection
    Friend WithEvents GroupHeaderSectionButTransition As Telerik.Reporting.GroupHeaderSection
    Friend WithEvents TextBoxButTransition2 As Telerik.Reporting.TextBox
    Friend WithEvents SqlDataSourcePlanTransition2 As Telerik.Reporting.SqlDataSource
    Friend WithEvents GroupPlanIntervention As Telerik.Reporting.Group
    Friend WithEvents GroupFooterSection2 As Telerik.Reporting.GroupFooterSection
    Friend WithEvents GroupHeaderPlanIntervention As Telerik.Reporting.GroupHeaderSection
    Friend WithEvents ObjectDataSource1 As Telerik.Reporting.ObjectDataSource
    Friend WithEvents TextBoxPersResponsables2 As Telerik.Reporting.TextBox
    Friend WithEvents TextBoxMesureNecessaires2 As Telerik.Reporting.TextBox
    Friend WithEvents TextBoxEcheance As Telerik.Reporting.TextBox
    Friend WithEvents PageHeaderSection1 As Telerik.Reporting.PageHeaderSection
    Friend WithEvents lblPlanTransitionTitre2 As Telerik.Reporting.TextBox
    Friend WithEvents Group1Titre As Telerik.Reporting.Group
    Friend WithEvents GroupFooterSection3 As Telerik.Reporting.GroupFooterSection
    Friend WithEvents GroupHeaderSection1 As Telerik.Reporting.GroupHeaderSection
    Friend WithEvents Shape1 As Telerik.Reporting.Shape
    Friend WithEvents lblResponsable As Telerik.Reporting.TextBox
    Friend WithEvents TextBox1 As Telerik.Reporting.TextBox
    Friend WithEvents lblEcheancier As Telerik.Reporting.TextBox
End Class