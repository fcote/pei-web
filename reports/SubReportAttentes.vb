Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms
Imports Telerik.Reporting
Imports Telerik.Reporting.Drawing

Partial Public Class SubReportAttentes
    Inherits Telerik.Reporting.Report
    Dim _reportManager As ReportManager

    Public Sub New()
        InitializeComponent()
        _reportManager = New ReportManager
    End Sub
    Private _attentes As List(Of ATTENTE) = Nothing
    Public Property attentes As List(Of ATTENTE)
        Get
            Return _attentes
        End Get
        Set(ByVal value As List(Of ATTENTE))
            _attentes = value
        End Set
    End Property
    Private _etape As Integer = 0
    Public Property etape As Integer
        Get
            Return _etape
        End Get
        Set(ByVal value As Integer)
            _etape = value
        End Set
    End Property
    Public Sub SetDataSource()
        Me.ObjectDataSourceAttentes.DataSource = _attentes
        Me.ReportParameters.Item("etape").Value = etape

        'format
        _reportManager.FormatFieldsH2(Me.lblButEntete)
    End Sub

End Class