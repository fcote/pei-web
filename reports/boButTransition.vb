﻿<Serializable()>
Public Class boButTransition

    Private _but_transition_id As Integer
    Public Property but_transition_id As Integer
        Get
            Return _but_transition_id
        End Get
        Set(value As Integer)
            Me._but_transition_id = value
        End Set
    End Property

    Private _description As String
    Public Property description As String
        Get
            Return _description
        End Get
        Set(value As String)
            Me._description = value
        End Set
    End Property

    Private _no_order As Integer
    Public Property no_order As String
        Get
            Return _no_order
        End Get
        Set(value As String)
            Me._no_order = value
        End Set
    End Property

    Private _plan_intervention As IList(Of boPlanIntervention) = New List(Of boPlanIntervention)
    Public Property plan_intervention As IList(Of boPlanIntervention)
        Get
            Return Me._plan_intervention
        End Get
        Set(ByVal value As IList(Of boPlanIntervention))
            Me._plan_intervention = value
        End Set
    End Property

End Class
